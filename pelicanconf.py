#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import calendar

from subprocess import check_output
from os.path import basename

import os

AUTHOR = u'Annika Backstrom'
SITENAME = u'sixohthree.com'
SITENAME_SHORT = u'603'
SITEURL = os.environ.get("PELICAN_SITEURL", "/")

ENVIRONMENT = 'development'

FEED_DOMAIN = SITEURL
FEED_MAX_ITEMS = 10
FEED_ALL_ATOM = 'feeds/atom.xml'
FEED_ALL_RSS = 'feeds/feed.rss'
TRANSLATION_FEED_ATOM = None
CATEGORY_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = 'themes/bleached'

CACHE_CONTENT = True
LOAD_CONTENT_CACHE = True

STATIC_PATHS = [
    'extra/robots.txt',
    'extra/favicon.ico',
    'extra/apple-touch-icon-precomposed.png',
    'extra/empty',
    'extra/keybase.txt',
]
EXTRA_PATH_METADATA = { f: { 'path': basename(f) } for f in STATIC_PATHS }

# extract full path into metadata
PATH_METADATA = '(?P<content_path>.*)'

BLOG_VERSION = check_output(['git', 'rev-parse', '--short', 'HEAD']).strip().decode('utf-8')

TIMEZONE = 'America/New_York'
DEFAULT_DATE_FORMAT = '%-d %B %Y'

# Don't parse html files
READERS = {'html': None}

DEFAULT_LANG = u'en'

DISPLAY_PAGES_ON_MENU = False

#LINKS = (
#    ('About', '/about'),
#    ('@abackstrom', 'https://twitter.com/abackstrom'),
#)

HEADER_PAGES = (
    ('About', 'about'),
    ('Blog', 'blog'),
    ('Projects', 'projects'),
)

MENUITEMS = ()

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.extra' : {},
    },
    'output_format': 'html5',
}

SOCIAL = (
    ('Mastodon', 'https://xoxo.zone/@annika', 'mastodon.svg'),
    ('Gitea', 'https://git.abackstrom.com/annika', 'gitea.svg'),
    ('Keybase.io', 'https://keybase.io/abackstrom', 'keybase.svg'),
    ('GitLab', 'https://gitlab.com/abackstrom', 'gitlab.svg'),
)

def num2month(index):
    return calendar.month_name[index]

JINJA_FILTERS = {
    'num2month': num2month,
}

DEFAULT_PAGINATION = 25
USE_FOLDER_AS_CATEGORY = False

CATEGORY_URL = 'category/{slug}'
CATEGORY_SAVE_AS = 'category/{slug}.html'

TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}.html'

ARTICLE_URL = '{slug}'
ARTICLE_SAVE_AS = '{slug}.html'

PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}.html'

INDEX_URL = '/blog'
INDEX_SAVE_AS = 'blog.html'

AUTHOR_SAVE_AS = ''

DIRECT_TEMPLATES = ( 'index', 'categories', 'tags', 'catalog' )
PAGINATED_DIRECT_TEMPLATES = ()

CATALOG_SAVE_AS = 'catalog.json'

PLUGIN_PATHS = [
    'pelican-plugins',
]

PLUGINS = [
    'gzip_cache',
    'replacer',
    'simple_footnotes',
    'sitemap',
]

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5,
    },
    'changefreqs': {
        'articles': 'weekly',
        'indexes': 'daily',
        'pages': 'weekly'
    },
}

REPLACES = (
)
