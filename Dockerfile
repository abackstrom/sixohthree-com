FROM alpine:3.9

COPY requirements.txt /requirements.txt

RUN apk add --no-cache curl git openssh-client python3 py3-pip rsync && \
	ln -s /usr/bin/pip3 /usr/bin/pip && \
	pip install --compile -r requirements.txt --no-cache-dir && \
	rm -f requirements.txt

CMD [ "pelican", "content", "-o", "output", "-s", "publishconf.py" ]
