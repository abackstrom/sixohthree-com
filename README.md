The source code for [sixohthree.com](https://sixohthree.com/).

# Development

Local development and deployment are assisted by Docker, Docker Compose, and
Drone CI. The `.drone.yml` file is specific to my environment and private image
repository, so if you're adapting my customizations to your environment, make
sure you change the `image: settings.

To develop locally:

    $ docker-compose up

This will start a persistent container for `nginx` (to serve content at
[localhost:8045](http://localhost:8045) and `sass` (to compile changed SCSS
files). It will also run `pelican` on your content dir.

If you make content or theme changes and want to regenerate the site, re-run the
`pelican` container:

    $ docker-compose run pelican

This command is aliased to `make all` and `make` thanks to the `Makefile`. A Vim
alias like `nnoremap <leader>m :silent make<CR>` lets you quickly rebuild the
site from within your editor.

## Editing the Docker containers

The `pelican` and `sass` containers are custom, generated from `Dockerfile` and
`Dockerfile.sass`, respectively. You can regenerate these containers using
`bin/docker-build-image`.

## Skipping CI (deploy) during push

Include `[skip ci]` in your commit message to exclude this commit from the Drone
CI deploy pipeline.

# Site Styles

## Margin and padding classes

Margin and padding can be controlled using spacer classes. These use a
multiplier in the class name, with a base of 6px. For example, `.p-2` would
apply 6px * 2 padding on all sides.

| Class           | Attributes |
| --------------- | ---------- |
| `p-[0/1/2/3]`   | `padding` |
| `p-t-[0/1/2/3]` | `padding-top` |
| `p-r-[0/1/2/3]` | `padding-right` |
| `p-b-[0/1/2/3]` | `padding-bottom` |
| `p-l-[0/1/2/3]` | `padding-left` |
| `p-x-[0/1/2/3]` | `padding-top` and `padding-bottom` |
| `p-y-[0/1/2/3]` | `padding-left` and `padding-right` |
| `m-[0/1/2/3]`   | `margin` |
| `m-t-[0/1/2/3]` | `margin-top` |
| `m-r-[0/1/2/3]` | `margin-right` |
| `m-b-[0/1/2/3]` | `margin-bottom` |
| `m-l-[0/1/2/3]` | `margin-left` |
| `m-x-[0/1/2/3]` | `margin-top` and `margin-bottom` |
| `m-y-[0/1/2/3]` | `margin-left` and `margin-right` |

## Responsive CSS

[@include-media](https://include-media.com) is used for responsive CSS. To use
this library, include one of the following directives in your SCSS file:

* `@include media('>tablet')` -- 768px and above
* `@include media('>desktop')` -- 1024px and above

By convention, styles should be mobile-first, with overrides at and above
certain breakpoints.

## Article Templates

A template article exists in `templates/article.md`. If you use Vim, you
can add an `autocmd` to inject this template into any new file in `content`.
Example snippet for `.vimrc`:

    if has("autocmd")
        augroup templates
            autocmd!
            autocmd BufNewFile */blogdir/content/*.md 0r %:s?content/.*?templates/article.md?
        augroup END
    endif

# Social embed images

**tl;dr** Add two headers for embeds:

    Meta-Image-Square: /media/2017/09/burrito/sniff.512x512.jpg
    Meta-Image-FB: /media/2017/09/burrito/sniff.1200x630.jpg

## Twitter

Code:

    Meta-Image-Square: /media/2017/09/burrito/sniff.512x512.jpg

Description:

> URL of image to use in the card. Images must be less than 5MB in size. JPG,
> PNG, WEBP and GIF formats are supported. Only the first frame of an animated
> GIF will be used. SVG is not supported.

https://dev.twitter.com/cards/markup

## Facebook (Open Graph)

Code:

    Meta-Image-FB: /media/2017/09/burrito/sniff.1200x630.jpg

Description:

> Use images that are at least 1200 x 630 pixels for the best display on high
> resolution devices. At the minimum, you should use images that are 600 x 315
> pixels to display link page posts with larger images. Images can be up to 8MB
> in size.

https://developers.facebook.com/docs/sharing/best-practices/ 
