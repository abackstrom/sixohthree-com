#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import sys
sys.path.append('.')
from pelicanconf import *

SITEURL = os.environ.get("PELICAN_SITEURL", "https://sixohthree.com")
FEED_DOMAIN = SITEURL

ENVIRONMENT = 'production'

DELETE_OUTPUT_DIRECTORY = True

RELATIVE_URLS = False

CSS_FILE = "main.{}.css".format(BLOG_VERSION)
