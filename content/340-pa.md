Title: Penny-Arcade @ MIT
Slug: 340/pa
Summary: Damn.
Date: 2005-09-15 09:38
Tags: Comics
WordPress-Post-ID: 340
WordPress-Post-Type: post

Mike Krahulik and Jerry Holkins of [Penny-Arcade][] fame will be [guest
speakers][] at MIT this Friday, September 16, at 5:00 PM. I'd love to
go, but I'm working tomorrow, so no dice. Is anyone planning on going?

  [Penny-Arcade]: http://www.penny-arcade.com/
  [guest speakers]: http://lsc.mit.edu/schedule/2005.4q/desc-pennyarcade.shtml
