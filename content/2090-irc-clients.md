Title: IRC Clients
Slug: 2090/irc-clients
Date: 2012-09-18 14:26
Tags: Personal, irc
WordPress-Post-ID: 2090
WordPress-Post-Type: post
WordPress-Post-Format: Aside

IRC is an important communication channel at my new job. After being a
long-time [irssi][] and [Textual][] user, I gave [Linkinus][] a spin
before finally installing [weechat][]. weechat looks to be the winner.

  [irssi]: http://www.irssi.org/
  [Textual]: http://codeux.com/textual/
  [Linkinus]: http://www.conceitedsoftware.com/linkinus
  [weechat]: http://www.weechat.org/
