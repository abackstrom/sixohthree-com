Title: Saving WordPress Media to Amazon S3 (Teaser)
Slug: 1575/saving-wordpress-media-to-amazon-s3-teaser
Summary: On saving media directly to Amazon S3.
Date: 2011-09-02 00:39
Tags: Web, cloud, plugins, s3, WordPress
WordPress-Post-ID: 1575
WordPress-Post-Type: post

I like [Linode][], but hey, disk is expensive (\$1/GB/month), especially
compared to things like [Amason S3][]. So why can't I store my WordPress
media in S3 without relying on local disk *at all*?

I've been thinking about this for a long time, but only now do I have a
[hacked version of WordPress][], a [small plugin][] to modify upload
file paths, and [a mutilated copy of the Zend S3 client][] which patches
in some necessary functionality. Uploads go directly into S3 without so
much as touching wp-content. (OK, the file *does* touch the local temp
directory while uploading, but it doesn't stick around.)

Now, to make it usable.

  [Linode]: http://www.linode.com/?r=3b19dabb9ed30b096be4bfc83724d4e7f4c89c15
  [Amason S3]: http://aws.amazon.com/s3/#pricing
  [hacked version of WordPress]: http://core.trac.wordpress.org/ticket/18543
  [small plugin]: http://static.bwerp.net/~adam/2011/08/29/s3-stream.php.txt
  [a mutilated copy of the Zend S3 client]: http://static.bwerp.net/~adam/2011/11/04/Zend_1.11.10_S3_WordPress.diff
