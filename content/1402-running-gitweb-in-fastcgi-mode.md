Title: Running gitweb in FastCGI Mode
Slug: 1402/running-gitweb-in-fastcgi-mode
Summary: Maybe my Google-fu is just failing me on this one, but I had a hell of a time getting gitweb to run in FastCGI mode.
Date: 2010-12-31 01:50
Tags: Web, fastcgi, git, gitweb, nginx
WordPress-Post-ID: 1402
WordPress-Post-Type: post

[![Screenshot of gitweb][img]{: .border .inline-media-right }][img-big]

Maybe my Google-fu is just failing me on this one, but I had a
hell of a time getting [gitweb][] to run in FastCGI mode. The key was
combining gitweb's `--fastcgi` flag with the [`FCGI_SOCKET_PATH`][FASTCGI_SOCKET_PATH]
environment variable, in addition to a quick library install:

    apt-get install libcgi-fast-perl libfcgi-procmanager-perl

Not complicated, but as a Perl novice it took some digging before I got
the right combo of settings. Also, I didn't realize that a
FastCGI-enabled script will run in CGI mode rather than listen for
connections if it's missing that socket environment variable. (An
oversimplification, but that's the specific behavior I saw with this
script.)

Here's my final setup:

/etc/init/gitweb-fcgi.conf
--------------------------

Here's an [Upstart][] script to start a gitweb wrapper under Ubuntu
10.04. I'm using [gitolite][] and I want the script to run as my git
user, hence the call to `su`.

    start on startup
    stop on shutdown

    exec su git -c /path/to/custom/gitweb.fcgi
    respawn

gitweb.fcgi
-----------

And here's that custom gitweb wrapper:

    #!/bin/sh
    export FCGI_SOCKET_PATH=127.0.0.1:9002
    /usr/local/share/gitweb/gitweb.cgi --fastcgi

And you might as well install the [GitHub copycat theme][] while you're
at it.

  [img]: https://sixohthree.com/files/2010/12/gitweb-300x222.jpg
    "gitweb"
  [img-big]: https://sixohthree.com/files/2010/12/gitweb.jpg
  [gitweb]: https://git.wiki.kernel.org/index.php/Gitweb
  [Upstart]: http://upstart.ubuntu.com/
  [gitolite]: https://github.com/sitaramc/gitolite/wiki/
  [GitHub copycat theme]: https://github.com/kogakure/gitweb-theme
  [FASTCGI_SOCKET_PATH]: http://search.cpan.org/~lds/CGI.pm-3.50/lib/CGI/Fast.pm#FCGI_SOCKET_PATH
