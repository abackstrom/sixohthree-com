Title: Will Intern for Food
Slug: 151/will-intern-for-food
Summary: Disillusionment is rearing its ugly head.
Date: 2003-04-28 11:12
Tags: Personal
WordPress-Post-ID: 151
WordPress-Post-Type: post

I've been stepping up my co-op<span class="footnote">1</span> search in
an effort to actually *graduate* some time in the next few years.
Basically, this means my amount of work increases, while the time I have
to complete said work stays exactly the same.



I've had little experience job hunting, so I have to go by what the
co-op office tells me most of the time. Make callbacks often; apply to
as many jobs as possible; apply to jobs even if you're not qualified,
let the employer sort you out, etc. Disillusionment is rearing its ugly
head.



I made a callback today, my first since I resolved to start working hard
at this. The woman I spoke with was surprised that I called, and
remarked, "Usually the students don't contact us directly." Um? We're
told to be absolutely rabid about callbacks.



As if that weren't discouraging enough, I tried to apply for a job in
systems administration last night. I've applied to less-than-ideal jobs
before: Taco Bell, Wegmans, indentured servant (well, maybe not that
last one). The difference between Taco Bell and systems administrator is
that the latter is akin to giving up on my search for a web development
job. I won't get this opportunity again. I'd like it to be meaningful
and useful. I don't want to spend six months of my life fighting with
Microsoft IIS. I already *know* it sucks. That's one thing I don't need
to learn.



<span class="footnote">1</span> "Co-op" is a fancy word for what most
people would call "internship:" paid work in the field while registered
with the university, for credit towards graducation. RIT requires me to
co-op for nine months.



