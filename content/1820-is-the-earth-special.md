Title: Is the Earth Special?
Slug: 1820/is-the-earth-special
Summary: I loved this recent Slashdot post. It's full of interesting facts about our planet.
Date: 2011-12-12 10:36
Tags: Personal, astronomy, biology
WordPress-Post-ID: 1820
WordPress-Post-Type: post

I loved this recent Slashdot post. It's full of interesting facts about
our planet.

> Planetary scientists say [there are aspects to our planet and its
> evolution that are remarkably strange][]. In the first place there is
> Earth's strong magnetic field. No one is exactly sure how it works,
> but it has something to do with the turbulent motion that occurs in
> the Earth's liquid outer core and without it, we would be bombarded by
> harmful radiation from the Sun. Next there's plate tectonics. We live
> on a planet that is constantly recycling its crust, limiting the
> amount of carbon dioxide escaping into the atmosphere — a natural way
> of controlling the greenhouse effect. Then there's Jupiter-sized outer
> planets [protecting the Earth from frequent large impacts][]. But the
> strangest thing of all is our big Moon. "As the Earth rotates, it
> wobbles on its axis like a child's spinning top," says Professor
> Monica Grady. 'What the Moon does is dampen down that wobble and that
> helps to prevent extreme climate fluctuations' — which would be
> detrimental to life. The moon's tides have also made long swaths of
> earth's coastline into areas of that are regularly shifted between dry
> and wet, providing [a proving ground for early sea life to test the
> land for its suitability][] as a habitat. [The "Rare Earth Hypothesis"
> is one solution to the Fermi Paradox][] (PDF) because, if Earth is
> uniquely special as an abode of life, ETI will necessarily be rare or
> even non-existent. And in the absence of verifiable alien contact,
> scientific opinion will forever remain split as to whether the
> Universe teems with life or we are alone in the inky blackness.

Source: [Is the Earth Special?][]

  [there are aspects to our planet and its evolution that are remarkably strange]: http://www.bbc.co.uk/news/science-environment-16068809
  [protecting the Earth from frequent large impacts]: http://science.slashdot.org/story/09/07/26/1332240/Is-Jupiter-Earths-Cosmic-Protector
  [a proving ground for early sea life to test the land for its suitability]: http://www.damninteresting.com/life-without-the-moon/
  [The "Rare Earth Hypothesis" is one solution to the Fermi Paradox]: http://www.ras.org.uk/images/stories/ras_pdfs/meetings2011-12/9.12.11%20Meeting%20Abstract%20Book.pdf
  [Is the Earth Special?]: http://science.slashdot.org/story/11/12/10/1335245/is-the-earth-special
