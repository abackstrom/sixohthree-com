Title: One Week Later
Slug: 220/later
Summary: At least it's over.
Date: 2004-06-12 00:55
Tags: Personal
WordPress-Post-ID: 220
WordPress-Post-Type: post

It's been one week and a few hours since surgery now. I'm well on my way
to recovery, happy to report that the surgery was a success. My case of
[strabismus][] (crossed eyes, in my case) is almost entirely a thing of
the past, though I still see shadows of a double image at this point. I
also need to keep using glasses for reading and computer use. I was and
still am farsighted, and my surgery had nothing to do with that problem
in particular.



There's a bit of scarring left and my eyes are slightly bloody, but at
least I can use it to turn Matt's stomach. (Could have provided a link
to photos of my eyes, but I'll spare you.)



Here's to modern medicine!



  [strabismus]: http://www.nlm.nih.gov/medlineplus/ency/article/001004.htm
