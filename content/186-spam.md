Title: More Spam, Please
Slug: 186/spam
Summary: "Like any person who portrays an image of power, this power is only derived from those willing to support them."
Date: 2003-11-15 23:21
Tags: Blogging, Spam
WordPress-Post-ID: 186
WordPress-Post-Type: post

[Mark Pilgrim][] posted a rather depressing commentary on [weblog
spam][] earlier today. All valid points, to be sure, but still
depressing. Why? He portrays spammers as a sort of perpetual motion
gauntlet, forever assaulting us with no forseeable end in sight. I won't
regurgitate what he's said here, but I'd like to say something to the
computer people among us.



Spammers themselves are powerless. Like any person who portrays an image
of power, this power is only derived from those willing to support them.
Spammers do not know how to exploit Windows vulnerabilities to create a
distributed network of SMTP servers. They don't know the first thing
about XML-RPC, or programming, or circumventing filters, or any of the
other things that have made them successful. Their only skill is the
cruel efficiency of their exploitation.



So, to my fellow geeks, I say this: just as the [true ninja][] does,
strive to use your powers for good, not evil. Some day you may get a
phone call from a spammer offering you a respectable salary to bludgeon
Internet-goers with offers of Viagra and pornography. Please, for all of
us (and not least of all, yourself), just say no.



Optimistic? Yes. Naive? Probably. I know what my answer would be,
though.



  [Mark Pilgrim]: http://diveintomark.org/
  [weblog spam]: http://diveintomark.org/archives/2003/11/15/more-spam
  [true ninja]: http://blogs.bwerp.net/~riff/
