Title: Accidental Absence
Slug: 299/absence
Summary: Sorry I've been away. I took a trip to the beach.
Date: 2005-03-27 19:38
Tags: Games
WordPress-Post-ID: 299
WordPress-Post-Type: post

Sorry I've been away. I took a trip to the beach.

![Screenshot of a beach in Final Fantasy XI][img-big]

Yes, I've been playing [Final Fantasy XI][] (FFXI) with extreme
frequency for the past six weeks. This game is nothing short of
immersive, with more to discover than I could ever possibly have time
for. Of course, I love it for that.

That's all, really. I won't bore you with the details... for now.

  [img-big]: /media/2005/03/27/beach.jpg
  [Final Fantasy XI]: http://www.playonline.com/ff11us/index.shtml
