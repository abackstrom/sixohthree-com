Title: With a "D"
Slug: 202/deafness
Summary: Observations of Deaf culture.
Date: 2004-03-10 22:43
Tags: Personal
WordPress-Post-ID: 202
WordPress-Post-Type: post

I'm taking 0504-545 Deaf Literature this quarter, the last course in my
[American Sign Language][] concentration at RIT. Today's article was
titled, "Is There Really Such a Thing as Deaf American Literature?" Deaf
literature is more than English works written by Deaf authors. (That's
"Deaf" as in "[culturally Deaf][]," as opposed to deaf as in "hearing
impaired.") To use a word from the article, this literature is unique,
kinetic, from a [whole other language][].



I watched a few Deaf students during lunch today, and got to thinking
about something that was brought to my attention in ASL class last year.
Before college, I didn't realize that "Deaf" was even a concept beyond
hearing loss, that there was a close-knit culture behind what most
people think of only as a disability. These people have their own
language, their own values, their own stories and art. They are a true
American subculture. But there's something else that makes them unique,
something unlike most other subcultures you'll come across: the Deaf
community faces an extinction.



Advances in medical science can no doubt eradicate hearing loss. Hearing
aids and [cochlear implants][] are already bringing deaf people into the
hearing world. In the future, doctors will identify and have the power
to alter genes that cause hearing loss. Many Deaf people feel threatened
by this, and understandably so: their "deafness" and "Deafness" are
intertwined, and every child that loses their deafness is a child that
loses their chance to be part of the Deaf community.



This is a sticky subject, and not one I have any answers for. Just
something that's been on my mind today.



  [American Sign Language]: http://www.deaflibrary.org/asl.html
  [culturally Deaf]: http://www.newcastle.edu.au/services/disability/disabilities/hearing-details.html
  [whole other language]: http://wlc.csumb.edu/asl/projects/202_1_98/Depaolo/jennifer.html
  [cochlear implants]: http://www.entnet.org/healthinfo/ears/cochlear-implant.cfm
