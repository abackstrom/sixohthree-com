Title: Random though
Slug: 243/random-though
Summary: Buenos dias, gringo.
Date: 2004-08-30 14:22
Tags: Personal
WordPress-Post-ID: 243
WordPress-Post-Type: post

We've all seen the futuristic visions of cars that drive themselves at
incredible speeds, freeing humans from the need to navigate and be
otherwise attentive to the trip. These self-driving cars would change
our lives in many ways, and almost certainly make drunk driving
accidents a thing of the past.



Maybe an owner could enable the blood alcohol content monitor in his
car, disabling the manual override when the driver is intoxicated. The
real interesting bit, though: Maybe this same BAC monitor would disable
voice navigation during intoxication, or only accept a subset of
locations. Imagine getting in your car completely trashed, and deciding
you wanted to go to Mexico City. Next thing you know, you've passed out
for 10 hours and wake up in the middle of Avenida Francisco Sosa.
There's an "oops" moment for you.



