Title: Upcoming: RFID on Raspberry Pi
Slug: upcoming-rfid-on-raspberry-pi
Summary: With a spare Raspberry Pi on hand, I can finally do a project I've had on ice for ages: an RFID-controlled, Pi-powered music player.
Date: 2015-02-19 11:39
Modified: 2015-03-05 23:00
Tags: raspberry pi, rfid
Category: Technology

The Raspberry Pi 2 launched earlier this month and I was lucky enough to snag
one right away. With a spare Pi on hand, I can finally do a project I've had on
ice for ages: an RFID-controlled, Pi-powered music player.

I've always wanted a child-friendly way to queue up music, something simple and
durable that gives my kids control over their environment. I've picked up an
RC522 RFID reader/writer and other goodies for the prototype and I can't wait to
put it all together. If I can manage to read and write the RFID cards, I'll
shift to writing an RFID-controlled music player.

This will be my first real electronics project so hopefully I won't blow
anything up.

![Screenshot of a list of items ordered on Adafruit.com](/media/2015/02/rpi-goodies.png "Pi enclosures, wiring, and other misc. parts")

## Update 2015-03-05

My first attempt (not blogged) was stalled due to unforseen problems: the RC522
reader won't actually work unless you solder the pinouts to a header. Skip this
step, and there isn't a stable electrical connection from the RPi to the reader,
and you get a lot of garbage errors. (Maybe there are pins that actually fit
this pinout, but I don't have them and can't find them.)

Not ready to give up, I watched a YouTube video on soldering, bought my own
iron, and managed to solder the header in place without destroying anything!
Here's my shoddy soldering job and console output of reading my first tags.

[![soldering job](/media/2015/03/rc522-soldered-sm.jpg)](/media/2015/03/rc522-soldered.jpg) [![console output](/media/2015/03/tag-reader-screenshot-sm.jpg)](/media/2015/03/tag-reader-screenshot.jpg)

I've placed an order for another batch of RFID cards, and I'll start working on
the software soon.
