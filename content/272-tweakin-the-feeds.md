Title: Tweakin' the Feeds
Slug: 272/tweakin-the-feeds
Summary: Whoops.
Date: 2004-11-10 23:13
Category: Meta
WordPress-Post-ID: 272
WordPress-Post-Type: post

With a little help from the [Feed Validator][] I now have [valid Atom
feed][], [valid RSS feed][], and [valid RDF feed][]. The Atom feed is
now compatible with the [Live Bookmarks][] feature in Firefox, and
problems with other browsers are likely fixed as well.

Thanks to [Justin][] for the heads-up.

  [Feed Validator]: http://feedvalidator.org/
  [valid Atom feed]: http://feedvalidator.org/check.cgi?url=http%3A%2F%2Fblogs.bwerp.net%2Ffeed%2Fatom
  [valid RSS feed]: http://feedvalidator.org/check.cgi?url=http%3A%2F%2Fblogs.bwerp.net%2Ffeed%2Frss
  [valid RDF feed]: http://feedvalidator.org/check.cgi?url=http%3A%2F%2Fblogs.bwerp.net%2Ffeed%2Frdf
  [Live Bookmarks]: http://www.mozilla.org/products/firefox/live-bookmarks.html
  [Justin]: http://www.callblog.net/
