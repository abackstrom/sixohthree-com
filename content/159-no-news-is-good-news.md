Title: No News is Good News?
Slug: 159/no-news-is-good-news
Summary: Not much to report. I am settled back into the apartment here at RIT, enjoying the peace and quiet, but missing friends of various size and stature.
Date: 2003-06-10 17:54
Tags: Personal
WordPress-Post-ID: 159
WordPress-Post-Type: post

Not much to report. I am settled back into the apartment here at RIT,
enjoying the peace and quiet, but missing friends of various size and
stature.



I'll be creating Mac OS X metapackages for work, about which
documentation is fairly scant. I'll be sure to post some appropriate
links and documentation as soon as that's in full swing. That should
give me something to post about.



Got my first two Game Boys, too. The first was a blue Game Boy Pocket,
and the second is a black Game Boy. I haven't powered on the black one
yet; its four AA battery requirement does not fit my shoestring budget.
Check back in a week and a half and then we'll talk. My first game is
Link's Awakening. Rough estimate, I'm probably half way through on six
or seven hours of playtime. It's been a while since I played an RPG, OK?



That's all for now. Just feeding the blog so it doesn't get angry.



