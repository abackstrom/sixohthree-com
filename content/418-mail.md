Title: Jumping Ship
Slug: 418/mail
Summary: Man the lifeboats.
Date: 2007-06-22 15:27
Tags: Mac OS X
WordPress-Post-ID: 418
WordPress-Post-Type: post

I abandoned Apple Mail today, at least for the time being. It having
trouble synchronizing over IMAP, getting stuck and crashing on certain
folders. If I drop into Offline mode I can tell the folder to rebuild
itself, but this only works for a couple hours at most. Deleting local
cache files didn't help. Once it got in its rut it would crash 5-10
seconds after I opened it. I don't need that sort of distraction when
I'm trying to find a piece of e-mail.

I installed Thunderbird and I'm surprised how much I like it. I was
never fond of the Windows version. Some of the Mac build's UI elements
are noticibly non-standard and sluggish, but it's better suited to power
users (subscriptions, variety of config options) and the threading is
just like Mutt's: no false message acting as a thread header, and
replies are indented in the tree order I'd expect.
