Title: State Speed Limit
Slug: 221/speed
Summary: Somebody explain this to me.
Date: 2004-06-12 12:41
Tags: Rants
WordPress-Post-ID: 221
WordPress-Post-Type: post

I have a question about the New York state speed limit. I understand the
rational for its existence, that's not the issue. What I question is the
state's decision to include "State Speed Limit" on its road signs.



The state speed limit in New York is 55 MPH. Of course, 55 MPH is not
appropriate everywhere. Sharp curves, city streets, interstate highways,
each needs a limit tailored to the individual road conditions. At the
very least, the state speed limit is a starting point. In the
programming world we call this a constant, perhaps `SPEED_LIMIT`. With
computers we can just substitute `SPEED_LIMIT` in place of the value
`55`. If we decide to change the limit we need only change one line of
our program.



This analogy quickly breaks down in the real world, but that's my point.
The state can't just say `SPEED_LIMIT` on their signs because not
everyone knows the value of `SPEED_LIMIT`. They can't update
`SPEED_LIMIT` without updating every sign. They can't very well let 55
MPH be the default speed limit and just say when the limit is different.
(Picture a sign that says "End Speed Limit" and the wreckage a few
hundred feet down the road.) Every sign that says "State Speed Limit"
must also say "55." This strikes me as a waste of resources, however
minimal. (Though there are likely thousands of these signs in the
state.)



Does anyone know the reasoning of adding "State Speed Limit" to the road
signs? Every state has [a state speed limit][], but why say so on the
sign itself?



  [a state speed limit]: http://orthoinfo.aaos.org/fact/thr_report.cfm?Thread_ID=117&topcategory=Driving%20Safety&all=all
