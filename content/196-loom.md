Title: School Days
Slug: 196/loom
Summary: Not many left.
Date: 2004-01-25 21:01
Tags: Personal
WordPress-Post-ID: 196
WordPress-Post-Type: post

There are three days until I register for next quarter's classes. Six
months after that I will be finished with college. There it is, I can
see it looming in the distance, I just can't make out all the details
yet. There's a B.S. in Information Technology, I can see that clearly
enough. Other things are more blurry. Is that a job I see? An apartment?
More than a month's rent? I'll get back to you on that.



It's down to the wire now, and I'm scratching my head over which courses
will keep me busy for the next two quarters. Got any suggestions? I'm a
SWM, ISO 12 cr. hr. of courses. Fun and easygoing a plus.



