Title: Upcoming: Portal
Slug: 375/portal
Summary: Check it out.
Date: 2006-07-21 17:25
Tags: Games
WordPress-Post-ID: 375
WordPress-Post-Type: post

Just in case you missed it, [Valve][] has released the trailer for
Portal, an upcoming puzzle-ish game, via [Steam][]. Seeing is believing,
so check out the trailer [on Steam][] or [YouTube][], or just download
the [high-res version][].

![Portal][img]

  [Valve]: http://www.valvesoftware.com/
  [Steam]: http://www.steampowered.com/
  [on Steam]: http://storefront.steampowered.com/v2/index.php?area=game&AppId=922&
  [YouTube]: http://www.youtube.com/watch?v=Ku9gHA22W7g
  [high-res version]: http://www.gametrailers.com/gamepage.php?id=13
  [img]: /media/2006/07/21/portal.jpg
