Title: Static Blogrolling
Slug: 145/static-blogrolling
Summary: Caching your blogroll.
Date: 2003-04-04 18:34
Category: Meta
WordPress-Post-ID: 145
WordPress-Post-Type: post

In the interest of more accessible and indexable [Blogrolling][], I use
a combination of PHP, cron, and some scripting to avoid the more
commonly seen JavaScript route. I thought I'd share with the Googlebot,
who seems to be my most frequent visitor, second only to myself.

Here's the script:

    #!/bin/sh

    WEBDIR=/var/www/blogs.bwerp.net/htdocs
    URL="http://rpc.blogrolling.com/display_raw.php"
    ROLLID="r=11a89b51a86123178572ef64d4d87f02"

    /usr/bin/curl "${URL}?${ROLLID}" -s -o ${WEBDIR}/blogroll.html.new

    if [ $? -eq 0 ]; then
        mv ${WEBDIR}/blogroll.html.new ${WEBDIR}/blogroll.html
    else
        echo "blogroll.sh failed: $?" 1>&2
    fi

Cron runs this every fifteen minutes, so my blogroll is pretty
up-to-date. Note that I don't replace blogroll.html right away. If
`curl` can't run for whatever reason (or if the network is slow), it's
going to create a text file that's blank, which is a Bad Thing. So, I
create a temp file, and if all went well, I rename it so it becomes the
include.

  [Blogrolling]: http://www.blogrolling.com/
