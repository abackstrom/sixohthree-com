Title: WordPress 3.0 Released
Slug: 1089/wordpress-3-0-release
Date: 2010-06-17 15:21
Tags: Web, WordPress, wordpress3, wp3, wpmu
WordPress-Post-ID: 1089
WordPress-Post-Type: post
Category: Technology

At long last, [WordPress 3.0 is here][]. Thanks to Pete Mall for his
post on [upgrading WordPress MU to WordPress 3][], and for
[@andrea\_r][]for [bringing it to my attention][]. Total upgrade time:
\~5 minutes on Subversion checkout with no mods.

Can't wait to use the [custom post types][] feature.

  [WordPress 3.0 is here]: http://wordpress.org/development/2010/06/thelonious/
  [upgrading WordPress MU to WordPress 3]: http://developersmind.com/2010/06/17/upgrading-wordpress-mu-2-9-2-to-wordpress-3-0/
  [@andrea\_r]: http://twitter.com/andrea_r
  [bringing it to my attention]: http://twitter.com/andrea_r/status/14910586522
  [custom post types]: http://codex.wordpress.org/Custom_Post_Types
