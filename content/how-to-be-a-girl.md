Title: How to Be a Girl by gendermom
Slug: how-to-be-a-girl-by-gendermom
Summary: A mother and the daughter she didn't know she had.
Date: 2014-06-26 10:55
Tags: gender

<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="//player.vimeo.com/video/93816674" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

> “I adore my daughter, but sometimes I look at photos of my baby boy, and I miss
> him so much. This probably sounds crazy, but sometimes I wonder where he is.
> It's like he was a dream I had one night, and then morning came, and I woke up,
> and he was gone. And now there's this little girl in his place, telling me that
> the boy was just a dream, just someone I imagined. As far as she is concerned,
> there never was a boy.”

[How to Be a Girl][1] from [gendermom][2] on [Vimeo][3].

  [1]: http://vimeo.com/93816674
  [2]: http://vimeo.com/user27600859
  [3]: https://vimeo.com
