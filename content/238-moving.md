Title: Moving On
Slug: 238/moving
Summary: The end of an era.
Date: 2004-08-19 18:46
Tags: Personal
WordPress-Post-ID: 238
WordPress-Post-Type: post

This past Tuesday, I took my last final. Tomorrow is my last day at
Information & Technology Services. Saturday, I drive home from RIT a
graduate. I will not be going right into a new job, but starting my job
search in earnest. It's all very exciting, in that nervous, scary way.



This also means the server array will be impacted, but only slightly.
Aziz and Shed are the only servers being moved, and the [gallery][]
(hosted on Shed) is the only service that will see downtime. I don't
anticipate running the gallery over my parents' cable modem, so it will
be down for an indeterminate amount of time. I will probably take the
hit and move it to the [sixohthree.com][] server within a week or two
(the same machine that hosts the blogs).



Hard to believe it's almost over.



<ins datetime="2004-08-24T22:00:00-04:00">

<span class="update">Update:</span> Adelphia blocks port 80 inbound on
my cable modem, so the home-based gallery is a no-go.


</ins>

  [gallery]: http://gallery.bwerp.net/
  [sixohthree.com]: https://sixohthree.com/
