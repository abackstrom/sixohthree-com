Title: More Blog Ignoring
Slug: 134/more-blog-ignoring
Date: 2003-02-12 13:25
Category: Meta
WordPress-Post-ID: 134
WordPress-Post-Type: post

I think I have more fun tweaking my MT templates than I do actually
posting to my blog. Just not a whole lot to talk about, I guess. I do
plan on doing useful things in the future, like dissecting
[cinnamon.nl][] and posting my findings, along with fixing up my
[graffiti][] page and posting the completed source. Ooh, maybe I'll make
it XML and use XSLT and a server-side parser. Mmm..

I should blog more often, if I'm going to come up with nice ideas like
that.

  [cinnamon.nl]: http://www.cinnamon.nl
  [graffiti]: http://www.bwerp.net/graffiti/
