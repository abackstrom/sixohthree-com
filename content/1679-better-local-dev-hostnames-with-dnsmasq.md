Title: Better Local Dev Hostnames with Dnsmasq
Slug: 1679/better-local-dev-hostnames-with-dnsmasq
Summary: I use ghost to configure development environment hostnames for all my local test sites. I wondered if there wasn't a more robust solution that supported wildcards.
Date: 2011-10-18 23:23
Tags: Web, development, dns, dnsmasq, Mac OS X
WordPress-Post-ID: 1679
WordPress-Post-Type: post

I use [ghost][] to configure development environment hostnames for all
my local test sites. Some [DTrace tomfoolery][] showed me how ghost
handles this config: each new hostname is saved to its own plist file.

**Update:** for zero-configuration wildcard DNS, check out [xip.io][]. {@class=update}

    ambackstrom@fsck:~:1$ sudo cat /var/db/dslocal/nodes/Default/hosts/example.com.plist
    Password:
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
        <key>ip_address</key>
        <array>
            <string>127.0.0.1</string>
        </array>
        <key>name</key>
        <array>
            <string>example.com</string>
        </array>
    </dict>
    </plist>

There's filesystem caching going on behind the scenes and I expect the
net impact is negligible, but I wondered if there wasn't a more robust
solution that supported wildcards. Having just configured [dnsmasq][] on
my [router][], I started with a [`brew`][homebrew]` install dnsmasq` and was
pleasantly surprised to get a progress bar rather than "no available
formula." Homebrew recommended a couple post-install actions for setting
up a default config file and configuring `launchd` to keep dnsmasq
alive, which I dutifully ran. After that, I enabled my fake TLD in
`dnsmasq.conf`:


    # respond to *.zomg with 127.0.0.1
    address=/zomg/127.0.0.1


By sheer luck I ended up on [this great serverfault.com post][]
explaining Mac OS X's `resolver(5)` and the `/etc/resolver` directory.
Forcing `dnsmasq` lookup on my zomg TLD is as easy as:


    echo 'nameserver 127.0.0.1' >/etc/resolver/zomg


These settings will work on any network we connect to, no need to modify
the DNS servers via System Preferences and put 127.0.0.1 (dnsmasq) in
front. Simple, clean, flexible.

  [ghost]: https://github.com/bjeanes/ghost
  [DTrace tomfoolery]: http://dtrace.org/blogs/brendan/2011/10/10/top-10-dtrace-scripts-for-mac-os-x/
  [xip.io]: http://xip.io/
  [dnsmasq]: http://thekelleys.org.uk/dnsmasq/doc.html
  [router]: http://www.polarcloud.com/tomato
  [this great serverfault.com post]: http://serverfault.com/questions/22419/set-dns-server-on-os-x-even-when-without-internet-connection
  [homebrew]: http://mxcl.github.com/homebrew/
