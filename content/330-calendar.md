Title: Flickr Calendar View
Slug: 330/calendar
Summary: My life, compressed.
Date: 2005-09-01 18:31
Tags: Photography
WordPress-Post-ID: 330
WordPress-Post-Type: post

[![Screenshot of Flickr Calendar][]][]Let me just say that I love the
concept of the [Flickr calendar view][![Screenshot of Flickr
Calendar][]]. There are some things I remember very well. Certain kinds
of data are easily locked away and recalled: driving directions,
computer functionality, other necessary but mundane things. The hard
part is remembering what I've done, and when I did it.

It's not that I forget these things, but the calendar compresses my time
and gives me that overhead view that I cannot otherwise experience.
<span style="font-style:italic">One week I was playing Lunch Money with
Craig and Abra, the next week Jen came to visit, the next week Hurricane
Katrina hit.</span> The visual cues are all I need to see those people
and feel those emotions again.

[Weez][] aims for a blog post every day. I wonder if I could take a
photo every day? Just something to remind myself,
<span style="font-style:italic">You were here.</span>

  

  [Screenshot of Flickr Calendar]: /media/2005/09/01/flickr-calendar.png
  [![Screenshot of Flickr Calendar][]]: http://www.flickr.com/photos/adambackstrom/archives/date-taken/2005/08/calendar/
  [Weez]: http://weez.oyzon.com/
