Title: Firefox Personas
Slug: 860/firefox-personas
Summary: Easy theming for Mozilla Firefox.
Date: 2009-05-29 07:39
Tags: Web, firefox, personalization, skinning, themes
WordPress-Post-ID: 860
WordPress-Post-Type: post

A [post over at Wowhead][] brought [Firefox Personas][] to my attention.
Once you have the Personas addon installed, new themes are super easy to
use: the Personas menu gives you a bunch of starter personas, including
"new," "popular" and by category (with a randomizer). All the personas
are previewed live as you scroll through the menus, which is the real
killer feature here. Even the mouseovers on the Personas site skin your
browser in real time.

Here's a small sample of personas. I usually hide some toolbars for
Firefox screenshots, but personas tend to look better with a larger
canvas.

[gallery link="file" columns="4"]

  [post over at Wowhead]: http://www.wowhead.com/?blog=98108
  [Firefox Personas]: http://personas.services.mozilla.com/
