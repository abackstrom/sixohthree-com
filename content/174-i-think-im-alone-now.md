Title: I Think I'm Alone Now
Slug: 174/i-think-im-alone-now
Summary: No roommates; Xircom Wireless LAN.
Date: 2003-08-25 00:37
Tags: Personal, 802.11b, m500, Palm, wifi
WordPress-Post-ID: 174
WordPress-Post-Type: post

As of this morning, [Karl][] and [Abby][] have cleared out of the
apartment and are now safely back in New Hampshire. The apartment is
once again [quiet][], but I will again have company in two shorts weeks.
Classes are slated to start up again on September 8, and I couldn't be
happier. Working and pretending to earn money is nice, but it will be
good to get back into the college groove.

[![Xircom Wireless LAN][]][]In other news, I finally broke down and
bought a [Xircom Wireless LAN][1] for my Palm m500. Ah, [the lure][]...
So far, it's very enjoyable. I have mobile AIM, e-mail, and web
browsing, all of which work pretty well. It's enough to warrant its own
entry, so I'll try and whip one up tomorrow.

Other than that, life goes on, much as it has these past months and
years. Deadlines approach, [projects][] call to me, and I still stay up
too late for my own good. Que será, será.

Buenas noches!

  [Karl]: http://blogs.bwer.net/~riff/
  [Abby]: http://blogs.bwerp.net/~abby/
  [quiet]: http://images.google.com/images?q=speak+no+evil
  [Xircom Wireless LAN]: /media/2003/08/24/xircom-03-thumb.png
  [![Xircom Wireless LAN][]]: /media/2003/08/24/xircom-03.jpg
  [1]: http://www.intel.com/support/network/xc/wireless/palm/
  [the lure]: http://blogs.bwerp.net/archives/2003/07/06/the_lure_of_data/
  [projects]: http://www.openldap.org/
