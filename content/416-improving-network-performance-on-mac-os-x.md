Title: Improving Network Performance on Mac OS X
Slug: 416/improving-network-performance-on-mac-os-x
Date: 2007-06-16 18:50
Tags: Mac OS X
WordPress-Post-ID: 416
WordPress-Post-Type: post

The default Mac OS X 10.4 has a default configuration for [delayed
ACK][] that made some of my network operations unbearably slow, most
notably Samba. [This note on Mac OS X Hints][] fixed things for me,
improving transfer speeds noticeably. The short version: add
`net.inet.tcp.delayed_ack=0` to the end of `/etc/sysctl.conf` and
reboot.

  [delayed ACK]: http://www.freesoft.org/CIE/RFC/1122/110.htm
  [This note on Mac OS X Hints]: http://www.macosxhints.com/article.php?story=20051107090652912
