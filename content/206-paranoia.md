Title: Musical paranoia
Slug: 206/paranoia
Summary: Spin it again.
Date: 2004-03-19 09:19
Tags: Music
WordPress-Post-ID: 206
WordPress-Post-Type: post

Via Slashdot, "[File Sharing Increases CD Sales][]." Reminiscent of [an
older article][], save for one important fact: the music industry's spin
doctors haven't poisoned the text (yet).



<!--more-->

The older article refers to [an AP release][] filled with gems like,
"Sales of compact discs singles fell by 39% last year," and "Napster
hurt record sales," a quote from the RIAA president herself. But
Slashdot elaborates: "CD singles account for how much of the RIAA's
profits? Not quite one percent. Yes, that's right: they lost 36% of 1%
of their profits."



The recent article is a welcome sight. Peter Martin, Economics
Correspondent for SBS television in Australia, takes a critical eye to
the recent ARIA earnings release. CD singles and cassette tapes are the
only products taking a hit, and I say that's for good reason: for [the
price][], the don't really offer a whole lot. [Six remixes][] of the
same song? I'll pass.



  [File Sharing Increases CD Sales]: http://slashdot.org/article.pl?sid=04/03/19/0112230
  [an older article]: http://slashdot.org/article.pl?sid=01/02/26/1812213
  [an AP release]: http://news.bbc.co.uk/1/hi/entertainment/1190724.stm
  [the price]: http://www.cduniverse.com/browsecat.asp?style=music&mode=top&feat=yes&cat=68&catname=Singles&cart=181487665
  [Six remixes]: http://www.cduniverse.com/productinfo.asp?pid=6619701&cart=181487665&style=music
