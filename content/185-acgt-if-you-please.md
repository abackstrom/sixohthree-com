Title: ACGT, If You Please
Slug: 185/acgt-if-you-please
Date: 2003-11-15 01:28
Tags: Personal
WordPress-Post-ID: 185
WordPress-Post-Type: post

I have this problem speaking my mind. I clam up, don't say anything that
might be remotely confrontational. Even sometimes, when I think it's
important, I still say nothing. Don't ruffle the feathers, don't stir up
those waters. I wonder where it comes from. I think, My dad seems like
this, did I get it from him? Is quiescence hereditary, a part of my
genes? Will I curse my kids with the same flaws, or am I just being
paranoid? Was the lack of social interaction growing up? Is it because
I'm a guy? (No, shouldn't stereotype.)



So since I can't tell people, I tell my blog instead. Everyone, and no
one in particular. A public apology that I'll probably want to push down
the stack as quickly as possible come morning.



