Title: Things I Will Miss About Brooklyn
Slug: things-i-will-miss-about-brooklyn
Summary: Let me count the ways.
Date: 2013-10-22 10:02
Tags: brooklyn

Things I will miss about Brooklyn:

* 50Mbit FiOS
* Five minute walks to get basic necessities
* The corner bodega (and its cat)
* Seamless
* Zoos, museums
* Cell phone towers
* The ferry
* Biking to work
* Walking my son to school a block away
* Never having to drive anywhere
* IKEA
* Seeing people who aren't like me
* The B train full of Russians
* Combination Mexican/Chinese food restaurants
* The Manhattan skyline from Sunset Park
* The office and all the people in it
