Title: I Like 'em Thin
Slug: 146/i-like-em-thin
Summary: Just placed the orders for my new thin client setup.
Date: 2003-04-08 23:22
Tags: Computers
WordPress-Post-ID: 146
WordPress-Post-Type: post

Just placed the orders for my new thin client setup. Here's the
"skinny," as it were:



-   [VIA EPIA-M10000][]
    -   1GHz C3 Processor
    -   Mini-ITX form factor
    -   RCA/S-Video Out
    -   USB 2.0
    -   Firewire
    -   [Better pictures][]

-   [Casetronic 2699R][]
    -   Approx. 12" x 11" x 2.5"
    -   Front USB/Firewire/Audio
    -   External power supply (fanless chassis)

-   [Kingston 256MB PC2100 RAM][]
    -   Note to self: next time, buy the PC2700 for \$3 more

-   [Linksys 10/100 Ethernet Card][]
-   [LNE100TX Boot ROM][]
    -   Allows for diskless booting over Ethernet

-   [Happy Hacking Keyboard][]
    -   60 key layout
    -   Approx. 12" x 4"



Notice that there's no mention of an optical or hard drive. I'm going to
attempt a netboot setup, which would give me a super-small, super-quiet
diskless workstation. One can only hope



I decided go the route of a boot ROM behind a dual-NIC machine. I mulled
over a [flash disk module][], but I'm more likely to encounter boot ROMs
in the future, so I thought it best to try one now. (Though I think in
practice they're pretty much the same. I may use one later to regain the
single PCI slot.)



I'll probably blog any interesting updates, as well as revising my
[Linux Netboot][] project page.



  [VIA EPIA-M10000]: http://www.lillicomputers.net/product.asp?pf_id=EPIA%2DM10000
  [Better pictures]: http://www.newegg.com/app/Showimage.asp?image=13-180-038-03.JPG/13-180-038-02.JPG/13-180-038-01.JPG/13-180-038-04.JPG
  [Casetronic 2699R]: http://www.casetronic.com/Product/PCcase/2699/CS-2699.html
  [Kingston 256MB PC2100 RAM]: http://www.newegg.com/app/ViewProduct.asp?description=20-141-002
  [Linksys 10/100 Ethernet Card]: http://www.newegg.com/app/ViewProduct.asp?description=33-124-107
  [LNE100TX Boot ROM]: http://www.disklessworkstations.com/cgi-bin/cat/150006?9yM39qhY;;437
  [Happy Hacking Keyboard]: http://shop.store.yahoo.com/pfuca-store/
  [flash disk module]: http://www.disklessworkstations.com/cgi-bin/cat/400013?9yM39qhY;;514
  [Linux Netboot]: http://www.bwerp.net/projects/linux-netboot/
