Title: Use openssl to Issue Raw HTTP Requests
Slug: 1712/use-openssl-to-issue-raw-http-requests
Summary: telnet is a handy tool for examining a raw HTTP connection, but it fails hard on HTTP over SSL. Turns out openssl can step in.
Date: 2011-11-01 09:40
Tags: Web, debugging, http, https, ssl, telnet
WordPress-Post-ID: 1712
WordPress-Post-Type: post

`telnet` is a handy tool for examining a raw HTTP connection, but it
fails hard on HTTP over SSL:


    ambackstrom@fsck:~:0$ telnet www.plymouth.edu 443
    Trying 158.136.1.105...
    Connected to algol.plymouth.edu.
    Escape character is '^]'.
    GET / HTTP/1.1
    Connection closed by foreign host.


Turns out `openssl` (which is a toolbox in its own right) [can step
in][]:


    openssl s_client -connect www.plymouth.edu:443



  [can step in]: http://advosys.ca/viewpoints/2006/08/testing-ssl-with-command-line-tools/
