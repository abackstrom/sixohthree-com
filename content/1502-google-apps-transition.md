Title: Google Apps Transition
Slug: 1502/google-apps-transition
Date: 2011-03-23 23:29
Tags: Technology, ga4e, Google
WordPress-Post-ID: 1502
WordPress-Post-Type: post
WordPress-Post-Format: Aside

I've started transitioning my Google app settings over to my
newly-transitioned Google Apps account. Google Reader is now done,
except for all my starred posts.
