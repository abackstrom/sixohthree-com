Title: Ding III
Slug: 950/ding-iii
Summary: Third time's a charm.
Date: 2009-10-05 13:07
Tags: world of warcraft
Category: Games
WordPress-Post-ID: 950
WordPress-Post-Type: post

[![img][thumb]][img] Continuing [the][] [tradition][]:
ding.

Looking through the archives, it seems I never documented the level 70
ding for Cetoh. The screenshot has been lost, probably to a dead hard
drive, so the exact date escapes me. But, the rogue was first to 60, the
priest was first to 70, and the death knight was first to 80. Old
characters lag behind: the rogue sits at 68, and the priest hasn't moved
past 70. Gotta wonder what I'll be playing at level 85.

The world looks a bit different since I last hit the cap. Cataclysm has
been announced, and promises to reshape Azeroth. The achievements system
has been brought online. Mounts are available at level 20. People have
started getting their [Reins of the Violet Proto-Drake][].

My death knight is Exalted with two factions (my first two on any
character). 1,008 quests brought her from level 55 to level 80, over the
course of nearly 11 months. (Winning a duel is a requirement of the
death knight starter quest line, and my character became [Duel-icious][]
on 2008-11-13.) She [Toured the Fjord][], got [Bored in Borean][], saw
the [Might of Dragonblight][], and was just two quests shy of that
[Grizzly Shizzle][] at ding. She has caught 1,405 fish and "other
things"; overall her skills languish at Cooking 206, Fishing 340,
Herbalism 158, and Mining 169. She is still waiting for Artisan flying.

  [thumb]: https://sixohthree.com/files/2009/10/ScreenShot_100509_121827-300x187.jpg
  [img]: https://sixohthree.com/files/2009/10/ScreenShot_100509_121827.jpeg
  [the]: https://sixohthree.com/355/ding
  [tradition]: https://sixohthree.com/382/ding-2
  [Reins of the Violet Proto-Drake]: http://www.wowhead.com/?item=44177
  [Duel-icious]: http://www.wowhead.com/?achievement=1157
  [Toured the Fjord]: http://www.wowhead.com/?achievement=34
  [Bored in Borean]: http://www.wowhead.com/?achievement=33
  [Might of Dragonblight]: http://www.wowhead.com/?achievement=35
  [Grizzly Shizzle]: http://www.wowhead.com/?achievement=37
