Title: Saving Music Videos in iTunes 5.0 for Windows
Slug: 335/itunes
Summary: No, I haven't done it yet.
Date: 2005-09-08 11:18
Tags: Computers, Music
WordPress-Post-ID: 335
WordPress-Post-Type: post

**Important:** I have not figured out how to archive music videos from
iTunes 5.0 for Windows. This post is a work in progress. If you have
more information, especially how to open or convert QuickTime Cache
(.qtch) files, please [leave a comment][].



This post is a work in progress, and will be messy for a time. It's a
followup to my previous post, [Saving iTunes Music Store Videos in
Windows][], updated for iTunes 5.0. We have good news and bad news.

The good news: iTunes does not exclusively lock the files. The bad news:
we're no longer working with simple movie files with .tmp extensions.
Apple has switched to some sort of QuickTime cache file, and a quick
rename is no longer an option.

iTunes cache files have moved slightly:
`C:\Documents and Settings\username\Local Settings\Application Data\Apple Computer\QuickTime\downloads1\11`
or
`C:\Documents and Settings\abackstrom.STONE\Local Settings\Application Data\Apple Computer\QuickTime\downloads\tmp`.
I assume the `01\11` in the first path changes, but that gives you the
basic framework we're dealing with.

I still had the old download of Alanis Morissette's *Everything*. Here
is a comparison of the files, filtered through `xxd(1)`. First, the old
file:


    0000ee0: 6f52 9e37 b977 527f 93c6 17e5 fce9 9212  oR.7.wR.........
    0000ef0: 8349 6b94 d4df a8fd 255d 57e2 3fa9 3fe9  .Ik.....%]W.?.?.
    0000f00: 8ac2 538e 23e5 5b7a 6779 6efc 5cdf 97b9  ..S.#.[zgyn.\...
    0000f10: 65ef 4aca e928 4748 fbf7 cfcd 7b9a f6bd  e.J..(GH....{...
    0000f20: a89c 483a 77c6 8952 8bd2 d56a f9bd ce5d  ..H:w..R...j...]
    0000f30: 41b5 307d b27f aece 4649 7522 ca3b a36a  A.0}....FIu".;.j
    0000f40: 7054 bd8d ba7f 3ab4 ac57 9237 a79c 7f3a  pT....:..W.7...:
    0000f50: 5324 d58b f054 f95d 5ad9 df4f 7115 743e  S$...T.]Z..Oq.t>
    0000f60: 49d7 9631 25e5 587a b0cc 6352 dd4b 5afb  I..1%.Xz..cR.KZ.



Next, the new:


    0000ff0: e5fb 98a9 4ced c135 d746 32b3 de1b 7393  ....L..5.F2...s.
    0001000: 8396 5685 d5e9 8c1f 8a48 7bde 0a55 6bca  ..V......H{..Uk.
    0001010: f2bb 7ac3 9463 1d6d 9762 b117 b5b8 3bc8  ..z..c.m.b....;.
    0001020: 65ef 4aca e928 4748 fbf7 cfcd 7b9a f6bd  e.J..(GH....{...
    0001030: a89c 483a 77c6 8952 8bd2 d56a f9bd ce5d  ..H:w..R...j...]
    0001040: 41b5 307d b27f aece 4649 7522 ca3b a36a  A.0}....FIu".;.j
    0001050: 7054 bd8d ba7f 3ab4 ac57 9237 a79c 7f3a  pT....:..W.7...:
    0001060: 5324 d58b f054 f95d 5ad9 df4f 7115 743e  S$...T.]Z..Oq.t>
    0001070: 49d7 9631 25e5 587a b0cc 6352 dd4b 5afb  I..1%.Xz..cR.KZ.


Notice how the data streams align midway through the pasted sections.
This is not a coincidence. At first glace, I would assume the underlying
movie data has not changed, only the header of the file. The big
question here is whether or not an old header can be applied to a new
movie. The end-of-file segments are identical, so there is no trailer
that needs modification.

Next steps:

1.  Do an additional comparison of old and new files.
2.  Compare and old and new header for length and content.
3.  Compare two old headers for length and content.
4.  Compare two new headers for length and content.



With any luck, the last step will be cutting out the new headers and
dropping in the old. More data forthcoming.

  [leave a comment]: /archives/2005/09/08/itunes#respond
  [Saving iTunes Music Store Videos in Windows]: /archives/2004/12/25/saving
