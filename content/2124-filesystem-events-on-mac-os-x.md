Title: Filesystem Events on Mac OS X
Slug: 2124/filesystem-events-on-mac-os-x
Summary: inotifywait is a Linux thang, but if you need similar functionality on Mac OS X, check out wait_on. Available via homebrew.
Date: 2012-11-08 11:38
Tags: Mac OS X, Scripting, shell, Terminal
WordPress-Post-ID: 2124
WordPress-Post-Type: post

`inotifywait` (of [inotify-tools]) is a Linux thang, but if you need
similar functionality on Mac OS X, check out [`wait_on`][wait_on]. Available
via [homebrew].

  [inotify-tools]: https://github.com/rvoicilas/inotify-tools
  [wait_on]: http://www.freshports.org/sysutils/wait_on/
  [homebrew]: http://mxcl.github.com/homebrew/
