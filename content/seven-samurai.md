Title: Seven Samurai
Slug: seven-samurai
Date: 2013-03-12 21:08
Tags: movies, quotes
Status: draft

> Even a bear comes down from the mountain to feed sometimes.

> Your head is on the chopping block, and all you can think of is your whiskers.

> In life, one finds friends in the strangest places.
