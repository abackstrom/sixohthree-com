Title: Apropos
Slug: 441/freedom
Summary: Wise words.
Date: 2008-01-27 13:32
Tags: Freedom
WordPress-Post-ID: 441
WordPress-Post-Type: post

"America will never be destroyed from the outside. If we falter and lose
our freedoms, it will be because we destroyed ourselves." -- [Abraham
Lincoln][]

"Those who would give up essential liberty to purchase a little
temporary safety, deserve neither liberty nor safety." -- [Benjamin
Franklin][]

  [Abraham Lincoln]: http://en.wikiquote.org/wiki/Abraham_Lincoln
  [Benjamin Franklin]: http://en.wikiquote.org/wiki/Benjamin_Franklin
