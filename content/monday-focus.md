Title: Monday Focus
Slug: monday-focus
Summary: Less Twitter, more Pomodoro.
Date: 2014-09-08 21:07
Tags: productivity

Mornings can be tough. Adding a first grader to the mix does not improve the
situation. Throw in a three year old on his second day of preschool and I'm
close to tears.

<blockquote class="twitter-tweet" lang="en"><p>8:30 Monday morning and I already
feel completely overwhelmed. Should be a good week.</p>&mdash; Annika Backstrom
(@abackstrom) <a href="https://twitter.com/abackstrom/status/508955251291226113">September 8,
2014</a></blockquote>

I didn't feel in control of my day or my life. But, encouraged by Mathias
Meyer's "[10 Things I Do Every Day To Stay Productive][1]," I gave myself a pep
talk and made a plan.

## The Setup

Minimize distractions. Turn off Twitter: no mobile notifications, no little blue
birdie in the menu bar promising social interaction. Turn off email: no bold
subject lines begging to be read, no tabs telling me how much unread mail I've
got. Hide IRC. If it has a status indicator, *hide it*.

Use the [Pomodoro][2] timer. Let it be a reminder that you're in work mode. Don't get
distracted, a short break is right around the corner.

Plan the day. I took Meyer's advice and wrote down three big things I wanted to
accomplish this week, and three things I wanted to accomplish today.

## The Result

Today felt great. Even with three meetings, I was more satisfied than I've been
in a long time. I made a strong effort to really stick to Pomodoro, and
disabling Twitter was a huge help. I probably pressed my Twitter hotkey a dozen
times during natural short pauses (compiles, long loading pages). What
would otherwise have turned into a minute or more catching up on Twitter,
followed by some amount of context switching, instead was this strange moment of
self-awareness and a return to the task at hand.

I found the Pomodoro breaks were more rewarding when I minimized distraction
between those breaks. Why take a break when you've just spent 10 minutes down
some social media rabbit hole? By sticking to the system, I felt less guilty
about the breaks I did take: even the most frivolous downtime was only a few
minutes long. It also condensed many forms of distraction: email, IRC, social
media, getting up for a drink or snack, all can wait until the next break.

It was one good day, but it was a day I sorely needed.

  [1]: http://blog.travis-ci.com/2014-09-04-10-things-i-do-to-stay-productive/
  [2]: https://itunes.apple.com/us/app/zen-pomodoro/id723888300?mt=8
