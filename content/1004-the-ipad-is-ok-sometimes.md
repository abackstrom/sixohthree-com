Title: The iPad is OK… Sometimes
Slug: 1004/the-ipad-is-ok-sometimes
Date: 2010-04-02 17:42
Tags: Computers, apple, ipad
WordPress-Post-ID: 1004
WordPress-Post-Type: post
Category: Technology

![iPad][ipad]{: align="left" } The impending release of the Apple iPad has increased the
chatter on my Twitter and Google Reader feeds, with some commentary
pro-iPad, some [con-][], and some [mocking the rest][]. It's also
subject of a lot of discussion here in the office, usually in terms of
which model we would like to purchase and why.

The device's controversy speaks to its importance. Apple, maligned and
ridiculed for so many years, is now a force. The iPod is ubiquitous, the
iPhone is commonplace, and MacBooks are [around every corner][]. Many of
the content creators I follow work in an Apple world, so Apple's
implementation is noticed and does matter.

I like the iPad as a consumer device, in that sense that I would like to
consume media on it. It's a device that's just begging to be touched. I
can foresee many hours spent browsing the web, looking at photos, and
watching movies with an iPad in my hands. I do all these things on my
MacBook Pro, and I see the iPad as a better way to accomplish this
subset of tasks.

The iPad is dangerous as a primary computing device, at least for the
younger generation. Despite the announced iWork apps, it's still a
device of consumption, and consumption does not engage the brain the
same way creation does. Sure you can [blog][], and there is potential
that someone will create video, audio, and other creative apps, but
those don't exist today. It's very important to me that my son has the
opportunity to pursue whatever his interests may be, technical or not.
The iPad has very little to offer in this regard.

It's also a device that makes it easy for you to spend money, with the
iBookstore, the App Store, and the iTunes Store in easy reach.
Unfortunately, Apple is forced to bargain with content producers, and
it's usually the consumer that loses out. The ability to "buy" many
sorts of media is disingenuous: DRM puts an expiration date on your
entertainment. Authentication servers get turned off, formats become
obsolete, and you have to repurchase relicense something that, at one
time, you thought you owned. (Reminds me of the VHS to DVD to HD-DVD to
whoops-HD-DVD-lost-to-Blu-ray transition, but that's a whole different
marketing strategy.)

For me, the iPad is still in "do want" territory. I think it will be a
great toy around the house, my MacBook will still be there when I need
to do some real work, and my [Palm Pre][] will fill in the gaps.

  [ipad]: https://sixohthree.com/files/2010/04/ipad_hero.jpg
    "iPad Stock Photo (Launcher View)"
  [con-]: http://www.boingboing.net/2010/04/02/why-i-wont-buy-an-ipad-and-think-you-shouldnt-either.html
  [mocking the rest]: https://twitter.com/hotdogsladies/status/11464302112
  [around every corner]: http://www.theawl.com/2010/03/why-apple-deserves-an-oscar-too
  [blog]: http://wordpress.com/
  [Palm Pre]: http://www.palm.com/us/products/phones/pre-family.html
