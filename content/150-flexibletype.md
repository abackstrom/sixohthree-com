Title: FlexibleType
Slug: 150/flexibletype
Summary: Generate .htaccess rules with MovableType templates.
Date: 2003-04-23 00:23
Tags: Blogging
WordPress-Post-ID: 150
WordPress-Post-Type: post

Did you know that MovableType can create .htaccess files with rules for
mod\_rewrite? Very handy, when modifying [archive URLs][].

Consider:

    RewriteEngine on

    <MTEntries lastn="99999">
    RewriteRule ^archives/<$MTEntryID pad="1"$>.php ¬
           /archives/<$MTEntryDate format="%Y/%m/%d"$>/ ¬
           <$MTEntryTitle dirify="1"$>.php [R=301]
    </MTEntries>


(Breaks added for readability.) Which produces this output:

    RewriteEngine on

    RewriteRule ^archives/000138.php ¬
           /archives/2003/04/23/flexibletype.php [R=301]
    RewriteRule ^archives/000137.php ¬
           /archives/2003/04/22/tracking_comments.php [R=301]
    RewriteRule ^archives/000135.php ¬
           /archives/2003/04/22/thin_client_update.php [R=301]
    ...




Here's text files for the [.htaccess][] and [template][]. Go forth and
create [persistent URIs][].



  [archive URLs]: /archives/2003/04/22/tracking-comments
  [.htaccess]: /media/2003/04/23/flexibletype/htaccess.txt
  [template]: /media/2003/04/23/flexibletype/template.txt
  [persistent URIs]: http://www.w3.org/Provider/Style/URI.html
