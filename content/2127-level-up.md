Title: Level Up
Slug: 2127/level-up
Summary: Archived here, for posterity: repeating the last command you typed into bash.
Date: 2012-11-09 00:07
Tags: Personal, Scripting, Bash, Terminal
WordPress-Post-ID: 2127
WordPress-Post-Type: post

Last month I [tweeted][] about the progression one might see as he gets
more comfortable in a terminal window (in my case, bash). Archived here,
for posterity: repeating the last command you typed into bash.

Level 1, history via the up arrow:

    $ <Up><CR>

Level 2, history without the arrow:

    $ <C-P><CR>

Level 3, parameter expansion:

    $ !!<CR>

Level 4, loop that waits for enter key:

    $ while true ; do cmd ; read foo ; done

**Bonus** [Level 5][], loop that waits for filesystem event:

    $ while true ; do inotifywait -e modify filename ; cmd filename ; done

  [tweeted]: https://twitter.com/abackstrom/statuses/256472016364920832
  [Level 5]: https://twitter.com/abackstrom/statuses/256474234858131457
