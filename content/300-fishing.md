Title: Phisher Fishing
Slug: 300/fishing
Summary: One down, several thousand to go.
Date: 2005-03-31 16:10
Tags: Web
WordPress-Post-ID: 300
WordPress-Post-Type: post

Took down a phisher today.

(You're welcome.)

(Please don't give your root account the password "root." It is terribly
insecure, you *will* get hacked, and people that don't know better
*will* suffer.)
