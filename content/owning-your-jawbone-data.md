Title: Owning Your Jawbone Data
Slug: owning-your-jawbone-data
Summary: summary
Date: 2015-02-01 12:44
Tags: tags
Status: draft


TOKEN="your secret token"
for month in 01 ; do
    for day in `seq -w 1 31` ; do
        curl -H "Authorization: Bearer $TOKEN" "https://jawbone.com/nudge/api/v.1.1/users/@me/sleeps?date=2015${month}${day}" > sleep_2015_${month}_${day}.json
        sleep 5
    done
done
