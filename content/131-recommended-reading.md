Title: Recommended Reading
Slug: 131/recommended-reading
Date: 2003-01-16 12:42
Tags: Freedom
WordPress-Post-ID: 131
WordPress-Post-Type: post

Guess it's a "freedom" kind of day. Here are a couple more things for
yous guys to read:



[*Now Corporations Claim The "Right To Lie"*][] documents the increasing
trend in *corporations* to claim the same *human rights* afforded to
living, breathing people. I didn't know it, but corporations have only
been allowed to donate money to political candidates since 1978, when
the Supreme Court (I believe) deemed that they are "persons". As the
author puts it, "they can't vote -- what are they doing in politics?"
Makes sense to me.



[*Supremes back Disney and pigopolists vs science and culture*][] is an
article from one of my favorite news sources of today, [The Register][].
They're not afraid to get personal, which is something I admire. This
particular article is about the recent [Eldred vs. Ashcroft][] case,
regarding copyright laws.



Enjoy.



  [*Now Corporations Claim The "Right To Lie"*]: http://www.commondreams.org/views03/0101-07.htm
  [*Supremes back Disney and pigopolists vs science and culture*]: http://theregister.co.uk/content/6/28897.html
  [The Register]: http://theregister.co.uk/
  [Eldred vs. Ashcroft]: http://eldred.cc/
