Title: Align Along a Line
Slug: 198/floating
Summary: They're trying, I know they are.
Date: 2004-02-05 23:27
Category: Meta
WordPress-Post-ID: 198
WordPress-Post-Type: post

After [yesterday's post][] I noticed a strange rendering feature of
[Safari][]. Here's a chance for me to reuse my new .screenshot class:

[![Screenshot of blog entry in Firebird.][img1]][img1-big]

Here is the page as I indended it to be viewed. Notice the uniform space
between the bottom of one screenshot and the top of the next.

[![Screenshot of the same page in Safari 1.2][img2]][img2-big]

Here is the same page in Safari 1.2. It's almost identical, but the `<br
style="clear: both;">` has pushed the second image and paragraph below
the sidebar. I assume this is because the sidebar is set to float to the
left, and the `clear` is clearing *everything*, not just what is inside
the current div.

I've mangled the screenshot a little to show the sidebar (red), the main
content area (green), and the line that the second screenshot has been
pushed below (blue).

According to [the spec][], Safari [does it right][]. Section 9.5.2
reads:

both
:   The generated box is moved below all floating boxes of earlier
    elements in the source document.

So, Safari seems to follow it to the letter, but the end result is a
little strange. Guess I'll try to come up with another way that
satisfies both browsers. Or maybe the floating sidebar is the part that
should be changed. I [hear][] it isn't the most compatible design
method.



  [yesterday's post]: http://blogs.bwerp.net/archives/2004/02/04/faker
  [Safari]: http://www.apple.com/safari/
  [img1]: /media/2004/02/05/t_firebird.png
  [img1-big]: /media/2004/02/05/firebird.png
  [img2]: /media/2004/02/05/t_safari-highlights.png
  [img2-big]: /media/2004/02/05/safari-highlights.png
  [the spec]: http://www.w3.org/TR/CSS2/
  [does it right]: http://www.w3.org/TR/CSS2/visuren.html#float-rules
  [hear]: http://mamamusings.net/archives/2004/01/31/pity_the_poor_sidekick_user.php
