Title: Yeah, Thank You, Bye
Slug: yeah-thank-you-bye
Summary: The machines are learning.
Date: 2013-11-07 09:35
Category: Personal
Tags: shitposting

Courtesy of Google Voice transcriptions:

> Yeah, to continue press any key bye hello. This is a courtesy call from C. V. S.
> Pharmacy 4 bye bye. Yeah we are calling to inform you that your prescription
> order is available for pick out that C. V. S, pharmacy, located at 345 Highland
> Street in Plymouth, New Hampshire. Bye. You may pick up your prescription at
> your earliest convenience, during normal business hours. Yeah it's not too late
> to get your flu shot. Bye yahoo shops are available every store every day. You
> know appointment is necessary. Bye you get your flu shot when you pick up your
> prescription. Bye. Yeah, if you've already picked up your prescription, please
> disregard this message. Bye. Yeah. Thank you for being a valued customer of C.
> V. S. Pharmacy. We look forward to serving you in the future. Yeah. Bye.

I'd listen to the actual message, but I'd like an air of mystery around whether
the caller was human or computer.
