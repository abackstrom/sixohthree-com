Title: Loading title, please wait...
Slug: 305/tape
Summary: Not <em>that</em> kind of tape. Well, actually, it is that kind of tape...
Date: 2005-04-19 22:42
Tags: Linux
WordPress-Post-ID: 305
WordPress-Post-Type: post

I have tape drive now, a used [Travan 10/20][]. Not bad, not bad. Going
to try my hand at automated backups again, we'll see how it turns out.



It's a tad slow, though I won't complain loudly since the price was
right. I backed up my 5.9GB /home partition with a
`tar cf /dev/tape /home`; I did not time it, but it took over an hour.
Here's how long it took to pull off 59MB of data near the end of the
tape:

    real    109m38.125s
    user    0m1.230s
    sys     0m16.340s


That's nearly two hours' time to perform under 20 seconds of work. Yay
for sequential data access!



  [Travan 10/20]: http://certance.com/products/travan/travan20/STT220000A-M
