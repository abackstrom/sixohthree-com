Title: Goats in the Machine
Slug: goats-in-the-machine
Summary: I think my hardware is conspiring against me.
Date: 2017-02-16 12:14
Category: Computers

I think my hardware is conspiring against me.

First, my decade-old Logitech MX 518 mouse starts crapping out while I play
Overwatch. Thankfully, it's happening more and more frequently, and also on my
Mac (which shows relevant "resetting device due to IO failures" errors in
the console), which made this way easier to troubleshoot and identify. [New
mouse][1] is on the way.

<!-- Logitech G402 -->

*But* this means I can't look to problems with the mouse (which I originally
thought was a symptom) as I debug possible hardware issues with my new PC.
Windows 10, including the recovery tools, crash a few minutes after the system
boots. MemTest86 says it's not the RAM, so it could be the motherboard, GPU, or
HDD. Early signs point to the GPU: it ran for several minutes, including an
Overwatch bot match, when I took the GPU out.

I don't have a ton of modern hardware outside this machine, so I can't do a lot
of part swapping to isolate bad components. I should have a new USB thumb drive
in the mailbox today, so I can at least try an alternate OS install to isolate
the HDD or deep-seated problems with my Windows install.


  [1]: https://smile.amazon.com/gp/product/B00LZVNWIA/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1
