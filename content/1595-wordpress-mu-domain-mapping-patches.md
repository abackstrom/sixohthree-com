Title: WordPress MU Domain Mapping Patches
Slug: 1595/wordpress-mu-domain-mapping-patches
Summary: Get domain mapped URLs for any blog, and fix paths to the MUPLUGINDIR.
Date: 2011-09-28 00:00
Tags: Programming, open source, patches, WordPress, wordpress netwok
WordPress-Post-ID: 1595
WordPress-Post-Type: post

I've been converting a large WordPress Network installation from a dual
subdomain+subdirectory setup to a subdirectory+domain mapping
configuration using the [WordPress MU Domain Mapping][] plugin. This
should allow for more flexibility when we need to break out of our
primary domain name, but so far it has required some extra features in
the domain mapping plugin.

Domain Mapped siteurl()
-----------------------

<script src="https://gist.github.com/abackstrom/5087996.js?file=arbitrary-siteurl.diff"></script>

Here's the filter I'm using to apply this function to all `siteurl()`
and `home()` calls:


    function do_canonical_siteurl( $value, $blog_id ) {
        // gotta do the remove/add dance to avoid recursion
        remove_filter( 'blog_option_siteurl', __FUNCTION__, 10, 2 );
        $url = domain_mapping_siteurl( 'siteurl', $blog_id );
        add_filter( 'blog_option_siteurl', __FUNCTION__, 10, 2 );

        return $url;
    }
    add_filter('blog_option_siteurl', 'do_canonical_siteurl', 10, 2);
    add_filter('blog_option_home', 'do_canonical_siteurl', 10, 2);



Fixup MUPLUGINDIR Paths
-----------------------


Plugin filters helpfully update the `PLUGINDIR` fragment in URLs, but
does not know about `MUPLUGINDIR`. This patch makes the filter a bit
more robust.

<script src="https://gist.github.com/abackstrom/5087996.js?file=mu-plugins-url.diff"></script>

  [WordPress MU Domain Mapping]: http://wordpress.org/extend/plugins/wordpress-mu-domain-mapping/
