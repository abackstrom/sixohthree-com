Title: "Goodbye, Boss"
Slug: 153/goodbye-boss
Date: 2003-05-04 18:43
Tags: Personal
WordPress-Post-ID: 153
WordPress-Post-Type: post

Everywhere you go in New Hampshire, you see the Old Man of the Mountain.
A natural formation of rocks, formed thousands of years ago, his profile
has watched over the White Mountains and been our state symbol for
generations.



Last Thursday, New Hampshire lost not only a monument, but a dear
friend.



It is hard for me to explain my personal sense of loss. The Old Man
stood for many things I love about the Granite State. He was strength.
He was of nature. He was larger than life, and seemingly immobile. And
true to his nature, our best efforts to hold him up could not persuade
him to stay. "Live Free or Die," indeed.



I am a product of New Hampshire. I value nature and rural sensibilities.
I am simple, and I am practical. I am sure some people want to rebuild
him, but I say this is how he wanted it to be. He was with us longer
than his time, and we should let him rest in peace.



So "Goodbye," Old Man. You live on on our signposts, on our coins, and
in my heart.



Those wishing for more information can read [this article][] in the [The
Union Leader][].



  [this article]: http://www.theunionleader.com/articles_show.html?article=20823
  [The Union Leader]: http://www.theunionleader.com/
