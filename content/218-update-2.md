Title: May Update
Slug: 218/update-2
Summary: It hurts.
Date: 2004-05-28 20:54
Tags: Personal
WordPress-Post-ID: 218
WordPress-Post-Type: post

Tomorrow I will travel to Boston via Amtrak, arriving in South Station
at roughly 6:30 PM. Isaac was his gracious self and agreed to bring me
the rest of the way to New Hampshire. I'll be there for two full weeks,
and at some point in the middle I'll be going under the knife at the
[Dartmouth-Hitchcock Medical Center][]. My eyes, they hurt all the time,
they need fixing you see, so doctors will hack away at them with knives
and put them back in slightly different places, and hopefully I will
again have depth perception and eye-strain related headaches will be few
and far between.



That's the idea, at least.



So, no work for two weeks, no fast computer, no RIT. (Poor [Matt][] will
be all alone, so say "Hi" to him so he doesn't get lonely.)



Catch you on the flip side.



  [Dartmouth-Hitchcock Medical Center]: http://www.hitchcock.org/
  [Matt]: http://blogs.bwerp.net/matt/
