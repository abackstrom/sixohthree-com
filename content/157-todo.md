Title: TODO
Slug: 157/todo
Date: 2003-05-23 00:49
Tags: Personal
WordPress-Post-ID: 157
WordPress-Post-Type: post

Things I will do when school ends (in no particular order):



-   Move [useful content][] over to the main Bwerp server.
-   Find a better way to manage online content. (Not talking blogs
    here.)
-   Stabilize my thin client setup.
-   Play old SNES RPGs, like:

    </p>

    -   Link to the Past
    -   Earthbound
    -   Secret of Mana
    -   Illusion of Gaia

    </p>
    <p>



Those are my more immediate concerns. Sweet freedom, how I have missed
thee.



  [useful content]: http://aziz.bwerp.net/~adam/examples/span_images/
