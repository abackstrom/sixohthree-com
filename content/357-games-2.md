Title: Opera for Nintendo DS, & Misc.
Slug: 357/games-2
Date: 2006-02-15 12:21
Tags: Games
WordPress-Post-ID: 357
WordPress-Post-Type: post

Via [Planet GameCube][]:


> Opera Software today announced that it will deliver the World Wide Web
> to Nintendo DS users in Japan. In Opera's agreement with Nintendo,
> Nintendo DS users will now be able to surf the full Internet from
> their systems using the Opera browser. The Opera browser for Nintendo
> DS will be sold as a DS card. Users simply insert the card into the
> Wi-Fi enabled Nintendo DS, connect to a network, and begin browsing on
> two screens.



Very cool, I hope this makes its way across the Pacific.

In other gaming news, I've purchased the [Penny Arcade UFS card game][].
I've yet to play it, but it looks pretty slick. I think the UFS
mechanics will take some getting used to. [Craig][] and [Karl][] will be
excellent guinea pigs!

[Justin][] stopped by last weekend and we played [Polarium][] for the
Nintendo DS. As they say in Futurama: "It's highly addictive!" My copy
from eBay should be here by the weekend.

Sarah was also a sweetheart and got me [City of Heroes][] for
Valentine's Day. I tried it out for a bit last night and it seems pretty
expansive. It's hard to comment yet, of course:
<acronym title="Massively Multiplayer Online Game">MMOG</acronym>s
generally have a steeper learning curve than your standard puzzle game
or first-person shooter, and CoH is no exception. More on that game
later.

  [Planet GameCube]: http://www.planetgamecube.com/newsArt.cfm?artid=11075
  [Penny Arcade UFS card game]: http://www.thinkgeek.com/pennyarcade/other/7faf/
  [Craig]: http://www.wikiwriter.org/
  [Karl]: http://riff.bwerp.net/
  [Justin]: http://www.callblog.net/
  [Polarium]: http://polarium.nintendods.com/
  [City of Heroes]: http://www.coh.com/
