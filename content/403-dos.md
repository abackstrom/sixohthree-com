Title: UDP DoS?
Slug: 403/dos
Summary: Traffic problem.
Date: 2007-04-15 00:00
Tags: Spam
WordPress-Post-ID: 403
WordPress-Post-Type: post

What's all this then?

    UDP (46 bytes) from 76.187.107.238:26546 to 76.178.xxx.yyy:33273 on eth0
    UDP (46 bytes) from 76.173.19.250:53379 to 76.178.xxx.yyy:33273 on eth0
    UDP (46 bytes) from 75.31.97.247:28101 to 76.178.xxx.yyy:33273 on eth0
    UDP (46 bytes) from 76.170.130.124:32888 to 76.178.xxx.yyy:33273 on eth0
    UDP (46 bytes) from 70.105.249.4:49884 to 76.178.xxx.yyy:33273 on eth0
    UDP (46 bytes) from 69.31.210.146:23186 to 76.178.xxx.yyy:33273 on eth0

/boggle. Hundreds of packets per minute.
