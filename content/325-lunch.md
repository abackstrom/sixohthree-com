Title: Lunch Money
Slug: 325/lunch
Summary: It's never been just video games.
Date: 2005-08-15 18:53
Tags: Games
WordPress-Post-ID: 325
WordPress-Post-Type: post

Remember [Lunch Money][]? I just picked up a copy, along with the
expansion set [Sticks & Stones][]. I haven't played this game since
freshman year, with Karl, Isaac, and Wheeler. (And others? It's been a
while.)



For the uninitiated, here are two card samples (the one on the left is
from Sticks & Stones):



![Wedgy, and Poke in the Eye][]



Charming, no? Time to find some playmates.



<span style="font-weight: bold">Update:</span> Forgot to give link
lovin' to [Dragon Talon Games][]. I shopped around, and their price was
by far the best. (Less than list price, even with shipping costs
involved.)



  [Lunch Money]: http://www.atlas-games.com/product_tables/AG1100.php
  [Sticks & Stones]: http://www.atlas-games.com/product_tables/AG1102.php
  [Wedgy, and Poke in the Eye]: /media/2005/08/15/lunch-money.jpg
  [Dragon Talon Games]: http://dragontalongames.com/
