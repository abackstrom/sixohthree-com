Title: A Source of Entertainment
Slug: 262/source
Summary: Counter-Strike\: Source has been released.
Date: 2004-11-01 22:38
Tags: Games
WordPress-Post-ID: 262
WordPress-Post-Type: post

I've been playing Counter-Strike: Source as often as possible during the
past few weeks, despite my computer being ill-equipped for such a game.
A 1.33GHz Athlon T-bird and GeForce2 GTS are quite possibly the bare
minimum required for the game to launch. I actively avoid servers with
more than 10 players. My frame rates drop to near-unusable levels under
such stress, and I become less of a member of the team and more of a
waste of a perfectly good AK-47.

Low FPS aside, I still have tons of fun playing Source. I imagine there
are players that curse Valve for destroying Counter-Strike, since that's
what happens every time a new version is released. Me, I just try and
adapt. Counter-Strike 1.6 is a dog compared to Source, and even if I had
the disk space to install both versions at once Source would still
garner most of my attention. The physics are awesome, the gameplay is
fast and furious, and it leaves me with that "one more round" feeling. I
couldn't ask for more.

I've collected a few screenshots during my few weeks of play. We'll
start with a crowdpleaser: Bloody massacres:

[![A pile of Counter-Terrorist bodies.](/media/2004/11/01/t_de_piranesi0001.jpg)](/media/2004/11/01/de\_piranesi0001.jpg)

Fun ragdoll deaths:

[![An uncomfortable position.](/media/2004/11/01/t_cs_italy0001.jpg)](/media/2004/11/01/cs\_italy0001.jpg)
[![The same uncomfortable position.](/media/2004/11/01/t_cs_italy0002.jpg)](/media/2004/11/01/cs\_italy0002.jpg)
[![Dead, sitting up.](/media/2004/11/01/t_cs_italy0003.jpg)](/media/2004/11/01/cs\_italy0003.jpg)
[![Another angle of dying, sitting up.](/media/2004/11/01/t_cs_italy0004.jpg)](/media/2004/11/01/cs\_italy0004.jpg)

Rendering errors:

[![Missing textures on Dust 2](/media/2004/11/01/t_de_dust20000.jpg)](/media/2004/11/01/de\_dust20000.jpg)

Smoke grenade bugs:

[![Smoke grenades stuck together in midair.](/media/2004/11/01/t_cs_italy0006.jpg)](/media/2004/11/01/cs\_italy0006.jpg)
[![1](/media/2004/11/01/t_cs_italy0005.jpg)](/media/2004/11/01/cs\_italy0005.jpg)
[![A smoke grenade stuck to the filing cabinet.](/media/2004/11/01/t_cs_office0000.jpg)](/media/2004/11/01/cs\_office0000.jpg)

And who could forget my unending dominance?

[![Scoreboard shot. I have 31 kills and 9 deaths.](/media/2004/11/01/t_cs_italy0000.jpg)](/media/2004/11/01/cs\_italy0000.jpg)
[![Scoreboard shot. I have 50 kills and 18 deaths.](/media/2004/11/01/t_de_piranesi0000.jpg)](/media/2004/11/01/de\_piranesi0000.jpg)

See you online.
