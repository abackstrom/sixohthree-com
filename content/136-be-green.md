Title: Be Green
Slug: 136/be-green
Date: 2003-02-15 16:33
Category: Meta
WordPress-Post-ID: 136
WordPress-Post-Type: post

I've created a [new template][] for MovableType that matches the style
of [Bwerp][], my main site (indeed, it uses the same stylesheet). Any
comments? Do you like it? Is it hard to read? Does it look like crap in
Browser X? Feedback would be greatly appreciated.

  [new template]: green
  [Bwerp]: http://www.bwerp.net/
