Title: Darkpole
Slug: 217/blackout
Summary: A rare opportunity.
Date: 2004-05-24 22:55
Tags: Photography
WordPress-Post-ID: 217
WordPress-Post-Type: post

[![Darkpole][]][]

RIT switched off the power for six hours tonight. A bit of the
wilderness, right here in Rochester.



[The photos][![Darkpole][]] are a little dark, but so was the real
thing. Low gamma readers, don't bother.



(Also, I have [hot pixels][], tear.)



  [Darkpole]: http://static.bwerp.net/~adam/2004/05/24/t_darkpole.jpg
  [![Darkpole][]]: http://gallery.bwerp.net/blackout
  [hot pixels]: http://www.nikon.com.sg/TechSupp/KB/TOPFAQ/DeadHotRandom.htm
