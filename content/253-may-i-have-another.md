Title: May I Have Another?
Slug: 253/may-i-have-another
Summary: Going broke has never been easier.
Date: 2004-10-04 21:55
Tags: Personal
WordPress-Post-ID: 253
WordPress-Post-Type: post

My mom just bought a leather jacket through QVC. Total transaction time,
from turning on the TV to placing the order: under 60 seconds. Phoning
the QVC "Automated Ordering" line asks the caller if she would like to
buy the currently-displayed item, confirming the item number and name.
The automated system asks which color and size she would like, confirms
her identity with a pin, automatically charges her Q Card, and kindly
thanks her for shopping with QVC.

There is something to be said for making intuitive interfaces.
