Title: MediaWiki Calendar Template
Slug: 347/calendar-2
Summary: I heart wikis.
Date: 2005-10-05 00:01
Tags: Web, Wiki
WordPress-Post-ID: 347
WordPress-Post-Type: post

Here's a post for any [MediaWiki][] people in the room. Karl has a [new
blog/wiki][] combo, and I've hacked up some templates for a calendar
view:

-   [Template:calday][] ([source][])
-   [Template:calweek][] ([source][1])
-   [Template:calmonth5][] ([source][2])



The whole mess can be seen at [BlackSheep:2005][]. The code to insert a
calendar looks something like this:


    == October ==

    {{calmonth5|=
       1 | | | | | | |2005-10-01|=
       2 |2005-10-02| | | | | | |=
       3 | | | | | | | |=
       4 | | | | | | | |=
       5 | | | | | | | |=
    }}

As time goes on, Karl can manually add more items to the template call.

This is the biggest template hack I've done so far, and I'd appreciate
feedback via comments or template edits. (Did I just tell someone to
edit Karl's blog?)

  [MediaWiki]: http://www.mediawiki.org/wiki/MediaWiki
  [new blog/wiki]: http://riff.bwerp.net/
  [Template:calday]: http://riff.bwerp.net/Template:Calday
  [source]: http://riff.bwerp.net/Template:Calday?action=edit
  [Template:calweek]: http://riff.bwerp.net/Template:Calweek
  [1]: http://riff.bwerp.net/Template:Calweek?action=edit
  [Template:calmonth5]: http://riff.bwerp.net/Template:Calmonth5
  [2]: http://riff.bwerp.net/Template:Calmonth5?action=edit
  [BlackSheep:2005]: http://riff.bwerp.net/2005
