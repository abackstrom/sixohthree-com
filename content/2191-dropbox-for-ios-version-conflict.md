Title: Dropbox for iOS: Version Conflict
Slug: 2191/dropbox-for-ios-version-conflict
Summary: Which version do I want to keep? Where's the button for, "I have no goddamn idea?"
Date: 2012-12-17 09:06
Modified: 2012-04-08 09:44
Tags: Computers, dropbox, ui
WordPress-Post-ID: 2191
WordPress-Post-Type: post

Which version do I want to keep? Where's the button for, "I have no idea?" The
worst part is that Dropbox sync makes this action destructive: the modal dialog
means I either have to force-quit the app, or let my choice overwrite the
version in the cloud.

[![screenshot][]][screenshot]

  [screenshot]: https://sixohthree.com/files/2012/12/IMG_1665.png
