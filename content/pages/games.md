Title: Games
Slug: games
Summary: summary
Date: 2018-02-12 01:41
Tags: games

What I've played, what I'm playing, what I want to play.

## Currently Playing

* World of Warcraft

## On Hold

Games I've started, am not actively playing, but would
like to complete someday.

* Legend of Zelda: Breath of the Wild (Nintendo Switch)
    * Completed 4 divine beasts
* Night in the Woods (PC)
* Stardew Valley (Nintendo Switch)
* Golf Story (Nintendo Switch)
* Octopath Traveler (Nintendo Switch)
* God of War (PS4)
* Baba Is You (Nintendo Switch)

## Future

Games I'd like to play someday.

* Horizon Zero Dawn (PS4)

## Standbys

Less regular games:

* Rocket League (PC, Nintendo Switch)
* World of Warcraft

## Completed

An incomplete list of games I've finished.

* Forager (PC)
* Tacoma (PC)
* What Remains of Edith Finch (PC)
* Celeste (Nintendo Switch)
* Donut County (Nintendo Switch)
