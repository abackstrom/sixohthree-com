Title: Windows Notes
Slug: windows-notes
Date: 2016-12-26 16:37

Some notes for working in Windows.

## Utilities

* Rufus -- writing ISO to USB (like `dd`)
* OpenHardwareMonitor -- all kinds of stats (cpu temp, fan speed, load, etc.)
* Babun -- Cygwin alternative

## Websites

* pcpartpicker.com -- create parts lists for computer builds
* logicalincrements.com -- hardware recommendations
* userbenchmark.com
