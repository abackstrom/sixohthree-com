Title: Billboard for webOS
Slug: projects/billboard
Date: 2011-09-10 02:15
WordPress-Post-ID: 1583
WordPress-Post-Type: page

Billboard: use your HP TouchPad as a message board and harass your
friends from across the room. [Find it in the HP App Catalog][] on your
TouchPad.

[![][img]][img-big]

Got feedback? Email [annika@sixohthree.com][].

Release History
---------------



-   **1.0.0** -- Improved vertical centering. 26 February 2012 (Pending
    Review)
-   **0.0.1** -- Initial App Catalog release. 13 September 2011



  [Find it in the HP App Catalog]: https://developer.palm.com/appredirect/?packageid=com.sixohthree.billboard
  [img]: https://sixohthree.com/files/2011/09/billboard_11-300x225.png
    "billboard_1"
  [img-big]: https://sixohthree.com/files/2011/09/billboard_11.png
  [annika@sixohthree.com]: mailto:annika@sixohthree.com
