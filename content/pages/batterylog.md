Title: batterylog.py
Slug: projects/batterylog
Date: 2009-06-25 12:08
WordPress-Post-ID: 899
WordPress-Post-Type: page

`batterylog.py` is a small Python script which reads battery information
from `system_profiler` and logs values to a SQLite database located in
`~/.batterylog.sqlite3`. This data can be used to track battery health
over time.

batterylog was written under Mac OS X 10.5.7 and Python 2.5.1. The code
is made available under the [MIT License][].

### Download


The current release, 0.2, may be downloaded [here][] (text/plain).

### Usage


The script may be run directly using `python path/to/batterylog.py`. You
may also put it in your `$PATH`:


    mv batterylog.py /usr/local/bin/batterylog
    chmod 755 /usr/local/bin/batterylog



### Automatic Execution via Cron


batterylog may be run hourly via `cron` with a crontab entry similar to
the following:


    59 * * * * python ~/path/to/batterylog.py
    */10 * * * * @AppleNotOnBattery python ~/path/to/batterylog.py


This combination runs batterylog one minute before the hour (I chose
this number for hourly results that would potentially get stats right
before midnight) as well as every 10 minutes when running on A/C power.

### Graphing


I haven't graphed any of my data yet, as the script was only written two
days ago. I'll update this space once I start graphing. In the mean
time, here's a line that will export the database to CSV:


    sqlite3 -separator , ~/.batterylog.sqlite3 'SELECT * FROM log'



  [MIT License]: http://www.opensource.org/licenses/mit-license.php
  [here]: /media/code/batterylog-0.2.py
