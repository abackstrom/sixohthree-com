Title: Realmwatch for webOS
Slug: projects/realmwatch
Date: 2010-02-01 13:10
WordPress-Post-ID: 990
WordPress-Post-Type: page

[![][img]][img-big]I am the author of the [Realmwatch][] app for webOS devices.
Realmwatch allows you to view the World of Warcraft® Realm Status feed
from on your Palm Pre or Palm Pixi webOS device. If you have questions
about this beta application, please email me at [annika@sixohthree.com][].

### Download


Realmwatch may be downloaded to your phone using the [Palm Beta App
Catalog][Realmwatch].

### Current Features



-   Live, filterable realm list
-   Tap to select favorite realms



### Upcoming Features



-   Toggle all realms/favorite realms
-   Background updates and notifications when favorites go up/down
-   Better icons?



### History



-   1 February 2010 – Realmwatch v0.1.0 release to Palm Beta App
    Catalog.



### Legal


I may not be a lawyer, but I do know that World of Warcraft® is a
registered trademark of Blizzard Entertainment, Inc., and this app is
neither approved nor supported by Blizzard.

  

  [img]: https://sixohthree.com/files/2010/02/ss3.jpg "ss3"
  [img-big]: https://sixohthree.com/files/2010/02/ss3.jpg
  [Realmwatch]: http://developer.palm.com/appredirect/?packageid=com.sixohthree.realmwatch
  [annika@sixohthree.com]: mailto:annika@sixohthree.com
