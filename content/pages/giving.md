Title: Giving
Slug: giving
Summary: A list of individuals and organizations I donate to on a recurring basis.
Date: 2018-07-21 10:41
Updated: 2019-01-03 06:59

I give monthly to the following people and groups. Check out their stuff,
they're pretty cool!

* [Aaron Diaz][3], [Dresden Codak][13] ($2/month)
* [Ashe Dryden][7], author, organizer, diversity & inclusion consulting
  ($20/month)
* [The Mutant Ages][4] ([Maddy Myers][5], [Ryan Pagella][6]), podcasts,
  videos ($2/month)
* [Gitea][14], open-source Git hosting ($5/month)
* [Lucy Bellwood][2], cartoonist, sailor ($2/month)
* [MariNaomi](https://www.patreon.com/marinaomi), artist, podcaster ($5/month)
* [MetaFilter][12], long-running community weblog ($5/month)
* [Nikatine](https://www.patreon.com/nikatine), twitch streamer ($10/month)
* [Simone Giertz][1], maker, queen of shitty robots ($5/month)
* [LizzinLaVida](https://www.patreon.com/LizzinLaVida), streamer, entertainer ($10/month)
* [Elizabeth][16], writer, sysadmin ($1/month)
* [girlcock.club](https://girlcock.club), trans-focused Mastodon instance run by [Elizabeth][16] ($3/month)
* [ContraPoints](https://www.patreon.com/contrapoints), YouTuber ($5/month)

## Twitch

I also support a few [Twitch][15] streamers.

* [Annie](https://www.twitch.tv/annie)
* [LizzinLaVida](https://www.twitch.tv/lizzinlavida)
* [Nikatine][9] -- since July 2017
* [ONE_shot_GURL](https://www.twitch.tv/one_shot_gurl)
* [QueenE][8] -- since August 2016
* [RadioHupfen](https://www.twitch.tv/radiohupfen)
* [miabyte](https://www.twitch.tv/miabyte)
* [ripleyviolet](https://www.twitch.tv/ripleyviolet)
* [FluffyDucky](https://www.twitch.tv/fluffyducky)

<!-- links -->

  [1]: https://www.patreon.com/simonegiertz
  [2]: https://www.patreon.com/LucyBellwood
  [3]: https://www.patreon.com/dresdencodak
  [4]: https://www.patreon.com/atomicblueproductions
  [5]: https://twitter.com/midimyers
  [6]: https://twitter.com/ryanpagella
  [7]: https://www.ashedryden.com/
  [8]: https://www.twitch.tv/queene
  [9]: https://www.twitch.tv/nikatine
  [10]: https://www.twitch.tv/kate
  [11]: https://www.twitch.tv/spontainy
  [12]: https://www.metafilter.com/
  [13]: http://dresdencodak.com/
  [14]: https://gitea.io/
  [15]: https://twitch.tv/
  [16]: https://www.patreon.com/FluffyPira
