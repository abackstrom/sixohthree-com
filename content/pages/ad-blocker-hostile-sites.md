Title: Ad-Blocker Hostile Sites
Slug: ad-blocker-hostile-sites
Summary: A list of sites that don't like ad blockers.
Date: 2017-09-25 13:12

I use Adblock Plus in Chrome to block ads. Here's a list of pages that don't like ad blockers.

* Observer ([observer.com][1], 2017-09-25)

  [1]: /media/sixohthree.com/ad-blocker-hostile-sites/2017-09-25-observer.png
