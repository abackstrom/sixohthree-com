Title: Mac OS X Setup Guide
Slug: mac-os-x-setup-guide
Date: 2012-08-16 23:12
WordPress-Post-ID: 2066
WordPress-Post-Type: page

I recently moved from a MacBook Pro to a MacBook Air, and I chose *not*
to restore my user account from the Time Machine backup, so I've spent a
not-insignificant amount of time replicating the things I liked about
that old setup. Feels like high time to collect that into a blog post,
for future reference.

Last updated for Mac OS X 10.7 (Lion) on 16 August 2012.

Applications
------------



Some obvious easy-to-find things like OmniFocus and Google Chrome are
omitted.

-   [nvALT][]
-   [Dropbox][]
-   [iTerm2][]
-   [MacVim][]
-   [Noisy][] white/pink/brown noise generator



Utilities
---------



-   [Quicklook for Markdown][]
-   [Homebrew][]

    </p>

    -   `brew install git ack tmux keychain`



Settings
--------



Disable new window animation:

    defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool NO

Expanded save dialogs:

    defaults write -g NSNavPanelExpandedStateForSaveMode -bool YES

Quicklook text selection:

    defaults write com.apple.finder QLEnableTextSelection -bool TRUE; killall Finder

Speed up sheets (Save…, Print…, etc.):

    defaults write NSGlobalDomain NSWindowResizeTime .001

Hide Spotlight menu bar icon:

    sudo chmod 600 /System/Library/CoreServices/Search.bundle/Contents/MacOS/Search && killall SystemUIServer

Other:


    defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false
    defaults write NSGlobalDomain NSQuitAlwaysKeepsWindows -bool false
    defaults write com.apple.finder _FXShowPosixPathInTitle 1
    defaults write NSGlobalDomain AppleFontSmoothing -int 2
    defaults write com.apple.screencapture type png && killall SystemUIServer


Sources: [1][], [2][], others.

  [nvALT]: http://brettterpstra.com/project/nvalt/
  [Dropbox]: http://db.tt/ELPGw3q
  [iTerm2]: http://www.iterm2.com/
  [MacVim]: http://code.google.com/p/macvim/
  [Noisy]: https://github.com/jonshea/Noisy
  [Quicklook for Markdown]: http://www.mdk.org.pl/2009/2/10/quicklook-for-markdown
  [Homebrew]: http://mxcl.github.com/homebrew/
  [1]: https://gist.github.com/3185148
  [2]: http://quickies.seriot.ch/index.php?cat=3
