Title: sixohthree.com - Annika Backstrom
Date: 2017-08-09 23:21
Updated: 2018-07-21 13:57
Template: home
URL:
save_as: index.html

Hi, I'm Annika Backstrom. This is my Internet website where I prove to myself
that I exist. Sometimes I [write](/blog) about things like
[Ansible](/tag/ansible) and [Docker](/tag/docker). I also keep a running list of
[people and organizations I donate to](/giving), and [games I've played or would
like to play](/games).

Professionally speaking, I'm a Software Engineer based in New Hampshire, US. I
work remotely for [Etsy, Inc.](https://www.etsy.com), creating cool tools for
Etsy employees. I'm also involved in [MyTransHealth](http://mytranshealth.com/),
a resource connecting trans\* folx with qualified health care professionals.
