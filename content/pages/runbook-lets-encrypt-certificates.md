Title: Runbook: Let's Encrypt Certificates
Slug: runbook/lets-encrypt-certificates
Date: 2017-12-23 14:01
Tags: tags

[Index](/runbook)

# Web servers

Web servers share a common cert with many Subject Alternative Names.

    $ git clone git@git.abackstrom.com:ansible.git
    $ cd ansible/helpers/

## Generate a new certificate

    $ ./certbot-request

## Renew certificates

    $ ./certbot-route53 renew

# Internal servers

Internal servers like the Edge Router Lite and Synology NAS share a certificate.

    $ ssh roku
    # --- on roku ---
    $ cd certbot

## Generate a new certificate

    $ ./certbot-generate-truck

## Renew certificates

    $ ./certbot-route53 renew

## Install certificates

To install on the Edge Router, create a certificate chain (including the key)
and copy to the router's Lighttpd directory.

On roku:

    $ chown -R annika:annika certs
    $ cat certs/live/truck/privkey.pem certs/live/truck/fullchain.pem > server.pem

On your local machine:

    $ scp roku:server.pem server.pem
    $ scp server.pem router.stop.wtf:

On the router:

    $ sudo chown root:root server.pem
    $ sudo mv server.pem /etc/lighttpd/server.pem
    $ sudo kill -SIGINT $(cat /var/run/lighttpd.pid)
    $ sudo /usr/sbin/lighttpd -f /etc/lighttpd/lighttpd.conf

To install on the Synology NAS:

1. Visit [truck.stop.wtf](https://truck.stop.wtf/)
2. Open **Control Panel**
3. Open **Security**
4. Click the **Certificate** tab
5. Click **Add**
6. Click **Replace an exiting certificate**
7. Follow the rest of the prompts, selecting **privkey.pem** and
   **fullchain.pem** as needed.
