Title: Web Server
Slug: web-server
Date: 2013-04-19 10:15

I maintain a Linode virtual server running Ubuntu 12.10.

Version numbers refer to the versions I have installed, which may be newer than the "Updated" date, which is the date the build instructions were modified.

Around May, 2011, I started using [spill](http://www.rpcurnow.force9.co.uk/spill/) to manage my `/usr/local` folder.

## MySQL 5.5.15

*Updated 3 May 2011.* I'm now using the binaries distributed by mysql.com.

    spill /usr/local/spill/mysql/5.5.15-1 /usr/local COPYING  INSTALL-BINARY  README data docs mysql-test scripts sql-bench support-files

## nginx 1.7.8

*Updated 15 December 2014.*

Get some supporting nginx modules. I put these it the same directory as the
nginx source, look for `--add-module=../*` in the `configure` call.

    git clone https://github.com/arut/nginx-dav-ext-module.git
    git clone https://github.com/simpl/ngx_devel_kit.git
    git clone https://github.com/chaoslawful/lua-nginx-module.git

Dependencies for building lua-nginx-module:

    sudo apt-get install luajit libluajit-5.1-dev

Configure:

    ./configure --prefix=/opt/nginx --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log \
        --pid-path=/var/run/nginx.pid --lock-path=/var/lock/nginx.lock --http-log-path=/var/log/nginx/access.log \
        --http-client-body-temp-path=/var/lib/nginx/body --http-proxy-temp-path=/var/lib/nginx/proxy \
        --http-fastcgi-temp-path=/var/lib/nginx/fastcgi --with-debug --with-http_stub_status_module \
        --with-http_flv_module --with-http_ssl_module --with-http_dav_module --with-http_gzip_static_module \
        --http-uwsgi-temp-path=/var/lib/nginx/uwsgi --http-scgi-temp-path=/var/lib/nginx/scgi \
        --with-http_mp4_module --add-module=../nginx-dav-ext-module --with-http_spdy_module \
        --add-module=../ngx_devel_kit --add-module=../lua-nginx-module

Build:

    make -j8 build
    sudo checkinstall -y
    dpkg -i nginx_1.7.8-1_i386.deb

## git 1.7.7

*Updated 11 October 2011.*

    ./configure --prefix=/usr/local/spill/git/1.7.7-1 && make -j4 GITWEB_CONFIG=/usr/local/etc/gitweb.conf

I also use gitolite and git-web.

## PHP 5.6.3

*Updated 15 December 2014.*

PHP 5.4+ allows simultaneous building of php-fpm and mod\_php.

    './configure' '--prefix=/opt/php' '--with-mysql' \
        '--with-mysqli' '--with-pdo-mysql' '--with-mysql-sock=/var/run/mysqld/mysqld.sock' \
        '--with-pgsql=/usr' '--with-tidy=/usr' '--with-curl=/usr/bin' \
        '--with-readline' '--with-openssl' \
        '--with-xsl=/usr' '--with-xmlrpc' '--with-iconv-dir=/usr' '--with-snmp=/usr' \
        '--enable-exif' '--enable-calendar' '--with-bz2=/usr' \
        '--with-mcrypt=/usr' '--with-gd' '--with-jpeg-dir=/usr' \
        '--with-png-dir=/usr' '--with-zlib-dir=/usr' '--with-freetype-dir=/usr' \
        '--enable-mbstring' '--enable-zip' '--with-pear' '--enable-opcache' \
        '--enable-sockets' '--with-mhash' '--enable-mbregex' \
        '--enable-pcntl' '--with-pcre-regex' '--enable-inline-optimization' \
        '--disable-debug' '--with-pic' '--enable-sysvsem' '--enable-sysvshm' \
        '--enable-sysvmsg' '--enable-shmop' '--enable-fpm' \
        '--with-apxs2=/usr/bin/apxs2' '--with-config-file-path=/usr/local/etc' "$@"
    make -j8
    sudo checkinstall -y

Sometimes:

    sudo pecl install memcache-beta
    sudo pecl install oauth-beta
    sudo pecl install ssh2-beta
