Title: About
Slug: about
Date: 2005-09-07 22:38
Modified: 2016-02-20 17:20
WordPress-Post-ID: 334
WordPress-Post-Type: page

Hi, I'm Annika Backstrom. I'm a software engineer in New Hampshire, US.
Find me at [@annika@xoxo.zone][4] on [Mastodon][6], or reach me [via email](mailto:annika@sixohthree.com).

The site is hosted on a [Linode][] server. Sign up with that link and I get a
pretty decent referral bonus!


## Site History

* This site was launched using Movable Type, and was [migrated to WordPress][]
  in August 2004.

* On January 23, 2005, content on this blog was licensed under the [Creative
  Commons][cc] [Attribution-ShareAlike 2.0 License][2].

* On June 1, 2009, the blog's domain changed from blogs.bwerp.net to
  sixohthree.com. "603" is the New Hampshire telephone area code.

* On March 8, 2013, the blog moved from [WordPress][] to
  [Pelican][].

* On August 9, 2017, I forked the simplegrey theme (which no longer bore much
  of a resemblance to simplegrey) and started on an organizational and visual
  refresh.

* On October 23, 2017, I relicensed all CC BY-SA 2.0 work on this site
  under [CC BY-SA 4.0][1].

The Git repository for the Pelican site is [available online][3].
An [incomplete visual history][5] of this site's theme is available for
posterity. Old WordPress theme files are [also available][] via Git.

  [cc]: http://creativecommons.org/
  [Linode]: http://www.linode.com/?r=3b19dabb9ed30b096be4bfc83724d4e7f4c89c15
  [also available]: https://git.abackstrom.com/wp-sixohthree.git
  [migrated to WordPress]: https://sixohthree.com/236/free
  [simplegrey]: https://github.com/fle/pelican-simplegrey
  [Pelican]: http://getpelican.com/
  [WordPress]: https://wordpress.org
  [1]: https://creativecommons.org/licenses/by-sa/4.0/
  [2]: https://creativecommons.org/licenses/by-sa/2.0/
  [3]: https://git.abackstrom.com/annika/sixohthree.com
  [4]: https://xoxo.zone/@annika
  [5]: /theme-evolution "Theme Evolution"
  [6]: https://joinmastodon.org/
