Title: Coming Out
Slug: coming-out
Date: 2014-02-05 13:21
Private: True

Friends and Family,

I've struggled to come up with an appropriate introduction to this letter.
Suffice it to say I want you to know about an important change in my life and
give you an opportunity to process it before the next time we see each other.

For some people, there is a disconnect between their sense of self and the way
they express themselves to the world. They may act a certain way because it is
expected, because it does not draw attention, because it is considered normal.
In the past few years I have accepted that my own gender expression is not
authentic. More importantly, I've realized there's something I can do about it.

I am transsexual, and I am actively transitioning in order to present full-time
as female. To that end, I began hormone replacement therapy in September 2013.
Coming out to you in this letter is an important part of this transition.

If you know me at all, you know I live a relatively uncomplicated life. I do
things that make me happy, and I keep company with positive people whose company
I enjoy. One is born transgender (or not), but decides to
transition (or not). This path is a struggle for many. I am transitioning
because I believe it will bring me more happiness, not less.

Gender expression is only part of a person's identity. I am still a devoted
partner, a caring parent, a curious friend who loves to laugh. I believe that
gender is incidental to our relationship, and I hope that we can continue to
enjoy each other's company just as we have in the past.

Please reach out if you have questions, or visit
**[pflag.org/transgender](http://community.pflag.org/transgender)** for
resources for friends and family of transgendered individuals.

With much love,

Annika (née *deadname*) Backstrom
