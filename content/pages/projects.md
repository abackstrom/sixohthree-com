Title: Projects
Slug: projects
Date: 2009-06-25 11:57
Modified: 2018-06-18 11:15
WordPress-Post-ID: 895
WordPress-Post-Type: page

Some of my current interests:

- [Ansible](http://www.ansible.com/) -- codifying adhoc server setups built over
  years into reproducible setup scripts committed to source control.
- Docker -- building images, development environments with Docker Compose,
  deploys using [Drone CI][1]

## Old stuff

- [batterylog.py][] — Track your MacBook's battery health over time.
  Logs data to a SQLite database using Python.
- [Wowhead Tooltips for Mediawiki][] -- link to World of Warcraft
  items on Wowhead.com using `<wowhead>Item</wowhead>` syntax
- [Realmwatch][] -- World of Warcraft realm status on your Palm webOS
  device (Palm Pre, Palm Pixi, etc.)
- Cocoa -- various exploratory projects on iOS and Mac OS X
    - [ios-snippets](https://github.com/abackstrom/ios-snippets) sandbox
    - [BrowserSwitcher](https://gitlab.com/abackstrom/browserswitcher) status
      bar item (incomplete)
    - additional work-related projects
- Deploy architecture -- improving current deploy process driven by
  [gitolite](http://gitolite.com/) hooks
    - Current: push to gitolite-managed server, hook triggers "deploy" script
      on another machine via ssh, deploy script rpulls and runs process
- Photo library management -- removing dependence on iPhoto, replacing with raw
  jpg files backed up with [BitTorrent Sync](http://getsync.com/)
- [twebsites](https://code.sixohthree.com/twitter/websites) -- highlighting websites of Twitter list members
    - Created so I could build a collection of websites for people in STEM
    - Future plans: Newsblur integration ("add all sites to a new folder")

  [batterylog.py]: /projects/batterylog
  [Wowhead Tooltips for Mediawiki]: http://wiki.sixohthree.com/wiki/Wowhead_tooltips
  [Realmwatch]: /projects/realmwatch
  [1]: https://drone.io/
