Title: Theme Evolution
Slug: theme-evolution
Date: 2008-11-26 14:29
Modified: 2017-08-09 23:13
WordPress-Post-ID: 624
WordPress-Post-Type: page

## Spring 2019 Restyle

Scaling things back to some simpler styles.

Home page before (left) and after (right):

![Home page before and after](/media/2019/04/home.png)

Article page before (left) and after (right):

![Article page before and after](/media/2019/04/article.png)

## Pelican

Continuously tweaking the simplegrey Pelican theme.

[![Thumbnail of site homepage](/media/2017/08/sixohthree.com.sm.png)](/media/2017/08/sixohthree.com.png)

## Landing Page

January, 2011. Remove full blog post from website front page, opting for
more of a landing page style.

<a href="/media/2008/11/20110111.jpg"><img src="/media/2008/11/20110111-272x300.jpg"></a>


## Header Update

May 26, 2010. New header with CSS3-based image rotations.

<a href="/media/2008/11/20100526.jpg"><img src="/media/2008/11/20100526-245x300.jpg"></a>


## Restyling

Simpler style, less image-reliant.

<a href="/media/2008/11/20100522.jpg"><img src="/media/2008/11/20100522-242x300.jpg"></a>


## Posts Lists get Googly [r50]

In this revision, posts lists (ie. in search results and archives) were
made a little more readable and space-efficient.

<a href="/media/2008/11/r50-786x1023.jpg"><img src="/media/2008/11/r50-230x300.jpg"></a> <a href="/media/2008/11/r51-870x1023.jpg"><img src="/media/2008/11/r51-254x300.jpg"></a>
