Title: OpenID
Slug: openid
Date: 2010-08-16 09:27
WordPress-Post-ID: 1267
WordPress-Post-Type: page

Hi there! You have probably directed to this page by another site which
doesn't really understand the [OpenID][] protocol. I ([Annika
Backstrom][]) may have left a comment on that site, or made some other
social connection. Sorry for the mixup.

Known sites that redirect here incorrectly:

* Blogger (Google)

  [OpenID]: http://en.wikipedia.org/wiki/OpenID
  [Annika Backstrom]: https://sixohthree.com/
