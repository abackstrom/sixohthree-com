Title: I should blog...
Slug: 189/updates
Date: 2003-12-19 00:12
Tags: Personal
WordPress-Post-ID: 189
WordPress-Post-Type: post

Because it's not like [Karl][] is going to.



Haven't had much to blog about lately. I've been reasonably busy with
classes and work, as well as [side projects][]. Most of the computer
related things I do end up in my [wiki][], which is turning out to be
invaluable. (And I finally moved it from my personal machine to my [paid
host][] so I can even view it when I reboot into Windows. A semi-rare
occurance to be sure, but it's nice to have the server somewhere else.)



The Holidays are upon us. Best wishes to you and your friends and
family, and enjoy your vacation. Time to get some sleep.



  [Karl]: http://blogs.bwerp.net/~riff/
  [side projects]: http://wiki.bwerp.net/MailServer
  [wiki]: http://wiki.bwerp.net/
  [paid host]: https://sixohthree.com/
