Title: WoW: Stargate SG-1 Tribute
Slug: 352/wow-stargate-sg-1-tribute
Summary: Easter eggs a-plenty.
Date: 2005-12-14 05:34
Tags: world of warcraft
Category: Games
WordPress-Post-ID: 352
WordPress-Post-Type: post

From the World of Warcraft [guide to instancing][], a tribute to
Stargate SG-1. Examine the first set of screenshots closely:
Amandacarter explains the finer points of instanced dungeons to
Deanguyver. "It's okay. I know how to fix the problem. All I need is
three soul shards, two other people, and some wire."

Beautiful.

  [guide to instancing]: http://www.worldofwarcraft.com/info/basics/instancing.html
