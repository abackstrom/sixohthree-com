Title: SlackBuilds.org: Community Package Scripts
Slug: 401/slackbuilds
Summary: All the fun without all the work.
Date: 2007-04-12 18:27
Tags: Linux
WordPress-Post-ID: 401
WordPress-Post-Type: post

Just wanted to drop a link to [SlackBuilds.org][], a community effort to
provide Slackware package scripts for applications that aren't part of
the standard distribution.

Slackware packages are relatively simple beasts: they are gzipped tar
files usually created by a single "SlackBuild" shell script. I was
looking for a possible SlackBuild template file and searched for
pdftk.SlackBuild on a whim, finding SlackBuilds.org in the process. I'll
check there first before I install software in the future.

  [SlackBuilds.org]: http://www.slackbuilds.org/
