Title: Journal from the Terminal
Slug: 2201/journal-from-the-terminal
Summary: For years, I've employed a very simple log file to track upgrades and significant configuration changes to my server: one update per line, starting with the output from date(1).
Date: 2012-12-31 18:03
Tags: Scripting
WordPress-Post-ID: 2201
WordPress-Post-Type: post

For years, I've employed a very simple log file to track upgrades and
significant configuration changes to my server: one update per line,
starting with the output from `date(1)`:

    Tue May 10 11:28:41 EDT 2011 -- started using spill for /usr/local management
    Tue May 24 10:39:18 EDT 2011 -- updated from mysql 5.5.11 to 5.5.12
    Thu Jun 16 14:01:26 EDT 2011 -- free disk space increase from 24320mb to 30464mb
    Fri Jul 22 06:50:49 EDT 2011 -- updated nginx from 1.0.4 to 1.0.5

The same format is useful for tracking the miscellaneous events in my
life. What started in 2005 as a MediaWiki page and later migrated to a
Markdown file in Dropbox is now a plain text file, easily updated at the
command line using a script I call `log`.

    #!/bin/sh

    LOG="$HOME/Dropbox/Notes/Timeline.md"

    # `log` to view recent entries
    if [ -z "$1" ]; then
        tail -n 10 "$LOG"
        exit $?
    fi

    # `log -e` to edit the file (MacVim, for me)
        if [ "$1" = "-e" ]; then
        open "$LOG"
    exit $?fi

    # `log message` to enter a new message
    echo `date +"%Y-%m-%d"` -- "$*" >>"$LOG"

Want to remember when you [cancelled your Vonage service][]?

    log 'cancelled vonage, 30 days left to transfer phone number'

Keeping track of when you kicked that World of Warcraft habit?

    log 'deleted my shaman (again)'

Curious how much you're spending on haircuts?

    log 'got a haircut at crazy dave's, $92'

Simple files, easy to update and parse.

  [cancelled your Vonage service]: https://twitter.com/abackstrom/status/285839055055360001
