Title: Ding!
Slug: 355/ding
Summary: Took me long enough.
Date: 2006-01-10 17:26
Tags: world of warcraft
Category: Games
WordPress-Post-ID: 355
WordPress-Post-Type: post

[![Screenshot of Ceto hitting level 60][img]][img-big]
{:.right}

I've finally hit the level cap in World of Warcraft.

Ceto, female human rogue of Uther, is my first Level 60, and hopefully
not my last. It's been a [long journey][]. I bought WoW nearly a year
ago. The exact date escapes me, but it may have been late March. Several
slumps, an enthusiasm for exploration, an unexpected six week hiatus due
to a broken motherboard, and real life obligations stretched my time
played to nearly 26 days over the course of eight months.

![Screenshot of "/played" command][1]

That's an average of 2.5 hours a day, which should give you an idea of
this game's deathgrip on my life. (Actually, if I only played 2.5 hours
in a sitting, but played every day, I think I would have a much more
balanced life.)

I'm glad I finally hit the cap. I intended to hit 60 several times
before, as early as mid-December, but in the end I'm glad I did it
before the expansion was released. That was my most realistic goal of
all. Now the focus of the game changes. Experience points are no longer
relevant. Farming a level 20 boar for Light Leather can be as productive
as farming a Level 50 skeleton for Runecloth. "Progression," is not
restricted by those things that directly translate into experience
points. I can go fishing if I feel like it. It's a good feeling.

Bonus link: [Apocalypse][], my guild. (Yet another website I'm working
on.)

  [img]: /media/2006/01/09/t_ding-60.jpg
  [img-big]: /media/2006/01/09/ding-60.jpg
  [long journey]: http://www.warcraftrealms.com/char/7210349
  [1]: /media/2006/01/09/played-60.jpg "Screenshot showing that I have played 25 days, 20 hours, 15 minutes, and 51 seconds on a character. Also shows that I have played 7 seconds at this level."
  [Apocalypse]: http://www.apocalypseguild.info/
