Title: Welcome to China, Here's Your Firewall
Slug: 126/welcome-to-china-heres-your-firewall
Date: 2003-01-02 11:30
Tags: Freedom
WordPress-Post-ID: 126
WordPress-Post-Type: post

There's a new article over at [Ars Technica][] by Johnny Brookheart
entitled [Put down the mouse, capitalist pigdog!][] The title's silly,
but his letter is pretty well thought out. Worth the read.

  [Ars Technica]: http://arstechnica.com/
  [Put down the mouse, capitalist pigdog!]: http://arstechnica.com/archive/news/1041120461.html
