Title: I Hope My PC Forgives Me
Slug: 120/i-hope-my-pc-forgives-me
Summary: For the next five days, my Linux machine will sit somewhere between shutting down and starting up.
Date: 2002-12-24 12:07
Tags: Computers
WordPress-Post-ID: 120
WordPress-Post-Type: post
Category: Personal

For the next five days, my Linux machine will sit somewhere between
shutting down and starting up. I can only hope that the hard drives are
not grinding away, destroying my precious data. Got any suggestions for
a solid Socket A motherboard that accepts SDRAM? I think my board may
have [a problem][].

  [a problem]: http://www.kxproject.spb.ru/686b.php?language=en
