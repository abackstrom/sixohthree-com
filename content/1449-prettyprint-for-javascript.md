Title: prettyPrint for JavaScript
Slug: 1449/prettyprint-for-javascript
Date: 2011-01-11 07:54
Tags: Programming, debugging, JavaScript
WordPress-Post-ID: 1449
WordPress-Post-Type: post
WordPress-Post-Format: Aside

I'm not a ColdFusion programmer, but I am a long-time fan of [dBug][]
for PHP. I just stumbled across James Padolsey's [prettyPrint][], which
adds the same debugging feature to JavaScript.

  [dBug]: http://dbug.ospinto.com/
  [prettyPrint]: http://james.padolsey.com/javascript/prettyprint-for-javascript/
