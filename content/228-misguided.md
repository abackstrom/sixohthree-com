Title: Misguided Hierarchies
Slug: 228/misguided
Summary: I am sorry for what I have done.
Date: 2004-06-29 19:58
Category: Meta
WordPress-Post-ID: 228
WordPress-Post-Type: post

Using the top-level blogs.bwerp.net for my blog and setting up
additional blogs in subdirectories was a huge mistake, and one I regret
regularly. Any sort of statistics or referral checking is completly
inaccurate. Technorati is one [glaring example][], possibly in part to
[Matt][]'s blog lacking a tilde in the URL. (Though I don't place any
blame on his shoulders. I should have known better.)



It's far too late to fix the problem at this stage. There are hundreds
of links back to every blog on the server, and changing anything would
be a redirection nightmare.



This is something of a public apology, I guess. Dreadfully sorry for all
this.



  [glaring example]: http://www.technorati.com/cosmos/search.html?url=blogs.bwerp.net
  [Matt]: http://blogs.bwerp.net/matt/
