Title: Variable Web Content
Slug: 210/transparency
Summary: This is what I call "fun!"
Date: 2004-04-27 11:48
Tags: Apache
WordPress-Post-ID: 210
WordPress-Post-Type: post

I've been knee-deep in server administration the past couple days,
mostly [Apache][]-related. I did some trickery with [AWStats][] page
statistics files, VirtualHosts, and mod\_rewrite, and thought I would
share.

My Apache virtual hosts live in directories in `/var/www`, each named
with their full domain name. Blogs live in `/var/www/blogs.bwerp.net/`,
and so on. Each `/var/www/<domain>` directory contains at minimum an
`htdocs` and `logs` directory, so web files and their corresponding logs
are segmented from the other domains.

I wanted to run AWStats on all these log files, and my first reaction
was to create `/var/www/<domain>/stats` for each domain I wanted to
analyze. This meant my number of tasks was high:

1.  Create the `stats` directory for each domain.
2.  Point the `awstats_buildstaticpages.pl` script to the correct
    destination directory.
3.  Create an Apache Alias directive for each domain with stats, or
    suffer through a `stats` directory in each `htdocs` folder.

My final solution was much more elegant than what I originally
considered. AWStats creates static pages in the format
`awstats.<domain>.<type>.html`, so each file implicitly "knows" what
domain it refers to. I created `/var/www/awstats-pages` to hold the
static pages for every domain. Next, I created a global Alias that is
applied to each VirtualHost:

    #
    # awstats generated pages
    #
    Alias /stats /var/www/awstats-pages
    <Directory /var/www/awstats-pages>
        Options FollowSymLinks
        AllowOverride All

        Order allow,deny
        Allow from all

        RewriteEngine on
        RewriteCond %{REQUEST_URI} ^/stats/$
        RewriteRule ^(.*)$ /stats/awstats.%{SERVER_NAME}.html
    </Directory>

When a user requests `http://<domain>/stats/`, they are transparently
redirected to `awstats.<domain>.html` in the same directory. The links
on each page already go to subpages for the domain, so no other work is
necessary. One Alias, one stats directory, infinite domains, less
changes in my scripts and the `httpd.conf` file, and transparency for
the user. All good things.

  [Apache]: http://httpd.apache.org/
  [AWStats]: http://awstats.sourceforge.net/
