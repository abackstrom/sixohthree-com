Title: Downtime
Slug: 398/downtime
Summary: Eight months.
Date: 2007-03-25 14:22
Tags: Personal
WordPress-Post-ID: 398
WordPress-Post-Type: post

Firefox locked up Shed today. I got an uptime just before I lost
keyboard access:

    root@shed:~# uptime
     13:13:36 up 265 days, 22:06,  2 users,  load average: 0.40, 1.23, 0.79

265 days since I moved into my new apartment.

That's all, really.
