Title: Related Feeds (Supposedly)
Slug: 294/related
Summary: And you were there, and you, and even you.
Date: 2005-01-29 15:37
Category: Meta
WordPress-Post-ID: 294
WordPress-Post-Type: post

Via [Doc Searls][], what Bloglines considers my "Related Feeds." First,
the Atom version:

1.  [Boing Boing][]
2.  [Crooked Timber][]
3.  [Talking Points Memo][]
4.  [Brad DeLong's Semi-Daily Journal][]
5.  [kuro5hin][]
6.  [MetaFilter][]
7.  [Lessig Blog][]
8.  [Joi Ito's Web][]
9.  [Eschaton][]
10. [Wired News][]



Now, the RSS version:



1.  [Joel on Software][]
2.  [Idle Words][]
3.  [Marc's Voice][]
4.  [misbehaving.net][]
5.  [Dan Gillmor's eJournal][]
6.  [Joi Ito's Web][]
7.  [Jon's Radio][]
8.  [Scobleizer: Microsoft Geek Blogger][]
9.  [Boing Boing][]



The lists do seem to change often. [Carlo][] was #9 on my RSS list on
successive loads. Actually, a lot of RIT blogs were, so it's less of a
"related" list and more of a "people who read this blog also read..."
list.



  [Doc Searls]: http://doc.weblogs.com/2005/01/29#atLeastAmongRelatives
  [Boing Boing]: http://www.boingboing.net/
  [Crooked Timber]: http://www.crookedtimber.org/
  [Talking Points Memo]: http://www.talkingpointsmemo.com/
  [Brad DeLong's Semi-Daily Journal]: http://www.j-bradford-delong.net/movable_type/
  [kuro5hin]: http://www.kuro5hin.org/
  [MetaFilter]: http://www.metafilter.com/
  [Lessig Blog]: http://www.lessig.org/blog/
  [Joi Ito's Web]: http://joi.ito.com/
  [Eschaton]: http://atrios.blogspot.com/
  [Wired News]: http://www.wired.com/
  [Joel on Software]: http://www.joelonsoftware.com/
  [Idle Words]: http://www.idlewords.com/
  [Marc's Voice]: http://marc.blogs.it/
  [misbehaving.net]: http://www.misbehaving.net/
  [Dan Gillmor's eJournal]: http://weblog.siliconvalley.com/column/dangillmor/
  [Jon's Radio]: http://weblog.infoworld.com/udell/
  [Scobleizer: Microsoft Geek Blogger]: http://radio.weblogs.com/0001011/
  [Carlo]: http://www.rit.edu/~cac7878/weblog/
