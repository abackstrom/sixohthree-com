Title: World of Warcraft: TCG Card Numbering
Slug: 1515/world-of-warcraft-tcg-card-numbering
Summary: The World of Warcraft Trading Card Game seems to have a very specific ordering of cards, which I'm attempting to accurately interpret.
Date: 2011-04-28 13:04
Tags: Games, ccg, world of warcraft, wowtcg
WordPress-Post-ID: 1515
WordPress-Post-Type: post

The World of Warcraft Trading Card Game seems to have a very specific
ordering of cards, which I'm attempting to accurately interpret. In
order, using [Icecrown][] as an example:

1.  Heroes; alphabetical by faction/reputation, then title (1-16)
2.  Abilities and Pets (class-specific); alphabetical by class, then
    title (17-87)
3.  Abilities and Pets (?) (multi-class); alphabetical by title (88-97)
4.  Allies; alphabetical by faction, then title (neutral allies last)
    (98-169)
5.  Equipment; unknown ordering (99-202)
6.  Quests; alphabetical by class, then title (all quests in Icecrown
    are faction-neutral) (203-218)
7.  Locations; alphabetical by title (219-220)


Subject to future revisions!

  [Icecrown]: http://www.wowtcgdb.com/imagebrowser-medium.aspx?start=1&end=220&set=ICE
