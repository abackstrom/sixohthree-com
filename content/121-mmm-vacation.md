Title: Mmm, Vacation
Slug: 121/mmm-vacation
Date: 2002-12-25 20:31
Tags: Personal
WordPress-Post-ID: 121
WordPress-Post-Type: post
Category: Personal

Sitting here in my old house, wearing flannel-lined khakis and my
extremely orange [topato hoodie][], with my [new keyboard][] in arm's
reach. Life is good.



Happy Holidays to everybody!



  [topato hoodie]: http://www.wigu.com/
  [new keyboard]: http://www.music-n-sound.com/keyboard/casio/ctk573.htm
