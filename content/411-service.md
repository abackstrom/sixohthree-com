Title: Adobe "Customer Service"
Slug: 411/service
Summary: Customer disservice.
Date: 2007-05-23 08:37
Tags: Computers, Rants
WordPress-Post-ID: 411
WordPress-Post-Type: post

I'm looking into purchasing Adobe Fireworks CS3 for a project at work.
Turns out it's incompatible with Windows 2000, so I submitted a question
to Adobe's customer service department:

> I just downloaded the trial version of Fireworks CS3 and found that it
> will not run on Windows 2000 SP4. Do you have any suggestions for
> users such as myself who would like to purchase software but do not
> meet the system requirements for the current version? (Upgrading past
> Windows 2000 is not an option in my environment.)


I received this bone-headed response, with additional links to forums
and some phone numbers:


> Annika, I understand that you are experiencing technical issues, since
> the trial version of Fireworks CS3 will not work on Windows 2000 SP4.
>
> Trial versions of Adobe software provide an introduction to our
> applications to help you with your purchase decision, but
> unfortunately, there is no technical support for these trial products.

Which really just sidesteps my original question about purchasing
software compatible with Windows 2000. Yeah, I'm just an individual
looking to purchase one license, but this reeks of a customer service
department that would rather close tickets fast than sell a \$299 piece
of software.

I submitted a reply to the ticket, repeating everything but the first
sentence. Maybe I'll get a more appropriate canned response this time
around.
