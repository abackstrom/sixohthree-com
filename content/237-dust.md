Title: Dust Settling
Slug: 237/dust
Summary: I hate doing the Right Thing.
Date: 2004-08-15 12:50
Category: Meta
WordPress-Post-ID: 237
WordPress-Post-Type: post

Nearly all my old URLs are in working condition, I have archive pages
that are somewhat useful, and `<p>` tags have been added to the majority
of my posts. Also, WordPress has this cool instant-gratification thing
going on. I make a change, and it's instantly live. I can't believe I
worked with static pages for so long.

Time to get some real work done. I'm supposed to graduate soon, I'd hate
to fail one of the last three courses counting towards my B.S.
