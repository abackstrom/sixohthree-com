Title: Moved to Pelican
Slug: moved-to-pelican
Date: 2013-03-08 23:25
Category: Meta
Summary: After lots of nitpicking and too many hours of fine-tuning by hand, I've finally migrated this site from self-hosted WordPress to Pelican.

After lots of nitpicking and too many hours of fine-tuning by hand, I've
finally migrated this site from self-hosted [WordPress] to [Pelican]. The move
has already spawned [one open-source contribution][182], and I expect a few more will
follow as I get more comfortable in Python.

Here's to leaving your comfort zone.

  [WordPress]: http://wordpress.org/
  [Pelican]: http://getpelican.com/
  [182]: https://github.com/waylan/Python-Markdown/pull/182
