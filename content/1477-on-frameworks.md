Title: On Frameworks
Slug: 1477/on-frameworks
Summary: Everyone wants what they don't have.
Date: 2011-03-06 19:38
Tags: Programming, design, object oriented programming, PHP
WordPress-Post-ID: 1477
WordPress-Post-Type: post

Martin Fowler, *Patterns of Enterprise Application Architecture*:


> I'm assuming that most of my readers will fall into two groups: those
> with modest needs who are looking to build their own software and
> readers with more demanding needs who will be using a tool. … There is
> a third category; those with demanding needs who want to build their
> own software. The first thing I'd say here is to look carefully at
> using tools. I've seen more than one project get sucked into a long
> exercise at building frameworks, which wasn't what the project was
> really about.


I have been acutely aware of this, lately. The specific sorts of
patterns I'm looking for (e.g. [active record)][] could be done for me,
were I to use a framework. There's a lot to weigh:

-   Increased difficulty of maintenance as we complement our own set of
    home-grown, evolved tools with third-party tools. All the old code
    doesn't just go away.
-   Training new and existing developers in a specific toolset (though
    in theory this would offset some of our own documentation
    requirements)
-   Finding a tool that actually fits our needs ([Zend][], [Lithium][],
    [Doctrine][], [CodeIgniter][], [Kohana][],  [etc][].)


My impression is that Zend would most easily fit into our existing
development. We could cherry-pick components, increasing our dependence
as we grow in comfort and retire old home-grown tools. Maybe the same is
true for other projects. I'd love to build a simple project using each
of these tools, to really get a feel for their ins and outs.

At some level, I'm just tired of writing simple SQL statements.

  [active record)]: http://en.wikipedia.org/wiki/Active_record_pattern
  [Zend]: http://www.zend.com/
  [Lithium]: http://lithify.me/
  [Doctrine]: http://www.doctrine-project.org/
  [CodeIgniter]: http://codeigniter.com/
  [Kohana]: http://kohanaframework.org/
  [etc]: http://www.phpframeworks.com/
