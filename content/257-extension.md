Title: Couldn't Resist
Slug: 257/extension
Summary: Which file extension are you?
Date: 2004-10-21 20:36
Tags: Personal
WordPress-Post-ID: 257
WordPress-Post-Type: post

I usually skip quizzes, but I couldn't help myself this time.

[You
are .exe When given proper orders, you execute them flawlessly. You're
familiar to most, and useful to all.][img]

[Which File Extension are You?][]

  [img]: http://www.bbspot.com/Images/News_Features/2004/10/file_extensions/exe.jpg
  [Which File Extension are You?]: http://www.bbspot.com/News/2004/10/extension_quiz.php
