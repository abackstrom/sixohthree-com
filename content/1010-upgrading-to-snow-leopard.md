Title: Upgrading to Snow Leopard
Slug: 1010/upgrading-to-snow-leopard
Summary: I've just upgraded to Mac OS X 10.6 "Snow Leopard," and had one problem along the way.
Date: 2010-04-05 11:40
Tags: Computers, Bash, Mac OS X, shell, snow leopard, Terminal
WordPress-Post-ID: 1010
WordPress-Post-Type: post
Category: Technology

I've just upgraded my MacBook Pro to Mac OS X 10.6 "Snow Leopard." A
little behind the curve, but I had some application compatibility
concerns that caused me to wait. Now I'm upgraded so I can try the iPad
emulator on a few of our websites.

I have one thing of note to log, so of course I'll post it here.

Bash Escape Sequences
---------------------

On opening iTerm for the first time, I was greeted with this:

    Last login: Mon Apr  5 11:29:31 on ttys001\e]1;fsck
    annika@fsck:~:0$

You can see a bit of gibberish after the date, but before my prompt. So,
I guess my old escape sequences stopped working in Snow Leopard. I have
a set of functions that [set window titles][] using escape sequences.
Changing `\e` and `\a` to their octal equivalents worked for me. So
these lines in `.bashrc`:

    function setlabel { echo -ne "\e]1;$1\a"; }
    function settitle { echo -ne "\e]2;$1\a"; }

Became this:

    function setlabel { echo -ne "\033]1;$1\007"; }
    function settitle { echo -ne "\033]2;$1\007"; }


  [set window titles]: http://www.faqs.org/docs/Linux-mini/Xterm-Title.html#ss4.3
