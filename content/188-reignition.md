Title: Of Scripts and Such
Slug: 188/reignition
Summary: Where to start? "Scripted Administration." That's been the phrase of the past three months of my life. Mac OS X Scripted Administration, to be more precise. Why? Who? Where? I'll get to it.
Date: 2003-11-21 20:33
Tags: Computers
WordPress-Post-ID: 188
WordPress-Post-Type: post

Where to start? "Scripted Administration." That's been the phrase of the
past three months of my life. Mac OS X Scripted Administration, to be
more precise. Why? Who? Where?



I'll get to it.



[![Reignition Screenshot 1][]][]

We at [ITS][] set up a lot of computers. Setting up computers takes a
long time, especially under Mac OS X. Count on at least half an hour for
the OS install from CDs, and at least another hour of setup and updates
after that. Multiply this times all the machines that will require an OS
upgrade or reinstall in the future, and you've got a lot of man-hours
built up. This is where I come in.



Since September, I've been concentrating on an app dubbed "Reignition."
Reignition automates much of the setup required by ITS employess,
everything from e-mail client configuration to turning on flashing time
separators in the menu bar clock. The interface was created in Project
Builder (and later, [Xcode][]) using the combination of tools Apple
calls [AppleScript Studio][].



[![Reignition Screenshot 2][]][]

I've included screenshots for some of the more interesting function of
the application. All told, I wrote *about* 3,000 lines of code, the
majority in AppleScript. Large portions are written in the Bash
scripting language as well. There are some interesting things going on
in the backend. I enjoyed getting the printer setup to work, and there's
a really superb script that adds applications or files to the Dock from
the command line. Also, the interface does niceties like try and
autodiscover your name and RIT ID from the currently logged in user
account. The app supports authentication to allow administrator-level
changes from its scripts.



It's my hope that the code will be released in some fashion in the
future. I'd like to see this go beyond the walls of RIT, and I know
other people in the department feel the same way. So, that sums up my
fall quarter.. Classes, here I come.



  [Reignition Screenshot 1]: /media/2003/11/21/reignition-01-sm.png
  [![Reignition Screenshot 1][]]: /media/2003/11/21/reignition-01.png
  [ITS]: http://www.rit.edu/~wwwits/
  [Xcode]: http://www.apple.com/macosx/features/xcode/
  [AppleScript Studio]: http://www.apple.com/applescript/studio/
  [Reignition Screenshot 2]: /media/2003/11/21/reignition-02-sm.png
  [![Reignition Screenshot 2][]]: /media/2003/11/21/reignition-02.png
