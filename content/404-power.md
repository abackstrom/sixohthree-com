Title: Power's Out
Slug: 404/power
Summary: Updates from the front line.
Date: 2007-04-16 02:22
Tags: Personal
WordPress-Post-ID: 404
WordPress-Post-Type: post

Taking down the computer now.

<ins datetime="2007-04-16T09:23:30-04:00">**Update:** the power came
back on for about 5 minutes at 5:30 AM. When I left for work there was a
pole snapped in half down the road and traffic was being diverted
through East Side Road. We have power at work for the moment.</ins>
