Title: The Twig is now Closed
Slug: 427/twig
Summary: Nice to know you.
Date: 2007-09-04 07:59
Tags: Personal
WordPress-Post-ID: 427
WordPress-Post-Type: post

Karl, C2 and I went up to [The Twig][] last night to bid it farewell. It
was pretty low key while we were there, but Becky said they were busy
all day. (We saw her brother Tom, by the way.)

Some onion rings, fries, and a pitcher saw us on our way. Thanks, Twig.
You gave me some good memories.

We'll be at the Twigwam walk on Saturday, moving the structure up to Joe
and Wendy's. Join us a little before 2:00 if you're in the area.

  [The Twig]: http://thetwiginwarren.com/
