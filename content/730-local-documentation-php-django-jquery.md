Title: Local Documentation: PHP, Django, jQuery
Slug: 730/local-documentation-php-django-jquery
Summary: Here's how I created local mirrors of the online documentation for several projects under Mac OS X 10.5.6.
Date: 2009-02-10 05:46
Tags: Django, Django, documentation, jquery, PHP
WordPress-Post-ID: 730
WordPress-Post-Type: post

As ubiquitous as wifi is these days, I still feel better with a local
copy of the documentation around. Here's how I created local copies of
the [PHP][], [Django][], and [jQuery][] docs on my MacBook running Mac
OS X 10.5.6.

### Django


[Django's documentation files][] are flat HTML, so the only *real* step
is to build the docs using [Sphinx][]. I'll do this in
`~/Sites/django-docs/` for consistency with my PHP setup, but strictly
speaking you can access these docs without the overhead of a web server.


    sudo easy_install sphinx
    cd ~/Sites/
    svn co http://code.djangoproject.com/svn/django/trunk/docs/ django-docs
    cd django-docs
    make html


The final step took several minutes to complete on my MacBook. Script
output notes that the new documentation is stored in `build/_html/`.
Open the index.html file in your browser. [There's no step 3][]!

### PHP


The PHP setup is slightly more complicated since the docs themselves
rely on PHP. (On the upside, this provides quick access to function
reference with URLs like [php.net/explode][explode].) Luckily the process is
[well documented][]. I won't duplicate their steps here, but I will
share some tips.

Here is [my Apache 2 configuration file][]. I saved this to
`/private/etc/apache2/other/php.localhost.conf` where it's automatically
included by `/private/etc/apache2/httpd.conf`. Open up `httpd.conf` add
the directive `NameVirtualHost *`.

Mac OS X 10.5 honors `/etc/hosts`, so appending something to the
standard localhost line gives you a quick-and-easy local domain name. I
used `php.localhost`:


    127.0.0.1       localhost php.localhost


The initial takes a few minutes. I highly recommend using the suggested
flags to ignore non-English language files and the documentation
tarballs.

Finally, I had to disable the inclusion of `httpd-manual.conf` in
`httpd.conf`, which was overriding `http://php.localhost/manual/`.

### jQuery


<span style="color: #ff0000">Unfortunately, this results in a broken API
browser for reasons unknown to me. I'll leave it here in case others
figure out where I went wrong.</span>

This one is pretty straightforward, but requires another `VirtualHost`
as the URLs are all relative to `/`. Personally I like
[docs.jquery.com][] better than the API browser, but those docs run off
a MediaWiki install. First, download the docs. (These are also available
as an Adobe AIR application, linked from [api.jquery.com][].)

First, add a new alias to `/etc/hosts`:


    127.0.0.1       localhost php.localhost jquery.localhost


Now download the API browser using Subversion. For some reason,
jquery-1.3.1.js is missing from trunk, so we'll download that as well.


    cd ~/Sites/
    svn co http://jqueryjs.googlecode.com/svn/trunk/tools/jquery-api-browser/ jquery-docs
    curl -o jquery-docs/lib/jquery/jquery-1.3.1.js http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js


A very simple Apache configuration file is needed. I created
`other/jquery.localhost.conf`, just like I did for the PHP docs:


    <VirtualHost *>
        ServerName jquery.localhost
        DocumentRoot /Users/adam/Sites/jquery-docs
        DirectoryIndex index.php
    </VirtualHost>


Restart Apache (`sudo apachectl restart`) and browse to
`http://jquery.localhost/`. Docs!

  [PHP]: http://www.php.net/
  [Django]: http://www.djangoproject.com/
  [jQuery]: http://jquery.com/
  [Django's documentation files]: http://docs.djangoproject.com/en/dev/
  [Sphinx]: http://sphinx.pocoo.org/
  [There's no step 3]: http://www.youtube.com/watch?v=YHzM4avGrKI
  [explode]: http://php.net/explode
  [well documented]: http://www.php.net/mirroring.php
  [my Apache 2 configuration file]: /media/2009/02/10/php.localhost.conf
  [docs.jquery.com]: http://docs.jquery.com/
  [api.jquery.com]: http://api.jquery.com/
