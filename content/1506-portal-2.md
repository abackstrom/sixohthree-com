Title: Portal 2: Now with More Portals
Slug: 1506/portal-2
Summary: When I think about the original Portal, I think of increasingly challenging puzzles, easter eggs, and the sarcasm and insults of a cruel AI. Portal 2 continues this tradition in a very satisfying way.
Date: 2011-04-25 12:53
Tags: Games, portal
WordPress-Post-ID: 1506
WordPress-Post-Type: post

<figure class="">
    <a href="https://sixohthree.com/files/2011/04/portal2-bsod.jpg">
        <img src="https://sixohthree.com/files/2011/04/portal2-bsod-300x187.jpg" height="187" width="300" />
    </a>
    <figcaption>Press any key to vent radiological emission into atmosphere.</figcaption>
</figure>

When I think about the original [Portal][], I think of increasingly
challenging puzzles, easter eggs, and the sarcasm and insults of a cruel
AI. [Portal 2][] continues this tradition in a very satisfying way.

Portal 2 differs from its predecessor in a few important areas. The
narrative is more complex, providing much of the backstory for Aperture
Science and some of its key players. Where Portal's test chambers became
more difficult and deadly as you progressed, Portal 2 has more variety,
introducing mechanics throughout the course of the game. Finally,
single-player replayability is very limited, as they seemingly replaced
the single-player challenges with a multiplayer co-op mode.

I completed Portal 2 in 8.3 hours, by Steam's reckoning. The dialog
sucked me in and did not let go. I found myself eagerly awaiting the
next interaction point, sometimes standing still in a single place or
performing an action contrary to the objective to elicit a response from
one of the actors. The environmental attention to detail spans from
haunting to hilarious. Do yourself a favor and take time to look around.

I've given only minimal attention to the multiplayer mode (two full sets
of test chambers complete) but the initial experience was mind-bending.
Four portals takes some getting used to. Add to that the complexity of
synchronizing actions with a remote partner.

Portal was good, but I'm well and truly hooked after Portal 2. Thanks,
Valve.

  [img]: 
    "Portal 2: Blue Screen of Death"
  [Portal]: http://en.wikipedia.org/wiki/Portal_(video_game)
  [Portal 2]: http://www.thinkwithportals.com/
