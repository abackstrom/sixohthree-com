Title: Server Move
Slug: 113/server-move
Date: 2002-12-08 21:04
Category: Meta
WordPress-Post-ID: 113
WordPress-Post-Type: post

In an effort to improve uptime, I have moved my Movable Type setup to my
iBook. It should be up pretty much constantly, at the address
[pridian.rh.rit.edu][].

Pridian, for the curious, is a Latin adjective meaning "pertaining or
belonging to the previous day." I think that loosely applies to a blog,
no?

  [pridian.rh.rit.edu]: http://pridian.rh.rit.edu/
