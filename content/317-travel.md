Title: Travelog: To Jen and Back
Slug: 317/travel
Date: 2005-06-21 20:06
Tags: Personal
WordPress-Post-ID: 317
WordPress-Post-Type: post

Early this AM I drove back into Wentworth, fresh off the train from
North Carolina after visiting with [Jen][] all weekend. Nice place, it
was. I saw a [bit][] of [art][], ate some [good food][], and generally
just hung out for a few days.



Also on the docket was a concert by [Aimee Mann][]. She played some new
stuff that I'm not familiar with, but also gave us
<span class="title">Save Me</span>, <span class="title">Driving
Sideways</span>, and <span class="title">Deathly</span>, not to mention
an impromptu <span class="title">Freebird</span> (lesson for the night:
you can't tame a bird) and Harry Nilsson's
<span class="title">One</span>, a tune they haven't played for a couple
years she noted.



No photos from me on this one: Nikon is still fixing my camera. If I get
cracking on Jen's site soon, maybe you'll be treated to pictures of tiny
fish swarming my pale feet in the Eno.



  [Jen]: http://www.jenniferkunz.com/
  [bit]: http://ncartmuseum.org/exhibitions/exhibitions/In%20Focus/blogphotos.shtml
  [art]: http://www.paulshambroomart.com/art/meetings%20revA/pages/UT%2C-Stockton.html
  [good food]: http://www.bluecorn-tosca.com/bc_home.asp
  [Aimee Mann]: http://www.aimeemann.com/home.html
