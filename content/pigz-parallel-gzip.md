Title: pigz: parallel gzip
Summary: pigz for multi-core gzip
Slug: pigz-parallel-gzip
Date: 2016-01-07 14:38
Tags: linux
Category: Technology

Neat find: [pigz](http://zlib.net/pigz/), "A parallel implementation of gzip
for modern multi-processor, multi-core machines."
