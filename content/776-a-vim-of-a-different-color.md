Title: A Vim of a Different Color
Slug: 776/a-vim-of-a-different-color
Date: 2009-03-26 23:07
Tags: Linux, Mac OS X, iterm, screen, Vim
WordPress-Post-ID: 776
WordPress-Post-Type: post

After one too many compilation errors due to a missing
quote/brace/bracket/etc., I finally enabled Vim syntax highlighting. The
rabbit hole:

1.  `:syn on`. These colors suck. Look for themes.
2.  4 bit themes suck. Let's set up an 8 bit color terminal.
3.  Leopard's Terminal.app [fails][], install iTerm 0.9.6.
4.  iTerm works, but I use GNU screen all day. Recompile screen with
    `--enable-colors256`.
5.  screen wins, but doesn't play nice with the delete key in iTerm.
    [Fix it][].
6.  Compile Vim 7.2 for 8 bit support.
7.  Get an [8 bit theme][]. `set t_Co=256` in `.vimrc`.
8.  `" syntax highlight html files using the php highlighter`  
    `autocmd BufRead,BufNewFile *.html set filetype=php`
9.  Install [FuzzyFinder][] for good measure now that we're out of Vim
    6.3 territory.
10. Start saving seconds/minutes/hours of my life that before were
    consumed by syntax errors.


![vim-color][]

  [fails]: http://www.vim.org/scripts/script.php?script_id=1349
  [Fix it]: http://akgeeks.net/node/83
  [8 bit theme]: http://www.vim.org/scripts/script.php?script_id=2175
  [FuzzyFinder]: http://www.vim.org/scripts/script.php?script_id=1984
  [vim-color]: /media/2009/03/vim-color.png
