Title: Webpages as Screen Savers in Mac OS X
Slug: 1771/webpages-as-screen-savers-in-mac-os-x
Summary: jQuery can power your screensaver. Your move.
Date: 2011-11-17 00:27
Tags: Web, HTML, JavaScript, jquery, Mac OS X, screen savers
WordPress-Post-ID: 1771
WordPress-Post-Type: post

[![][img]][img-big]

For years I've used the [Fliquo][] screen saver, which displays the
current time in the style of an old flip clock. Unfortunately, the
graphics have been glitchy of late, and a question has been worming its
way into my mind: can I display a web page as my screensaver? After all,
I already know how to code for the web, so a web-enabled screensaver has
unlimited possibilities.

[WebSaver][] plus a slapdash [Local & UTC clock][] makes it happen. So,
nerds: jQuery can power your screensaver. Your move.

  [img]: https://sixohthree.com/files/2011/11/websaver-clocks-300x263.png
    "websaver-clocks"
  [img-big]: https://sixohthree.com/files/2011/11/websaver-clocks.png
  [Fliquo]: http://9031.com/goodies/#fliqlo
  [WebSaver]: http://www.brock-family.org/gavin/macosx/websaver.html
  [Local & UTC clock]: http://code.sixohthree.com/clock/
