Title: Light Reading
Slug: 1456/light-reading
Date: 2011-01-11 09:58
Tags: Programming, oop, Programming
WordPress-Post-ID: 1456
WordPress-Post-Type: post
WordPress-Post-Format: Aside

Picked up [Clean Code][] and [Design Patterns][] based on some
[programmers.se][] recommendations. Hopefully I'll be able to apply the
concepts to PHP.

  [Clean Code]: http://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882
  [Design Patterns]: http://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612
  [programmers.se]: http://programmers.stackexchange.com/
