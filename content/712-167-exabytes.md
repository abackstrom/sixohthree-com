Title: 16.7 Exabytes
Slug: 712/167-exabytes
Summary: I'm a big deal.
Date: 2009-01-15 13:26
Tags: Computers, Mac OS X, processes, sqldeveloper, top, virtual memory
WordPress-Post-ID: 712
WordPress-Post-Type: post

Here's my currently-running Activity Monitor:

[![Activity Monitor][]][activity-monitor-orig]

That process at the top of the list? [SQL Developer][].

  [Activity Monitor]: /media/2009/01/picture-1-300x235.png
  [activity-monitor-orig]: /media/2009/01/picture-1.png
  [SQL Developer]: http://www.oracle.com/technology/products/database/sql_developer/index.html
