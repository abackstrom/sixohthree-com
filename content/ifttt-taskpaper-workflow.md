Title: IFTTT and Taskpaper TODO Workflow
Slug: ifttt-and-taskpaper-todo-workflow
Summary: A writeup on my DO Note/Dropbox/Taskpaper workflow which allows "ubiquitous capture" on iOS devices.
Date: 2015-07-19 06:34
Meta-Image-Square: /media/2015/07/tasks-ios-action-727x727.jpg
Category: Technology
Tags: taskpaper, ifttt

*This is a writeup on my DO Note/Dropbox/Taskpaper workflow which allows
"[ubiquitous capture]" on my iOS devices.*

Two decades of computer usage taught me to fear vendor lock-in: situations where
your software holds volumes of data but lacks an export feature, making it
difficult to switch software if your needs change or if the software becomes
unsupported. Apps like [nvALT], [Byword], and [Marked] are an important part
of my writing and note taking workflow. These programs are Mac-specific, but
they operate on flat text files, so they also work with file-agnostic services like
[Dropbox] and [BitTorrent Sync], or Unix-standard text tools like `grep(1)`.

[Taskpaper], a task list app by [Jesse Grosjean][@jessegrosjean], is a great
example of building software around flat files without sacrificing usability.
Headers (lines that end ":"), tasks (lines that start with "-"), and tags
(keywords that start with "@") are perfectly readable and transformable in a
plain text editor, but are nicely formatted (and have useful tap actions) when
you use Taskpaper.

By combining [DO Note], Dropbox, and Taskpaper, I have a simple, text-driven task
list that's available everywhere but still easy to update on the go.

DO Note for iOS lets you perform actions ("recipes") on arbitrary text, like
sending a tweet or email. My "Add task to inbox" recipe
([screenshot][do-recipe]) formats the note as a Taskpaper task and appends it to
a Dropbox file called "INBOX.txt",

- File name: INBOX.txt
- Content: "- \[NoteText\]"
- Dropbox folder path: "Notes"[ref]"Notes" is the directory I use for flat
  text files. Apps like [nvALT] and [Byword] use this directory by
  default.[/ref]

With this recipe, I can quickly add tasks to my Taskpaper Inbox either by
opening the DO Note app, or (better) using the ubiquitous iOS "Share" dialog
([screenshot][do-share]). Tasks are added to the Inbox, then I move them to the
correct list (personal or work) next time the Inbox is reviewed.

![Taskpaper Inbox][taskpaper-inbox]

Back on my laptop, I have an [Alfred] workflow which quickly opens all my
standard Taskpaper files, including the Inbox.[ref]I share Alfred settings
between my work and personal computers, so unfortunately this workflow opens my
personal task list on my work computer, and vice-versa. I would prefer to share
the "tasks" keyword but not see my work TODOs on my personal computer.[/ref]

![Alfred workflow](/media/2015/07/tasks-alfred-workflow.png "A screenshot of Alfred workflow.
A 'tasks' keyword is configured to open three Taskpaper files, including
INBOX.txt.")

![Alfred keyword](/media/2015/07/tasks-alfred-keyword.png "A screenshot of Alfred
window with 'tasks' keyword filled in. The 'TaskPaper Lists' action is selected
in the results list.")

This constellation of apps and services enables the same flavor of task capture
you get in apps like OmniFocus or Todoist, without locking your todo list to a
specific platform or app. As long as there are sync services and plain text
editors, this workflow survives. That means a whole lot to me.

  [ubiquitous capture]: http://www.43folders.com/topics/ubiquitouscapture
  [Dropbox]: https://db.tt/ELPGw3q
  [Taskpaper]: http://www.hogbaysoftware.com/products/taskpaper
  [Marked]: http://marked2app.com/
  [Byword]: http://bywordapp.com/
  [nvALT]: http://brettterpstra.com/projects/nvalt/
  [do-recipe]: /media/2015/07/tasks-do-recipe.png "Screenshot of DO Note recipe configuration"
  [do-share]: /media/2015/07/tasks-ios-action-sm.jpg "Screenshot of DO Note dialog in iOS"
  [taskpaper-inbox]: /media/2015/07/tasks-taskpaper-inbox.png "Screenshot of Taskpaper Inbox file containing two items"
  [Do Note]: https://ifttt.com/products/do/note
  [BitTorrent Sync]: https://www.getsync.com/
  [@jessegrosjean]: https://twitter.com/jessegrosjean
  [Alfred]: http://www.alfredapp.com/
  [Unix philosophy]: https://en.wikipedia.org/wiki/Unix_philosophy
