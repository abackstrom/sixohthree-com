Title: Help from Unlikely Places
Slug: 259/help
Summary: For the less stylish among us.
Date: 2004-10-24 18:32
Tags: Computers, Web
WordPress-Post-ID: 259
WordPress-Post-Type: post

Introducing [BEHR ColorSmart][]. At first glance, this looks worthy of
my web developer toolkit. I'll file it next to the Pixy Color Scheme
Generator (which has been upgraded to [version 2][], very nice).

  [BEHR ColorSmart]: http://www.behr.com/behrx/workbook/index.jsp
  [version 2]: http://wellstyled.com/tools/colorscheme2/index-en.html
