Title: Rasmus on Interpolation vs. Concatenation
Slug: 2031/rasmus-on-interpolation-vs-concatenation
Summary: Rasmus Lerdorf has been tweeting this morning about concatenation vs. interpolation in PHP. Micro-optimizations to be sure, but an interesting look under the hood.
Date: 2012-05-30 09:31
Tags: Programming, PHP
WordPress-Post-ID: 2031
WordPress-Post-Type: post

[Rasmus Lerdorf][] has been tweeting this morning about concatenation
vs. interpolation in PHP. Micro-optimizations to be sure, but an
interesting look under the hood.

<blockquote class="twitter-tweet"><p>Question from a colleague: Should we use 1. &quot;abc <a href="https://twitter.com/search?q=%24a&amp;src=ctag">$a</a> def&quot; or 2. &#39;abc &#39;.$a.&#39; def&#39; ? Answer next tweet <a href="https://twitter.com/search?q=%23php&amp;src=hash">#php</a></p>&mdash; Rasmus Lerdorf (@rasmus) <a href="https://twitter.com/rasmus/statuses/207811953593303040">May 30, 2012</a></blockquote>

<blockquote class="twitter-tweet"><p>It&#39;s a toss-up. 1. Takes an extra opcode, but 2. creates more tmpvars. Performance is similar. <a href="https://twitter.com/search?q=%23php&amp;src=hash">#php</a></p>&mdash; Rasmus Lerdorf (@rasmus) <a href="https://twitter.com/rasmus/statuses/207814295508090880">May 30, 2012</a></blockquote>

<blockquote class="twitter-tweet"><p>And no, for those who asked, &quot;{$a}&quot; makes no difference. Same generated opcodes. <a href="https://twitter.com/search?q=%23php&amp;src=hash">#php</a></p>&mdash; Rasmus Lerdorf (@rasmus) <a href="https://twitter.com/rasmus/statuses/207815518344855552">May 30, 2012</a></blockquote>

<blockquote class="twitter-tweet"><p>And yes, the more variables you have, the slower option 2 becomes because each concat creates another tmpvar. <a href="https://twitter.com/search?q=%23php&amp;src=hash">#php</a></p>&mdash; Rasmus Lerdorf (@rasmus) <a href="https://twitter.com/rasmus/statuses/207817187266805762">May 30, 2012</a></blockquote>

<blockquote class="twitter-tweet"><p>sprintf() for lots of variables is about the same speed as concatenation, but not as fast as interpolation. <a href="https://twitter.com/search?q=%23php&amp;src=hash">#php</a></p>&mdash; Rasmus Lerdorf (@rasmus) <a href="https://twitter.com/rasmus/statuses/207818331741356032">May 30, 2012</a></blockquote>

<blockquote class="twitter-tweet"><p>Bear in mind that these are micro-optimizations. If you have even a single SQL/memcache lookup, there is no point worrying about this. <a href="https://twitter.com/search?q=%23php&amp;src=hash">#php</a></p>&mdash; Rasmus Lerdorf (@rasmus) <a href="https://twitter.com/rasmus/statuses/207819016386002944">May 30, 2012</a></blockquote>

<blockquote class="twitter-tweet"><p>Someone asked about Heredoc. Heredoc is just another syntax for interpolation. Identical opcodes, so identical (opcode cached) perf <a href="https://twitter.com/search?q=%23php&amp;src=hash">#php</a></p>&mdash; Rasmus Lerdorf (@rasmus) <a href="https://twitter.com/rasmus/statuses/207820956507451392">May 30, 2012</a></blockquote>

If you want to [examine the opcodes][] yourself, you can use [vld][],
the Vulcan Logic Disassembler.

  [Rasmus Lerdorf]: https://twitter.com/rasmus
  [examine the opcodes]: http://stackoverflow.com/questions/1795425/how-to-get-opcodes-of-php
  [vld]: http://pecl.php.net/package/vld
