Title: To Have, or Have Not
Slug: 265/have
Summary: But still, my gut tells me that it is wrong.
Date: 2004-11-06 01:50
Tags: Personal
WordPress-Post-ID: 265
WordPress-Post-Type: post

My parents grew up in [Rockport, MA][], a cozy town of a few thousand
people overlooking the Atlantic Ocean. The War had ended not many years
prior to their births, families were growing, and the town was full of
roving bands of children. The fishing industry and "the tool company"
(the only name I have ever heard it called) and the granite quarries fed
my parents and my parents' parents.



After graudating from Rockport High School, my parents, not yet
acquainted with each other, traveled to San Diego, CA. Fast forward to
1986: they are married with a family of their own, looking to move away
from the city and settle in an environment like the one they were raised
in. Rockport was considered, of course, but property values had risen so
high they could not afford to move to what had once been their home.



I have not been alive very long. I never experienced the Rockport of my
parents' childhood, and I will never really grasp the difference between
what was and what is. I only have ten years' worth of memories of family
visits, but every time I go it feels more crowded. Million dollar houses
line the perimiter of half the cape, the rich build houses on lots where
such a thing seems impossible. A modest lot with a house and a yard is
worth hundreds of thousands of dollars; such a prime piece of real
estate is a candidate for a bulldozer and three new residences. Climbing
property values are great for those that wish to sell, but it's nothing
but [increased taxes][] for the ones that call Rockport "home."



I try to place the feeling I get when I think about this. I have been
reading *Atlas Shrugged* recently. I am nearly two-thirds through the
book, and it has been enjoyable so far. I find many of the characters
unrealistic, caricatures that take certain emotions to an extreme. Rand
wants you to hate the Jim Taggarts, the Orren Boyles, everyone who
believes success is a right, not something to be earned through hard
work and intelligence.



I can see Rand's point. The rewards received for a job well done are our
motivation, whether those rewards be monetary, or spiritual, or whatever
else feeds the fire that keeps us moving forward. If a man works hard
all his life, builds a fortune, and decides he is going to build a \$1.5
million summer cottage on the last remaining lot in Rockport, who am I
to tell him "no?" By what right? By what standard?



But still, my gut tells me that it is wrong.



Maybe it's envy, jealousy. Maybe I want to be surrounded by people like
me, people that don't buy a third house or car on a whim. Maybe I don't
want it to happen in my town. What if I came back one day and found out
I couldn't afford to live across the street from the house I grew up in?
What if property taxes began to climb, making country life too expensive
for those who have lived here for their whole lives? I don't really have
to imagine, it's already starting. New Hampshire is becoming the bedroom
community for the greater Boston area.



At least some people have [the right idea][].



  [Rockport, MA]: http://www.rockportusa.com/
  [increased taxes]: http://yourtown.boston.com/town/rockport/trends.shtml
  [the right idea]: http://www.thismagazine.ca/issues/2004/07/yankee.php
