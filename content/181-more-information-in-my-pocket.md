Title: More Information in my Pocket
Slug: 181/more-information-in-my-pocket
Summary: What about...
Date: 2003-09-29 07:19
Tags: Computers
WordPress-Post-ID: 181
WordPress-Post-Type: post

What about an application that synchronizes [my wiki][] with a
predefined "Wiki" category in my Palm m500's "Notes" application? That
would be *most excellent*, Garth.

  [my wiki]: http://wiki.bwerp.net/
