Title: Well, that was fast
Slug: 277/fast
Summary: In record time.
Date: 2004-12-08 22:57
Tags: Personal
WordPress-Post-ID: 277
WordPress-Post-Type: post

Bwerp.net has been moved to a new server. The [Graffiti board][] is
running, the [forum][] is up, and e-mail seems to be working fine.
Karl's site is absent, but that's his deal.



This has been a dotdotdot production: "Your source for up-to-the-minute
Bwerp news."



  [Graffiti board]: http://www.bwerp.net/graffiti/
  [forum]: http://www.bwerp.net/forum/
