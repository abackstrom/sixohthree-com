Title: Making Windows Bearable
Slug: 118/making-windows-bearable
Date: 2002-12-16 18:10
Tags: Computers
WordPress-Post-ID: 118
WordPress-Post-Type: post
Category: Technology

I sort of stumbled across [Cygwin][] the other day. It's by Red Hat, but
I actually like it. Cygwin gives Windows users access to many of the
tools included with most Linux distributions. It's comforting having
native access to [mutt][], vim, bash, and ssh, on those occasions when I
need to boot into Windows. I give it TWO THUMBS UP.

  [Cygwin]: http://www.cygwin.com/
  [mutt]: http://www.mutt.org/
