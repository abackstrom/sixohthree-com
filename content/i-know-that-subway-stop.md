Title: I Know That Subway Stop!
Slug: i-know-that-subway-stop
Summary: Watching Glengarry Glen Ross, I realized the subway stop was eerily familiar. On closer inspection, it's the local stop I take to work every day.
Date: 2013-06-10 09:19
Tags: Movies

Watching Glengarry Glen Ross, I realized the subway stop was eerily familiar.
On closer inspection, it's the local stop I take to work every day.

![Comparison of Sheepshead Bay subway stop in the movie Glengarry Glen Ross, and present day](//i.abackstrom.com/sixohthree.com/glengarry-glen-ross.1370612743.jpg)
