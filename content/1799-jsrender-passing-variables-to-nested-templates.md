Title: jsRender: Passing Variables to Nested Templates
Slug: 1799/jsrender-passing-variables-to-nested-templates
Summary: Is there a cleaner way than $view.parent.parent.data.key?
Date: 2011-12-01 14:28
Tags: Web, JavaScript, jsRender
WordPress-Post-ID: 1799
WordPress-Post-Type: post

jsRender is the in-development successor to jQuery Templates
(jquery-tmpl). I've been playing around with it, and have struggled with
a clean way to pass additional parameters to nested templates. What I've
got so far:

    <script id="t-parent" type="text/x-jquery-tmpl">
        <h1>{{=section}}</h1>
        <ul>
            {{#each subsections}}
                <li>{{=subsection}} in {{=$view.parent.parent.data.section}} AKA {{section_name}}</li>
            {{/each}}
        </ul>
    </script>

And the relevant JavaScript, including the `{{section_name}}` template
tag:

    $.views.registerTags({
        section_name: function() {
            return this._view.parent.parent.data.section;
        }
    });

    var o = { section: "Sample", subsections: [ { subsection: "Skydiving"}, { subsection: "Skiing"} ] };

    $('#container').html($('#t-parent').render(o));

I'm questioning `$view.parent.parent.data.section`. Seems a bit verbose,
but I can't find a cleaner way to step out of the nested template and
access variables from the outer scope.

I've [posted a jsFiddle][] of the above.

  [posted a jsFiddle]: http://jsfiddle.net/abackstrom/YHvMf/
