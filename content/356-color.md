Title: 4096 Color Wheel
Slug: 356/color
Summary: Pretty colors.
Date: 2006-01-27 14:34
Tags: Web
WordPress-Post-ID: 356
WordPress-Post-Type: post

I've been looking for a workable web-based color picker for a long time.
The [4096 Color Wheel][] is it.

  [4096 Color Wheel]: http://www.ficml.org/jemimap/style/color/wheel.html
