Title: Websites that Changed My Life
Slug: 374/change
Summary: One woman's quest to conquer the web.
Date: 2006-07-06 17:54
Tags: Web
WordPress-Post-ID: 374
WordPress-Post-Type: post

OK, that's a bit dramatic. More like, "websites that made me realize
things were changing," or "websites that showed me there was a better
way to do things." I write it here, lest I forget. Beware: extreme navel
gazing ahead.

### Symantec Visual Page



We start with a couple non-website items. Visual Page was my first
serious introduction to HTML, probably around 1997. I spent hours
crafting pages, watching how Visual Page generated markup. Tweaking
layout in the WYSIWYG and seeing attributes change and tags shift
position was an incredible learning experience. Several sites that began
as Visual Page endeavors still exist today.

### MacAddict



One especially important issue of MacAddict featured an introduction to
Cascading Style Sheets (CSS). I could hardly comprehend it. I remember a
lot of brackets, and some colons, possibly a simulated drop shadow.
Things are much clearer now. Those were the baby steps, though, coding
in BBEdit and refreshing the page in Internet Explorer 5.

### RIT IPEdit Tools



We skip ahead several years, possibly to 2002 or 2003 now. I am working
in customer support at the Rochester Institute of Technology as a
student employee. The IPEdit system, used by students to self-register
their MAC address or staff to administer parts of the network, is a bit
of a kludge. It does have one interesting feature, though: a "status
lights" display during processing. Sure, it's broken on most of the Mac
browsers, but it's rewriting (or trying, at least) the live page using
JavaScript, allowing changes to happen without reloading the page. Big
stuff, and the essence of Gmail and hundreds of other AJAX web
applications.

### Cinnamon Interactive



It was always my intention to dissect [Cinnamon Interactive][] and blog
the results. That never happened, but to this day the website is a
shining example of semantic design and the separation of style and
content. When I first came across the page, few websites were equally
readable in Internet Explorer 3 *and* the newest builds of Mozilla *with
no code changes*. Of special note was the unordered list, absolutely
positioned to function as a horizontal navigation bar. This was a
revelation for me.

### Orkut



While I often found it sluggish and lacking in the interface department,
[Orkut][] did have some interesting code behind it. The profile system
allowed you to rate your friends' as cool, sexy, or friendly, with up to
three points in each category. You could also mark someone as a
favorite. Clicking the image for two "cool" points would change two
images on the live page, but *would also update the backend database*.
This was the first time I saw XMLHttpRequest in action, modifying stored
data on a site without requiring a page reload. Google purchased Orkut
before producing any AJAX applications of its own, to my knowledge.
(Feel free to correct me.)

### And more?



Other developments have been important to me: Fahrner Image Replacement,
sliding window rollovers, etc. Above are specific examples of things
that made me go, "whoa," where I just had to dig into the code and see
what was going on. It's a great feeling.

  [Cinnamon Interactive]: http://www.cinnamon.nl/
  [Orkut]: http://www.orkut.com/
