Title: Post-Boskone Post
Slug: 297/boskone
Summary: It was cool, yeah yeah.
Date: 2005-02-21 00:02
Tags: Personal
WordPress-Post-ID: 297
WordPress-Post-Type: post

Just got back from [Boskone][]. This was my first, and the only
convention I've attended besides [Macworld Expo][]. I haven't done much
reading lately and was a bit out of the loop, but I had a lot of fun
regardless.



Highlights of the trip were Literary Beers with Jeffrey A. Carver, Bruce
Coville, et. al. (see below) and pillowfights with Justin for the girls'
amusement (photographs withheld).



[![Photograph of Justin, Jenny, and Bruce Coville.][]][]



Also notable is the excellent Halo road sign by Michael Lopes Jr., who
seems to be lacking a website. I was lucky enough to be the winning
bidder in the art auction:



[![Photograph of Justin, Jenny, and Bruce Coville.][1]][]



(I'll post a better picture after I can photograph it during daylight
hours.)



Thanks to everyone who made this weekend possible.



  [Boskone]: http://www.boskone.org/
  [Macworld Expo]: http://www.macworldexpo.com/
  [Photograph of Justin, Jenny, and Bruce Coville.]: /media/2005/02/20/t_beer.jpg
  [![Photograph of Justin, Jenny, and Bruce Coville.][]]: /media/2005/02/20/beer.jpg
  [1]: /media/2005/02/20/t_halo-01.jpg
  [![Photograph of Justin, Jenny, and Bruce Coville.][1]]: /media/2005/02/20/halo-01.jpg
