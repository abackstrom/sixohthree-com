Title: Fresh Starts
Date: 2014-10-26 2:55
Summary: For the first time in many years, I have a Windows PC.

For the first time in many years, I have a Windows PC. This is the first
computer I've owned outright since ~2009, when I starting using company-owned
laptops full-time. Having my own computer has been refreshing: separating my
work and home computing has been on my TODO list for ages.

As a challenge, I'm trying to set up the new computer without any help from my
very customized MacBook. Shortcuts like using an existing SSH key are out. Just
to edit this blog:

1. Install Cygwin
2. Generate new ssh key
3. Login to server using web console to add new public key, but paste isn't working
4. Add public key to GitHub (note: could/should have used GitLab)
5. Use curl + tail to append [public key] to ~/.ssh/authorized_keys
6. Fix permissions problems on cygwin ~/.ssh directory and files
7. On server, checkout gitolite admin directory and add new public key
8. On Windows machine, checkout blog directory

So that's how I end up with a very useful post about how I made the post.

  [public key]: https://github.com/abackstrom.keys
