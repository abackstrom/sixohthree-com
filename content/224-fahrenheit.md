Title: Fahrenheit 9/11 in Theatres Friday
Slug: 224/fahrenheit
Summary: Be there or be Republican.
Date: 2004-06-22 15:07
Tags: Politics
WordPress-Post-ID: 224
WordPress-Post-Type: post

[Michael Moore][]'s new film, [Fahrenheit 9/11][], will [open in
theatres][] this Friday, June 25. The film, [winner of the Palm d'Or][]
at this year's Cannes film festival, has been under aggressive
supression by Republican groups and big business for months. I'm glad to
see Moore and his crew stuck to their guns and got the movie into
theatres.



For those of you in Rochester, it's playing at [Cinemark Tinseltown][]
several times on Friday. Hope to see you there.



(Thanks to [Abby][] for the heads-up.)



  [Michael Moore]: http://www.michaelmoore.com/
  [Fahrenheit 9/11]: http://www.fahrenheit911.com/
  [open in theatres]: http://www.michaelmoore.com/words/message/index.php?messageDate=2004-06-04
  [winner of the Palm d'Or]: http://www.michaelmoore.com/words/message/index.php?messageDate=2004-05-23
  [Cinemark Tinseltown]: http://www.cinemark.com/theater_showtimes.asp?theater_id=202&show_date=6/25/2004
  [Abby]: http://blogs.bwerp.net/~abby/
