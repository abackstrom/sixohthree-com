Title: TaskPaper 3: Child-aware Archiving
Slug: taskpaper-3-child-aware-archiving
Summary: Here's a TaskPaper "Archive" replacement that keeps child tasks with
    their parents.
Date: 2018-06-10 12:11
Category: Technology
Tags: taskpaper, javascript

[TaskPaper 3][1] is a critical part of my workflow.[ref]For more about
why I like and how I use TaskPaper, see [IFTTT and Taskpaper TODO
Workflow][2].[/ref] I use it to track what's happening now and what's up
next, but also what I've done recently. Having a log of recent tasks is
great during times of retrospection or when timeline questions come up.

In TaskPaper, completed tasks are tagged with `@done`. The built-in
"Archive" action moves all done tasks to an "Archive" Project.
Unfortunately, Archive will also disconnect child tasks from their
parents:

<video autoplay loop controls preload="none"
    poster="/media/2018/06/taskpaper.jpg"
    src="/media/2018/06/taskpaper.mp4"></video>

*Fotunately*, TaskPaper is scriptable with JavaScript, and we can create
a custom action with archiving more to my liking.

## Goals

Ideally, we would not move child tasks to the Archive until their parent
task is complete. The default behavior disconnects children from their
parents, losing important context. Here's a document before archiving:

```
Inbox:
  - Build login page @due(2018-12-01)
    - Add auth0 library to composer.json @done(2018-08-17)
    - Write tests
    - Create tables
  - Book hotel for trip to sf @done(2018-06-10)
    - Get corporate card @done(2018-06-10)
```

And, after archiving:

```
Inbox:
  - Build login page @due(2018-12-01)
    - Write tests
    - Create tables
Archive:
  - Add auth0 library to composer.json @done(2018-08-17)
  - Book hotel for trip to sf @done(2018-06-10)
    - Get corporate card @done(2018-06-10)
```

Our history no longer tells us why the auth0 task existed. My preference
is to keep the hierarchy, and only archive the "top-level" tasks that
sit directly below a Project.

## The "Smart Archive" extension

Here's an extension that accomplishes our goal.

1. Ensure an "Archive" project exists
2. Find all `@done` tasks that are immediate children of projects (excluding the
Archive project)
3. Move these items to the Archive, preserving their current order in the
document

```js
var TaskPaper = Application('TaskPaper');

function fn(editor, options) {
    var outline = editor.outline;
    var archive = outline.evaluateItemPath("//Archive:")[0];

    var insertBeforeElement = archive ? archive.firstChild : undefined;

    outline.groupUndoAndChanges(() => {
        if (typeof archive === "undefined") {
            archive = outline.createItem("Archive:");
            outline.root.appendChildren(archive);
        }

        var topLevelItems = outline.evaluateItemPath("project/@done except Archive//*")

        topLevelItems.map(item => {
            item.removeFromParent();
            if (insertBeforeElement) {
                archive.insertChildrenBefore(item, insertBeforeElement)
            } else {
                archive.appendChildren(item);
            }
        });
    });
}

TaskPaper.documents[0].evaluate({
    script: fn.toString()
})
```

If we install this script to the TaskPaper Scripts folder, we can quickly run it
using `cmd-shift-P` and typing the script name. I find it's also helpful to
[rebind the default Archive shortcut][6] in the Keyboard preference pane to
combat muscle memory.

For more information about TaskPaper scripting, see the [Scripting API][3], [Extensions Wiki][4], and [script installation guide][5].

  [1]: https://www.taskpaper.com/
  [2]: ifttt-and-taskpaper-todo-workflow
  [3]: https://guide.taskpaper.com/reference/scripting
  [4]: http://support.hogbaysoftware.com/t/taskpaper-extensions-wiki/1628/1
  [5]: http://support.hogbaysoftware.com/t/how-do-i-run-install-a-script/1740?u=jessegrosjean
  [6]: https://support.apple.com/kb/PH25377
