Title: Death Knight Primer
Slug: 602/death-knight-primer
Summary: Quick tips for the newbies.
Date: 2008-11-15 12:13
Tags: death knight, world of warcraft
Category: Games
WordPress-Post-ID: 602
WordPress-Post-Type: post

Hi, my name is Annika, and I have a [Death Knight][]. I'm writing this
post as a helpful guide to starting out with the Death Knight class, for
others going into it blind like I did. It's not about spoilers or
walkthroughs, just those little things that were not immediately
obvious.

Some miscellaneous points of interest:

- You are confined to the Death Knight starting area until you
  complete the initial quest line.
- You will replace all your green starter gear with blue gear by the
  time you finish. This can be a nice gauge of progress. ("Two items
  left, almost done!")
- You will get an epic (100% speed increase) ground mount as a quest
  reward.
- You will always have access to the Runeforges in Ebon Hold through
  [Death Gate][]. During specific quest line events, the Runeforges
  will not function. Don't worry about it, they'll be back.
- When you finish the last quest in your faction's capital city, go to
  Outland. You should be level 58, and you're going to destroy
  anything you come across in Hellfire Peninsula.
- Professions
    - You will get First Aid level 270 for free, with Heavy Runecloth
    Bandage as an orange skill.
    - You will have the Journeyman riding skill.
    - Everything else has to start from Level 1. Enjoy grinding
    skillups!
- You get all the Eastern Kingdom and Kalimdor flight paths for free.
- You start with 10 [Black Mushrooms][], and these cannot be purchased
  from vendors. Send them to other characters for [Tastes Like
  Chicken][] and the ilk.


And finally, notes about specific quests:

### Quest: [Grand Theft Palomino][]


Bring the horse back to Salanar, then look for a button in your new
vehicle action bar for turning in the horse.

### Quest: [Into the Realm of Shadows][]


The Dark Riders were hotly contested when I did this, but they do spawn
in the town. I just hung out where I initially picked up my horse.

### Quest: [A Special Surprise][]


Dialog is a big part of this quest. When you enter the prison house,
you're looking for an NPC of your race. Listen to what he has to say.
You'll know when to head back to Plaguefist.

### Quest: [An End to All Things…][]


When you're done destroying stuff, return to The Lich King and dismount
using the "Leave Vehicle" button in your toolbar.

### Quests: [The Lich King's Command][] and [The Light of Dawn][]


During this quest you'll see two NPC counters and a timer at the top of
your screen. If the event is already in progress, or seems to have
stopped, just hang out by Browman's Mill (or maybe even further back,
I'm not sure how big the completion radius is) and wait for it to reset.
It really is epic, do yourself a favor and experience the whole thing.

Several of us grouped which turned out to be unnecessary, and another
Death Knight showed up after all the dialog and got quest credit for the
whole thing. Nice time saver, but wait for the reset if you care about
lore.

### That's It


Have fun. I am.

  [Death Knight]: http://www.worldofwarcraft.com/wrath/features/deathknight/gameplay.xml
  [Death Gate]: http://www.wowhead.com/?spell=50977
  [Black Mushrooms]: http://www.wowhead.com/?item=41751
  [Tastes Like Chicken]: http://www.wowhead.com/?achievement=1832
  [Grand Theft Palomino]: http://www.wowhead.com/?quest=12680
  [Into the Realm of Shadows]: http://www.wowhead.com/?quest=12687
  [A Special Surprise]: http://www.wowhead.com/?search=a+special+surprise
  [An End to All Things…]: http://www.wowhead.com/?quest=12779
  [The Lich King's Command]: http://www.wowhead.com/?quest=12800
  [The Light of Dawn]: http://www.wowhead.com/?quest=12801
