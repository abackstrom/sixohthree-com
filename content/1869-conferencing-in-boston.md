Title: Conferencing in Boston
Slug: 1869/conferencing-in-boston
Date: 2012-01-02 01:18
Tags: Web, Conferences, jquery, jQuery Conference, WordCamp, WordPress
WordPress-Post-ID: 1869
WordPress-Post-Type: post
WordPress-Post-Format: Aside

I've been fortunate enough to attend three conferences through [my
employer][]: jQuery Conference (2010 and 2011), and WordCamp Boston
(2011). I've regretted not blogging about these, so in lieu of a full
recap, suffice it to say these were awesome, energizing events and I
hope to keep participating in the future. The tech scene in Boston is
alive and well.

  [my employer]: http://www.plymouth.edu/
