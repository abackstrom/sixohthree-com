Title: Adventures in Git: Deploy on Push
Date: 2013-03-27 12:49
Category: Adventures in Git
Tags: git
Slug: git-deploy-on-push
Summary: For posterity, here's a small Git post-receive script to update a checkout when a bare repository receives a push.

For posterity, here's a small Git `post-receive` script to update a checkout
when the bare repository receives a push:

    #!/bin/sh
    GIT_WORK_TREE=/path/to/git/clone git checkout -f

Note that `GIT_WORK_TREE` is the path to the checked-out files, not the bare
repository. You can only push to bare repos! If you get this error message on
push, make a bare repo with `git init --bare` and push to that instead.

    Writing objects: 100% (3/3), 274 bytes, done.
    Total 3 (delta 0), reused 0 (delta 0)
    remote: error: refusing to update checked out branch: refs/heads/master
    remote: error: By default, updating the current branch in a non-bare repository
    remote: error: is denied, because it will make the index and work tree inconsistent
    remote: error: with what you pushed, and will require 'git reset --hard' to match
    remote: error: the work tree to HEAD.
    remote: error:
    remote: error: You can set 'receive.denyCurrentBranch' configuration variable to
    remote: error: 'ignore' or 'warn' in the remote repository to allow pushing into
    remote: error: its current branch; however, this is not recommended unless you
    remote: error: arranged to update its work tree to match what you pushed in some
    remote: error: other way.
    remote: error:
    remote: error: To squelch this message and still keep the default behaviour, set
    remote: error: 'receive.denyCurrentBranch' configuration variable to 'refuse'.
    To /path/to/repo
     ! [remote rejected] master -> master (branch is currently checked out)
     error: failed to push some refs to '/path/to/repo'
