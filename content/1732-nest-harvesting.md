Title: Nest Harvesting
Slug: 1732/nest-harvesting
Summary: Bald-face hornet's nest, found off Buffalo Road in Rumney, NH.
Date: 2011-11-06 08:38
Tags: Nature, Photography
WordPress-Post-ID: 1732
WordPress-Post-Type: post

<div>
[![][img]][img-big] [![][1]][]

</div>

Bald-face hornet's nest, found off Buffalo Road in Rumney, NH.

  [img]: https://sixohthree.com/files/2011/11/baldfacehornet-nest1-224x300.jpg
    "Bald Face Hornet's Nest"
  [img-big]: https://sixohthree.com/files/2011/11/baldfacehornet-nest1.jpg
  [1]: https://sixohthree.com/files/2011/11/baldfacehornet-nest2-224x300.jpg
    "Bald Face Hornet's Nest"
  [![][1]]: https://sixohthree.com/files/2011/11/baldfacehornet-nest2.jpg
