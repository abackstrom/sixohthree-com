Title: Updated: Amazon S3 for WordPress
Slug: 1729/updated-amazon-s3-for-wordpress
Date: 2011-11-04 13:18
Category: Meta
Tags: WordPress
WordPress-Post-ID: 1729
WordPress-Post-Type: post
WordPress-Post-Format: Aside

I've updated my "[WordPress Media in Amazon S3][]" post with a diff of
my Zend library modifications.

  [WordPress Media in Amazon S3]: http://mu.sixohthree.com/sixohthree/1575/saving-wordpress-media-to-amazon-s3-teaser
    "Saving WordPress Media to Amazon S3 (Teaser)"
