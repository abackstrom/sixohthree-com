Title: Background Noise
Slug: 360/rings
Summary: Green is good.
Date: 2006-02-28 17:37
Tags: Photography
WordPress-Post-ID: 360
WordPress-Post-Type: post

A desktop background, for fun.

[![][img]][img-big]

[1600x1200][![][]], [1280x1024][].

([Original][].)

  [img]: /media/2006/02/28/rings_thumb.jpg
  [img-big]: /media/2006/02/28/rings_1600x1200.jpg
  [1280x1024]: /media/2006/02/28/rings_1280x1024.jpg
  [Original]: http://www.flickr.com/photos/adambackstrom/76286918/in/set-1635750/
