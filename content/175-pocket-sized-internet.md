Title: Pocket-sized Internet
Slug: 175/pocket-sized-internet
Summary: As I <a href="http://blogs.bwerp.net/archives/2003/08/25/i_think_im_alone_now/">mentioned</a>, I'm the proud owner of an 802.11b wireless networking adapter for my Palm m500. I promised I'd post about it, and who am I to disappoint?
Date: 2003-08-25 22:48
Tags: Computers, Palm
WordPress-Post-ID: 175
WordPress-Post-Type: post

As I [mentioned][], I'm the proud owner of an 802.11b wireless
networking adapter for my Palm m500. I promised I'd post about it, and
who am I to disappoint?



So far, I've had really good luck with the Xircom Wireless LAN. It's not
the fastest kid on the block, but it's not like *I* can convert radio
signals into binary data, so who am I to judge? The RIT campus is pretty
well covered in access points, and I can make it all the way from
Gannett to a hundred feet past Crossroads before I lose signal. That's
about a quarter mile of walking with no interruption.



Walking around with an IP address is all well and good, but what about
the software? There are a pretty wide range of Internet-enabled
applications for the Palm. Here's a brief rundown of the ones I
currently use:



-   [AOL Instant Messenger][]. A decent app. It lacks some of the
    features of its destop brethren, but that's to be expected.
-   [iambic Mail][]. Supports IMAP, but isn't great about deleting on
    the server or updating the message list.
-   [upIRC][]. Pretty full featured, though I'm not a big IRC junkie.
-   [Blazer 2.0][]. A decent browser, with support for images and HTTPS.
-   [httpd][]. What's an Internet-enabled Palm without its own web
    server?



With any luck, I'll find a replacement for iambic Mail in the near
future. If anyone has suggestions, I'd be glad to hear them.



I'll see you on AIM, most likely while I wander the brick sidewalks and
dodge the less connected around me.



  [mentioned]: http://blogs.bwerp.net/archives/2003/08/25/i_think_im_alone_now/
  [AOL Instant Messenger]: http://www.aol.co.uk/aim/palm/
  [iambic Mail]: http://www.iambic.com/mail/palmos/
  [upIRC]: http://www.smittyware.com/palm/upirc/
  [Blazer 2.0]: http://www.palmgear.com/index.cfm?fuseaction=software.showsoftware&prodID=33508
  [httpd]: http://www.citi.umich.edu/u/rees/pilot/
