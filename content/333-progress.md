Title: WordPress Upgrade
Slug: 333/progress
Summary: Progress, they call it.
Date: 2005-09-07 22:22
Category: Meta
Tags: WordPress
WordPress-Post-ID: 333
WordPress-Post-Type: post

I bit the bullet and upgraded my installation of WordPress. Now, instead
of my aging WordPress 1.3 CVS, I have version 1.5.2.

The previous installation was heavily modified at all levels. Because of
this, a conventional upgrade was not possible. Suffice it to say I've
lost a few custom features. This blog has changed significantly, and
I'll be working to restore lost functionality over the next few days and
weeks.

Progress, they call it.
