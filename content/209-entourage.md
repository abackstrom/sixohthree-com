Title: Offline IMAP in Entourage
Slug: 209/entourage
Summary: Hint, it's supported.
Date: 2004-04-23 21:39
Tags: Mac OS X
WordPress-Post-ID: 209
WordPress-Post-Type: post

[![Entourage screenshot][1]][2]{: .inline-media-left } Microsoft Entourage X is a decent mail
program, but one thing that's peeved me in the past was the (apparent) lack of
support for offline reading of IMAP messages. Sure you could view messages you
had already downloaded and read, but who wants to click on each of fifty
messages before disconnecting from the Internet?

Turns out a user is free to change this functionality. Open the "Tools"
menu, and select "Rules." Double-click the "Send and Receive All" rule,
and click the account options button next to your target IMAP account.
You'll be presented with a list of all the folders in your account.
Checking the box next to a folder displays a corresponding popup menu
for setting synchronization options, defaulting to "Headers Only."
Select your preferred setting, ie. "All Messages" to download every byte
of each new message when you check your mail.

Nifty.

  [1]: /media/2004/04/23/entourage-synchronization-thumb.png
  [2]: /media/2004/04/23/entourage-synchronization.png
