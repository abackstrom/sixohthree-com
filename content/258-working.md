Title: Working Stiff
Slug: 258/working
Summary: I have a job.
Date: 2004-10-23 12:39
Tags: Work
WordPress-Post-ID: 258
WordPress-Post-Type: post

I have a job.

As of Wednesday, October 13, 2004, I am a Technical Support
Representative / Engineer at [Stonewall Cable, Inc.][] in Rumney, NH. As
technical support, I help customers connect with the product they are
looking for when questions arise, in addition to the normal problem
resolution duties. As an engineer, I help create and maintain the
technical drawings in the company's part library.

The "Cable" in "Stonewall Cable" is a very broad term. Most of the
business at Stonewall involves standard cabling (like [USB AB cables][])
and custom OEM-equivalent cables (like this [Cisco CAB-232MC
equivalent][]). Fiber Optics are my primary responsiblity, to start
with. Last week I didn't have any idea what Multimode Duplex 62.5/150
2.0mm with SC terminators even remotely referred to. How times change.

Don't let the post title mislead you, I really enjoy work a lot. I'm
learning ton of new things that I find very interesting, the people are
great, and it's close to home. I guess it's a reference to being out in
the real world, out of school after 22 years. It hasn't really sunk in
yet, since I'm still living at home. Finding an apartment is high on my
todo list.

A toast, to industry.

  [Stonewall Cable, Inc.]: http://www.stonewallcable.com/
  [USB AB cables]: http://www.stonewallcable.com/product.asp?dept%5Fid=20&pf%5Fid=AUSB%2D10
  [Cisco CAB-232MC equivalent]: http://www.stonewallcable.com/product.asp?dept%5Fid=114&pf%5Fid=SC%2D9445%2D9M
