Title: How Not to Serve Ads
Slug: 1425/how-not-to-serve-ads
Summary: I don't have Flash installed in my primary web browser. Google Chrome tries to download ads served by npr.org. Sometimes this causes tabs to close.
Date: 2011-01-04 12:37
Tags: Web, Advertising, chrome, flash
WordPress-Post-ID: 1425
WordPress-Post-Type: post

<video width="720" height="544" controls class="align-center"><source src="http://static.bwerp.net/~adam/2011/01/04/npr-no-flash.m4v"></source><source src="http://static.bwerp.net/~adam/2011/01/04/npr-no-flash.ogv"></source></video>

I don't have Flash installed in my primary web browser. Google Chrome
tries to download ads served by npr.org. Sometimes this causes the NPR
tab to close.
