Title: Microsoft is Pain
Slug: 143/microsoft-is-pain
Date: 2003-03-25 16:04
Tags: Computers
WordPress-Post-ID: 143
WordPress-Post-Type: post

So I'm taking a client/server database course this quarter. Now, I'm no
stranger to working with databases. I've done a few projects of varying
size in MySQL and PHP, most of which I enjoyed thoroughly. (Yes, I'm a
database weirdo. Sue me.) PHP's database access is straightforward and
sensible: send SQL statements to the database, and PHP will turn the
results into arrays or objects, whichever you prefer.



Microsoft, on the other hand, take the approach of Steamy Pile of Crap.



Cursors? Why on earth do you need to walk forwards and backwards through
a dataset? Server-side keysets? Yes, let's bog down the database server
*even more*.



Seriously, I've never had to write so much code to accomplish so little.
So many damn ODBC wrapper functions. Sigh.



(I'm done ranting now. I promise.)



