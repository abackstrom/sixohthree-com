Title: Backup Musings
Slug: 290/backup
Summary: I'll scratch your back if you scratch mine.
Date: 2005-01-25 23:38
Tags: Computers
WordPress-Post-ID: 290
WordPress-Post-Type: post

I've been thinking about backups again. I always have the crazy pipe
dreams of removable hard drives, cycled every day (expensive), or the
slightly more realistic thoughts of tape drives (lacking in capacity).
What about the sometimes-explored online backup method, though? For
argument's sake, I'll assume several people want to share the backup
load, backing up others' files in exchange for others doing the same.



[Rsync][] has obvious potential here, as it's very good at being
incremental and can be tunneled over SSH. This method would require
either: a centralized point of storage for all clients to upload to and
download from; or rsync access to each machine that needed backing up.
Not bad, but there's another method I like a little more...</a>

Picture a nightly backup tarball, encrypted and signed for security. A
[.torrent][] file is uploaded to a tracker and simultaneously handed to
the backup peers. The peers download the tarball from the tracker and
from each other, and we end up with our redundant remote backup.



Who's in?



<span style="font-weight: bold">Update:</span> [Azureus][] (my preferred
BitTorrent client) has an [RSS plugin][].



  [Rsync]: http://samba.anu.edu.au/rsync/
  [.torrent]: http://en.wikipedia.org/wiki/BitTorrent
  [Azureus]: http://azureus.sourceforge.net/
  [RSS plugin]: http://azureus.sourceforge.net/plugin_details.php?plugin=RSSImport
