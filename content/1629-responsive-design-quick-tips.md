Title: Responsive Design Quick Tips
Slug: 1629/responsive-design-quick-tips
Summary: "Responsive design quick tips," reblogged from Brad Frost on Google+.
Date: 2011-10-10 21:27
Tags: Web, responsive
WordPress-Post-ID: 1629
WordPress-Post-Type: post

Via +[Brad Frost][], on Google+:


> Responsive design quick tips:
>
> </p>
>
> - Start mobile-first: start with the lowest screen size
>
> - Keep in mind that there's plenty of devices less than 320px
>
> - The absence of a media query is the first media query. - +[Bryan
> Rieger][]
>
> - Let the fluid grid do most of the work. You don't want to manage 100
> layouts caused by 100 media queries, so let the fluidity of the web do
> its thang.
>
> - Use min-width, not max-width for the most part (again part of the
> mobile-first strategy)
>
> - Be mindful of media. Start with small images and swap them out for
> larger resolutions. Check out +[Jason Grigsby][] 's fantastic in-depth
> look at
> techniques. [http://www.cloudfour.com/responsive-imgs-part-2/][]
>
> - Don't lean on the DOM too much. Performance is your \#1 feature and
> manipulating things with JS too much can bog down your site's
> performance.
>
> - Don't do 100 backflips to rearrange a bunch of shit. Make sure your
> design is smart enough to scale up without essentially designing
> multiple sites.
>
> - Don't hide content for mobile. Mobile users want to access content
> just as much as desktop users.
>
> - Keep hit areas big. Design for touch input methods, and keep in mind
> that desktop users can benefit from increased hit areas as well.
>
> <p>
> Just some stream-of-consciousness stuff off the top of my head. What
> are some other tips?


[Google+][] via [@brad\_frost][] via [@paul\_irish][] vaya con dios.

  [Brad Frost]: https://plus.google.com/103751101313992876152
  [Bryan Rieger]: https://plus.google.com/106488504689809263842
  [Jason Grigsby]: https://plus.google.com/114344004397144865316
  [http://www.cloudfour.com/responsive-imgs-part-2/]: http://www.cloudfour.com/responsive-imgs-part-2/
  [Google+]: https://plus.google.com/103751101313992876152/posts/4NC8gjGy517
  [@brad\_frost]: https://twitter.com/#!/brad_frost/status/123494369612406784
  [@paul\_irish]: https://twitter.com/paul_irish
