Title: Making Plain Text Useful
Slug: 132/making-plain-text-useful
Date: 2003-01-29 09:09
Tags: Computers
WordPress-Post-ID: 132
WordPress-Post-Type: post

I've been taking class notes on my computer (and Palm m500) for quite
some time now, but I'm always left with the question of how to format
them so they're actually useful later. Plain text has formatting issues,
and isn't easily malleable. HTML looks nice, but it's hard to write on
the fly without a decent WYSIWYG editor. XML might work. Microsoft Word
will not.



Last night, I finally took the time to look for a decent script that
converts plain text into HTML. I found a couple of tools that offer
various levels of thoroughness and complexity. Which is good, because
now I don't have to try and write something from scratch.



The first is called [Gruatxt][]. It does all the things you would
expect, like \*emphasized\* text, bulleted lists, horizontal rules, and
headings. It's also very good at doing tables. Unfortunately, Gruatxt
wouldn't let me nest a list inside another list, which I need for any
sort of note taking. Here is a [sample file][], and the [same file
converted][] with Gruatxt.



I stopped my search after finding [txt2html][]. The structure of a plain
text file for txt2html is a bit less rigid, which I find an annoyance,
but it does my nested lists beautifully. Again, here's a [sample
file][1] and [formatted document][]. This is the tool I'll be tweaking
to work *just right* for me. Mmm.. open source.



  [Gruatxt]: http://www.triptico.com/software/grutatxt.html
  [sample file]: http://www.triptico.org/download/README_grutatxt
  [same file converted]: http://www.triptico.org/download/README_grutatxt.html
  [txt2html]: http://www.aigeek.com/txt2html/
  [1]: http://www.aigeek.com/txt2html/sample.txt
  [formatted document]: http://www.aigeek.com/txt2html/sample.html
