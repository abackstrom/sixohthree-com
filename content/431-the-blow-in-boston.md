Title: The Blow in Boston
Slug: 431/the-blow-in-boston
Summary: The Blow doesn't blow.
Date: 2007-10-07 10:50
Tags: Music, boston, mfa, the blow
WordPress-Post-ID: 431
WordPress-Post-Type: post

Saw [The Blow][] and Saturday Looks Good to Me at the [Museum of Fine
Arts][] in Boston last night. We showed up a lot earlier than most and
sat in the third row, right behind the reserved seats. (My head was at
about guitar level for the opening act.)

It was a great show. Khaela is a good storyteller and showman, and very
funny. It was neat to see how she added some level of improv to the
prerecorded beats. I'm not all too familiar with her library, but I
think she hit most (if not all) of the songs off Paper Television, plus
Come On Petunia as the encore. It was a solo show, with Jona off doing
his Yacht thing, and her last northeastern date of the Paper Television
stuff. I got the impression she wanted to leave it behind after the
split with Jona.

So, thanks, The Blow, as well as [Justin][] for including C2 and I.
Let's do it again some time.

  [The Blow]: http://www.myspace.com/theblowus
  [Museum of Fine Arts]: http://www.mfa.org/calendar/event.asp?eventkey=29513&date=10/6/2007
  [Justin]: http://www.callblog.net/
