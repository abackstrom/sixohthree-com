Title: Being Nice
Slug: 1912/being-nice
Date: 2012-01-31 12:30
Tags: Personal
Summary: For my own reference, on running processes nicely.
WordPress-Post-ID: 1912
WordPress-Post-Type: post

For my own reference, on running processes nicely:

    nice -n 19 ionice -c2 -n7

See also:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">it&#39;s
getting to the point where I won&#39;t even run &#39;ls&#39; without equipping
it with ionice, nice, setuidgid, softlimit and envdir. level 80 nerd</p>&mdash;
Pinboard (@Pinboard) <a
href="https://twitter.com/Pinboard/status/161598242671435776?ref_src=twsrc%5Etfw">January
23, 2012</a></blockquote>
