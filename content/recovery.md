Title: Recovery
Slug: recovery
Summary: Marking the passage of time.
Date: 2020-02-16 15:25
Category: Personal

I'm writing this from a bed in Philadelphia, which is where I've spent the past
week and a half.

I won't go into the details. I just want to mark this point in time, which I've
been thinking about and working towards for many years.

One more thing crossed off the list.
