Title: SOPA Protests
Slug: 1877/sopa-protests
Summary: Archived for posterity.
Date: 2012-01-20 11:02
Tags: Personal, Freedom, PIPA, Protests, SOPA
WordPress-Post-ID: 1877
WordPress-Post-Type: post

Archived for posterity.

<style type='text/css'>
#gallery-1 {
    margin: auto;
}
#gallery-1 .gallery-item {
    float: left;
       margin-top: 10px;
       text-align: center;
    width: 25%;
}
#gallery-1 img {
    border: 2px solid #cfcfcf;
}
#gallery-1 .gallery-caption {
    margin-left: 0;
}
</style>

<div id='gallery-1' class='gallery galleryid-1877 gallery-columns-4 gallery-size-thumbnail'>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/wired.com_.jpg' title='wired.com SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/wired.com_-150x150.jpg" class="attachment-thumbnail" alt="wired.com SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/4chan.org_.jpg' title='4chan.org SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/4chan.org_-150x150.jpg" class="attachment-thumbnail" alt="4chan.org SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/imgur.com_.jpg' title='imgur.com SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/imgur.com_-150x150.jpg" class="attachment-thumbnail" alt="imgur.com SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/craigslist.org_.jpg' title='craigslist.org SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/craigslist.org_-150x150.jpg" class="attachment-thumbnail" alt="craigslist.org SOPA protest" /></a>
</dt>
</dl>
<p><br style="clear: both" />
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/flickr.com_.jpg' title='Flickr.com SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/flickr.com_-150x150.jpg" class="attachment-thumbnail" alt="Flickr.com SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/google.com_.jpg' title='google.com SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/google.com_-150x150.jpg" class="attachment-thumbnail" alt="google.com SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/php.net_.jpg' title='php.net SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/php.net_-150x150.jpg" class="attachment-thumbnail" alt="php.net SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/wordpress.org_.jpg' title='wordpress.org SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/wordpress.org_-150x150.jpg" class="attachment-thumbnail" alt="wordpress.org SOPA protest" /></a>
</dt>
</dl>
<p><br style="clear: both" />
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/wordpress.com_.jpg' title='wordpress.com SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/wordpress.com_-150x150.jpg" class="attachment-thumbnail" alt="wordpress.com SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/en.wikipedia.org_.jpg' title='en.wikipedia.org SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/en.wikipedia.org_-150x150.jpg" class="attachment-thumbnail" alt="en.wikipedia.org SOPA protest" /></a>
</dt>
</dl>
<dl class='gallery-item'>
<dt class='gallery-icon'>
<a href='https://mu.sixohthree.com/sixohthree/files/2012/01/garfieldminusgarfield.net_.jpg' title='garfieldminusgarfield.net SOPA protest'><img width="150" height="150" src="https://mu.sixohthree.com/sixohthree/files/2012/01/garfieldminusgarfield.net_-150x150.jpg" class="attachment-thumbnail" alt="garfieldminusgarfield.net SOPA protest" /></a>
</dt>
</dl>
</div>
