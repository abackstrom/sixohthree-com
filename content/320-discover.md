Title: Brought to you by the number 1
Slug: 320/discover
Summary: This is profound.
Date: 2005-07-25 23:54
Tags: Computers, Freedom
WordPress-Post-ID: 320
WordPress-Post-Type: post

Via [Groklaw][], an excerpt from an interview with Donald Knuth:




> I come from a mathematical culture where we don't charge money from
> people who use our theorems. There is the notion that mathematics is
> discovered rather than invented. If something was already there, how
> you patent it?



From a discussion on the parallels between programming and mathematics,
and the detrimental role copyright plays in both fields.



  [Groklaw]: http://www.groklaw.net/article.php?story=20050724140820490
