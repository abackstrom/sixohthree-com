Title: Virii Lie
Slug: 205/virii
Summary: I don't remember sending this.
Date: 2004-03-18 17:32
Tags: Computers
WordPress-Post-ID: 205
WordPress-Post-Type: post

I received some pretty annoying e-mails this afternoon, both
masquerading as a warning to bwerp.net e-mail users from "bwerp.net
staff." (For those that don't know, *I* am "bwerp.net staff.") Here's a
message with some headers removed:



<!--more-->

Delivered-To: annika@sixohthree.com  

Date: Mon, 19 Jan 2004 12:49:30 -0500  

To: slashdot@bwerp.net  

Subject: Important notify about your e-mail account.  

From: support@bwerp.net  

  

Dear user of "Bwerp.net" mailing system,  

  

We warn you about some attacks on your e-mail account. Your computer may

contain viruses, in order to keep your computer and e-mail account safe,

please, follow the instructions.  

  

Further details can be obtained from attached file.  

  

Sincerely,  

The Bwerp.net team (http://www.bwerp.net)



I've also made the [message source][] available (sans virus), for those
that are interested in the full headers. I received [another message][]
(virus removed) that was slightly more convincing, including the
supposed user's password as a bitmap image. Both messages purported to
be from system administrators warning of dangers to their e-mail, and
both included viruses as attachments.



While I can spot a fake pretty easily, most users don't even realize the
From: line is completely arbitrary. And even though I know that fact,
it's not like I check the SMTP servers of every message I receive.



E-mail is flawed... What else can I say?



  [message source]: /media/2004/03/18/support_at_bwerp.txt
  [another message]: /media/2004/03/18/staff_at_bwerp.txt
