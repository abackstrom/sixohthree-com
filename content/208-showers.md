Title: Snowy Sunday
Slug: 208/showers
Summary: It's April and it's showing. April showers... April snow showers?
Date: 2004-04-04 16:49
Tags: Personal
WordPress-Post-ID: 208
WordPress-Post-Type: post

It's April and it's snowing. April showers... April snow showers?



[Boggle][] is all-consuming. Today will be an exercise in self-control.



I finally have a Palm again, thanks to Bob. This time around it's an
[m130][], slightly bulkier than my old m500 but with a color screen.



Note to self: don't go to bars to hear live music, because everyone else
will be at the bar to drink and yell.



I would like to thank [Nikon][] customer support for their excellent
service and speedy turnaround. They even shipped my camera back before
the week-long support downtime for upgrades.



That's all I've got. Carry on.



  [Boggle]: http://plutor.org/boggle/
  [m130]: http://www.the-gadgeteer.com/m130-review.html
  [Nikon]: http://www.nikonusa.com/
