Title: Thin Client Update
Slug: 148/thin-client-update
Date: 2003-04-22 01:39
Tags: Computers
WordPress-Post-ID: 148
WordPress-Post-Type: post

After an excruciating ten day long wait, I finally received all the
parts for my [new computer][]. After wrestling with a handicapped
onboard bootrom, a harddrive that doesn't seem to be bootable, and some
[assorted NFS problems][], everything seems to be in order. (Tip: make
sure rpc.statd is running on the client.) Ah, the joy of remote hard
drives, happily grinding away out of earshot.

I'm going to wander off and muse about my blog now.

When I blog, I have this complex where I want to provide useful
information. I want people to find this site by searching for something
more meaningful than "[rit direct connect][]." I do *tons* of stuff on
the computer, most of which I find pretty useful. Of course, I run into
a proportinally large amount of problems. Between my job and my
technical writing course, I'm on a bit of a documentation kick. I'd like
my blog to be a resource, or at least an interesting read for people
that have interests similar to mine.

I think I need to comb the blogsphere for [blogs to admire][]. My 'roll
is woefully small at this point.

  [new computer]: http://blogs.bwerp.net/archives/000109.php
  [assorted NFS problems]: http://www.google.com/search?q=nfs+cannot+monitor
  [rit direct connect]: http://www.google.com/search?q=rit+direct+connect
  [blogs to admire]: http://fintel.mit.edu/geek/
