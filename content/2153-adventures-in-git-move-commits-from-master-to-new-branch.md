Title: Adventures in Git: Move Commits from Master to New Branch
Slug: 2153/adventures-in-git-move-commits-from-master-to-new-branch
Summary: The other night, I sat down with Git to solve what turned out to be a very simple problem: what if you've started making commits to your master branch, but want to move your work into a feature branch?
Date: 2012-12-01 13:43
Category: Adventures in Git
Tags: Computers, git
WordPress-Post-ID: 2153
WordPress-Post-Type: post

The other night, I sat down with Git to solve what turned out to be a
very simple problem: what if you've started making commits to your
master branch, but want to move your work into a feature branch? After
staring at `git log` for a few minutes, I had a forehead-slapping
moment. Here's one way to move recent commits into a branch:

Start with a clean master, based off origin/master:

<div class="gitcanvas">
{ "hash": "a" },
{ "hash": "b" },
{ "hash": "c" }
</div>

Make a few commits of your own:

<div class="gitcanvas">
{ "hash": "a" },
{ "hash": "b" },
{ "hash": "c" },
{ "hash": "d" },
{ "hash": "e" },
{ "hash": "f" }
</div>

Now, let's move these commits to a branch. Simply create a new branch at
the current commit, then reset master back to the state of
origin/master.

    (master)$ git branch my-feature
    (master)$ git reset --hard origin/master

<div class="gitcanvas">
{ "hash": "a" },
{ "hash": "b" },
{ "hash": "c" },
{ "hash": "d", "branch": "feature" },
{ "hash": "e", "branch": "feature" },
{ "hash": "f", "branch": "feature" }
</div>

That's all. No `git rebase`, no `git cherry-pick`, just make your branch
and reset master.

<script type="text/javascript" src="/theme/js/raphael-min.js"></script>
<script type="text/javascript" src="/theme/js/git-canvas.min.js"></script>
<script>
(function(){
    var c = new git_canvas(), i = 0, len = 0, hashdata, parseddata;
    var placeholders = document.querySelectorAll('.gitcanvas');
    for (i = 0, len = placeholders.length; i < len; i++) {
        hashdata = placeholders[i].textContent;
        placeholders[i].textContent = '';
        parseddata = JSON.parse('[' + hashdata + ']');
        c.commits(parseddata).draw(placeholders[i]);
    }
})();
</script>

