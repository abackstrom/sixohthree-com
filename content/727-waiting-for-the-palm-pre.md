Title: Waiting for the Palm Pre
Slug: 727/waiting-for-the-palm-pre
Summary: Pre-release Pre thoughts, and some words on competition in the smartphone space.
Date: 2009-05-18 23:08
Tags: Technology, iphone, Palm, palmpre, pda, phones
WordPress-Post-ID: 727
WordPress-Post-Type: post

![palm-pre][]

For a few months now I've been salivating over the [Palm Pre][]. A cell
phone driven by browser-based apps is extremely appealing: I know HTML,
JavaScript, and CSS, so I know how to program applications for the Pre's
webOS. Historically, I've liked Palm's offerings very much: the m125,
m500, and T5 were a big part of my day for years.

Pre news is scarce thus far. Periodically developers get a new nugget in
the form of a new [rough cut][] or [blog post][]. Rumors continually
surface about who is getting prerelease units and what the release
timeframe is.

Occasionally I see [what the Twitterverse has to say][], but, as often
happens on the Internet, it's a jumble of pointless
my-phone-could-kick-your-phone's-ass arguments. Honestly, I don't care
much for the iPhone: as a gadget, it has never held my interest; as
competition to the Pre, I think the market is big enough to handle more
than one progressive smartphone. I'm not worried Apple will push Palm
out of the market, and Apple's momentum will prevent the Palm from doing
the same. The technology (and we, the consumers) can only benefit from
the competition between two great companies producing such great wares.
There will be a sharing of ideas, and gnashing of teeth, and threats of
lawsuits, and when the dust settles both phones will be better for it.

So, Palm, don't keep us waiting for the Pre. And Apple, bring on the
iPhone 3.0.

Then let the melting pot begin anew.

<ins datetime="2009-05-19T10:47-04:00">**Update:** Details about the
Palm Pre launch [have been released][] by the Wall Street Journal.</ins>

  [palm-pre]: /media/2009/01/palm-pre.png
  [Palm Pre]: http://www.palm.com/us/products/phones/pre/
  [rough cut]: http://pdnblog.palm.com/category/oreilly/
  [blog post]: http://pdnblog.palm.com/
  [what the Twitterverse has to say]: http://search.twitter.com/search?q=palm+pre+OR+%23palmpre
  [have been released]: http://online.wsj.com/article/SB124273162439334195.html
