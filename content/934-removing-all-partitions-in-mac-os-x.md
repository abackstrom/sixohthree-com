Title: Removing All Partitions in Mac OS X
Slug: 934/removing-all-partitions-in-mac-os-x
Summary: Here's now to "remove" all partitions on a disk under Mac OS X by clobbering the partition map.
Date: 2009-09-08 13:41
Tags: Computers, Mac OS X, encryption, security, Terminal, unix
WordPress-Post-ID: 934
WordPress-Post-Type: post

When setting up a new [TrueCrypt][] device under Mac OS X, you may
receive this message if you try to encrypt a whole disk rather than a
partition:

[![device-contains-partitions][]][device-contains-partitions]

I could not find a way to remove the partition via the Mac OS X Disk
Utility, so I resorted to some terminal work. By clobbering the first
few bytes of the disk's partition map, I essentially made the whole disk
look like random garbage, and TrueCrypt no longer presented a warning.
<span style="color: #ff0000">This will cause data loss, so be
careful.</span>

First, find the drive's device number using Disk Utility. Right click on
the drive (not one of the partitions) and select "Information."

[![right-click-info][]][right-click-info]

Look for "Disk Identifier" in the property list. In this case, my drive
is called <span style="color: #ff0000">disk3</span>, which corresponds
to /dev/disk3 on the filesystem. Tomorrow it might be disk4 or disk6, so
don't make any assumptions here. Failure to find the correct drive
number in this step will cause you to erase the wrong drive in a future
step.

[![device-information][]][device-information]

Open up your terminal and umount this disk's volumes. Note the asterisk,
which should be included in your command.

-   sudo umount -f /dev/<span style="color: #ff0000">disk3</span>\*


Unmounting drives in the Finder will make the disk unavailable in /dev,
and we need this block device if we're going to clobber the filesystem.

Once the partitions are unmounted, we can write junk to the partition
table and essentially unformat the drive.
<span style="color: #ff0000">This is the dangerous part, so don't screw
it up or you will destroy the wrong drive, lose all your data, and
seriously hate life.<span style="color: #000000"> Replace
<span style="color: #ff0000">FULL\_OF\_FAIL</span> with your drive
identifier!</span>

</span>

-   sudo dd if=/dev/zero
    of=/dev/<span style="color: #ff0000">FULL\_OF\_FAIL</span> bs=1 count=1024


After writing zeroes to the front of the drive, TrueCrypt happily made
an encrypted volume on the disk.

  [TrueCrypt]: http://www.truecrypt.org/
  [device-contains-partitions]: https://sixohthree.com/files/2009/09/device-contains-partitions.png
  [right-click-info]: https://sixohthree.com/files/2009/09/right-click-info.jpg
  [device-information]: https://sixohthree.com/files/2009/09/device-information.png
