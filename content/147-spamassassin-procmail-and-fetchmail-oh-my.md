Title: SpamAssassin, Procmail, and Fetchmail, Oh My
Slug: 147/spamassassin-procmail-and-fetchmail-oh-my
Date: 2003-04-13 02:41
Tags: Computers, Spam
WordPress-Post-ID: 147
WordPress-Post-Type: post

I followed [Mark Pilgrim's lead][] and set up [SpamAssassin][] tonight,
with [some help][]. I've actually been pretty lucky in the spam
department, with one spam every couple days. Having a [dummy account][]
with [aliases][] helps, plus I get to see exactly where people are
getting my address from.



Here's the lowdown on my current setup:



1.  Mail is delivered to my bwerp.net address.
2.  Every 10 minutes, [`fetchmail`][] pulls e-mail from bwerp.net and
    delivers it to annika@localhost.
3.  sendmail consults my .forward file, which says "filter mail through
    [`procmail`][]."
4.  `procmail` in turn filters all mail through SpamAssassin
5.  Any messages marked as spam are moved to my \~/Maildir/junk folder.



Pretty slick, if I do say so myself. Next step: consolidate my e-mail
accounts through `fetchmail` and `procmail`.



  [Mark Pilgrim's lead]: http://diveintomark.org/archives/2003/04/08/spamassassin_makes_spam_fun.html
  [SpamAssassin]: http://spamassassin.org/
  [some help]: http://codesorcery.net/docs/spamtricks.html
  [dummy account]: mailto:spam@bwerp.net
  [aliases]: mailto:slashdot@bwerp.net
  [`fetchmail`]: http://www.catb.org/~esr/fetchmail/
  [`procmail`]: http://www.procmail.org/
