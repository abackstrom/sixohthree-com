Title: Computer Dark Ages
Slug: computer-dark-ages
Summary: Steve Jobs on IBM (1985)
Date: 2013-08-28 21:46
Tags: apple

Steve Jobs, 1985:

> "If, for some reason, we make some giant mistakes and IBM wins, my personal
> feeling is that we are going to enter sort of a computer Dark Ages for about
> 20 years. Once IBM gains control of a market sector, they almost always stop
> innovation. They prevent innovation from happening."

(via [Business Insider][1])

  [1]: http://www.businessinsider.com/apple-needs-to-reinvent-the-iphone-business-2013-8
