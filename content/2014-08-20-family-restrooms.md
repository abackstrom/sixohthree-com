Title: Family Restrooms
Slug: family-restrooms
Summary: The perils of needing to pee.
Date: 2014-08-20 00:51
Tags: trans

Today while grocery shopping at Walmart, Marshall needed to pee. This usually
happens at least once per trip. We trekked off to the back of the store, home to
the film counter, men's & women's restrooms, and our destination, the only
"family" restroom in the building. Unfortunately, the door to the family
restroom is padlocked shut.

As a transwoman, I do not feel safe using most public restrooms. I fear the
unwanted attention, and in some cases, the threat of physical violence. I can
deal with it when it's just me. I get mad when when it prevents my son from
taking care of his bodily needs.

I will not send my six year old into the restroom alone, be it the men's or
women's room. I will send him into a room where he can lock the door, or I will
go with him into a unisex bathroom. But sometimes, to my great frustration, I
can only sigh, and look back down to him, and tell him: I'm sorry, but you need
to wait.
