Title: Google Chrome 13 "Pretty Print" Scripts
Slug: 1555/google-chrome-13-pretty-print-scripts
Summary: Screenshots of Google Chrome 13's "pretty print" feature for scripts.
Date: 2011-08-05 09:03
Tags: Programming, Web, chrome, debugging, JavaScript
WordPress-Post-ID: 1555
WordPress-Post-Type: post

Google Chrome 13 added a "pretty print" formatting option for scripts.
It's still not fun debugging third-party minified code, but at least
it's possible. jQuery before and after:

[![][img]][img-big][![][1]][]

  [img]: https://sixohthree.com/files/2011/08/devtools-script-300x164.png
    "devtools-script"
  [img-big]: https://sixohthree.com/files/2011/08/devtools-script.png
  [1]: https://sixohthree.com/files/2011/08/devtools-script-pretty-300x164.png
    "devtools-script-pretty"
  [![][1]]: https://sixohthree.com/files/2011/08/devtools-script-pretty.png
