Title: When Consoles Die
Slug: when-consoles-die
Summary: It didn't used to be this way.
Date: 2014-09-06 05:21
Tags: Nintendo, Wii
Category: Games

I took these photos on September 15, 2013, nearly seven years after the 2006
North American release of the Nintendo Wii. Massive, worldwide experiences
become ghost towns before disappearing forever.

![Check Mii Out][1]

![Everybody Votes][2]

![Forecast][3]

![News][4]

  [1]: //i.abackstrom.com/sixohthree.com/when_consoles_die/wii_check_mii_out_discontinued.jpg
  [2]: //i.abackstrom.com/sixohthree.com/when_consoles_die/wii_everybody_votes_discontinued.jpg
  [3]: //i.abackstrom.com/sixohthree.com/when_consoles_die/wii_forecast_discontinued.jpg
  [4]: //i.abackstrom.com/sixohthree.com/when_consoles_die/wii_news_discontinued.jpg
