Title: Palm Goodies
Slug: 117/palm-goodies
Summary: As a follow-up to my <a href="http://pridian.rh.rit.edu/archives/000022.html">last post</a>, here's a catalog of some interesting apps I've found for my <a href="http://www.palm.com/products/palmm500/">Palm m500</a>.
Date: 2002-12-14 03:00
Tags: Computers, Palm
WordPress-Post-ID: 117
WordPress-Post-Type: post
Category: Technology

As a follow-up to my [last post][], here's a catalog of some interesting
apps I've found for my [Palm m500][].

<!--more-->

Just stumbled across [Phoinix][], a Game Boy emulator. Pretty nifty, but
games only run at around 1/3 the speed of the normal GB clock. I took my
[screenshot][] using [TealPaint][], by TealPoint software.

[CheckBook][] is a nicely written piece of software for managing
expenses. It's got goodies like multiple accounts, running totals, and
transfers from one account to another.

[Plucker][] is a great tool for downloading websites for offline
viewing. It's open source, with the conversion program available for
every major OS. Plucker is also showing itself to be very useful for
eBooks. [The Plucker Book Repository][] has many books available for
browsing and .pdb download.

I'll include another update for games at some point. To whet your
appetite, here's [Patience][]. It's free, and it's got 15 solitaire
variations in one tiny app.

Check out [PalmOpenSource.com][] for more apps.

  [last post]: http://pridian.rh.rit.edu/archives/000022.html
  [Palm m500]: http://www.palm.com/products/palmm500/
  [Phoinix]: http://phoinix.sourceforge.net/
  [screenshot]: http://pridian.rh.rit.edu/media/zelda_palm.gif
  [TealPaint]: http://www.tealpoint.com/softpnt.htm
  [CheckBook]: http://www.ultrasoft.com/CheckBook/overview.shtml
  [Plucker]: http://www.plkr.org/index.plkr
  [The Plucker Book Repository]: http://www.pluckerbooks.com/
  [Patience]: http://keithp.com/pilot/patience/
  [PalmOpenSource.com]: http://www.palmopensource.com/
