Title: Filesystem Caching without Cruft in PHP
Slug: 1578/filesystem-caching-without-cruft-in-php
Summary: Cleanup your file caches while you're still using them.
Date: 2011-09-04 01:58
Tags: Programming, caching, performance, PHP
WordPress-Post-ID: 1578
WordPress-Post-Type: post

In PHP on a Unix system, you can `unlink()` files after `fopen()` to
keep `/tmp` free of cruft. I'm currently using this in a file caching
setup, where the cache exists only for the lifetime of the request.

[code name="caching-without-cruft.php"]

The file is unlinked as the last step, removing it from `/tmp`, but it's
still available for `fseek()` and `fread()` through the cached file
pointer (in a static, singleton, or other non-persistent store of
choice). When the request ends, the pointer is closed, and the
filesystem will free the space used by that file.

No keeping track of files for cleanup, no registering a shutdown
function, and we don't have to retain the file contents in memory.
