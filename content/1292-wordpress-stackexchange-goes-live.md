Title: WordPress Stack Exchange Goes Live
Slug: 1292/wordpress-stackexchange-goes-live
Summary: WordPress Answers is in public beta. Get in.
Date: 2010-08-20 22:11
Tags: Web, area51, stackexchange, WordPress
WordPress-Post-ID: 1292
WordPress-Post-Type: post

[![Screenshot of WordPress Stack Exchange{@align=left}][img]][img-big] The WordPress Stack Exchange I [blogged about][] last week has
moved into its [open beta][] phase. Over the next 81 days the community
will attempt to build a following of committed users with the goal of
becoming a full-fledged StackExchange site.

I enjoy the Stack Exchange model very much. It's Q&A with a social
twist, as your peers upvote your answers and increase your site
reputation. I have been off-and-on active on [Stack Overflow][], the
programmer-focused Q&A site, for two years now, dropping in occasionally
to answer a few questions when I need to switch gears. Participating in
WordPress Answers (as it's canonically known) has been a whole new
experience, starting with the empty canvas of a website, participating
in [meta][], being a more involved member of a community that is still
in its infancy.

If WordPress is a part of your online life, I encourage you to help out
by asking or answering questions. Do you have a problem with WordPress
you just can't figure out on your own? Can you remember a question from
your past that could benefit someone else if it were archived on the
site? Do you want to challenge yourself and improve your skills by
helping others with WordPress?

[Get in on the ground level][open beta].

  [img]: https://sixohthree.com/files/2010/08/wordpress-answers-beta-300x213.png
    "wordpress-answers-beta"
  [img-big]: https://sixohthree.com/files/2010/08/wordpress-answers-beta.png
  [blogged about]: https://sixohthree.com/1262/wordpress-stackexchange
  [open beta]: http://wordpress.stackexchange.com/
  [Stack Overflow]: http://stackoverflow.com/
  [meta]: http://meta.wordpress.stackexchange.com/
