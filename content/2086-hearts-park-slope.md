Title: Hearts, Park Slope
Slug: 2086/hearts-park-slope
Date: 2012-09-15 23:34
Tags: Photography, Brooklyn
WordPress-Post-ID: 2086
WordPress-Post-Type: post
WordPress-Post-Format: Image

[![Hearts in Park Slope][img]][img-big]

  [img]: https://sixohthree.com/files/2012/09/20120915-233411-768x1024.jpg "A photo taken in Park Slope, Brooklyn. Two hearts hang from a utility wire strung between a pole and a building."
  [img-big]: https://sixohthree.com/files/2012/09/20120915-233411.jpg
