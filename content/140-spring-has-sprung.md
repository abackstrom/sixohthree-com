Title: Spring has Sprung
Slug: 140/spring-has-sprung
Date: 2003-03-10 09:03
Tags: Personal
WordPress-Post-ID: 140
WordPress-Post-Type: post

Everything but the weather, that is. Spring break has come to a close,
and today is the first day of the last quarter of my junior year. I'm
simply *bursting* with excitement.



I've been playing around with the green stylesheet, and it's here for
good this time. I made sure to back it up, so I don't accidentally erase
an hour's worth of tweaking again. More changes to come, like adding the
top navigation bar and such.



I'll also be making some of my other content public, in an effort to
make Bwerp.net a more useful resource. I've got some Perl scripts to
post, along with a few projects I've been researching and documenting.
For the fans of my now-ancient (what, last winter?) PHP-Schedule, I've
got another iteration in the works. It's XML based, so it *has* to be
good, right?



"Catch you on the flip side."



