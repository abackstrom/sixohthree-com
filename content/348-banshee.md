Title: Banshee
Slug: 348/banshee
Summary: It plays music, it doesn't just scream wildly.
Date: 2005-10-11 12:48
Tags: Linux, Music, Wiki
WordPress-Post-ID: 348
WordPress-Post-Type: post

Linking to [Banshee][] for two reasons:

1.  It's an iTunes-like music player for Linux (is this nee sumi?), and
    I want to remember it.
2.  The website runs on [MediaWiki][].



Todo: find a wiki page that list websites that are styled like websites,
but run on MediaWiki (or other wikis). Alternately, start a list on
[Bwerp][].

  [Banshee]: http://banshee-project.org/
  [MediaWiki]: http://www.mediawiki.org/wiki/MediaWiki
  [Bwerp]: http://wiki.bwerp.net/
