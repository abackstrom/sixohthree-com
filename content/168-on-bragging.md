Title: On Bragging
Slug: 168/on-bragging
Summary: I twitch.
Date: 2003-07-13 12:18
Tags: Personal, Rants
WordPress-Post-ID: 168
WordPress-Post-Type: post

I was browsing [bash][] the other day, and came across a quote that
touched on one of my pet peeves. (For those who don't know, bash is a
quote database, mostly of things said on
<acronym title="Internet Relay Chat">IRC</acronym>.) First, the quote:


> \<digidug\> i love it when geeks take every chance they get to brag
> about their hardware  
>
> </p>
> \<digidug\> so i was lying in my bed and my DUAL ATHLON 2GHZ SERVER
> was keeping me awake with its 5 SUPER HIGH THROUGHPUT FANS so i went
> downstairs and played a SPECIAL SUPER BETA VERSION OF QUAKE 3 (FOR
> WHICH I AM A BETA TESTER) on my PLEXIGLASS-COVERED TRANSPARENT TOWER
> WITH FIVE INTEL PENTIUM 4'S RUNNING A CUSTOMIZED VERSION OF LINUX  
>
> <p>
> \<digidug\> so i didn't get much sleep last night



So, yeah. I find this *extremely* annoying. Don't be all non-chalant,
it's generally unbecoming of human beings. If you want to tell me
something, just tell me.

OK, now that I've got that out of my system...

  [bash]: http://bash.org/
