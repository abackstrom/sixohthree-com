Title: July Updates
Slug: 233/updates-2
Summary: I have much to be thankful for.
Date: 2004-07-21 23:45
Tags: Personal
WordPress-Post-ID: 233
WordPress-Post-Type: post

Nature is really cool sometimes.

[![Clouds over Fort Ann, New York][]][]

Also, I have the best friends ever. Your generosity (and teamwork) has
left me speechless. Thank you.

  [Clouds over Fort Ann, New York]: http://gallery.bwerp.net/albums/summer2k4/cloud_stitch.sized.jpg
  [![Clouds over Fort Ann, New York][]]: http://gallery.bwerp.net/summer2k4/cloud_stitch?full=1
