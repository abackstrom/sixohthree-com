Title: Free Words
Slug: 236/free
Summary: The blog is dead, long live the blog.
Date: 2004-08-14 17:21
Category: Meta
WordPress-Post-ID: 236
WordPress-Post-Type: post

Today I switch from [Movable Type][] to [WordPress][]. Please forgive
any mess you encounter along the way. Some old posts will be
ill-formatted, and the archive pages are not quite ready, but most of
the important URLs still work (including the feeds).



Feel free to tell me about any problems you encounter, either through
[e-mail][] or comments to this post.



  [Movable Type]: http://www.movabletype.org/
  [WordPress]: http://wordpress.org/
  [e-mail]: mailto:annika@sixohthree.com
