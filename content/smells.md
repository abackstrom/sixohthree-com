Title: Smells
Slug: smells
Summary: Apropos of nothing, a list of smells I enjoy.
Date: 2014-09-26 22:56
Category: Personal

Apropos of nothing, a list of smells I enjoy:

* sawdust
* camp fires
* cut grass
* pesto

Goodnight.
