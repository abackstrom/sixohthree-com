Title: Palm Pre Countdown in your Mac OS X Dashboard
Slug: 844/palm-pre-countdown-in-your-mac-os-x-dashboard
Summary: Hurry up and wait.
Date: 2009-05-22 13:55
Tags: Technology, dashboard, Mac OS X, Palm, palmpre
WordPress-Post-ID: 844
WordPress-Post-Type: post

<figure class="right">
    <a href="/media/2009/05/countdown.jpg">
        <img src="/media/2009/05/countdown-300x160.jpg" width="300" height="160">
    </a>
    <figcaption>Widget in Action</figcaption>
</figure>

1. Open [this page][] in Safari.
2. Right click some whitespace on the page.
3. Select “Open in Dashboard…”
4. Click the ad. The targetting square will expand to fit the ad,
    leaving the rest of the page dimmed.
5. Click the purple “Add” button at the top right of the page.
6. Wait for June 6.


**Note:** I'm not making any money from the above ad, I just pulled the
generic flash object URL out of an ad on another site.

<ins datetime="2009-05-23T11:20-04:00">**Update:** Having people pull a
web clip from the front page of my site was less than ideal, so I've
moved it off to a more permanent location. New version will stay
put.</ins>

  [this page]: http://static.bwerp.net/~adam/20090522/palmpre.html
