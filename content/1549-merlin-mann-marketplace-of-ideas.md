Title: Merlin Mann on Marketplace of Ideas
Slug: 1549/merlin-mann-marketplace-of-ideas
Summary: Words on caring.
Date: 2011-07-27 10:55
Tags: Quotes, podcast, productivity
WordPress-Post-ID: 1549
WordPress-Post-Type: post


> If you don't care a lot about the thing you're trying to fix, there is
> no solution for that. When I say "first, care," I mean, that is as
> they say in programming, "step zero." There is no step one, there is
> no step two, there is no step anything if you don't care, because
> otherwise you're just gonna wander around lamely…


[Merlin Mann][], [Marketplace of Ideas, 26 July 2011][].

  [Merlin Mann]: http://www.merlinmann.com/
  [Marketplace of Ideas, 26 July 2011]: http://colinmarshall.libsyn.com/we-have-ham-radios-merlin-mann-on-media-fear-and-caring-about-what-you-make
