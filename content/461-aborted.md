Title: IE "Operation Aborted" and jQuery
Slug: 461/aborted
Summary: I should know better.
Date: 2008-07-09 07:58
Tags: Web, ie, ie7, internet explorer, internet explorer 7, JavaScript, jgrowl, jquery
WordPress-Post-ID: 461
WordPress-Post-Type: post

[![Operation Aborted dialog][]][]I ran into a problem with Internet
Explorer 7's "[Operation Aborted][]" dialog yesterday. When displaying a
message via [jGrowl][], IE7 would display the contents leading up to my
`<script>` tag, then die with that error and redisplay a "friendly"
error page claiming, "Internet Explorer cannot display the webpage."

The previously mentioned IEBlog article immediately shined light on my
mistake: `$.jGrowl()` was attempting to modify `<body>` before the tag
had been closed. The solution: place my `$.jGrowl()` calls in
`$(document).ready()`.

  [Operation Aborted dialog]: /media/2008/07/09/aborted.sm.jpg
  [![Operation Aborted dialog][]]: /media/2008/07/09/aborted.jpg
  [Operation Aborted]: http://blogs.msdn.com/ie/archive/2008/04/23/what-happened-to-operation-aborted.aspx
  [jGrowl]: http://stanlemon.net/projects/jgrowl.html
