Title: The More Things Change...
Slug: 338/pacify
Summary: The more they stay exactly the same.
Date: 2005-09-13 10:26
Tags: Politics
WordPress-Post-ID: 338
WordPress-Post-Type: post

From Wikipedia, a conversation between Nazi leader [Hermann Göring][],
and [Gustave Gilbert][], an intelligence officer and psychologist:


> <span style="font-weight: bold">Gilbert</span>: There is one
> difference. In a democracy the people have some say in the [decision
> to go to war] through their elected representatives, and in the United
> States only Congress can declare wars.
>
> </p>
>
> <p>
> <span style="font-weight: bold">Göring</span>: Voice or no voice, the
> people can always be brought to the bidding of the leaders. That is
> easy. All you have to do is tell them they are being attacked, and
> denounce the pacifists for lack of patriotism and for exposing the
> country to danger. It works the same in any country.



That's April 18, 1946. Sounds familiar, no?

  [Hermann Göring]: http://en.wikipedia.org/wiki/Hermann_G%C3%B6ring
  [Gustave Gilbert]: http://en.wikipedia.org/wiki/Gustave_Gilbert
