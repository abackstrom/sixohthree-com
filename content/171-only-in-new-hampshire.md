Title: Only in New Hampshire
Slug: 171/only-in-new-hampshire
Summary: Welcome to the <a href="http://www.nh.gov/dmv/">New Hampshire Division of Motor Vehicles</a>. If you would like more information, you should probably go somewhere else.
Date: 2003-07-30 23:05
Tags: Personal
WordPress-Post-ID: 171
WordPress-Post-Type: post

Welcome to the [New Hampshire Division of Motor Vehicles][]. If you
would like more information, you should probably go somewhere else.

Anyone else having flashbacks of [1997][]?

  [New Hampshire Division of Motor Vehicles]: http://www.nh.gov/dmv/
  [1997]: http://web.archive.org/web/19970429064212/http://www.rit.edu/
