Title: Number 8!
Slug: 399/mpaa
Summary: Call the cheerleading squad.
Date: 2007-04-02 09:03
Tags: Personal
WordPress-Post-ID: 399
WordPress-Post-Type: post

I was [worried][], but you did it, RIT: you made the MPAA's hitlist for
[schools with the most copyright violations][]. Quite the showing for
the school that didn't make the RIAA's list.

  [worried]: /archives/2007/02/22/rit-falling-behind
  [schools with the most copyright violations]: http://arstechnica.com/news.ars/post/20070402-mpaa-names-its-top-25-movie-piracy-schools.html
