Title: There's Always a New Tweet
Slug: 1787/theres-always-a-new-tweet
Summary: I don't spend much time tweaking my UI, but I do attempt to minimize distractions. New mail notifications, badges showing unread messages, growls of all shapes and sizes, all these have a very poor signal to noise ratio, and rarely do they impact my current priority.
Date: 2012-01-02 01:06
Tags: Work, Distraction, productivity
WordPress-Post-ID: 1787
WordPress-Post-Type: post

I don't spend much time tweaking my UI, but I do attempt to minimize
distractions. New mail notifications, badges showing unread messages,
growls of all shapes and sizes, all these have a very poor signal to
noise ratio, and rarely do they impact my current priority.

Do you have a Twitter icon like this in your menu bar?

![Twitter menu bar notification][]

Using the default settings, the birdy turns blue when there's a new
tweet in your timeline. The problem is, there's always a new tweet.
Asking your Twitter client for timeline notifications is like letting
Mail.app check for new messages every 5 minutes, or welcoming people to
stick their head inside your door at all hours.

Diverting your attention means losing your momentum. An hour with 1 ten
minute interruption is more productive than an hour with 10 one minute
interruptions, so if you have to lose focus, make sure it's happening
for the right reasons.

  [Twitter menu bar notification]: https://sixohthree.com/files/2011/11/twitter.png
