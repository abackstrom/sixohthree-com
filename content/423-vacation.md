Title: Vacation, Interrupted
Slug: 423/vacation
Summary: I hate airlines.
Date: 2007-08-17 14:32
Tags: Personal
WordPress-Post-ID: 423
WordPress-Post-Type: post

Yesterday's trip to Albuquerque turned into a 13 hour layover in
Cleveland followed by a return to Manchester, NH. Had I followed through
with the possible flights today, I would have left Cleveland almost 24
hours after arriving, pushing my arrival back by exactly 24 hours. I
decided not to risk it, not wanting to spend yet another full day in an
airport. (Incidentally, both flights I was scheduled for this morning
have arrived on-time.)

I saw Manchester once more than I really wanted to yesterday, but at
least their WiFi didn't cost \$2/hour.
