Title: Firefox 1.0 Released
Slug: 271/firefox
Summary: Spread the word.
Date: 2004-11-10 23:03
Tags: Internet, Web
WordPress-Post-ID: 271
WordPress-Post-Type: post

[Firefox 1.0][] has been released. Join the 20th century and leave
Internet Explorer (and all its insecurity) behind.

[![Get Firefox!][]][]

  [Firefox 1.0]: http://www.mozilla.org/products/firefox/
  [Get Firefox!]: http://www.spreadfirefox.com/community/images/affiliates/Buttons/180x60/take.gif
  [![Get Firefox!][]]: http://www.spreadfirefox.com/?q=affiliates&id=0&t=56
