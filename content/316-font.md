Title: Font Resource
Slug: 316/font
Date: 2005-06-10 11:20
Tags: Computers, Rants
WordPress-Post-ID: 316
WordPress-Post-Type: post

Over at [FileFormat.info][] there is a pretty cool [resource][] for
finding Unicode characters. For example: the [inverted Ohm sign][].
They've got character codes, a Java reference, raster and vector files
of the character, and more. That's a lot of data for just one character.



  [FileFormat.info]: http://www.fileformat.info/
  [resource]: http://www.fileformat.info/info/unicode/char/search.htm
  [inverted Ohm sign]: http://www.fileformat.info/info/unicode/char/2127/index.htm
