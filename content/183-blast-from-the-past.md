Title: Blast From the Past
Slug: 183/blast-from-the-past
Summary: Ah, memories.
Date: 2003-10-23 09:30
Tags: Personal
WordPress-Post-ID: 183
WordPress-Post-Type: post

Anybody remember [this site][], circa January, 2002? Yet another
incarnation of [Bwerp][], culled from an aging MySQL dump and posted
here for posterity. Memmmories.

  [this site]: http://static.bwerp.net/~adam/php-nuke/html/
  [Bwerp]: http://www.bwerp.net/
