Title: Winding Down
Slug: 187/holiday
Summary: Winter quarter is drawing to a close. It's already ancient history for most of the students here...
Date: 2003-11-19 23:02
Tags: Personal
WordPress-Post-ID: 187
WordPress-Post-Type: post

Winter quarter is drawing to a close. It's already ancient history for
most of the students here, but I'm on the tail end of my co-op with ITS.
I've gotten the OK to blog about what I've been doing. (It's not exactly
sensitive stuff, but figured I shouldn't take any chances.) Look for
that in the next couple days, if you're interested in [AppleScript
Studio][] or Mac OS X administration.



Saturday brings New Hampshire, and [all][] that [comes][] [with it][].
Thursday, Turkey Day and family. Saturday, the long drive home.
Hopefully, some R&R in between.



Enjoy the holidays.

  [AppleScript Studio]: http://www.apple.com/applescript/studio/
  [all]: http://www.callblog.net/
  [comes]: http://blogs.bwerp.net/~abby/
  [with it]: http://blogs.bwerp.net/~isaac/
