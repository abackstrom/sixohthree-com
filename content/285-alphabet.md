Title: Internet Alphabet
Slug: 285/alphabet
Summary: A-Z, but !V and !Y.
Date: 2004-12-29 22:21
Tags: Personal
WordPress-Post-ID: 285
WordPress-Post-Type: post

Hey, I was curious:



1.  [http://adbusters.org/metas/corpo/blackspotsneaker/][]
2.  [http://www.bwerp.net/links/][]
3.  [http://www.capitalone.com/indexa.php][]
4.  [http://dev.jenniferkunz.com/][]
5.  [http://www.ebay.com/][]
6.  [http://forum.skype.com/viewforum.php?f=19][]
7.  [https://gmail.google.com/gmail][]
8.  [http://hotmail.com/][]
9.  [http://www.imdb.com/find?q=resident%20evil][]
10. [http://joannsjottings.blogspot.com/][]
11. [http://kernel.com/][]
12. [http://www.lighthouseyoga.com/][]
13. [http://mamamusings.net/][]
14. [https://www.netteller.com/pemibank/default.cfm][]
15. [http://www.oldradio.com/archives/warstories/640.htm][]
16. [https://panel.dreamhost.com/][]
17. [http://www.qwantz.com/][]
18. [http://riaaradar.com/][]
19. [http://www.scarygoround.com/][]
20. [http://www.thewotch.com/][]
21. [http://www.unclefred.com/][]
22. [http://www.videohelp.com/][]
23. [http://wiki.bwerp.net/][]
24. [http://www.zdnet.com/][]



Others: [Sheri][]'s and [Liz][]'s. Curiously absent from my list: both
"X" and "Y."



Number of sentences in this post: 5. Percentage of these sentences that
contain a colon: 100.



  [http://adbusters.org/metas/corpo/blackspotsneaker/]: http://adbusters.org/metas/corpo/blackspotsneaker/
  [http://www.bwerp.net/links/]: http://www.bwerp.net/links/
  [http://www.capitalone.com/indexa.php]: http://www.capitalone.com/indexa.php
  [http://dev.jenniferkunz.com/]: http://dev.jenniferkunz.com/
  [http://www.ebay.com/]: http://www.ebay.com/
  [http://forum.skype.com/viewforum.php?f=19]: http://forum.skype.com/viewforum.php?f=19
  [https://gmail.google.com/gmail]: https://gmail.google.com/gmail
  [http://hotmail.com/]: http://hotmail.com/
  [http://www.imdb.com/find?q=resident%20evil]: http://www.imdb.com/find?q=resident%20evil
  [http://joannsjottings.blogspot.com/]: http://joannsjottings.blogspot.com/
  [http://kernel.com/]: http://kernel.com/
  [http://www.lighthouseyoga.com/]: http://www.lighthouseyoga.com/
  [http://mamamusings.net/]: http://mamamusings.net/
  [https://www.netteller.com/pemibank/default.cfm]: https://www.netteller.com/pemibank/default.cfm
  [http://www.oldradio.com/archives/warstories/640.htm]: http://www.oldradio.com/archives/warstories/640.htm
  [https://panel.dreamhost.com/]: https://panel.dreamhost.com/
  [http://www.qwantz.com/]: http://www.qwantz.com/
  [http://riaaradar.com/]: http://riaaradar.com/
  [http://www.scarygoround.com/]: http://www.scarygoround.com/
  [http://www.thewotch.com/]: http://www.thewotch.com/
  [http://www.unclefred.com/]: http://www.unclefred.com/
  [http://www.videohelp.com/]: http://www.videohelp.com/dvdwriters.php?DVDnameid=40&Search=Search&list=2
  [http://wiki.bwerp.net/]: http://wiki.bwerp.net/
  [http://www.zdnet.com/]: http://www.zdnet.com/
  [Sheri]: http://www.frognamedminerva.com/wordpress/index.php?p=21
  [Liz]: http://mamamusings.net/archives/2004/12/22/my_online_life_in_26_25_links.php
