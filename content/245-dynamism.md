Title: Dynamic Pages with JavaScript and the DOM
Slug: 245/dynamism
Summary: Potentially confusing and/or helpful.
Date: 2004-09-04 00:36
Tags: Computers, Web
WordPress-Post-ID: 245
WordPress-Post-Type: post

I've been waist-deep in my independent study, and as usual I've been
using my current project to explore bits of technology I haven't had an
excuse to use before. One such example: JavaScript and the DOM.
Coincidentally, [Matt][] asked me a web design question similar to
something I built a couple days ago, so I whipped up [an example page][]
and figured I'd blog it as well.



The page is an example of a dynamic form that changes depending on the
user's input so far. If a user indicates they want to enter a widget
unknown to the system, they are provided with a text box for this new
widget name. Pretty straightforward, as is the implementation. The
driving force behind my page? The DOM method `getElementById()`.



The Mozilla.org website has [good DOM reference][], and it's been pretty
compatible from what I've seen. I'm still new to the scene, though, and
not up on all the compatibility issues. More to come later, possibly, as
I poke around this topic further.



  [Matt]: http://blogs.bwerp.net/matt/
  [an example page]: http://static.bwerp.net/~adam/2004/09/02/new-field
  [good DOM reference]: http://www.mozilla.org/docs/dom/domref/dom_shortTOC.html
