Title: Variable Variables and Arrays
Slug: 664/variable-variables-and-arrays
Summary: Memorize the order of operations, I guess.
Date: 2008-12-23 14:04
Tags: Web, PHP, Programming
WordPress-Post-ID: 664
WordPress-Post-Type: post

I'm a big fan of PHP's [variable variables][] functionality. It's great
for abstracting code with this sort of syntax:


    $property_name = 'color';
    echo $theClass->$property_name; // same as $theClass->color


The [documentation][variable variables] does mention a potential
tripping point with array index ambiguity. You might expect the
following references to be equal:


    $prop = 'internalArray';
    $theClass = new stdClass;
    $theClass->internalArray = array('red', 'white', 'blue');
    echo $theClass->internalArray[2] == $theClass->$prop[2] ? "equal" : "not equal";


However, the above code will output "not equal" because
`$prop[2]` is evaluated first. This returns a character index
within `$prop`, so the final check is against
`$theClass->t`, which is not set. Curly braces around the
property can be used to clarify your meaning:


    echo $theClass->internalArray[3] == $theClass->{$prop}[2] ? "equal" : "not equal";



  [variable variables]: http://www.php.net/manual/en/language.variables.variable.php
