Title: Psychedelic TV, Courtesy of Netflix
Slug: 437/netflix
Summary: Broken things ahead.
Date: 2007-12-04 08:58
Tags: Computers, heroes, netflix, tv, watchinstantly
WordPress-Post-ID: 437
WordPress-Post-Type: post

I tried out Netflix's "Watch Instantly" feature a few nights ago.
Unfortunately, "[One Giant Leap][]" was a no-go: the colors were all
off, full of greens and yellows to a degree that you couldn't actually
see what was going on.

That's with Windows XP SP2, a Radeon 9800 PRO, and the ATI 7.10 drivers.
Guess I'll try again another day.

<ins datetime="2008-02-25T14:05:00EST"><span class="update">Update:</span>
as of last night, "Watch Instantly" is working for me. I didn't try this
episode specifically, but I was able to watch a video. The quality is
very, very good, identical to DVD on my small television as far as I'm
concerned.</ins>

  [One Giant Leap]: http://www.netflix.com/WatchNowMovie/Heroes_Season_1/70057024
