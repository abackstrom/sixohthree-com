Title: Summer of Three Day Vacations
Slug: 166/summer-of-three-day-vacations
Date: 2003-07-02 22:31
Tags: Personal
WordPress-Post-ID: 166
WordPress-Post-Type: post

Tomorrow, I leave work early to drive to New Hampshire. Friday, I will
leave New Hampshire for Massachusetts. Saturday, I will leave
Massachusetts for New Hampshire. Sunday, I will leave New Hampshire for
Rochester.

Estimated driving time: 22 hours.

Have a good weekend. Enjoy the holiday.
