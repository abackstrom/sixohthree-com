Title: Where Are My Pills?
Slug: 1740/where-are-my-pills
Summary: Wherein the machinations of the health care industry adversely affect my ability to function.
Date: 2011-11-07 07:34
Tags: Personal, allergies, medicine
WordPress-Post-ID: 1740
WordPress-Post-Type: post

[![][img]][img-big]

I'm not a pharmaceuticals manufacturer. I've never had to answer to a
board of directors, or the FDA, or stockholders.

I'm not a politician. I haven't had to balance social issues and the
constitution while creating laws and answering to my constituents and
campaign contributors.

I'm not a doctor. I don't even play one on TV.

But I do know that fexofenadine pseudoephedrine ("Allegra-D") has
improved my quality of life significantly for the past year, and my
prescription can no longer be filled because the drug is now available
over the counter, and this change increases the drug's monthly cost from
\$5 (under my health care plan) to \$78 (out of pocket) at the local
[Hannaford][]. Less than 24 hours after taking my last pill, my nose is
running, I'm congested, and I've started sneezing. I'm out of bed
instead of resting with my family before dawn breaks and I head to work.

Not a good feeling.

  [img]: https://sixohthree.com/files/2011/11/pillbox-300x233.jpg "pillbox"
  [img-big]: https://sixohthree.com/files/2011/11/pillbox.jpg
  [Hannaford]: http://www.hannaford.com/
