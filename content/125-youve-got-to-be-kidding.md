Title: You've Got to be Kidding..
Slug: 125/youve-got-to-be-kidding
Summary: I doubt the people behind this are for real, but I sent them an e-mail anyway.
Date: 2002-12-28 22:13
Tags: Movies
WordPress-Post-ID: 125
WordPress-Post-Type: post

I doubt the people behind this are for real, but I sent them an e-mail
anyway.

> From: Annika Backstrom &lt;annika@sixohthree.com&gt;  
> To: twotowersprotest@twotowersprotest.org  
> Subject: Two Towers Protest  
> Date: Sat, December 28, 2002 22:08
>
> To whom it may concern,
>
> I will make this e-mail brief, because I assume you know as well as I do
> how mind-boggling asinine a protest of "The Two Towers" really is. I also
> assume that your website, [twotowersprotest.org][], is a ruse to confuse
> the stupid, and to annoy Tolkien fans.
>
>
> That said, J.R.R. Tolkien received the copyright for his book, The Two
> Towers, in 1965, five years before the first of the World Trade Center
> towers was completed. Peter Jackson et al had begun work on the Lord of the
> Rings trilogy as early as 1997, by some accounts. Any capitalization on the
> events of September 11, 2001, by either Tolkien or Peter Jackson, are
> obviously very impressive acts of clairvoyance.
>
> Best of luck in your petition, though.
>
> --
>
> Annika Backstrom  
> annika@sixohthree.com

There's a line between being sensitive to the events on Sept. 11, and
making a fool of yourself.

  [twotowersprotest.org]: http://www.twotowersprotest.org/
