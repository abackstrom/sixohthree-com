Title: Sunday Miscellaneous
Slug: 214/misc
Summary: Miscellaneous stuff on a gray Sunday.
Date: 2004-05-09 14:57
Tags: Computers
WordPress-Post-ID: 214
WordPress-Post-Type: post

Google: ["uri vs. url" OR "url vs. uri"][]. Also, [number range
searches][].



[Intel Gigabit NICs][] cost [a mere \$50][], even cheaper through some
retailers. I know many of Apple's machines have been Gigabit-capable for
years, but I had not realized standalone cards had such a low price tag.
I'm already maxing out the 100Base-TX between my destop and server, so I
smell Gigabit in my future.



  ["uri vs. url" OR "url vs. uri"]: http://www.google.com/search?q=%22uri+vs.+url%22+OR+%22url+vs.+uri%22
  [number range searches]: http://www.google.com/help/refinesearch.html#numrange
  [Intel Gigabit NICs]: http://www.intel.com/network/connectivity/products/pro1000mt_desktop_adapter.htm
  [a mere \$50]: http://www.cdw.com/shop/search/Results.aspx?grp=1gb&mfrn=5737884&FilteredSortOrder=PRICEASC
