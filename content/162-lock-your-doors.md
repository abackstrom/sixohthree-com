Title: Lock Your Doors
Slug: 162/lock-your-doors
Summary: "It appears that CONNECT is used for tunneling proxy servers. Apparently, these proxies can be used for spam."
Date: 2003-06-26 21:26
Tags: Internet, Spam
WordPress-Post-ID: 162
WordPress-Post-Type: post

I'm overdue for a post, so I figured this was as interesting a thing as
any. I checked out my [Apache][] logs a few minutes ago, and noticed
this interesting line:

    154.6.115.154 - - [26/Jun/2003:21:13:50 -0400] ¬
         "CONNECT 1.3.3.7:1337 HTTP/1.0" 302 272 "-" "-"

Being the geek that I am, my curiosity was piqued. It appears that
[CONNECT][] is used for tunneling proxy servers. Apparently, these
proxies can be [used for spam][].

I did a port scan, and lo and behold, both [SubSeven][] *and* [Back
Orifice][] were running. Here's the output from a no-frills portscan:

    Starting nmap V. 3.00 ( www.insecure.org/nmap/ )
    Interesting ports on mars.ritlogic.com (154.6.115.154):
    (The 1583 ports scanned but not shown below are in state: closed)
    Port       State       Service
    21/tcp     open        ftp
    25/tcp     open        smtp
    80/tcp     open        http
    135/tcp    open        loc-srv
    139/tcp    filtered    netbios-ssn
    443/tcp    open        https
    445/tcp    open        microsoft-ds
    1025/tcp   open        NFS-or-IIS
    1026/tcp   open        LSA-or-nterm
    1433/tcp   open        ms-sql-s
    1434/tcp   filtered    ms-sql-m
    3372/tcp   open        msdtc
    5800/tcp   open        vnc-http
    5900/tcp   open        vnc
    12345/tcp  filtered    NetBus
    12346/tcp  filtered    NetBus
    27374/tcp  filtered    subseven
    31337/tcp  filtered    Elite

    Nmap run completed -- 1 IP address (1 host up) scanned in 10 seconds


Futher investigation in my logs showed 68 CONNECT attempts from 23
different hosts. Here's [portscans for 14 of them][]. As expected, most
are running Back Orifice and SubSeven. One is even running
[pcAnywhere][], and [VNC][] shows up more than once.

I might play around with Apache's settings and extensions and see if I
can capture the CONNECT data. Might be interesting to see exactly what
is coming throught the pipeline. In any case, let this be a lesson to my
fellow webmasters: batton down the hatches, the 'net isn't a friendly
place.

(Oh, and don't be surprised if my blog page looks like hell in the near
future. It needs a style update to fit with the [main site][], but
mostly I need to see *a lot* less green.)

<ins>Update: Looking at my post, I realized that
some of these ports (including Back Orifice and SubSeven) are actually
filtered, not open. So, really, they might not be running those
applications at all. But it still doesn't change the fact that some of
these hosts tried to access 1.3.3.7:1337 through my box, so, eh.</ins>

  [Apache]: http://httpd.apache.org
  [CONNECT]: http://asg.web.cmu.edu/rfc/rfc2616.html#sec-9.9
  [used for spam]: https://lists.umr.edu/pipermail/security/Week-of-Mon-20030602/004593.html
  [SubSeven]: http://www.subseven.ws/
  [Back Orifice]: http://www.cultdeadcow.com/tools/bo.html
  [portscans for 14 of them]: /media/2003/06/26/lock_your_doors/portscan.txt
  [pcAnywhere]: http://www.symantec.com/pcanywhere/Consumer/index.html
  [VNC]: http://www.uk.research.att.com/vnc/
  [main site]: http://www.bwerp.net/
