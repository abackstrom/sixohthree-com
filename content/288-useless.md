Title: Useless Spam?
Slug: 288/useless
Summary: Whyfor?
Date: 2005-01-19 00:34
Tags: Spam
Category: Meta
WordPress-Post-ID: 288
WordPress-Post-Type: post

My blog has received literally hundreds of garbage comments over the
past few weeks. They're not spam in the traditional sense, in that
they're not trying to sell anything. (I suppose that's not a hard and
fast definition for spam, but still.) If anything, I've been the victim
of incessant compliments. The spammers claim to love my site and design,
and they're all very thankful that I've made this website. Well, you're
welcome, but please shut up now.



The commenters leave no name, but do list an e-mail address. There are
no URLs in the comment at all. What's the point? To deluge me with too
many comments to moderate effectively, in the hopes that true spam will
slip through the cracks? That's the only thing I've come up with.



