Title: The Business of Being Born
Slug: 446/business
Summary: "Business" being the operative word.
Date: 2008-03-02 21:49
Tags: Kid, baby, birthing, midwife
WordPress-Post-ID: 446
WordPress-Post-Type: post

Charlotte and I just finished watching [The Business of Being Born][] on
[Netflix][]. I would highly recommend it to anyone having a baby in this
country. The high rate of cesarean sections in the US compared to the
rest of the western world (despite the dangers), the sharp decline of
midwifery (and the reasons behind it), the mortality rate statistics...
it's an eye-opener.

  [The Business of Being Born]: http://www.thebusinessofbeingborn.com/
  [Netflix]: http://www.netflix.com/WatchNowMovie/The_Business_of_Being_Born/70075502
