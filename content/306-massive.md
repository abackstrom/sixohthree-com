Title: Massive Experiences
Slug: 306/massive
Summary: Geeky post, beware.
Date: 2005-05-07 00:12
Tags: final fantasy, guild wars, world of warcraft
Category: Games
WordPress-Post-ID: 306
WordPress-Post-Type: post

[![Aestus (FFXI)][aestus-ffxi]][aestus-ffxi-big]

* **Aestus**
* Level: 20
* Class: White Mage
* Server: Carbuncle

In the interest of being a more responsible gamer, I've been investing
more time and money in the craft. Video games have always been a small
part of my life; I owned an NES, then a SNES, and always had [a][]
[few][] [games][] on my old Macintosh systems. Post-college, I have more
free time, more numerous and more powerful gaming systems, and more
disposable income. I have a lot of catching up to do.

[![Ceto (FFXI)][ceto-ffxi]][ceto-ffxi-big]

* **Ceto**
* Level: 4
* Class: Red Mage
* Server: Carbuncle

One genre I have missed out on these past few years is the Massively
Multiplayer Online Role Playing Game, or "[MMORPG][]". Since February, I
have been testing the waters. I have invested significant time in [Final
Fantasy XI][] (FFXI), [World of Warcraft][] (WoW), and, as of the April
28 release date, [Guild Wars][] (GW). Each game differs slightly in the
implementation, but all are based around role-playing in a fantasy-style
environment.

Of all these games, <span class="title">Final Fantasy XI</span> has
required the largest investment of time and been the least rewarding
experience. Don't mistake me: of all the hours I have spent playing this
game, I would only call a small fraction unpleasant. But, compared to
the other games, much more work is required to accomplish even the most
basic task. From the time spent running about at low levels, to the
patience required to farm effectively, to the hours and hours spent
partying for experience, as a whole it feels like a job, not a game.

Its most redeeming quality has been the social factor. Such a thing is
highly variable and a case of being in the right place at the right
time, but I've found FFXI players to be extremely helpful and friendly.
(Shout out to Phalanx and the crew, long live [The Faith][]!)

[![Ceto (WoW)][ceto-wow]][ceto-wow-big]

* **Ceto**
* Level: 20
* Class: Rogue
* Server: Uther

<span class="title">World of Warcraft</span> is a purchase I have never
regretted. The story is more engaging, the leveling less tedious, the
whole experience more fun. From questing to crafting, the game is more
discoverable and thereby less frustrating. Where a FFXI player might beg
an <acronym title="Non-player character">NPC</acronym> for hints on
crafting recipes (or, worse, resort to a guide or the web), WoW players
have a good number of these laid out before them from the word "go". The
result? More time spent playing, less time spent researching
out-of-character, and no more crafting for 15 minutes for a one-point
increase in skill.

[![Aestus (WoW)][aestus-wow]][aestus-wow-big]

* **Aestus**
* Level: 16
* Class: Druid
* Server: Burning Legion

The relaxed character creation rules in WoW are a nice feature. I am
allowed to choose which server my character is created on, and I am far
less limited in the number of characters I create. (FFXI players may
have up to 16 characters, may not choose their server without a World
Pass from a user on that server, and must pay $1.00/mo for the new
character.) Even the user interface is less painful. FFXI has the
distinct disadvantage of being cross-platform. PC users must jump
through hoops since all elements of the game must be accessible by a
PlayStation 2 game pad. The button-pushing simplicity of WoW beats the
macro- and list-based ability access of FFXI any day of the week.

[![Ceto (Guild Wars)][ceto-guild-wars]][ceto-guild-wars-big]

* **Ceto**
* Level: 8
* Profession: Ranger

Last but not least, we have <span class="title">Guild Wars</span>. What
can I say, really? It's been out in the wild for a week, and I am still
learning how the world works. It has put a twist on the MMORPG style,
expanding on the instance feature of WoW to an extreme. Initial
prognosis is good. I hope it stays that way.

I'm by no means an expert in any of these games. My highest level
characters have not yet passed level 20, meaning I have only scratched
the surface of each world. Consider this, though: I have spent 342 hours
as Aestus (FFXI) to reach the point I'm at today. Ceto (WoW), on the
other hand, is the result of 58 hours work. Pound for pound, I have
accomplished more as Ceto. Her skills are more well-developed, and she
is closer to exiting the leveling treadmill we find in so many MMORPGs.

Whatever floats you boat, though.

  [aestus-ffxi]: /media/2005/05/06/ffxi_aestus_scaled.jpg
  [aestus-ffxi-big]: /media/2005/05/06/ffxi_aestus_big.jpg
    "Aestus (Final Fantasy XI)"
  [a]: http://www.lgm.com/bolo/
  [few]: http://www.ambrosiasw.com/games/evn/
  [games]: http://www.bungie.net/Games/Marathon/
  [ceto-ffxi]: /media/2005/05/06/ffxi_ceto_scaled.jpg
  [ceto-ffxi-big]: /media/2005/05/06/ffxi_ceto_big.jpg
    "Ceto (Final Fantasy XI)"
  [MMORPG]: http://en.wikipedia.org/wiki/MMORPG
  [Final Fantasy XI]: http://www.playonline.com/ff11us/
  [World of Warcraft]: http://www.worldofwarcraft.com/
  [Guild Wars]: http://www.guildwars.com/
  [The Faith]: http://www.geocities.com/fkerfku/The_Faith.html
  [ceto-wow]: /media/2005/05/06/wow_ceto_scaled.jpg
  [ceto-wow-big]: /media/2005/05/06/wow_ceto_big.jpg
    "Ceto (World of Warcraft)"
  [aestus-wow]: /media/2005/05/06/wow_aestus_scaled.jpg
  [aestus-wow-big]: /media/2005/05/06/wow_aestus_big.jpg
    "Aestus (World of Warcraft)"
  [ceto-guild-wars]: /media/2005/05/06/gw_ceto_scaled.jpg
  [ceto-guild-wars-big]: /media/2005/05/06/gw_ceto_big.jpg
    "Ceto (Guild Wars)"
