Title: Baby Food
Slug: 642/baby-food
Summary: Just eat it.
Date: 2008-11-26 19:35
Tags: Kid, bacon, gadgets
WordPress-Post-ID: 642
WordPress-Post-Type: post

Marshall's diet has become more interesting in recent weeks. He's always
been very interested in what we eat and drink, and we've often shared
our glasses of water with him, sometimes letting him sip though he's
usually content to hold the glass in his hands and gum on the rim. While
there's definitely a few schools of thought on solids, we did start
feeding him some "real" food about three weeks ago. His first food was
avocado, mashed into a paste, which was a hit. Next came banana, yams,
apple sauce, pear, and, most recently (and most favorite), green peas.
Along the way he's also had quinoa, usually mixed with other foods.

Tofu is this evening's exciting dish, but Charlotte did some research
and we'll be waiting to introduce that again. Sorry, guy!

So far we've mostly stuck with your basic fruit and vegetable aisle
fare, organic when possible, prepared for baby here at home. We've also
added a [food mill][] to our arsenal of tools. Charlotte is very
thorough in researching the safety of each food for an infant, which
should be no surprise to anyone who knows her well. The hardest part is
yet to come, when Marshall offers me a spoonful of spinach with all the
innocence in the world.

It's added something new to Marshall's day, which is great. We love to
engage him and let him surprise us, and food provides plenty of
opportunity for that. It will be a long time before he and I share a
BLT, but seeds have been sewn.

  [food mill]: http://www.kidco.com/main.taf?erube_fh=kidco&kidco.submit.feedingproducts=1&kidco.step=1&kidco.bc=fd
