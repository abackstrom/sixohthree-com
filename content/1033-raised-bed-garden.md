Title: Raised Garden Bed
Slug: 1033/raised-bed-garden
Date: 2010-04-24 21:47
Tags: carpentry, garden, home, projects
WordPress-Post-ID: 1033
WordPress-Post-Type: post
X-Photo: 1
Category: Personal

[![Photo of our raised garden bed filled with soil][img]][img-big]

Weekend project.

  [img]: /media/2010/04/garden-box-1024x680.jpg
  [img-big]: /media/2010/04/garden-box.jpg
