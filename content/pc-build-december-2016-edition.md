Title: PC Build (December 2016 Edition)
Slug: pc-build-december-2016-edition
Summary: Details on my Core i5 Z170 Radeon RX 480 build.
Date: 2017-02-17 13:28
Category: Computers
Tags: gaming, pc build
Meta-Image-Square: /media/2017/02/topdown.scaled.512.jpg

As a follow-on to [PC Build (Fall 2014 Edition)][1], here's my newest build:
a Core i5 Z170 with 8GB Radeon RX 480.

[![img][img-angled-scaled]][img-angled-cropped]

This is a departure from my last build, more mid-high range than budget to work
around the bottlenecks from the last build.

| Part | Name | Price |
| ---- | ---- | ----- |
| Case | NZXT S340 Mid Tower (White) | [$69.99][part-case] |
| Power Supply | EVGA SuperNOVA 650 Modular | [$79.99][part-psu] |
| Motherboard | Gigabyte GA-Z170X-UD3 | [$144.99][part-mobo] |
| CPU | Intel Core i5-6600K 6M Skylake Quad-Core 3.5 GHz | [$239.99][part-cpu] |
| RAM | Ballistix Sport LT 8GB DDR4 2400 | [$53.99][part-ram] |
| HDD | Intel 600p Series 256GB M.2 80mm SSD | [$84.99][part-hdd] |
| Cooler | CORSAIR Hydro Series H60 | [$59.99][part-cooler] |
| Graphics Card | SAPPHIRE Radeon RX 480 8GB | [$256.98][part-gpu] |
| Monitor | HP Pavilion 22xw 60Hz 21.5-in IPS | [$99.99][part-monitor] |
| Case fans | Corsair Air Series SP120 Quiet Edition Twin Pack | [$26.54][part-fans] |
| OS | Windows 10 Home | $119.99 |

Some of these parts are reused: I purchased the RX 480 for my last build, but
the Athlon CPU was such a bottleneck that I saw very little improvement over the
GeForce GTX 750. The monitor is also a holdover.

The Intel 600p doesn't have super great reviews compared to more traditional
SATA SSDs, but the price is comparable right now, the form factor is impressive
(look for it tucked below my GPU in the photos), and it's plenty fast for me: I
can go from cold boot to Chrome window in under 15 seconds.

There's no optical drive: the case doesn't even have 5 1/4" drive bays.

I purchased a new copy of Windows 10 and opted for the full (not OEM) version
this time. During my last build I didn't realize the extent of OEM limitations
(like the inability to change your motherboard). This pushed me to do a bigger
upgrade than I might have otherwise done.

The water cooler! I didn't even know self-contained water coolers were a thing
So awesome.

[I FORGOT TO ORDER RAM][2]. I ordered almost all the parts in one big push.
Wednesday afternoon rolls around and everything is in the mail, and I realize
there's no RAM on the way. Luckily it arrived before the weekend. (Thanks, Amazon
Prime!) 8GB is fine for now, but I'll bump that up at some point.

All in all, this was a great buy. The case is really sleek and I took care with
the cable management, and it's a huge jump in performance: Overwatch usually
hovers around 120 FPS, probably 100 FPS better than before. I'll probably round
on this build with a SATA SSD and more RAM at some point, but I'm really in no
rush.

[![img][img-cooler-scaled]][img-cooler-cropped]

[![img][img-topdown-scaled]][img-topdown-cropped]

[![img][img-gpu-scaled]][img-gpu-original]

  [1]: /pc-build-fall-2014-edition
  [2]: https://twitter.com/abackstrom/statuses/814310593276575745

  [part-mobo]: https://www.newegg.com/Product/Product.aspx?Item=N82E16813128840
  [part-case]: https://smile.amazon.com/gp/product/B00NGMIBXC/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1
  [part-cpu]: https://www.newegg.com/Product/Product.aspx?Item=N82E16819117561
  [part-cooler]: https://www.newegg.com/Product/Product.aspx?Item=N82E16835181030
  [part-fans]: https://www.newegg.com/Product/Product.aspx?Item=N82E16835181025
  [part-gpu]: https://www.newegg.com/Product/Product.aspx?Item=N82E16814202221
  [part-ram]: https://smile.amazon.com/gp/product/B0198QDLXO/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1
  [part-hdd]: https://smile.amazon.com/gp/product/B01K375Q6C/ref=oh_aui_detailpage_o03_s01?ie=UTF8&psc=1
  [part-psu]: https://www.newegg.com/Product/Product.aspx?Item=N82E16817438026
  [part-monitor]: https://smile.amazon.com/gp/product/B00TJQX9D6/ref=oh_aui_search_detailpage?ie=UTF8&psc=1

  [img-angled-scaled]: /media/2017/02/angled.scaled.jpg
  [img-cooler-scaled]: /media/2017/02/cooler.scaled.jpg
  [img-topdown-scaled]: /media/2017/02/topdown.scaled.jpg
  [img-gpu-scaled]: /media/2017/02/gpu.scaled.jpg
  [img-angled-cropped]: /media/2017/02/angled.cropped.jpg
  [img-cooler-cropped]: /media/2017/02/cooler.cropped.jpg
  [img-topdown-cropped]: /media/2017/02/topdown.cropped.jpg
  [img-gpu-original]: /media/2017/02/gpu.original.jpg
