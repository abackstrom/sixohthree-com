Title: DBMS Updates
Slug: 152/dbms-updates
Date: 2003-05-01 07:49
Tags: Computers
WordPress-Post-ID: 152
WordPress-Post-Type: post

Today's gonna be a good day.



I've already [ranted][] on the physical pain I experience working with
Visual Basic.NET/Access. Today, my prayers are being answered. PHP and
Apache are swooping down like a sort of heavenly convoy, my shining
light in the darkness that is Client/Server Database.

As much as I enjoy database manipulation with PHP, I'm not wholly
opposed to learning new technologies. I just don't feel that Microsoft
is worth my time and effort. I understand that some employers look for
experience with Microsoft IIS, Microsoft SQL Server, and so on.
Microsoft has a bad habit of ripping of other company's innovations,
extending them just enough to be incompatible, botching them in one way
or another, and selling them for a few hundred dollars. What is so
attractive about this company?



At least I'm getting a reprieve, however short. I just have to contain
myself during the lecture. "No, you can do that with print\_r()!" "Use
the \<?= ?\> syntax!" "Fetch that row as an object!"



See what I mean?



  [ranted]: http://blogs.bwerp.net/archives/2003/03/25/microsoft_is_pain
