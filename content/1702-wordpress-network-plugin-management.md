Title: WordPress Network Plugin Management
Slug: 1702/wordpress-network-plugin-management
Summary: I need to test all of these, at some point.
Date: 2011-10-24 12:25
Tags: Web, network, plugins, todo, WordPress
WordPress-Post-ID: 1702
WordPress-Post-Type: post

I need to test all of these, at some point:

-   [Exclude Plugins][] -- Exclude plugins from appearing in plugins
    menu for normal user in WordPress multisite.
-   [Restrict Multisite Plugins][] -- Allows network admins to restrict
    which plugins are available on sites, similar to themes.
-   [Plugins Enabler][] -- This plugin adds the ability to only show the
    plugins you enabled for a blog.
-   [YD Network-wide Options][] -- Automatically replicate any plugin
    setting network-wide: apply sitewide settings. Spread your settings
    or options on all your multisite blogs.


In other Multisite news, [scribu][] posted "[The Future of Multisite][]"
with links to a few of his own plugins:

-   [User Management Tools][]
-   [Proper Network Activation][]
-   [My Sites Widget][]



  [Exclude Plugins]: http://wordpress.org/extend/plugins/exclude-plugins/
  [Restrict Multisite Plugins]: http://wordpress.org/extend/plugins/restrict-multisite-plugins/
  [Plugins Enabler]: http://wordpress.org/extend/plugins/plugins-enabler/
  [YD Network-wide Options]: http://wordpress.org/extend/plugins/yd-wpmu-sitewide-options/
  [scribu]: http://scribu.net/
  [The Future of Multisite]: http://scribu.net/wordpress/the-future-of-multisite.html
  [User Management Tools]: http://wordpress.org/extend/plugins/user-management-tools/
  [Proper Network Activation]: http://wordpress.org/extend/plugins/proper-network-activation/
  [My Sites Widget]: http://wordpress.org/extend/plugins/my-sites-widget/
