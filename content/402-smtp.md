Title: Sixohthree.com qmail fixed
Slug: 402/smtp
Summary: Running a mail server is <em>almost</em> worth all the trouble.
Date: 2007-04-13 19:24
Tags: Linux
WordPress-Post-ID: 402
WordPress-Post-Type: post

Today I fixed a configuration problem with the [qmail][] SMTP server
handling sixohthree.com mail. Incoming messages from [Gmail][] have been
failing even though my port was open and accepting connections. Turns
out I was still attempting connections to the mail blacklist service at
ORDB, which [went offline in December][]. Whoops. Any incoming SMTP
connection that wouldn't wait for the timeout (ie Gmail) would assume my
mail server wasn't responding and would kick back an error to the
sender. I don't get a lot of e-mail so it took a while for the problem
to become noticeable.

Watch out for that one.

  [qmail]: http://cr.yp.to/qmail.html
  [Gmail]: http://mail.google.com/
  [went offline in December]: http://it.slashdot.org/article.pl?sid=06/12/18/154259
