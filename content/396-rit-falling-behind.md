Title: RIT Falling Behind
Slug: 396/rit-falling-behind
Summary: Flex those pipes.
Date: 2007-02-22 11:55
Tags: Personal
WordPress-Post-ID: 396
WordPress-Post-Type: post

The numbers don't lie, and they don't look good. Maybe RIT's heyday has
passed... it is unfair to judge new generations by the standards of the
old, but the disappointment is still there. RIT, you are not one of the
25 universities who received the [most copyright violation notices][]
this school year.

No, the year isn't over, but perhaps you should shift your focus to
training for next year. The hard drives will be bigger, the pipes
fatter, the stakes higher. It's time to show the world what you're made
of.

  [most copyright violation notices]: http://arstechnica.com/news.ars/post/20070222-8900.html
