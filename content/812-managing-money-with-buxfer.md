Title: Managing Money with Buxfer
Slug: 812/managing-money-with-buxfer
Summary: I think this is the app I've been waiting for.
Date: 2009-05-11 21:04
Tags: Personal, finances, money
WordPress-Post-ID: 812
WordPress-Post-Type: post

As a family in the early stages of buying a home, we've become very
budget-focused. We're examining our spending, getting a better handle on
what's essential and what can be trimmed. The foundation of all this is
accurate accounting: if you aren't tracking spending, you don't know
where you can cut back. Bills are easy to track, but it's the small
purchases that add up and cause trouble. A $3 coffee might seem
insignificant, but have one every other day for a month and that's
nearly $50 down the drain.

Years ago, [Ultrasoft CheckBook][] was my go-to application for
balancing my checking register. I always had my Palm and the
application's UI was superb, and no transaction was left by the wayside.
Recently Charlotte and I tried a simple [Google Docs][] spreadsheet; it
was functional but featureless, and we fell out of the habit.

A recent search in [Delicious][] brought me to [Buxfer][] almost
immediately. I'll admit I haven't shopped around, but Buxfer is almost
exactly what I would have created had I created my own transaction
register. A quick rundown of the killer features:

-   Transaction splitting, with an awesome UI that handles the math for
    you
-   Upcoming (and, optionally, recurring) transactions that show up in
    your dashboard, on your calendar, and can create transactions
    automatically when they're due (a godsend for automatic withdrawls)
-   Login via OpenID, Facebook, AOL, Google ID, etc. (cool)
-   Transactions via Twitter or email (I'm using this to add new debits
    via SMS)
-   API for everything (JSON and XML responses, plus PNG for graphs)
-   RSS feed of transactions and alerts
-   Free for basic use, with extra features available for $2-3 monthly


Pretty great, so far. The Twitter integration is key for my usage: I use
my phone to direct message [@bux][] and the debit is there for cleanup
next time I log in. For those with beefier phones, they have *two*
mobile versions: [iPhone][] and [Mobile][], depending on your
capabilities.

I think this is the app I've been waiting for.

Screenshots up next. Note that some of these are from the [free demo
site][].

[gallery]

  [Ultrasoft CheckBook]: http://www.ultrasoft.com/checkbook/
  [Google Docs]: http://docs.google.com/
  [Delicious]: http://delicious.com/
  [Buxfer]: http://www.buxfer.com/
  [@bux]: http://twitter.com/bux
  [iPhone]: http://i.buxfer.com/
  [Mobile]: http://m.buxfer.com/
  [free demo site]: http://demo.buxfer.com/demo.php
