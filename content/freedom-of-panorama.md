Title: Freedom of Panorama
Summary: Today I learned of Freedom of Panorama, a broad term for copyright law concerning works of art in public or private places.
Date: 2015-05-02 06:14
Tags: copyright

Today I learned of [Freedom of Panorama][1], a broad term for copyright law
concerning works of art in public or private places. From [Wikimedia][1]:

> The works to which the FOP exception applies vary widely from country to
> country. The exception generally applies only to works on permanent public
> display. In some countries, this is only in outdoor public places; in others
> it extends to indoor places where admission is charged. It may cover only
> architecture, only architecture and sculpture, or all copyrightable works
> including literary works.

The article digs into specific copyright law for many countries, including the
United States. In the US, buildings completed before 1990-12-01 are in the
public domain and may be photographed. Buildings completed after 1990-12-01 are
subject to copyright law, with a special exemption for photographs of
"permanent" "stationary" buildings: the photographer retains copyright over
these photos.

US copyright law extends to artwork, including sculpture permanently installed
in public places:

> ... public artwork installed before 1923 is considered to be public
> domain, and can be photographed freely. In addition, any public artwork
> installed before 1978 without a copyright notice is also in the public domain
> (unless the copyright owner actively prevented anyone from copying or
> photographing the work until 1978). ([Wikimedia][3])

Photographs of any other artwork are considered derivative works, giving the
copyright holder of the work certain rights over the publication of that
photograph.

## Sources

* "[Commons:Freedom of Panorama][1]", licensed under [CC-BY-SA 3.0][2]

  [1]: http://commons.wikimedia.org/wiki/Commons:Freedom_of_panorama
  [2]: http://creativecommons.org/licenses/by-sa/3.0/
  [3]: http://commons.wikimedia.org/wiki/Commons:Freedom_of_panorama#United_States
