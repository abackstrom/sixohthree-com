Title: Open Tech NYC 2013 Notes
Slug: open-tech-nyc-2013-notes
Date: 2013-04-04 11:32
Modified: 2013-04-08 09:52
Tags: conferences
Summary: The inagural Open Tech NYC conference was held in Long Island City, Queens this past Saturday, March 30. My notes are rough and incomplete compared to the talks themselves, but here they are for posterity.
Category: Technology

The inagural [Open Tech NYC](http://www.opentech2013.org/) conference was held
in Long Island City, Queens this past Saturday, March 30. There were some
thought provoking talks and a lot of projects I'll be following up on. My notes
are rough and incomplete compared to the talks themselves, but here they are
for posterity.

## Sumana Harihareswara (Wikimedia)

Sumana also blogged [a much more complete collection of links](http://www.harihareswara.net/sumana/2013/03/31/0) from her talk.

* [Archive of our Own](http://archiveofourown.org/) fanfic
    * [Organization for Transformative Works](http://transformativeworks.org/)
* [NYC Open Data](https://nycopendata.socrata.com/)
* Open data needs open formats
* [OpenITP](http://openitp.org/) -- open, anonymous, anti-censorship, anti-surveillance
* [QuestionCopyright.org](http://questioncopyright.org/)

## Joel Natividad (Ontodia)

* [Center for Urban Science and Progress](http://cusp.nyu.edu/), Brooklyn
* [Big Apps NYC 2013](http://nycbigapps.com/)
* "6 star data," a riff on Tim Berners-Lee's 5 stars
    * All of the above, plus metadata
* [SPARQL](http://en.wikipedia.org/wiki/SPARQL), [OWL](http://en.wikipedia.org/wiki/Web_Ontology_Language)
* CleanWeb Hackathon, May 18 (I can't find a link for this!)
* Q. Skills needed to work with data, without being a data scientist?
    * A. Ontodia trying to clean and link data ahead of time, allow people to work
      with data without being a data scientist.
* Q. Licensing around open data?
    * A. See [OpenDefinition.org](http://opendefinition.org/)
* Sometimes it's easy to find data for the present, but hard to find
  historical data.

## Alan Hudson (Shapeways)

I didn't take any specific notes for Alan's talk, but 3D printing looks like an
incredibly enabling technology. During afterparty discussions with other
attendees I learned of [Autodesk 123D](http://www.123dapp.com/) which can be
used to build models for 3D printing. [Google SketchUp](http://www.sketchup.com/)
is also on the [long list of supported design
applications](http://www.shapeways.com/tutorials/supported-applications).

## Jon Gottfried (Twilio)

* History of the Hackathon
* "Hackathons are the tech scene's watering hole"
* [Hackerleague](https://www.hackerleague.org/)
* [Super Happy Dev House](http://superhappydevhouse.org/), 2005
* [Hack'n Jill](http://hacknjill.com/)
* [Hack Day Manifesto](http://hackdaymanifesto.com/)
* Q. What makes hackathons fail?
    * A. Logistical issues (food, wifi, power, etc.). This is people's free time,
      make them comfortable. Be more inviting and less insular.

## Vanessa Hurst (Developers for Good)

* [Developers for Good](http://developersforgood.org/) is currently a meetup, and they are growing the community
* [Catchafire](http://www.catchafire.org/) -- pro bono for everybody
* Sometimes nonprofits aren't connected with technologists who can help them
* [Ohours](http://ohours.org/) -- open office hours platform
    * "I'll be at Starbucks from 2 to 4, stop by to talk about blogging"
* [Donors Choose](http://www.donorschoose.org/)
    * Open JSON API, explore trends around classroom needs
* [Code Montage](http://codemontage.com/) -- Improve your skills by working on real-world problems

## Tianhui "Michael" Li (Foursquare)

* 1929 French city guide. Age of the automobile, travel is easier, more people need
  guides to cities they're not familiar with
* Michael shows some awesome visualizations of checkins over time on a world map
* [Scoobi](https://github.com/NICTA/scoobi) -- "A Scala productivity framework for Hadoop"
* "How can we be the Michelin Guide of the 21st century?"
* Making suggestions relies on machine learning
* Leverage a multitude of signals
    * Personal (73% of visits are repeat visits, lists, todos)
    * Social (what your friends did)
    * Context (time of day, is this place open?)
    * Machine-learned signals (category affinity)
* Metrics
    * Web has click-through, Foursquare has walk-through
    * Are you predicting the future, or the past?
* Tile maps for visualizations/pinpoints

## Andy Parsons (Happify)

State of open tech in NYC.

* [Brubeck](https://github.com/j2labs/brubeck) -- Python asyncronous web framework
* Cornell Tech Campus
