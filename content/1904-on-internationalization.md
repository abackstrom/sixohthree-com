Title: On Internationalization
Slug: 1904/on-internationalization
Date: 2012-01-28 11:23
Tags: internationalization, i18n
Category: Pictures
WordPress-Post-ID: 1904
WordPress-Post-Type: post

[![Photo of product signage in a Walmart with missing translations][img]][img-big]

  [img]: https://sixohthree.com/files/2012/01/internationalization-1024x765.jpg "Photo of product signage in a Walmart. The sign for a Folding Aluminum Table has placeholder text rather than the expected translation, and says 'Spanish Here' repeatedly."
  [img-big]: https://sixohthree.com/files/2012/01/internationalization.jpg
