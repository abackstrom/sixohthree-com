Title: Painting With Light
Slug: 192/painting
Summary: Experiments with aperture, shutter speed, a bright blue LED in an otherwise dark room. My kingdom for a tripod.
Date: 2003-12-26 19:20
Tags: Photography
WordPress-Post-ID: 192
WordPress-Post-Type: post

![Painting with light][]



Experiments with aperture, shutter speed, a bright blue LED in an
otherwise dark room.



My kingdom for a tripod.



  [Painting with light]: /media/2003/12/26/t-painting.jpg
