Title: Is a Recount in Order?
Slug: 267/recount
Summary: Maybe we didn't lose after all.
Date: 2004-11-09 21:15
Tags: Politics
WordPress-Post-ID: 267
WordPress-Post-Type: post

Hey, maybe (just maybe) [we didn't lose after all][]. Who knows, though.
I hope this is examined more fully.

And I can't help but say I could come up with a more secure eVoting
system in five minutes using only a pen and a napkin.

  [we didn't lose after all]: http://web.archive.org/web/20050211093154/http://www.commondreams.org/headlines04/1106-30.htm
