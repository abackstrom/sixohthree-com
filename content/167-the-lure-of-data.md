Title: The Lure of Data
Slug: 167/the-lure-of-data
Date: 2003-07-06 22:16
Tags: Personal
WordPress-Post-ID: 167
WordPress-Post-Type: post

[Slashdot][] linked to an article at the New York Times entitled [The
Lure of Data: Is It Addictive?][] The basic gist is this: some people
become addicted to information, getting a rush from multitasking and
being connected. In other words: it's me, and it's why I feel compelled
to catch up on web comics while a pot of water comes to a boil in the
kitchen.



I've been an information hog for some time now. It's why I am drawn to
databases and web interfaces: I love to sift through data, to comb it
and organize it and always have it right at my fingertips. The people in
the article have a "pseudo-ADD," and have trouble focusing on projects
for long periods of time. They will browse the web and check their
e-mail during presentations, insisting that such activities boost their
productivity when the opposite is really the case.



Thankfully, I'm not as bad as the people mentioned by the New York
Times, but I confess I've read eBooks in the men's room more than once.
My personal projects don't hold my interest for long, with newer,
fresher ideas taking hold. I have a slightly better track record than
those mentioned in the article; I usually work at a project for at least
a few days to a week before moving on to something else.



It helps that I keep myself disconnected much of the time. My Palm does
not have wireless Internet, and I do not own any other sort of mobile
device. It's a bit of a forced state of unconnectedness, but perhaps I
should try to keep it that way.



Gotta go, an e-mail just came in.



  [Slashdot]: http://slashdot.org/
  [The Lure of Data: Is It Addictive?]: http://www.nytimes.com/2003/07/06/business/yourmoney/06WIRE.html?pagewanted=1&ei=5062&en=027a31a06e611f55&ex=1058068800&partner=GOOGLE
