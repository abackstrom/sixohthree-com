Title: Google on the iPhone
Slug: 2003/google-on-the-iphone
Summary: In which I explore things which were taken for granted.
Date: 2012-04-12 22:46
Tags: Computers, gmail, Google, iphone, iPhone 4S, palm pre, webOS
WordPress-Post-ID: 2003
WordPress-Post-Type: post
WordPress-Post-Format: Image

I've finally replaced my aging Palm Pre (released June 2009) with an
iPhone 4S. I wasn't looking forward to the move, but only by using the
iPhone full-time do I finally realize what I'm missing. Most frustrating
is the poor support for Google services, which have been my central
repository thus far.

-   No built-in GTalk, so far no compelling third-party options
-   Contacts not supported by built-in "Gmail" account option
-   Contacts supported using Exchange (Google Sync) account type for
    Google Apps, but email labels are not supported (Home, Work, etc.)


As a guy who doesn't use any of the built-in Mac OS X applications, I'm
not feeling a lot of the standard integration points. My stuff's in the
cloud, which is seemingly incompatible with the iCloud.
