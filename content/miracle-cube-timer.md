Title: Miracle Cube Timer
Slug: miracle-cube-timer
Summary: Here are a few ways I've used the dead-simple Miracle Cube Timer from Datexx.
Date: 2017-09-13 18:36
Category: Personal
Tags: productivity
Meta-Image-Square: /media/2017/09/miracle-cube-timer/cube.600x600.jpg

I'm really liking the "Miracle Cube Timer" by Datexx. It's dead-simple to use
(no buttons to press), and while it does have a countdown timer, it's very
small and easy to hide and won't distract.

Here are a few ways I've used this timer:

- **Quickly timebox an activity.** Sitting down to work on a meaty problem? Set
  a 30 minute timer. If the timer goes off and you're not making progress,
  figure out what needs to change.
- **Follow up on notifications.** When you get a 10 minute meeting reminder,
  set a 5 minute timer on the cube. Find a stopping point on your current task
  before those 5 minutes are up: when the timer goes off, that's your last
  chance to stretch your legs, use the bathroom, or refill your drink.
- **Use the [Pomodoro technique][1].** Quickly create timers for 30 minute work
  periods and 5 minute breaks without fiddling with apps on your phone or
  computer. (Traditional Pomodoro uses 25 minute work/5 minute rest, but I
  found the 5/15/30/60 timer less expensive and more aesthetically pleasing
  than the red 5/10/20/25 timer.)

All these things are possible with any timer, but the simple, unobtrusive
interface of the cube doesn't break my concentration. I find myself reaching
for it often, if only just as a reminder that time is passing (say, reminding
myself that a meeting is half over).

![Photo of Miracle Cube Timer](/media/2017/09/miracle-cube-timer/cube.600x600.jpg)

  [1]: https://en.wikipedia.org/wiki/Pomodoro_Technique
