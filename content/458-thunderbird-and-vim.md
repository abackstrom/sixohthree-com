Title: Thunderbird and Vim
Slug: 458/thunderbird-and-vim
Summary: Why don't I use mutt, again?
Date: 2008-06-05 08:18
Tags: Computers, email, thunderbird, Vim
WordPress-Post-ID: 458
WordPress-Post-Type: post

I just configured Thunderbird to edit messages within vim. This gets me
somewhere in the vicinity of my old [mutt][] setup, which also used vim
for editing. I'm using the following software versions under Mac OS X
10.5.2:

-   [External Editor 0.7.4][]
-   <a href="http://www.mozilla.com/thunderbird/">Thunderbird 2.0.0.14
-   [MacVim 7.1 (28)][]



External Editor's editor is set to:
`/Applications/MacVim.app/Contents/MacOS/Vim -g --nofork`. Works like a
charm.

  [mutt]: http://www.mutt.org/
  [External Editor 0.7.4]: http://globs.org/articles.php?lng=en&pg=2
  [MacVim 7.1 (28)]: http://code.google.com/p/macvim/
