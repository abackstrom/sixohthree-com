Title: Cron Output in Google Reader
Slug: 1323/cron-output-in-google-reader
Summary: I've created a Django app to log cron output, simultaneously reducing the amount of email I get and enabling me to make cron output more verbose.
Date: 2010-09-30 08:58
Tags: Linux, Programming, cron, Django
WordPress-Post-ID: 1323
WordPress-Post-Type: post

[![A screenshot of cron job output displayed in an Atom feed via Google Reader.][img]][img-big]

I've been trimming down my Inbox, filing things more aggressively and
unsubscribing from the dozens of newsletters that have accumulated over
the years. I've also created a [Django][] app to log cron output,
simultaneously reducing the amount of email I get and enabling me to
make cron output more verbose. Recent log entries are exposed via an
Atom feed, which I've plugged into Google Reader (above).

I spend a lot of time in Google Reader, but it's stuff like this that
makes it feel truly useful rather than just convenient.

  [img]: https://sixohthree.com/files/2010/09/google-reader-cronjobs-1024x670.png
    "Cron Jobs in Google Reader"
  [img-big]: https://sixohthree.com/files/2010/09/google-reader-cronjobs.png
  [Django]: http://www.djangoproject.com/
