Title: Ansible 2.0 Released
Slug: ansible-2-0-released
Summary: Exciting news: Ansible 2.0 was released earlier this month!
Date: 2016-01-23 10:55
Tags: ansible, automation, Linux

Exciting news: Ansible 2.0 was [released][1] earlier this month! The Homebrew
upgrade is painless and I've already updated some of my roles to use [task
blocks][2]. I'll also switch some playbooks to the ["free" strategy][3],
allowing faster hosts (like Linode servers) to complete playbooks without
waiting for slower hosts (my oldest Raspberry Pi).

[1]: http://www.ansible.com/blog/ansible-2.0-launch
[2]: http://docs.ansible.com/ansible/playbooks_blocks.html
[3]: http://docs.ansible.com/ansible/playbooks_strategies.html
