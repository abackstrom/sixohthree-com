Title: Those "Game" Things
Slug: 158/those-game-things
Summary: After reminiscing about old SNES RPGs in my last post, I sort of went insane for a couple hours. I would like to collect Game Boys. There, I said it.
Date: 2003-05-23 10:40
Tags: Games
WordPress-Post-ID: 158
WordPress-Post-Type: post

After reminiscing about old SNES RPGs in my [last post][], I poked
around on eBay to price out an old console and some games. Then I sort
of went insane for a couple hours.



I would like to collect Game Boys. There, I said it. Now, why would I
want to do a silly thing like that? For starters, the Game Boy is a
relic from my childhood, and who doesn't feel a little nostalgic about
their past? Furthermore, do you have any concept of just how many Game
Boys there are? It's like trying to buy every [Nine Inch Nails][] album.
Just when you buy the Swedish import with the acid remix of Broken,
Trent puts another version of the same song out in Japan. Witness:



-   **Original Game Boy**. You know it and love it.
-   **Game Boy Pocket**. Thinner and lighter than the original.
    Available in five colors.
-   **Game Boy Color**. Nintendo moves into the 20th century with a new
    color LCD. Available in six colors.
-   **Game Boy Advance**. New style, all around betterer. Available in
    several colors.
-   **Game Boy Advance SP**. Even more changes to the Game Boy's design.



At quick count, that's fifteen different styles and colors of Game Boys.
There are special Pokémon editions in *at least* two colors, and I
haven't done enough research to know the available colors for the
Advance models. That at least puts us over twenty, and those are just
the main ones. There's also: Game Boy Light (old style, with backlight);
Super Game Boy (play Game Boy games on your SNES); and other goodies
like the Game Boy Camera.



I only had a Game Boy of my own for a short time, but it was a lot of
fun. There are some great old games, like the Kirby and Mega Man titles,
as well as the always-classic Tetris. Hell, they even have Link to the
Past for Game Boy Advance.



Most of this is fairly new to me. I've been under the proverbial rock
since my subscription to Nintendo Power ran out. As much as I'd enjoy a
little deal-searching and collecting, though, I'll try to be good. After
all, there are [more important][] places for me to spend money.



  [last post]: http://blogs.bwerp.net/archives/2003/05/23/todo
  [Nine Inch Nails]: http://www.nin.com/
  [more important]: http://www.wegmans.com/
