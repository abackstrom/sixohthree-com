Title: Sharing Files with Expiration Times
Summary: For me, Cyberduck + DreamObjects
Date: 2013-12-17 11:10
Tags: s3
Status: draft

[DreamObjects] is DreamHost's S3-compatible file storage system. I've been using
it lightly but today had a breakthrough in one use case: sharing files with an
expiration date.

DreamObjects buckets can be created as "Public" or "Private." In the latter,
directory listings are denied. Take note though: files within private buckets
can still be downloaded if the 

  [DreamObjects]: http://www.dreamhost.com/cloud/dreamobjects/
  [the cloud]: https://github.com/BenWard/cloud-to-moon
