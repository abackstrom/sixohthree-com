Title: Forest: Stay Focused, Be Present
Slug: forest-stay-focused-be-present
Summary: Forest for iOS. Stay focused, grow your forest.
Date: 2016-02-01 21:38
Tags: productivity
Meta-Image-Square: /media/2016/01/forest-512x480.jpg

New to me: [Forest][] for iOS. Need to focus? Set a timer, plant a seed. Interrupt the timer (by switching apps) and your tree dies. Stay strong, and your forest gets a little bigger.

![Forest for iOS][forest img]

This is a feedback mechanism I can totally get with.

[forest img]: /media/2016/01/forest.jpg "Screenshot of Forest for iOS. An isometric grid of grass, five blocks wide and five blocks long. Four trees and a bush are randomly placed."
[Forest]: http://www.forestapp.cc/
