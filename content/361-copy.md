Title: Monkey See
Slug: 361/copy
Summary: Or maybe, great minds think alike?
Date: 2006-03-02 13:48
Tags: Personal
WordPress-Post-ID: 361
WordPress-Post-Type: post

Or maybe, great minds think alike?

[![You're Handsome (mirrored) on BustedTees{@class=border}](/media/2006/03/02/bt-backwards-t.jpg)](/media/2006/03/02/bt-backwards.jpg)
[![You're Handsome (mirrored) on Jerkass{@class=border}](/media/2006/03/02/jerkass-backwards-t.jpg)](/media/2006/03/02/jerkass-backwards.jpg)

[!["Gave my Word to Stop at Third" on BustedTees{@class=border}](/media/2006/03/02/bt-gaveword-t.jpg)](/media/2006/03/02/bt-gaveword.jpg)
[!["Gave my Word to Stop at Third" on Jerkass{@class=border}](/media/2006/03/02/jerkass-gaveword-t.jpg)](/media/2006/03/02/jerkass-gaveword.jpg)

[!["Rogues Do It From Behind" on Offline Tshirts{@class=border}](/media/2006/03/02/offline-rogues-t.jpg)](/media/2006/03/02/offline-rogues.jpg)
[!["Rogues Do It From Behind" on Penny Arcade{@class=border}](/media/2006/03/02/pa-rogues-t.jpg)](/media/2006/03/02/pa-rogues.jpg)
