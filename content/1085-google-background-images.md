Title: Google Background Images
Slug: 1085/google-background-images
Summary: Google tests background images.
Date: 2010-06-10 11:19
Tags: Google, UI
WordPress-Post-ID: 1085
WordPress-Post-Type: post
X-Photo: 1
Category: Technology

[![Google background images][img]][img-big]

  [img]: https://sixohthree.com/files/2010/06/google-background-image-1024x706.png "google-background-image"
  [img-big]: https://sixohthree.com/files/2010/06/google-background-image.png
