Title: Of Indoor Swimming Pools
Slug: 142/of-indoor-swimming-pools
Date: 2003-03-25 10:33
Tags: Personal
WordPress-Post-ID: 142
WordPress-Post-Type: post

Most people go through life not knowing what a soaking wet carpet sounds
and feels like. This morning, my life was *enriched*.



I saw the dark footprints on the carpet as soon as I woke up. I assumed
there had been a spill of some sort, and thought about calling Housing
to get it cleaned up. (The smell of moldy carpet does not rank high on
the list of things we need in this apartment.) I went to take my shower,
and I realized things were much worse than I had imagined.



I practically *swam* to the bathroom, navigating around the wet floor
rugs, and found that the toilet bowl was completely full, and every inch
of tile from the tub to the sink was covered in a fine layer of water.
It had already began seeping into the carpet, and made it all the way
into the living room. I'm not sure, but I think it was trying to escape
out the front door before I woke up.



Albert came to our rescue, as he often does. The carpet is still
waterlogged, but at least the flooding has ceased. Albert's veritable
Plunger of Justice saw to that.



I think I've had enough morning for one day.



