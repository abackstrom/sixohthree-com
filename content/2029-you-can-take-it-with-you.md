Title: You Can Take It With You
Slug: 2029/you-can-take-it-with-you
Summary: I look forward to the day when my various bits of gear learn to talk to each other.
Date: 2012-05-16 11:01
Tags: Technology, integration, mobile
WordPress-Post-ID: 2029
WordPress-Post-Type: post

I look forward to the day when my various bits of gear learn to talk to
each other. Today, I dock my MacBook Pro under my desk, plugging in
power, Ethernet, and a 23" display. The keyboard and mouse are wireless,
Ethernet could be replaced by Wifi, and I could feasibly add Bluetooth
headphones to the mix. Imagine if the phone were at the center of this
constellation, rather than a laptop: walk up to your workstation, and
your phone connects to your keyboard and mouse and starts sending
wireless video to your display. Walk away and everything goes dark,
available on your phone for small-screen viewing and ready to resume at
another workstation.
