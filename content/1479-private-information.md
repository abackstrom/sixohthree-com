Title: Private Information
Slug: 1479/private-information
Summary: Somehow, I feel like the NH Electric Co-op values their own privacy far more than they value mine.
Date: 2011-03-07 13:42
Tags: Personal, privacy
WordPress-Post-ID: 1479
WordPress-Post-Type: post

[![][img]][img-big]{@class=right}

[@NHEC_OUTAGE][], a Twitter account for the NH Eelectric Coop,
is marked Private.

Calling the NH Electric Co-op's 800 number to report an outage allows
you to reverse-lookup a customer's street address based on their phone
number.

Somehow, I feel like they value their own privacy far more than they
value mine.

  [img]: https://sixohthree.com/files/2011/03/nhec-outage-300x220.png
    "nhec-outage"
  [img-big]: https://sixohthree.com/files/2011/03/nhec-outage.png
  [@NHEC_OUTAGE]: http://twitter.com/NHEC_OUTAGE
