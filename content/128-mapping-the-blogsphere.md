Title: Mapping the Blogsphere
Slug: 128/mapping-the-blogsphere
Date: 2003-01-06 13:55
Category: Meta
WordPress-Post-ID: 128
WordPress-Post-Type: post

Here I go again, blogging about blogging.

Anyway, I was browsing the [recently updated blogs][], as I am wont to
do when I am bored, and I came across [Peter Lindberg's blog][]. His
latest post is about something new called [GeoURL][]. Add a couple
metatags to the front page of your blog (or any other site, for that
matter), fill out the form on GeoURL to ping your site, and you'll be
added to a global map of website. Pretty cool. Here are the [blogs near
my blog][].

They've only got 124 listings right now, so I encourage anybody reading
this to blog it as well.


  [recently updated blogs]: http://blo.gs/
  [Peter Lindberg's blog]: http://www.tesugen.com/
  [GeoURL]: http://www.geourl.org
  [blogs near my blog]: http://www.geourl.org/near/?p=http%3A%2F%2Fblogs.bwerp.net%2F
