Title: Saving iTunes Music Store Videos in Windows
Slug: 284/saving
Summary: Be the archive you want to see in the world.
Date: 2004-12-25 03:11
Tags: Computers, Music
WordPress-Post-ID: 284
WordPress-Post-Type: post

**Update:** Please note that iTunes'
methodology has changed with the release of iTunes 5.0. What little
information I have on the new version has been logged on a new post,
[Saving Music Videos in iTunes 5.0 for Windows][]. Please contribute,
especially if you know how to open QuickTime Cache (.qtch) files.
{: .update }


As a self-professed digital packrat, I can't stand the thought of ever
losing some piece of information or media. I download, I checksum, I
archive, I store for all eternity. I do not like to be at the mercy of
others, and I positively sweat at the prospect of having some
previously-available datum taken away from me. So when I couldn't save
music videos from the iTunes Music Store (aka iTMS), I got a little
agitated.

Saving these videos under Mac OS X is [practically trivial][]. Step to
the other side of the fence, and you're in a world of pain:


![Screenshot of Windows error dialog][error-dialog]



The cached videos themselves are easy enough to find:
`C:\Documents and Settings\<user>\Local Settings\Temp\` contains a
".tmp" file for the currently-playing video. iTunes maintains an
exclusive lock on this file, and other programs are not allowed to read
the file in any way. This means no opening, no copying, nothing. Well,
not quite nothing. (Did you forget who you're talking to here?)



I'll start from the beginning. Here is the iTunes Music Store's Music
Video section:



[![Screenshot of the iTunes Store][itunes-store-thumbnail]][itunes-store]


Further clicking brings the user to the videos themselves:


[![Screenshot of video in iTunes Store][2]][2-link]


At this point, the video is saved in the Temp directory mentioned above:


[![Screenshot of Windows temp directory][3]][3-link]


But, as I mentioned, it's seemingly impossible to copy the file:


![Screenshot of Windows error dialog][error-dialog]


"Seemingly impossible" is not the same as "extra mega impossible,"
though. Studious hackers will remember the Windows Backup Utility,
located in Start Menu → Programs → Accessories → System Tools → Backup.
(Windows XP Home users will need to [install this application][].) This
tool has a "shadow copy" backup function that backs up locked files,
allowing backups to take place while the computer is in use. A little
misappropriation, and this music video will be liberated.


[![Screenshot of Windows Backup Utility][4]][4-link]


You'll notice that you can't selelct a single file to back up. The
default file exclusion patters for the Backup Utility prevent this. To
change this setting, select "Options..." from the "Tools" menu, and
click on the "Exclude Files" tab. Scroll down to the rule about files in
the Temp directory, select it, and click "Remove." (This amount of
destruction is OK for me, since I use the utility for archiving music
videos and nothing else.)


Start the backup using the default settings:


[![Screenshot of backup settings dialog][5]][5-link]


At this point, you can start doing other things in iTunes. Backup
Utility has a death grip on the file, and it *will* be backed up. The
progress bar should be well on its way:


[![Screenshot of backup progress window][6]][6-link]


And restore the file back to your drive using the newly-created backup
file. I haven't included all the steps for the restore process. I'm sure
you can figure it out. I prefer restoring to the Desktop, myself.


After the restore, give the file a .mov suffix so it's associated with
QuickTime Player. You now have a nice, clean video with which to do as
you please:


[![Screenshot of video playing in QuickTime Player][7]][7-link]



Remember: as we learned in Spider-Man, "with great power comes great
responsibility." Apple made this hard for a reason: they don't want
people throwing around all this copyrighted media willy-nilly. I can't
control what you do with this information, but please support the
artists as well as Apple's Music Store. Lord knows I do.



Speaking of the Lord, Happy Birthday, Baby Jesus. This is my Christmas
present to all you anonymous Internet folk. Enjoy.



Updates to this post:



-   March 23, 2005: Added note about renaming \*.tmp file.
-   April 6, 2005: Added file exclusion list information. Added link to
    Microsoft's site on installing Backup Utility. Confirmed that this
    process works in iTunes 4.7.1.



  [Saving Music Videos in iTunes 5.0 for Windows]: /archives/2005/09/08/itunes
  [practically trivial]: http://www.macosxhints.com/article.php?story=20040428183055735
  [error-dialog]: /media/2004/12/25/04-error.png
  [itunes-store-thumbnail]: /media/2004/12/25/01-itms_t.gif
  [itunes-store]: /media/2004/12/25/01-itms.jpg
  [2]: /media/2004/12/25/02-video_t.gif
  [2-link]: /media/2004/12/25/02-video.jpg
  [3]: /media/2004/12/25/03-temp_t.gif
  [3-link]: /media/2004/12/25/03-temp.png
  [install this application]: http://www.microsoft.com/athome/security/update/howbackup.mspx
  [4]: /media/2004/12/25/05-backup_t.gif
  [4-link]: /media/2004/12/25/05-backup.png
  [5]: /media/2004/12/25/06-dialog_t.gif
  [5-link]: /media/2004/12/25/06-dialog.png
  [6]: /media/2004/12/25/07-progress_t.gif
  [6-link]: /media/2004/12/25/07-progress.png
  [7]: /media/2004/12/25/08-qtplayer_t.gif
  [7-link]: /media/2004/12/25/08-qtplayer.jpg
