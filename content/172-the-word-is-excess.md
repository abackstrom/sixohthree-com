Title: The Word is "Excess"
Slug: 172/the-word-is-excess
Summary: We have a new resident in the University Commons parking lot.
Date: 2003-08-06 18:45
Tags: Rants
WordPress-Post-ID: 172
WordPress-Post-Type: post

We have a new resident in the University Commons parking lot: the
obscenely large tank of an SUV, enemy of all things organic, the one,
the only, [Hummer 2][]. I am shocked an appalled.



Apparently, this beast gets [about 10 miles per gallon][]. Yes, you
heard right. It also retails for just under \$50,000. (Nearly \$60,000
after car payments for 72 months.) Let's compare this to my '95 Escort
LX, shall we? I bought my car for \$2,000, used. I've put less than
\$1,500 into it since I bought it, but let's be generous. I still say
it's a bargain at \$3,500. What's more, I get 35 to 40 miles/gallon on
the highway. It's not much to look at, but it gets me around, and damned
if it's not economical.



Quick calculations: assuming 10,000 road miles a year and \$1.55 a
gallon for gas, I estimate I spend \$442 a year on gas. The proud owner
of the H2 outside my apartment will shell out \$1,550 for the same
mileage.



Gah, just needed to vent. Thanks for listening. =P



  [Hummer 2]: http://store.310motoring.com/image/sku/632/2/2003%20h2%20Hummer%20LARGE.jpg
  [about 10 miles per gallon]: http://www.businessreport.com/pub/21_6/betterlife/2932-1.html
