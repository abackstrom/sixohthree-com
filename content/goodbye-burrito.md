Title: Goodbye, Burrito
Slug: goodbye-burrito
Summary: We say goodbye to a little friend.
Date: 2017-09-07 22:38
Category: Personal
Tags: rats, pets, loss
Meta-Image-Square: /media/2017/09/burrito/sniff.512x512.jpg
Meta-Image-FB: /media/2017/09/burrito/sniff.1200x630.jpg

Burrito, my little black and white hooded rat, died around 17:15 this
evening. He passed away in Charlotte's arms, in his favorite blanket
next to his friend Abraham, with our hands to keep him warm in his last
moments.

A rescue rat, he had chronic breathing trouble from the day we met,
likely the result of scarring from exposure to ammonia fumes during his
life before being rescued. He and Abe never really got along with our
other rats; they preferred their own quiet, away from the youngsters.

He was always the weakest of the bunch. Even Abe with his recurring
tumor was more energetic. We noticed Burrito losing weight over the last
few weeks, and he was cold to the touch the past day or two. I knew in
my heart that today was probably his last day with us.

Burrito, I'll miss your little wheeze as you rested on my lap: I always
felt this weird kinship with you, me with my asthma, you with your own
breathing troubles. I'll miss the way you closed your eyes tight and
leaned into my scritches. I'll miss the way you would peek out and come
to the door whenever you heard my voice. I'll miss the way, only in the
last days before you passed on, you finally, determinedly, climbed out
of your habitat onto my shoulders and hands.

![Breath][3]

Many thanks to [Mainley Rat Rescue][1] and the [Flower City Critters Small
Animal Rescue][2] for facilitating the Holbook rescue, finding Burrito
his foster home, and finally his forever home with us.

[![Look][5]][4] [![Sniff][7]][6] [![Snuggle][9]][8] [![Tired][11]][10]

  [1]: http://mainelyratrescue.org/
  [2]: https://www.facebook.com/flowercitycrittersrescue/

  [3]: /media/2017/09/burrito/breath.gif "Burrito taking some breaths, laying on a blanket"
  [4]: /media/2017/09/burrito/look.jpg
  [5]: /media/2017/09/burrito/look.sm.jpg "Burrito looking down from his cage"
  [6]: /media/2017/09/burrito/sniff.jpg
  [7]: /media/2017/09/burrito/sniff.sm.jpg "Burrito having a sniff"
  [8]: /media/2017/09/burrito/snuggle.jpg
  [9]: /media/2017/09/burrito/snuggle.sm.jpg "Burrito enjoying a snuggle under a scarf"
  [10]: /media/2017/09/burrito/tired.jpg
  [11]: /media/2017/09/burrito/tired.sm.jpg "Burrito, very tired, resting on a blanket"
