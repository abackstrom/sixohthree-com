Title: Custom Plates
Slug: 323/plates
Summary: I want them.
Date: 2005-08-01 12:35
Tags: Personal
WordPress-Post-ID: 323
WordPress-Post-Type: post

It's time to register my car again. I want custom plates this time
around. Current top choices:

1.  [BWERP][]
2.  [SLACK][]
3.  [MMORPG][]



All represent some facet of my personality. What to do?

Buy two more cars, obviously.

  [BWERP]: http://www.bwerp.net/
  [SLACK]: http://www.slackware.com/
  [MMORPG]: http://www.worldofwarcraft.com/
