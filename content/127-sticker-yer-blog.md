Title: Sticker Yer Blog
Slug: 127/sticker-yer-blog
Date: 2003-01-05 03:59
Category: Meta
WordPress-Post-ID: 127
WordPress-Post-Type: post

You might have notice the new black and white badges on the left side of
the main screen. They're so-called "[blogstickers][]", and taste
*wonderful*. Jen and I created the stickers you see here, but they have
plenty on the site of various quality and chromatic variation. Through
the wonders of PHP, you get a random one each time you visit my page.
Only five at the moment, but I'll add more if the mood strikes me.

If you want to create your own, they have a [sticker factory][] to help.
I used it for all of mine, but had to use [wget][] to download the
images. Try this if you have trouble:

    wget --referer="http://www.jngm.net/cgi-bin/blogstickers/stickerpage.py" \
        "http://image.url/goes/here" -O filename.gif



  [blogstickers]: http://www.blogstickers.com/
  [sticker factory]: http://www.jngm.net/arjlog/sticker.html
  [wget]: http://www.gnu.org/software/wget/wget.html
