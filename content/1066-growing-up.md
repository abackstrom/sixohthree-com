Title: Growing Up
Slug: 1066/growing-up
Summary: How can growing up feel so slow, but so fast, at the same time?
Date: 2010-05-10 12:14
Tags: Kid
WordPress-Post-ID: 1066
WordPress-Post-Type: post
Category: Personal

[![Marshall][img]][img-big]

How can growing up feel so slow, but so fast, at the same time?

Marshall has always been an amazing thing to behold. To be a first-time
father with essentially no baby experience, everything was brand new.
Like many things in life, it is moments of joy that bury any lack
thereof. You easily forget the diaper disasters, the lost sleep, the
crying fits you are powerless to end. Everyone experiences these, but to
see your child sleeping peacefully, to hold him as he rests against your
chest, to watch him smile and laugh and stand and walk and eat for the
first time, those are the moments we all live for.

I have always loved my son, but I have looked forward to this time in
his life. Babies can feel unknowable at times, but my son is no longer a
baby. He will hold my hand and lead me into his "Sun Room," pull me to
the floor and have me sit down, and hand me an instrument while he
positions himself in front of his piano. He sings songs, he repeats
words with understanding. Last night I asked him to take the clothespin
I was holding, and slide it under the door to the basement stairs. He
did so with great determination and seriousness, pushing the pin under
the door, laying prone on his belly.

These are not trivial things.

His words surprise us, and he finds new ones every day. He joins many of
them together, now. "Bye bye \<pause\> Bella. Bye bye \<pause\> Neko."
(He has recently found his "k," and can say "Neko" instead of "Nono.")
"Blue sky," "sunny day." Food is an important part of his vocabulary.
People. Emotions. "Happy baby," "hurt," "stop it" (frustration). And,
though we aren't really sure where it came from, "Happy birthday,"
iterating birthday wishes to everyone he can remember.

There is this explosion of activity, and it catches me by surprise when
I look back through time. I can clearly remember those first months in
the old apartment, tenderly holding this new person who I barely knew.
Now he is becoming his own person. The person I looked forward to
meeting for so long.

You are almost two years old, Marshall. I resolve to give you all my
love, and always put you first. May I never take you for granted.

  [img]: https://sixohthree.com/files/2010/05/CIMG0047-765x1024.jpg
    "Marshall"
  [img-big]: https://sixohthree.com/files/2010/05/CIMG0047.jpg
