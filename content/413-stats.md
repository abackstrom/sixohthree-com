Title: Statistics
Slug: 413/stats
Summary: Some figures.
Date: 2007-06-05 09:58
Tags: world of warcraft
Category: Games
WordPress-Post-ID: 413
WordPress-Post-Type: post

-   Creation date of my World of Warcraft account: 8 April 2005
-   Account expiration date: 5 June 2007
-   Cost of intial game purchase: $49.99
-   Cost of [Burning Crusade Collector's Edition][] $69.99
-   Subscription fees since 8 April 2005: $350.73
-   Total costs since game purchase: $470.71

  [Burning Crusade Collector's Edition]: /393/bc
