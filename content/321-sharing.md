Title: It's the Thought That Counts
Slug: 321/sharing
Summary: Wait, I thought sharing was bad?
Date: 2005-07-25 23:54
Tags: Computers, Freedom
WordPress-Post-ID: 321
WordPress-Post-Type: post

Via the [eReader.com][] ["Now Featuring" feed][]:


> [God's Debris][] is an engaging fictional story about an old man who
> knows the answers to all of the Big Mysteries of life. See if you can
> figure out what's wrong with the old man's view of reality. "For
> maximum enjoyment," recommends author Scott Adams, "share your legal
> copy with a smart friend and then discuss it while enjoying a tasty
> beverage."



Ironically enough, eReader books are subject to a form of digital rights
management, and each download is keyed to the buyer's credit card
number. Not a whole lot of sharing going on there, legal or otherwise.

  [eReader.com]: http://www.ereader.com/
  ["Now Featuring" feed]: http://www.ereader.com/now_featuring.rdf
  [God's Debris]: https://secure.ereader.com/product/detail/5630?book=Gods_Debris:_A_Thought_Experiment
