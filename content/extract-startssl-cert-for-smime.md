Title: Extract StartSSL Cert for S/MIME
Date: 2014-11-19 11:27
Status: draft

1. Find StartSSL personal cert in Keychain Manager. Name is "email@example.com ($RANDOMNESS)"
2. Right-click cert, export for "Privacy Enhanced Mail (.pem)" named cert.pem
3. Find corresponding private key ("Key from www.startssl.com")
4. Export private key to ("Personal Information Exchange (.p12)" named key.p12.  You will have to secure the key with a passphrase.
5. Use openssl to extract RSA private key from .p12: `openssl pkcs12 -info -in key.p12 > rsa.key`. You will be asked to decrypt the p12 key using its passphrase, then encrypt the private key with a passphrase.
6. Use openssl to convert .pem + extracted key to .p12: `openssl pkcs12 -export -inkey rsa.key -in cert.pem -out cert.p12`. You will be asked to decrypt the rsa.key, then encrypt the .p12 file.
7. Import cert.p12 into Thunderbird (Account > Security > View Certificates > Your Certificates)
8. Import [StartSSL CA][1] into Thunderbird and mark as trusted for email

  [1]: http://aia.startssl.com/certs/sub.class1.client.ca.crt
