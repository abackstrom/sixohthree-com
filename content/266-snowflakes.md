Title: Snowflakes
Slug: 266/snowflakes
Summary: Ice crystals in November.
Date: 2004-11-09 20:22
Tags: Personal, Photography
WordPress-Post-ID: 266
WordPress-Post-Type: post

[![Ice crystals on my car windshield, shot 1.][]][]

[![Ice crystals on my car windshield, shot 2.][]][]

[![Ice crystals on my car windshield, shot 3.][]][]



The view from my car, looking through my front windshield to the trees
beyond.



Winter's here.



(Should have used a smaller aperture.)



  [Ice crystals on my car windshield, shot 1.]: /media/2004/11/09/t_crystals_1.jpg
  [![Ice crystals on my car windshield, shot 1.][]]: /media/2004/11/09/crystals_1.jpg
  [Ice crystals on my car windshield, shot 2.]: /media/2004/11/09/t_crystals_2.jpg
  [![Ice crystals on my car windshield, shot 2.][]]: /media/2004/11/09/crystals_2.jpg
  [Ice crystals on my car windshield, shot 3.]: /media/2004/11/09/t_crystals_3.jpg
  [![Ice crystals on my car windshield, shot 3.][]]: /media/2004/11/09/crystals_3.jpg
