Title: Movie Night: Hawk & Amélie
Slug: 122/movie-night-hawk-amlie
Summary: A clinical review.
Date: 2002-12-27 14:02
Tags: Movies
WordPress-Post-ID: 122
WordPress-Post-Type: post
Category: Personal

Karl and Arne came over last night, and we watched [Hawk the Slayer][]
and [Amélie][]. Hawk was your standard joke movie, and we made fun of it
pretty much the entire time. I don't really recommend it, and I won't
bore you with it here.

Amélie, on the other hand, was excellent. The cinematography was
beautiful, and the characters were portrayed oh-so-well. This movie will
suck you in, and you'll never want it to stop. I recommend seeing it.

  [Hawk the Slayer]: http://us.imdb.com/Title?0080846
  [Amélie]: http://us.imdb.com/Title?0211915
