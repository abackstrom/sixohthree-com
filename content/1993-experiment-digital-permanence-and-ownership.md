Title: Experiment: Digital Permanence and Ownership
Slug: 1993/experiment-digital-permanence-and-ownership
Date: 2012-04-01 21:32
Tags: Computers, Freedom, drm, ebooks, kindle
WordPress-Post-ID: 1993
WordPress-Post-Type: post

When it comes to digital goods, I'm hesitant where I throw my money.
Value is subjective and situational. I'm not a huge movie buff and I
rarely rewatch, so \$8.99/monthly for Netflix isn't bad. On the other
hand, I loathe the idea of subscription-based music: if I wanted to
listen to it once, I *will* want to listen again in the future, and I
don't want to be at the mercy of someone else's uptime and pricing
scheme.

I've bought a handful of technical books from O'Reilly, but never any
fiction. So, an experiment: I've purchased Suzanne Collins *[The Hunger
Games][]* from the Amazon Kindle store for \$5.00. This book is
DRM-locked, and can only be read using a compatible application. I'll
use this post to track the lifespan of this purchase. At least if I
remember to.

Hm, I should also buy the paperback for a true comparison of digital vs.
analog.

Predictions
-----------


Amazon appears to be a stable company with a long future, though the
eBook market as a whole lacks cohesion and predictability. If the Kindle
(or at least the Kindle book market) survives, I expect modern platforms
to always have a working Kindle reader. Older platforms may fall into
disrepair, and fringe platforms may lack compatibility. I will always be
able to download the book from the Kindle store.

I expect to be able to read this eBook indefinitely, though other
"richer" forms of media may try to entice me to buy a more modern
version with more featuers.

March 2012
----------


I purchased the eBook the last day of March, 2012. I was able to read it
on my HP TouchPad (a discontinued device) using the Amazon Kindle Beta
application for webOS. I also have the ability to read this book through
a web browser or the Kindle for Mac application.

  [The Hunger Games]: http://www.amazon.com/gp/product/B002MQYOFW/ref=kinw_myk_ro_title
