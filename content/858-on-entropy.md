Title: On Entropy
Slug: 858/on-entropy
Summary: Hulu's desktop application has been released.
Date: 2009-05-28 14:40
Tags: Video, hulu, tv
WordPress-Post-ID: 858
WordPress-Post-Type: post

[![hulu-loading-random-video][]][hulu-loading-random-video]

[Hulu's desktop application][] has been released.

  [hulu-loading-random-video]: /media/2009/05/hulu-loading-random-video.png
  [Hulu's desktop application]: http://www.hulu.com/labs/hulu-desktop
