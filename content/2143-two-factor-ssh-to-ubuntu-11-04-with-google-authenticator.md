Title: Two-factor SSH to Ubuntu 11.04 with Google Authenticator
Slug: 2143/two-factor-ssh-to-ubuntu-11-04-with-google-authenticator
Summary: Read on for instructions setting up two-factor auth for SSH logins under Ubuntu. I used 11.04 Natty Narwhal; other releases may require small tweaks.
Date: 2012-11-18 17:51
Tags: Linux, security, ssh, totp
WordPress-Post-ID: 2143
WordPress-Post-Type: post

Read on for instructions setting up two-factor auth for SSH logins under
Ubuntu. I used 11.04 Natty Narwhal; other releases may require small
tweaks.

My employer is piloting the use of [Duo Mobile][] for our VPN logins.
(Aside: on iOS the app supports push notifications, so two-factor means
pressing a green button on my phone at login time. Nice!) The latest
update included "3rd-party TOTP passcode support." Searching around, I
found [hotp-php][], and, more to the point of this post, the
[google-authenticator][] project on Google Code. The project includes a
libpam module for secondary token authentication in supported services,
including the OpenSSH server.

If you're using public key authentication, you won't be prompted for
your two-factor auth token. Even so, this gives you peace of mind when
enabling password authentication, if you ever have to login on a machine
that doesn't have your key.

First, install `libqrencode3` so you don't have to manually type the
resulting key.

    ~$ sudo apt-get install libqrencode3

Next, download and install the [`google-authenticator` PAM module][]:

    git clone https://code.google.com/p/google-authenticator/
    cd google-authenticator/libpam/
    make
    sudo make install

Run the binary to enable two-factor auth for your user account:

    ~$ google-authenticator

Follow the prompts to customize your desired token lifetime window,
whether or not tokens can be reused, and other settings. You'll get a
sweet-as-hell ASCII art QR code which you can feed to your TOTP app's
"Scan Barcode" feature. Go ahead and scan using Duo Mobile, Google
Authenticator, or another TOTP app.

Now add the Google authenticator to your PAM settings file for sshd:

    ~$ sudo vim /etc/pam.d/sshd

Add the following line:

    auth required pam_google_authenticator.so

You may also need to enable challenge/response authentication in sshd:

    $ sudo vim /etc/ssh/sshd_config

Make sure you have this directive:

    ChallengeResponseAuthentication yes

And restart sshd:

    sudo service ssh restart

Key-based authentication will override token-based. If you usually use
tokens, temporarily disable them in `sshd_config` before testing token
logins:

    PubkeyAuthentication no

You should be prompted for the token before being prompted for a
password. Cheers! Your remote logins are now protected by two-factor
auth.

  [Duo Mobile]: http://www.duosecurity.com/docs/authentication#passcodes_via_duo_mobile
  [hotp-php]: https://github.com/Jakobo/hotp-php
  [google-authenticator]: http://code.google.com/p/google-authenticator/
  [`google-authenticator` PAM module]: http://code.google.com/p/google-authenticator/wiki/PamModuleInstructions
