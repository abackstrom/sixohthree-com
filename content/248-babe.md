Title: Righteous Babeage
Slug: 248/babe
Summary: Buy tickets or perish.
Date: 2004-09-15 08:04
Tags: Personal
WordPress-Post-ID: 248
WordPress-Post-Type: post

Ani DiFranco will be [performing][] at the following locations of
interest this winter:



-   Burlington, VT (Nov. 30, 2004)
-   Concord, NH (Dec. 1, 2004)
-   Worcester, MA (Dec. 3, 2004)
-   Raleigh, NC (Dec. 7, 2004)
-   Rochester, NY (Dec. 11, 2004)



I will make best effort to attend at least one of these shows.



  [performing]: http://righteousbabe.com/tour/index.asp
