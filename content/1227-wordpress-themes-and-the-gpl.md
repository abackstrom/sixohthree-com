Title: WordPress Themes and the GPL
Slug: 1227/wordpress-themes-and-the-gpl
Summary: There's some debate over whether WordPress themes are automatically GPL because of the way they are loaded by, integrate with, and depend on WordPress, which is GPL.
Date: 2010-07-22 23:57
Tags: Freedom, Web, copyleft, gpl, WordPress
WordPress-Post-ID: 1227
WordPress-Post-Type: post
Category: Technology

There's [some debate][] over whether WordPress themes are automatically
GPL because of the way they are loaded by, integrate with, and depend on
WordPress, which is GPL.

So what's the legality around creating a new publishing platform that
uses the **same WordPress function names** (but none of the WordPress
code), effectively making it compatible with WordPress themes?

What if I already had written a publishing platform in the public
domain, using different function names for themes, then wrote a
compatibility layer so that someone could drop WordPress themes into my
application? What if someone then wrote a theme intending it for my
application and its compatibility layer, and WordPress compatibility was
not a consideration for that developer? The theme would work in
WordPress, but would not require WordPress in order to function.

I believe in open source and the GPL. I don't believe you should shove
it down peoples' throats. Yes, the WordPress [developers][] have put a
lot of time and effort into creating this code base with the intention
of it being open source. Obviously the [Thesis][] folks shouldn't have
copied code, but don't let that muddy the discussion: WordPress takes a
lot of effort, but so does theme and plugin development. A commercial
theme does not diminish the open source foundation it sits upon, nor
does it make that foundation any less open.

  [some debate]: http://markjaquith.wordpress.com/2010/07/17/why-wordpress-themes-are-derivative-of-wordpress/
  [developers]: http://core.trac.wordpress.org/search?q=abackstrom
  [Thesis]: http://diythemes.com/
