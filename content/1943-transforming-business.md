Title: Transforming Business
Slug: 1943/transforming-business
Summary: You can ride the wave, or watch it pass by. Just don't waste your time trying to stop it.
Date: 2012-02-08 10:27
Tags: Internet, culture, Freedom, PIPA, social media, SOPA
WordPress-Post-ID: 1943
WordPress-Post-Type: post

On Business Insider, [P&G To Lay Off 1,600 After Discovering It's Free
To Advertise On Facebook][]:

> P&G said it would lay off 1,600 staffers, including marketers, as part of a
> cost-cutting exercise. More interestingly, CEO Robert McDonald finally seems
> to have woken up to the fact that he cannot keep increasing P&G's ad budget
> forever, regardless of what happens to its sales.
>
> He told Wall Street analysts that he would have to "moderate" his ad budget
> because Facebook and Google can be "more efficient" than the traditional
> media that usually eats the lion's share of P&G's ad budget.

You have a right to [get mad][] about opposition to SOPA and PIPA, but
unless that quote gets you riled up, you're a hypocrite. The Internet,
like many technologies of the past 150 years, is shaping culture and
commerce. You can ride the wave, or watch it pass by. Just don't waste
your time trying to stop it.

[Via @prwood][].

  [P&G To Lay Off 1,600 After Discovering It's Free To Advertise On Facebook]: http://www.businessinsider.com/pg-ceo-to-lay-off-1600-after-discovering-its-free-to-advertise-on-facebook-and-google-2012-1
  [get mad]: http://blogs.seattleweekly.com/reverb/2012/01/quit_whining_about_sopa_and_pi.php
  [Via @prwood]: https://twitter.com/prwood/status/167067348707196929
