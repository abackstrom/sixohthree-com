Title: Announcing Marshall Leif
Slug: 456/baby
Summary: Welcome to the world.
Date: 2008-05-19 21:27
Tags: Kid
WordPress-Post-ID: 456
WordPress-Post-Type: post

![Marshall resting on Charlotte's chest][]

May 19, 2008, 11:03 AM. 8 lbs, 8 oz, 20" long. Our healthy baby boy.
Named after his great-grandfather, Marshall Turner Moulton.

I could say a lot. I will say: I am so proud of my wife for all her hard
work, digging deep within herself to find the strength to push after so
many hours. Thank you for this gift.

  [Marshall resting on Charlotte's chest]: /media/2008/05/19/mlb.jpg
