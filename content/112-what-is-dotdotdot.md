Title: What is "dotdotdot"?
Slug: 112/what-is-dotdotdot
Summary: Origins.
Date: 2002-12-08 01:22
Category: Meta
WordPress-Post-ID: 112
WordPress-Post-Type: post

Some of you may be wondering what the name of this blog means. As
always, I'm oh-so-eager to please:

When I created this blog, I wanted something a little more interesting
than my name, but didn't really want to spend half a day coming up with
something witty. "dotdotdot" comes from the song *Whatall Is Nice* by
the inimitable [Ani DiFranco][]. To quote:

> And you were not a dot-dot-dot,  
> Waiting for me to complete you.  
> And it was like I just forgot  
> To measure everything that I do.

No deep meaning here. Just think it's a pretty song... It's off of
Revelling/Reckoning, if you'd like to check it out.

  [Ani DiFranco]: http://www.righteousbabe.com/
