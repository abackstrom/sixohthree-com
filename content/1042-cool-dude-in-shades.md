Title: Cool Dude in Shades
Slug: 1042/cool-dude-in-shades
Date: 2010-04-26 12:09
Tags: Photography, Kid
WordPress-Post-ID: 1042
WordPress-Post-Type: post
X-Photo: 1
Category: Personal

[ ![Marshall at 2 years old wearing sunglasses in his car seat][img] ][img-big]

He's good at putting them on, now, and he likes to push them up on his
head and tell us "hat" very matter-of-factly. (Actually, he says most
things matter-of-factly.)

  [img]: https://sixohthree.com/files/2010/04/marshall-shades-1024x684.jpg "Marshall's Shades"
  [img-big]: https://sixohthree.com/files/2010/04/marshall-shades.jpg
