Title: Hacker Culture
Slug: 207/hacker
Summary: Hack, don't crack.
Date: 2004-04-01 18:44
Tags: Computers
WordPress-Post-ID: 207
WordPress-Post-Type: post

I had a meeting with [Weez][] earlier this week regarding my independant
study, the fog through which she has graciously agreed to guide me. She
mentioned something that struck me, but I didn't mention it at the time,
so this will be news to her as well. (Hi, Professor Oyzon!)



<!--more-->

A little background: I'm creating a web application ([PHP][]) with a
database backend ([MySQL][]). At one point during our back-and-forth
Professor Oyzon asked, "Is it hackable?" I started to answer, but as an
afterthough I asked her to clarify instead. I was glad I did; her
question revolved around malicious hacking (often "[cracking][]"), while
my answer would have revolved around the [hacker's definition][] of
hacking.



The difference is not insignificant. Had I not stopped myself, I would
have asserted that it was indeed hackable, with well-commented code and
an easy-to-understand structure. But, no, she wanted to know if it was
secure, resistant to cracking. Which it is, or at least will be as I
write more code. I don't hold it against her, of course. She was only
using the popular definition.



Just a <acronym title="Public Service Announcement">PSA</acronym> about
some jargon, I guess. Carry on.



  [Weez]: http://weez.oyzon.com/
  [PHP]: http://www.php.net/
  [MySQL]: http://www.mysq.com/
  [cracking]: http://www.catb.org/~esr/jargon/html/C/cracker.html
  [hacker's definition]: http://www.catb.org/~esr/jargon/html/H/hacker.html
