Title: Morgamic: Reorgs: Choose Wisely
Summary: Mike Morgan's thoughts on reorgs.
Date: 2015-11-05 21:26
Tags: management

[Reorgs: Choose Wisely][1], from [Morgamic][2]:

> If you are a progressive leader, your team may have even come up with the idea
> in the first place.

Related:

> I think a better approach is to help your team understand why changes are
> needed, help them find a solution for how they want to be structured and
> ultimately they will arrive at the correct what — the right org to match the
> challenges they face.

Also enjoyable: the Golden Circle diagram, and how much changes when you start
with the "Why?"

  [1]: http://morgamic.com/2013/06/10/reorgs-choose-wisely/
  [2]: http://morgamic.com/
