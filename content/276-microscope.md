Title: HTML Under the Microscope
Slug: 276/microscope
Summary: Yeah, I know you don't care.
Date: 2004-12-08 21:31
Tags: Web
WordPress-Post-ID: 276
WordPress-Post-Type: post

Apple Computer has a [Student Blog][]. If you look at the HTML for this
blog, the \<p\> tag containing the copyright noticed has a class of
"sosumi."



You have to appreciate a number of very specific things to appreciate
this.



  [Student Blog]: http://education.apple.com/students/blog/
