Title: This is My Life
Slug: 144/this-is-my-life
Summary: I'm glad I'm not the only one who does this.
Date: 2003-03-31 01:25
Tags: Comics
WordPress-Post-ID: 144
WordPress-Post-Type: post

It's official: [Drew Weing][] is the [smartest man alive][]. I'm glad
I'm not the only one who does this.

Many thanks to [Chris Onstad][] for bringing Drew and The Journal Comic
to my attention.

  [Drew Weing]: http://www.drewweing.com/
  [smartest man alive]: http://www.drewweing.com/journalcomic/?date=20020418
  [Chris Onstad]: http://www.achewood.com/
