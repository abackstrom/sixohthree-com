Title: Moving On
Slug: 450/moving-3
Summary: A change of scenery.
Date: 2008-04-01 17:46
Tags: Work
WordPress-Post-ID: 450
WordPress-Post-Type: post

Friday, April 4 will be my last day at Stonewall Cable, Inc. I've
accepted a web development position with the local university, and my
first day will be Monday, April 7 assuming there's no dirt to be found
in my background check.

I've had a great time at Stonewall. There are so many opportunities to
get your hands dirty and be involved in all kinds of projects in a
company this size. I've done such a wide variety of things here, from
implementing more dynamic content onto the company website, to moving
furniture and cutting heavy rubber matting for use as floor protectors.
I've changed hats many times and found a good rhythm here, but I'm ready
to switch gears and do the kind of work I've always hoped to do. It's
bittersweet, but I'm excited for the change.

So, thanks. It's been a good time, and I'll remember it fondly.
