Title: On Burning Men
Slug: 425/burning
Summary: Burn, baby.
Date: 2007-08-30 13:55
Tags: Personal
WordPress-Post-ID: 425
WordPress-Post-Type: post

I find it amusing that you can be called an [arsonist][] for setting a
thing on fire which was created to be set on fire.

  [arsonist]: http://laughingsquid.com/burning-man-set-on-fire-early-arson-is-to-blame/
