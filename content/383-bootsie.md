Title: Rest in Peace
Slug: 383/bootsie
Date: 2006-10-12 18:19
Tags: Personal
WordPress-Post-ID: 383
WordPress-Post-Type: post

Bootsie, family pet of 20 years, sworn enemy of canines, and friend of
everyone else, has passed away.

![Bootsie, held by Patty][]

Thank you for the companionship.

  [Bootsie, held by Patty]: /media/2006/10/12/bootsie.jpg
