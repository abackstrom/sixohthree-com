Title: Controlling Spam
Slug: 331/control
Summary: Stripped of all power.
Date: 2005-09-01 18:31
Tags: Spam
Category: Meta
WordPress-Post-ID: 331
WordPress-Post-Type: post

The fight against comment spam continues. My [blessing feature][] works
really well, and while it's still somewhat cumbersome to bless a
comment, at least it's doing its job. Still, though, there was one
nagging problem that I needed to take care of: the delay between spam
comment postings and their eventual deletion.

When my blog gets spammed, I'm responsible for those outgoing links. I
authorized the anonymous comments, I let the link show up, and, while I
have every intention of deleting the spam, it's still going to sit there
for hours or days, possibly in view of an indexer. I'm not cool with
that. [Nofollow][] would fix my problem, but it's not in my release of
WordPress. I plan on upgrading in the near future, but it will be a
task, and I'm not ready for it yet. Instead, I'm leveraging my "blessed"
attribute to decide which comments can and cannot use HTML.

New functionality is as such: by default, all comments are not blessed.
The poster's name will not link to his website even if he provided a
URL, and all HTML will be stripped from his comment. Hours pass. I find
the comment and bless it. WordPress shields it from future comment
cleansing, and enables the poster URL and tags. Simple. There is a
period of time where the legitimate comments are somewhat crippled, but
at least the content is present. Seems like a decent compromise to me.



  [blessing feature]: /archives/2005/07/20/spam
  [Nofollow]: /archives/2005/01/28/nofollow
