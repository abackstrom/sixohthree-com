Title: Git Subtree Merges, Orphaned Branches, and GitHub
Slug: 1955/git-subtree-merges-orphaned-branches-and-github
Summary: Git supports a merge strategy called the subtree merge which brings a branch into a subdirectory of another branch. How are these created, what if the dependency isn't a Git project, and how does GitHub display unrelated branches?
Date: 2012-03-06 10:27
Tags: Computers, DVCS, git
WordPress-Post-ID: 1955
WordPress-Post-Type: post

Git supports a merge strategy called the [subtree merge][] which brings
a branch into a subdirectory of another branch. This can be used as an
alternative to submodules: add a remote, create a remote tracking
branch, and use `git read-tree` to import that branch into a
subdirectory of your main project. The remote tracking branch mirrors
the external project in your own repository, and shares no ancestors
with your main branch.

If it's not possible to track the dependency as a git repository, you
can manually [create an orphaned branch][] that shares no ancestors with
your main project:


    git symbolic-ref HEAD refs/heads/newbranch
    rm .git/index
    git clean -fdx
    <do work>
    git add your files
    git commit -m 'Initial commit'



Orphan Branches in GitHub
-------------------------



So, what does GitHub think of these orphaned branches? Here's a project
that has two unrelated branches:

![Network Diagram 1][]

If we fork the project and make commits to these branches, we can see
the game of hopscotch that ensues. Adding to master:

![Network Diagram 2][]

Adding to the orphan branch:

![Network Diagram 3][]

This is a bit cluttered, so let's look at that last one again without
the labels:

![Network Diagram 4][]

Additional commits in the original repository work as expected, but I
imagine the network graph gets pretty confusing after a while:

![Network Diagram 5][]

  [subtree merge]: http://progit.org/book/ch6-7.html
  [create an orphaned branch]: http://book.git-scm.com/5_creating_new_empty_branches.html
  [Network Diagram 1]: https://sixohthree.com/files/2012/03/orphan-network-1.png
  [Network Diagram 2]: https://sixohthree.com/files/2012/03/orphan-network-2.png
  [Network Diagram 3]: https://sixohthree.com/files/2012/03/orphan-network-3.png
  [Network Diagram 4]: https://sixohthree.com/files/2012/03/orphan-network-4.png
  [Network Diagram 5]: https://sixohthree.com/files/2012/03/orphan-network-5.png
