Title: Unlocking iTMS Tracks
Slug: 255/itms
Summary: Much as I love their products, Apple has still failed me in one important regard: they have not released iTunes for Linux.
Date: 2004-10-07 14:57
Tags: Personal
WordPress-Post-ID: 255
WordPress-Post-Type: post

Much as I love their products, Apple has still failed me in one
important regard: they have not released iTunes for Linux. iTunes is, I
think, my favorite application, and the one I miss the most now that I'm
full-time Linux. I even dig the iTunes Music Store, and I've been known
to purchase songs from it.



Yesterday I was browsing around the local [pit of despair][] and saw the
new [Cake][] album "Pressure Chief." I made a mental note to purchase it
when I got home. I did so, then used [hymn][] to clean the DRM filth off
the .m4p files, leaving pure .m4a in their place. I copied the new files
over to my Linux box and ran the following command:

    for file in *.m4a ; do
        mplayer -vo null "$file" -ao pcm -aofile "$file.wav"
    done

My installation of MPlayer can handle any file format QuickTime
understands, and I ended up with a bunch of WAV files ready for
conversion to MP3. I hope this helps anyone else who tries the same
procedure.



  [pit of despair]: http://www.walmart.com/
  [Cake]: http://www.cakemusic.com/
  [hymn]: http://hymn-project.org/
