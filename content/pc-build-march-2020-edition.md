Title: PC Build March 2020 Edition
Slug: pc-build-march-2020-edition
Summary: Dipping my toes into Ryzen Gen 3.
Date: 2020-03-08 23:30
Category: Computers
Tags: pc build, gaming

Time for another PC refresh! I upgraded [my last
build](/pc-build-december-2016-edition) from an Intel Core i5 to an AMD Ryzen 5.
Same case, new core. Here's the bulk of the parts list:

* [NZXT S340 Mid Tower (White)](https://pcpartpicker.com/product/Vpdqqs)
* \*[Gigabyte X570 UD motherboard](https://pcpartpicker.com/product/LcYQzy)
* [Gigabyte GeForce RTX 2070 8GB](https://pcpartpicker.com/product/TkfFf7)
* \*[AMD Ryzen 5 3600X 3.8 GHz 6-Core Processor](https://pcpartpicker.com/product/3WYLrH)
* \*[Cooler Master Hyper 212 EVO](https://pcpartpicker.com/product/hmtCmG)
* \*[G.Skill Aegis 16 GB (2x8GB) DDR4-3200 RAM](https://pcpartpicker.com/product/mcH8TW)
* [Intel 600p Series 256GB M.2 80mm SSD](https://pcpartpicker.com/product/MFs8TW)
* [Samsung 860 Evo 1TB](https://pcpartpicker.com/product/yzfhP6)
* [EVGA SuperNOVA G2 650 W 80+ Gold (Modular)](https://pcpartpicker.com/product/9q4NnQ)
* [Acer XF270HU 27.0" 2560x1440 144Hz Monitor](https://pcpartpicker.com/product/sqp323)
* [Elgato HD60 Pro 1080p HDMI Capture Card](https://www.elgato.com/en/gaming/game-capture-hd60-pro)

\* *Indicates the current upgrades*

Here's that list (and a couple more things) [on PCPartPicker][1], and [here's
the UserBenchmark][2].

* UserBenchmarks: Game 102%, Desk 94%, Work 81%
* CPU: AMD Ryzen 5 3600X - 93.8%
* GPU: Nvidia RTX 2070 - 110.6%
* SSD: Intel 600p Series NVMe PCIe M.2 256GB - 125%
* SSD: Samsung 860 Evo 1TB - 114.6%
* RAM: G.SKILL Aegis DDR4 3200 C16 2x8GB - 87.9%
* MBD: Gigabyte GA-X570 UD


  [1]: https://pcpartpicker.com/list/XpL627
  [2]: https://www.userbenchmark.com/UserRun/25310999
