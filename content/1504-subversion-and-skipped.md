Title: Subversion and "Skipped '.'"
Slug: 1504/subversion-and-skipped
Date: 2011-04-08 15:06
Tags: Personal, shell, Subversion
WordPress-Post-ID: 1504
WordPress-Post-Type: post
WordPress-Post-Format: Aside

*Skipped '.'* seems to be Subversion's way of saying, "*svn up* hit an
external that isn't a working copy."
