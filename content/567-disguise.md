Title: A Cunning Disguise
Slug: 567/disguise
Summary: With a wick-wack bitter lack of youthfulness &amp; charm, this old man kept rhyming on.
Date: 2008-10-11 16:05
Tags: Kid, halloween, Kid, mcfrontalot
WordPress-Post-ID: 567
WordPress-Post-Type: post

I think we found Marshall's Halloween costume:

![Baby Frontalot][img] ![MC Frontalot][1]

([MC Frontalot][] photo by [quinnums][], Creative Commons [by-cc-na 2.0][].)

  [img]: /media/2008/10/marshall-frontalot.jpg "Photo of a bald baby in thick-rimmed glasses"
  [1]: /media/2008/10/mc-frontalot.jpg "Photo of a bald nerdcore rapper in thick-rimmed glasses"
  [MC Frontalot]: http://flickr.com/photos/quinn/120447193/
  [quinnums]: http://flickr.com/photos/quinn/
  [by-cc-na 2.0]: http://creativecommons.org/licenses/by-nc-sa/2.0/deed.en
