Title: Miscellaneous Hackery
Slug: 180/miscellaneous-hackery
Summary: Mostly UML.
Date: 2003-09-20 23:36
Tags: Computers, Mac OS X, qmail, hosting, jvds, user-mode linux, virtualization
WordPress-Post-ID: 180
WordPress-Post-Type: post

I've been experimenting with [User-mode Linux][1] lately. Let's just say I'm impressed.
UML allows me to run fully encapsulated Linux systems on my host, fully
partitioned out from normal userspace. I'm going to toy around with the
possibilities and maybe move some Apache vhosts or other server services
inside. (This is useful for damage control in the event of a break-in.)

Here's a Mac OS X tip for y'all: `cron` on Mac OS X has the ability to
run commands only when operating on A/C power:

> The command can optionally be prefixed by "@AppleNotOnBattery " to
> tell cron not to run the command when functioning on battery power.
> For example, the "sixth" field when using this option would appear
> something like "@AppleNotOnBattery /usr/bin/touch /tmp/foo"

I signed up for hosting from [JVDS][]. \$20 a month gives me root on my
own Slackware Linux 9.0 UML. I've been configuring it over the past few
days, and initial impressions are very good. I've configured BIND to be
the name server for my [new domain][], and Apache is happily serving up
several vhosts. Let me also say that [qmail][] is the most wonderful
piece of software in the world. It loves [Maildir][] even more than I
do, which is saying a lot.

  [1]: http://user-mode-linux.sourceforge.net/
  [JVDS]: http://www.jvds.com/
  [new domain]: https://sixohthree.com/
  [qmail]: https://mirrors.dotsrc.org/qmailwww/top.html
  [Maildir]: https://mirrors.dotsrc.org/qmailwww/qmail-manual-html/man5/maildir.html
