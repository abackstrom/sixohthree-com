Title: Mean Beef Stroganist
Summary: On Distilling a Woman's Accomplishments
Slug: mean-beef-stroganist
Date: 2013-04-01 9:45:00
Tags: Gender

This popped up in my feed yesterday:

<blockquote class="twitter-tweet"><p>Guys come on writing obituaries of
prominent women isn't rocket science.</p>&mdash; Paul Ford (@ftrain) <a
href="https://twitter.com/ftrain/status/318180452269715456">March 31,
2013</a></blockquote>

At the time, I had no context. This helped:

<blockquote class="twitter-tweet"><p>Whoa:<br><br>--- mean beef
stroganoff<br>+++ brilliant rocket scientist<br><br><a
href="http://t.co/nTJ8WMOkDK"
title="http://www.newsdiffs.org/diff/192021/192137/www.nytimes.com/2013/03/31/science/space/yvonne-brill-rocket-scientist-dies-at-88.html">newsdiffs.org/diff/192021/19…</a><br><br>/via
@<a href="https://twitter.com/jayrosen_nyu">jayrosen_nyu</a></p>&mdash; Al Shaw
(@A_L) <a href="https://twitter.com/A_L/status/318542326622453760">April 1,
2013</a></blockquote>

Related:

<blockquote class="twitter-tweet"><p>"Margaret Thatcher's beef stroganoff was
delicious. She also ran England." - @<a
href="https://twitter.com/mkramer">mkramer</a></p>&mdash; Heidi N. Moore
(@moorehn) <a href="https://twitter.com/moorehn/status/321250680838967296">April
8, 2013</a></blockquote>
