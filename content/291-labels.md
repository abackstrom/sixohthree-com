Title: HTML Labels & Netscape 7.0
Slug: 291/labels
Summary: News flash, three years late.
Date: 2005-01-26 20:48
Tags: Web
WordPress-Post-ID: 291
WordPress-Post-Type: post

Maybe I'm the only one that missed this announcement, but [floated
`<label>` tags disappear in Netscape 7.0][forms]. I use this code often:


    <div class="formrow">
        <label>Name:</label>
        <input type="text" name="user_name">
    </div>



With some CSS to style:

    .formrow label {
        width: 8em;
        float: left;
        text-align: right;
        display: block;
    }


This code nicely aligns the form labels and edges of form inputs along a
line, as in the following screenshot:



[![Screenshot of form from web browser.][thumb]][big]



Of course, Netscape 7.0 throws a wrench in the works. Floating labels
disappear without a trace. The fix is pretty easy, though: embed a
\<span\> in the \<label\> and apply the styles to the span instead. The
HTML becomes:




    <label><span>Name:</span></label>



And the CSS turns into:




    .formrow label span { }



That's it. Uh, thanks for reading.



  [forms]: http://tom.me.uk/html-to-css/forms.html
  [thumb]: /media/2005/01/26/form-labels_t.gif
  [big]: /media/2005/01/26/form-labels.gif
