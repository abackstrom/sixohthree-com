Title: Expiring Trees in Dirvish
Slug: 448/expire
Summary: Perl isn't fun to read.
Date: 2008-03-17 15:52
Tags: Linux, backup, dirvish
WordPress-Post-ID: 448
WordPress-Post-Type: post

We've taken to using Dirvish to back up files here at work. It's similar
to Apple's Time Machine backup mechanism: hard links provide directory
snapshots while preserving disk space. This is most effective when most
of your files are static, as the hard links can share these files across
all your snapshots without using any additional disk space.

I didn't find any documentation on expiring trees before their time, so:
yes, you can edit a simple text file and expire a tree early. (Honestly,
it may be acceptable to `rm -rf` a tree; I really don't know. I'd rather
let `dirvish-expire` do whatever cleanup it has to do, not being
familiar with the code.) I happen to be removing some trees that didn't
back up correctly, as general maintenance.

Each backup tree contains a "summary" file. This file includes an Expire
line:


    Expire: +14 days == 2008-03-30 02:06:38



My original expire-rule expanded to 14 days from the date of backup,
which was March 30. If I want to delete the tree right now, I can change
the date after the equality test:


    Expire: +14 days == 2007-03-30 02:06:38



Anything in the past is sufficient. Run `dirvish-expire`, and the tree
will drop out of your vault.
