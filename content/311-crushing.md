Title: That Crushing Feeling, and Tangled Webs
Slug: 311/crushing
Summary: Unweaving the web.
Date: 2005-05-26 15:29
Tags: Computers, Freedom
WordPress-Post-ID: 311
WordPress-Post-Type: post

Hey, does anyone know if the FBI has permission to [reproduce the
EliteTorrents.org][] logo? Oh well, it's probably another hoax anyway.



I'm having a hard time untangling this web, though: the
elitetorrents.org page is now a frameset that points to an IP
([192.31.21.68][]) owned by the [San Diego Supercomputer Center][]. It
looks like they've set a reverse pointer on this IP which makes it
resolve to the [Department of Homeland Security][] website, which is the
technological equivalent of a fake ID.



SDSC is running `Apache/2.0.52 Unix PHP/4.3.10` on Solaris 9. The
frameset site is running `Apache/2.0.46 (Red Hat)`. DHS has some Akamai
voodoo going on. As late as [two weeks ago][], ET were running
`Apache/1.3.33 Unix PHP/4.3.10` on some flavor of Linux. At that time,
the domain pointed to an IP (83.149.101.146) under the control of
[LeaseWeb][], a web host and DSL provider in the Netherlands. (That
would explain the ICE connection.)



So, the ET site is pointing to a server at a [government-funded][]
research facility. That's the only thing that's really clear at this
point. Does anyone have any more concrete information?



  [reproduce the EliteTorrents.org]: http://www.elitetorrents.org/
  [192.31.21.68]: http://ws.arin.net/cgi-bin/whois.pl?queryinput=192.31.21.68
  [San Diego Supercomputer Center]: http://www.sdsc.edu/
  [Department of Homeland Security]: http://www.dhs.gov/
  [two weeks ago]: http://toolbar.netcraft.com/site_report?url=http://www.elitetorrents.org
  [LeaseWeb]: http://www.leaseweb.com/
  [government-funded]: http://www.sdsc.edu/Press/2005/05/050305_nsf_award.html
