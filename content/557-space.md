Title: Space
Slug: 557/space
Summary: Freedom is never having to worry about disk space.
Date: 2008-10-07 20:35
Tags: Personal
WordPress-Post-ID: 557
WordPress-Post-Type: post

I can has some. [![Hard drive with 225.7GB of free space.][]][]

I've been obsessing about this for a while now. Today I finally received
my Seagate 7200.3 320GB 7200 RPM 2.5" SATA drive ([ST9320421AS][]) for
my MacBook, as well as a macally FireWire ([PHRS250CC][]) case to house
my old 80GB drive. It's liberating to have more than 2GB of disk space
free. I'm expecting slightly worse battery performance due to the faster
drive speed, with the potential for some speed improvements for the same
reason.

The install went off without a hitch. I booted from the Mac OS X 10.5
install disc and cloned the internal drive to the new drive (temporarily
in the macally case) using Disk Utility. After maybe 90 minutes the
process finished and I booted from FireWire to make sure everything was
kosher. One quick drive swap between the macally and the MacBook and I'm
in business.

Now to see how many weeks go by before another drive is filled.

(As a side note, Mac OS X decided the existing Spotlight index was bunk,
and my fans have been spinning like mad while it's rebuilt. I wish it
wouldn't do that on battery power.)

  

  [Hard drive with 225.7GB of free space.]: /media/2008/10/available-208x300.png
  [![Hard drive with 225.7GB of free space.][]]: /media/2008/10/available.png
  [ST9320421AS]: http://www.newegg.com/Product/Product.aspx?Item=N82E16822148336
  [PHRS250CC]: http://www.newegg.com/Product/Product.aspx?Item=N82E16817347013
