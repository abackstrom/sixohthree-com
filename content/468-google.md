Title: Google: Search Result Editing
Slug: 468/google
Summary: Great googly moogly.
Date: 2008-07-17 10:19
Tags: Personal, Google, socialweb
WordPress-Post-ID: 468
WordPress-Post-Type: post

Earlier this week, I noticed my Google results looked a bit different.
I'm blogging it partially to share, and partially to try out WordPress
galleries.

[gallery]

I'm generally pretty satisfied with Google, but it's nice to be able to
tweak things if need be.
