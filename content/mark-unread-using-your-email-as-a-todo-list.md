Title: Mark Unread: Using Your Email as a TODO List
Slug: mark-unread-using-your-email-as-a-todo-list
Summary: 
Date: 2019-02-04 21:42
Category: Personal
Tags: tags
Status: draft

*In this article, I suggest an email folder layout for organizing messages by
the action they require, as well as a script to mark messages in those folders
as "unread."*

I've always been moderately averse to using my email account as a TODO list.
Despite that aversion, tasks often hinge on an email in some way. Sometimes the
stars align and your email client has usable URLs that can be pasted into a task
management app, but this tends to fluctuate for me as my taste in email clients
changes.

I'm not bold enough presume my most annoying character trait, but my vocal pet
peeve around perceptions of Inbox Zero has got to be up there. The popular
definition of Inbox Zero is that you battled through your inbox and drained it
of email. You probably responded to a ton of things, went to a lot of other
apps, etc. You did a lot of "work." For a brief moment, you achieved Inbox Zero:
zero items in your inbox.

This should sound suspicious to anyone who's used email in 2019. Even if you
somehow get through *everything*, you have zero items in your inbox for all of
about 15 seconds. There is no maintaining Inbox Zero, you would have time for
nothing else. It's a fleeting achievement requiring a massive time investment.

Inbox Zero, [as originally defined][1], is not an achievement, but a process for
dealing with your inbox. I'm not gonna delve super deep into that, but to
distill it down beyond the basics, it's about triage. You can "deal with"
everything in your inbox in a relatively small amount of time (Merlin says 20
minutes) but that doesn't mean everything is "done." It *does* mean you've
sorted it into buckets that make sense for you. Maybe these ones need in-depth
replies, maybe you have action to take on these others, maybe this one is
blocked by something out of your control. Inboz Zero means you have a system for
sorting this stuff so you can deal with it on your own terms and at your own
pace.

For years, I stubbornly stuck to one email folder and moved as much as
possible into Taskpaper.app. When I switched from Gmail to Fastmail, I decided I
needed a few folders I didn't have before, including a "Purchases" folder for
receipts and automated payments. At some point I pinned a couple active orders
to the top of this folder so I could easily get to the order status or tracking
number when I needed it. It dawned on me how nice it was to have these things
*out of my inbox* but easily accessible. I looked at some of the emails trapped
in my inbox and thought I'd try something new.

For me, two folders make sense: Reply and Action. (I also added a Read folder
for work, which tends to receive more longform emails.)

The first version of mark-unread.py fit inside [a toot][3]:

```
from imaplib import IMAP4_SSL
import config

with IMAP4_SSL(config.IMAP_HOST, config.IMAP_PORT) as m:
    m.login(config.IMAP_USER, config.IMAP_PASS)
    for f in config.IMAP_FOLDERS:
        m.select(f)
        typ, data = m.search(None, 'SEEN')
        for num in data[0].split():
            m.store(num, '-FLAGS', '\\SEEN')
```

I have this running an a vm, setting all emails in my Reply and Action folders
to unread every 5 minutes. Using this script cuts down the cognitive overhead of
having multiple folders: I don't have to check the constantly or remember if
there's something undone in there. If something's waiting for me, I'll see the
number.

<!-- links -->

  [1]: http://www.43folders.com/izero
  [2]: http://www.43folders.com/2005/02/18/quick-tips-on-processing-your-email-inbox
  [3]: https://xoxo.zone/@annika/101497746001895269

