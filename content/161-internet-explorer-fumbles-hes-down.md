Title: Internet Explorer fumbles.. he's down!
Slug: 161/internet-explorer-fumbles-hes-down
Summary: There goes the neighborhood.
Date: 2003-06-13 17:58
Tags: Computers, Internet
WordPress-Post-ID: 161
WordPress-Post-Type: post

According to Roz Ho, general manager of Microsoft's Mac Business Unit
[via [Bob][], via [PC Pro][], via MacUser], development of Internet
Explorer 5 for the Mac has ceased. "OK," I say. "So what?"



IE5 for the Mac was released some time in early 2000, I believe March or
April. Generally speaking, the browser has not changed since its initial
release date. Don't get me wrong; it's a [good browser][], and it was an
important win for the Mac community. Despite that, it has flaws, and the
developers have not kept up with existing technologies. Microsoft gave
up on this product two years ago. It just took a long time for them to
admit it.



Ho claims that Safari pushed them out of the market, as Apple is able to
provide more integration with the system than the Microsoft developers.
I think this is a complete load of crap. Fix the stupid margin bugs, add
tabbed browsing, and your browser is almost top-notch. Too many
companies use the "Apple made a competing product" excuse as an easy
out. I say, if you've got a good product, it's *probably* going to
succeed. Either that, or Apple will buy the rights to it. (Then you
*definitely* won't have to compete.)



So, really, what does this annoucement mean? Not much. We still have a
choice of browsers, however limited. And it's not like Internet Explorer
is going away any time soon. Hell, many of the people I support at RIT
use *Netscape Communicator 4.7*. Such is life.



  [Bob]: http://homepage.mac.com/boberito/
  [PC Pro]: http://www.pcpro.co.uk/?http://www.pcpro.co.uk/news/news_story.php?id=43191
  [good browser]: http://www.alistapart.com/stories/ie5mac/
