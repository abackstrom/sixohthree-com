Title: Leakage
Slug: 301/leakage
Summary: How did the web seep so far into e-mail?
Date: 2005-03-31 19:26
Tags: Rants, Web
WordPress-Post-ID: 301
WordPress-Post-Type: post

I am an edge case at times, I know. I can accept that, and expect that
some things will be difficult for me. Still, I get annoyed when my
e-mail looks like this:



[![Thumbnail of mutt textual e-mail client in a PuTTY terminal
window.][]][]



I love HTML, but not in my e-mail client.



  [Thumbnail of mutt textual e-mail client in a PuTTY terminal window.]:
    /~adam/2005/03/31/mutt-html_t.png
  [![Thumbnail of mutt textual e-mail client in a PuTTY terminal
  window.][]]: /media/2005/03/31/mutt-html.png
