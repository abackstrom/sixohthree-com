Title: You Should Read This
Slug: 194/games
Summary: There are things people do, good things.
Date: 2004-01-04 03:53
Tags: Games
WordPress-Post-ID: 194
WordPress-Post-Type: post

There are things people do, good things, but despite the extent, the
*severity* of these things, they go without recognition.



[Penny-Arcade][] and several thousand of their readers did a [good
thing][]. How good? A little over \$200,000 good, some in cash but
mostly in [games][] and [toys][], all for the Seattle Children's
Hospital. Who are these people, wealthy philanthropists smoking cigars
in their Swiss villas? Nah, they're a couple guys that write a comic
about gaming, along with the gaming community that reads their comic.



Mike Krahulik and Jerry Holkins set out to prove a point: gamers aren't
a bunch of psychopathic murders, but rather a pretty decent community
that happens to like video games. They did their bit -- they put in the
hours, they pulled the community together, and they delivered the goods
with a smile. But it looks like their message [may never get out][].
During their followup report with the local news channel the station not
only misreported the people behind the charity, but underquoted the
donated amount by, oh, \$199,000.



Yes, there are less than angelic games. Yes, in the oft-maligned Grand
Theft Auto 3 you can have sex with a hooker in the back of your car and
then beat her to death with a baseball bat. I don't think I'm qualified
to say whether or not that should be in the game. Does anyone actually
get abused or killed? No. Does the simulated act enforce some people's
view of women as objects? I don't know. Maybe there are people that
cannot separate the virtual world from the real world, particularly very
young people.



I would put forward that it's not a game meant for children (hence the
"Mature" rating), and I maintain that some people will do [stupid
things][] no matter what. After all, I don't see any video games about
[hurtling rocks onto freeways][], do you? Remember, "[Every tool is a
weapon if you hold it right][]." Violence of people against people is a
social issue, and one that existed long before guns and video games.
There are billions of people that go all day long without killing or
even maiming someone around them. Imagine that.



I'm getting sidetracked, and I haven't said all I wanted to say, but I
didn't intend for this post to be about me, or Grand Theft Auto, or
violence. Just, there are a lot of gamers, and they're a really together
group of people, and I admire and respect them and hope the charity
continues so I can be part of it in the future. Here's to that.



  [Penny-Arcade]: http://www.penny-arcade.com/
  [good thing]: http://www.penny-arcade.com/childsplay/
  [games]: http://www.penny-arcade.com/childsplay/47.htm
  [toys]: http://www.penny-arcade.com/childsplay/31.htm
  [may never get out]: http://www.penny-arcade.com/news.php3?date=2004-01-02
  [stupid things]: http://www.iol.co.za/index.php?click_id=22&art_id=qw106213446315M365&set_id=4
  [hurtling rocks onto freeways]: http://www.theaustralian.news.com.au/common/story_page/0,5744,8220538%255E1702,00.html
  [Every tool is a weapon if you hold it right]: http://www.danah.org/Ani/PuddleDive/MyIQ.html
