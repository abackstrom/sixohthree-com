Title: ISO, and You?
Slug: 193/noise
Summary: I am learning.
Date: 2003-12-31 20:44
Tags: Photography
WordPress-Post-ID: 193
WordPress-Post-Type: post

After several nights of shooting with my
<a href="http://www.nikonusa.com/template.php?cat=1&amp;grp=2&amp;productNr=25503">Nikon

Coolpix 4500</a> I started to notice a trend in low-light situations:

there seemed to be a high amount of noise in many of the pictures.

A quick look at the manual showed that this is a known problem

during extended exposures, but it [turns out][] the ISO

setting has a lot to do with noise as well. Digital Photography Review
has a [good explanation][] of why this happens.



I took some sample images to compare noise at four ISO settings:

ISO 100, ISO 200, ISO 400, and ISO 800. These shots used automatic

sharpening, automatic contrast, and incandescent white balance

adjustment. ISO, exposure time, and noise reduction (NR) state are

shown below the image.



<div style="width:138px;margin-right: 5px;text-align: center;float: left">

![Sample image at ISO 100.][]

<span>ISO 100, 0.71s</span>

<span><acronym title="noise reduction">NR</acronym> off</span>


</div>


<div style="width:138px;margin-right: 5px;text-align: center;float:left">

![Sample image at ISO 200.][]

<span>ISO 200, 1/3s</span>

<span><acronym title="noise reduction">NR</acronym> off</span>


</div>


<div style="width:138px;text-align: center;margin-right: 5px;float:left">

![Sample image at ISO 100.][1]

<span>ISO 400, 1/6s</span>

<span><acronym title="noise reduction">NR</acronym> off</span>


</div>


<div style="width:138px;text-align: center;margin-right: 5px;float:left">

![Sample image at ISO 100.][2]

<span>ISO 800, 1/12s</span>

<span><acronym title="noise reduction">NR</acronym> off</span>


</div>


  

Ever-increasing amounts of noise can be seen as we move from ISO 100 to
ISO 800. Quality on ISO 200 is acceptable to my eyes, with slight
amounts of noise visible in areas of solid color.

For comparison, I reshot ISO 100 and ISO 200 with noise reduction

enabled. ISO 400 and ISO 800 were shot at faster than 1/4s, which

automatically disables <acronym title="noise reduction">NR</acronym>.

<div style="width:138px;text-align: center;margin-right: 5px;float:left">

![Sample image at ISO 100 with noise reduction.][Sample image at ISO
100.]

ISO 100, 0.77s  
<acronym title="noise reduction">NR</acronym> on


</div>


<div style="width:138px;text-align: center;margin-right: 5px;float:left">

![Sample image at ISO 200 with noise reduction.][Sample image at ISO
100.]

ISO 200, 1/3s  
<acronym title="noise reduction">NR</acronym> on


</div>


  

Both shots appear identical to the ISO 100 image with NR disabled. Looks
like I'll be relying on long exposure times instead of unnaturally high
ISO settings. (Initial testing shows that a high ISO is much more
damaging than a long exposure time. I can shoot an 8" exposure without
noticible noise.)

  [turns out]: http://www.fredmiranda.com/Coolpix/
  [good explanation]: http://www.dpreview.com/learn/Glossary/Digital_Imaging/Sensitivity_01.htm
  [Sample image at ISO 100.]: /media/2003/12/31/small/iso100.jpg
  [Sample image at ISO 200.]: /media/2003/12/31/small/iso200.jpg
  [1]: /media/2003/12/31/small/iso400.jpg
  [2]: /media/2003/12/31/small/iso800.jpg
