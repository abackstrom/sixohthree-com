Title: Installing CPAN Modules without Root
Slug: 1690/installing-cpan-modules-without-root
Summary: Today I needed to run mysqlhotcopy, but it was failing "undefined symbol: mysql_init" in DBD::mysql. My Perl is rusty at best, and I finally resorted to reinstalling DBI and DBD::mysql using the local user.
Date: 2011-10-21 00:08
Tags: Web, perl, scripting
WordPress-Post-ID: 1690
WordPress-Post-Type: post

Today I needed to run mysqlhotcopy, but it was failing "undefined
symbol: mysql\_init" in DBD::mysql. My Perl is rusty at best, and I
finally resorted to reinstalling DBI and DBD::mysql using the local
user. (I don't have root on this box, and I was under some time
constraints.)

Mostly for my own future reference:

1.  Set your home directory as the default CPAN install prefix
    in `~/.cpan/CPAN/MyConfig.pm`:
    `'makepl_arg' => q[PREFIX=/var/home/mysql]`
2.  Export `PERL5LIB` in `~/.bash_profile` (your values may vary):
    `export PERL5LIB=$HOME/lib/perl5/5.8.8:$HOME/lib64/perl5/site_perl/5.8.8`



Resources
---------



-   [Installing mod_perl without superuser privileges][]



  [Installing mod_perl without superuser privileges]: http://www.perl.com/pub/2002/04/10/mod_perl.html?page=2
