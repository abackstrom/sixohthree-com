Title: I get to
Slug: 1978/i-get-to
Summary: Thank you, Paul.
Date: 2012-03-22 09:23
Tags: Quotes, Twitter
WordPress-Post-ID: 1978
WordPress-Post-Type: post

Thank you, [Paul][].

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I go
down to the little gym in my building in the mornings and watch Coney Island
Avenue from the exercise bike.</p>&mdash; Paul Ford (@ftrain) <a
href="https://twitter.com/ftrain/status/180262775858266112">March 15,
2012</a></blockquote>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I like
to switch out the words have to with get to. I get to ride the bike. I get to go
to the gym.</p>&mdash; Paul Ford (@ftrain) <a
href="https://twitter.com/ftrain/status/180263345365069824">March 15,
2012</a></blockquote>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I get to
watch tiny kids in hijab wait for the B68. I get to watch someone exercise their
pigeons. I get to listen to<br>music and type.</p>&mdash; Paul Ford (@ftrain) <a
href="https://twitter.com/ftrain/status/180263902989393920">March 15,
2012</a></blockquote>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I get to
see the price of gas across the street $4.01, I get to watch 70 people walk by
and maybe 2000 in cars. I get to go upstairs home.</p>&mdash; Paul Ford
(@ftrain) <a href="https://twitter.com/ftrain/status/180264538141229056">March
15, 2012</a></blockquote>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I get to
help libraries, and get to write checks to help women own their bodies. I get to
answer long emails about corporate priorities.</p>&mdash; Paul Ford (@ftrain) <a
href="https://twitter.com/ftrain/status/180265443225911297">March 15,
2012</a></blockquote>

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I get to
drink water and take the train across the bridge and go to the shrink and eat
low-calorie breakfasts. Done.</p>&mdash; Paul Ford (@ftrain) <a
href="https://twitter.com/ftrain/status/180265869392347136">March 15,
2012</a></blockquote>

  [Paul]: http://www.ftrain.com/
