Title: Keeping Up
Slug: 2094/keeping-up
Summary: When what you know is who you are.
Date: 2012-10-08 17:03
Tags: Personal, aging, Technology
WordPress-Post-ID: 2094
WordPress-Post-Type: post

I've had two strange digital experiences in the past couple weeks.
First, I find out [PDF][] is an [open standard][], then I find out
[SNI][], which I've been cautiously optimistic about for some time, is
relatively well-supported and requires no setup under my [web server of
choice][].

I turned 30 this summer. At this point I've crossed a threshold and have
spent half my life truly immersed in technology. In years past I have
not felt "out of the loop." Some [popular technologies][] went in and
out of vogue around me without ever really drawing my attention, but I
had a pretty solid core that stayed current.

Those recent learning experiences were strange because I consider those
pieces of data pretty important in their own spheres. It's like someone
baking for years before finding out yeast is a leavening agent. It's one
thing to learn about things as they develop, but another to hear about
something that happened four years ago and be surprised. In some ways,
it felt like I was stuck in time, behind the curve in a space where I'm
usually closer to the forefront.

Maybe that's more likely to happen as my experience is measured in
decades rather than years.

  [PDF]: http://en.wikipedia.org/wiki/Portable_Document_Format
  [open standard]: http://www.iso.org/iso/catalogue_detail.htm?csnumber=51502
  [SNI]: http://en.wikipedia.org/wiki/Server_Name_Indication
  [web server of choice]: http://nginx.org/
  [popular technologies]: http://rubyonrails.org/
