Title: XKCD: Students
Slug: 755/xkcd-students
Summary: xkcd, comics, school
Date: 2009-03-18 12:56
Tags: Comics
WordPress-Post-ID: 755
WordPress-Post-Type: post

[![students][]][]

It's so very true. The most anxiety I ever experience is from these
dreams. I imagine I'm at the end of the quarter, and I realize I've
forgotten to attend a class for the past ten weeks.

(Image copyleft [Randall Munroe][], [BY-NC][].)

  [students]: /media/2009/03/students.png
  [![students][]]: http://xkcd.com/557/
  [Randall Munroe]: http://xkcd.com/
  [BY-NC]: http://creativecommons.org/licenses/by-nc/2.5/
