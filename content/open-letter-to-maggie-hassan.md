Title: Open Letter to Maggie Hassan
Slug: open-letter-to-maggie-hassan
Summary: I'm writing in to voice my support for Senator-elect Hassan, and to express a desire for more frequent communications from her and her staff.
Date: 2016-11-27 20:54
Category: Politics

> Good evening, and I hope you had an enjoyable Thanksgiving holiday.
>
> I'm writing in to voice my support for Senator-elect Hassan, and to express a desire for more frequent communications from her and her staff. This is a time of great uncertainty for myself and many of my friends and colleagues, as we wait anxiously for President-elect Trump's inauguration. As a nontraditional family with many low-income, queer, or otherwise at-risk friends and family, we feel firmly in the President-elect's sights as he looks to roll back years of social and civic progress. The actions he has taken or failed to take since the election have not calmed our fears, but heightened them.
>
> At times like this, I feel it's more important than ever that our representatives in Congress are communicative and accessible. We need reassurances that our voices will be heard in Washington, that our votes did matter even if we could not elect our president of choice. With domestic hates crimes on the rise, neo-nazis and special interests worming their way into the White House, and a man who regularly takes to Twitter to rail against his detractors poised to become POTUS, we could really benefit from having your voice in the conversation.
>
> I realize this is a very busy time for you and your staff as you prepare for your first term, but I hope my concerns are heard and considered.
>
> Thank you,
>
> Annika Backstrom  
> Rumney, NH

*Sent to Senator-elect Maggie Hassan, November 27, 2016.*
