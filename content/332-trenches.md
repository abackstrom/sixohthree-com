Title: In the Trenches
Slug: 332/trenches
Summary: So much for civilization.
Date: 2005-09-07 13:54
Tags: World
WordPress-Post-ID: 332
WordPress-Post-Type: post

You need to read [Hurricane Katrina - Our Experiences][], by Larry
Bradsahw and Lorrie Beth Slonsky, if you haven't done so already. The
whole Katrina mess has been supremely bungled, from start to finish.
Things weren't immediately as bad as [the NOAA bulletin][] forecast.
Good thing, too, seeing as how poorly the situation has been handled
thus far.

  [Hurricane Katrina - Our Experiences]: http://www.emsnetwork.org/artman/publish/article_18337.shtml
  [the NOAA bulletin]: http://wikisource.org/wiki/August_28_2005_11:01_AM_CDT_NOAA_Bulletin
