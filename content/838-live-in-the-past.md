Title: Live in the Past
Slug: 838/live-in-the-past
Summary: Just caught this advert on an Engadget article.
Date: 2009-05-20 08:27
Tags: Technology, palmpre, sprint
WordPress-Post-ID: 838
WordPress-Post-Type: post

Just caught this advert on an [Engadget article][]. What a depressing
"whoops."

![Get ready to live in the now][]

  [Engadget article]: http://www.engadget.com/2009/05/20/sprint-ceo-expects-palm-pre-shortages-sleeping-bag-sales-skyroc/
  [Get ready to live in the now]: /media/2009/05/june6.png
