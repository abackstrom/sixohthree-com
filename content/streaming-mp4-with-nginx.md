Title: Streaming MP4 with Nginx
Slug: streaming-mp4-with-nginx
Summary: For my later reference, here's a guide to preparing MPEG-4 video for web streaming.
Date: 2013-09-02 00:05
Tags: video, term

Ages ago, I set up a mini video server in Nginx using [JW Player][5], testing
out its [pseudo-streaming][6] support. Revisiting this, HTML5 video seems to
have picked up the slack and seeking in large videos is now possible without
depending on Flash. Even with client improvements, the video files themselves
can be the limiting factor and a poorly-structured file will prevent streaming.

For my later reference, here's a guide to preparing MPEG-4 video for web
streaming.

## "Fast Start" and the moov Atom

To stream web video most effectively, the MPEG-4 "moov" atom must be [located at
the beginning][2] of the file. This allows for seeking through the movie before
the whole file is downloaded, requiring less processing and data transfer to the
client. This method works for browsers that can play MPEG-4 natively (like
Google Chrome) and the Nginx [ngx\_http\_mp4\_module][4] pseudo-streaming module
that works in conjunction with Flash players.

[AtomicParsley][3] can inspect the movie container and tell you where the moov
atom is located inside the file.

    sudo apt-get install atomicparsley

Use "test" mode (-T) to output the atom structure:

    AtomicParsley input.mp4 -T | less

"Atom moov" should be located at the top, just after "Atom ftyp." If it comes
after "Atom mdat", your moov atom is after the raw audio/video data, and your
video is not optimized for streaming.

To relocate the moov atom, use `ffmpeg` or `avconv` to build a new .mp4 file
without reencoding. Recent Ubuntu seems to have [replaced ffmpeg with
avconv][1], so here's how I did it:

    avconv -i input.mp4 -movflags faststart -c:v copy -c:a copy output.mp4

This should work for ffmepg:

    ffmpeg -i input.mp4 -movflags faststart -vcodec copy -acodec copy output.mp4

## Other notes

You may need to reencode your video as h.264 in order to stream. (I don't have a
link for this one, but my test video would not stream until I reencoded from
XVID "Advanced Simple Profile" to h.264.) Use `avprobe` or `ffprobe` to figure
out which codecs are in play. To reencode, first make sure you have the
appropriate codecs installed:

    sudo apt-get install libavcodec-extra-53

This combination of flags worked for me:

    avconv -i input.mp4 -c:v libx264 -c:a libvo_aacenc -movflags faststart output.mp4

  [1]: http://stackoverflow.com/a/9477756/7675 "Stack Overflow answer detailing ffmpeg/avconv relationship"
  [2]: http://superuser.com/questions/559372/using-ffmpeg-to-locate-moov-atom "Using ffmpeg to locate moov atom"
  [3]: http://atomicparsley.sourceforge.net/ "AtomicParsley homepage"
  [4]: http://nginx.org/en/docs/http/ngx_http_mp4_module.html "Nginx module ngx_http_mp4_module"
  [5]: http://www.longtailvideo.com/jw-player/ "JW Player, an HTML5/JavaScript-powered video player"
  [6]: http://www.longtailvideo.com/support/jw-player/28855/pseudo-streaming-in-flash/ "JW Player pseudo-streaming"
