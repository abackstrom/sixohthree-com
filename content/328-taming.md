Title: Taming Google
Slug: 328/taming
Summary: Down, boy.
Date: 2005-08-26 13:13
Tags: Computers, Scripting, Web
WordPress-Post-ID: 328
WordPress-Post-Type: post

You may not have noticed, but [Google][] uses JavaScript to rewrite link
URLs as you click on search results. The is presumably used for tracking
and statistics and advertising number voodoo. Whatever, I don't care. I
just want it to go away.

If you're running the [Greasemonkey][] extension for [Firefox][], you're
in luck. Greasemonkey allows a surfer to install custom scripts for any
site, effectively letting him customize pages as he sees fit. Remove
content blocks, change colors and fonts, add text, or, drumroll: remove
`onmousedown` events.

Here's the script:

    // By Annika Backstrom <annika@sixohthree.com>
    // Public Domain, 8 August, 2005.
    // Block Google's data mining on search results.

    // find all paragraph tags
    thePs = document.getElementsByTagName("P");

    for(var i = 0; i < thePs.length; i++) {
        theP = thePs[i];
        theLinks = theP.getElementsByTagName("A"); // find anchors

        for(var j = 0; j < theLinks.length; j++) {
            theLinks[j].removeAttribute("onmousedown");
        }
    }


Works well enough for me.

  [Google]: http://www.google.com/
  [Greasemonkey]: http://greasemonkey.mozdev.org/
  [Firefox]: http://www.mozilla.org/products/firefox/
