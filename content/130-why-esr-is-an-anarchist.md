Title: Why ESR is an Anarchist
Slug: 130/why-esr-is-an-anarchist
Date: 2003-01-16 07:35
Tags: Freedom
WordPress-Post-ID: 130
WordPress-Post-Type: post

I just read Eric S. Raymond's <span class="worktitle">[Why I Am An
Anarchist][]</span>. Here's a quote:



... A majority of the people themselves are too easily seduced into
abandoning their own institutional protections against tyranny by the
false promises and poisonous dreams of statist propaganda.



I'd like to have a little more faith in democracy than Raymond puts
forward. Then again, maybe I'm just not cynical enough. I always have
this general feeling that the Bush administration is doing what it
wants, not what the people want. And *that* sure doesn't feel right.



  [Why I Am An Anarchist]: http://www.catb.org/~esr/writings/anarchist.html
