Title: Defining INI Entries for PHP Scripts
Slug: 2139/defining-ini-entries-for-php-scripts
Summary: Mostly useful to passing PHP INI settings to wrapper scripts like phpcs and phpunit.
Date: 2012-11-15 10:29
Tags: Programming, PHP, PHPUnit, PHP_CodeSniffer, scripting
WordPress-Post-ID: 2139
WordPress-Post-Type: post

Any semiregular PHP user knows how to pass INI settings to the command
line interface:

    $ php -d apc.enable_cli=1 script.php

But what about [scripts that are executable][], with a shebang and
`chmod +x`? Here's a simple debug script we'll call `apc-status.php`:

    #!/usr/bin/php
    <?php

    echo json_encode($argv), PHP_EOL;
    echo "apc.enable_cli = ", ini_get('apc.enable_cli'), PHP_EOL;

Normal invocation gives us this:

    $ ./apc-status.php hello
    [".\/apc-status.php","hello"]
    apc.enable_cli = 0

But the `-d` flag doesn't work as we might expect:

    $ ./apc-status.php -d apc.enable_cli=1 hello
    [".\/apc-status.php","-d","apc.enable_cli=1","hello"]
    apc.enable_cli = 0

We can see that `-d apc.enable_cli=1` has made it thorugh to the script,
rather than being absorbed and handled by the PHP interpreter. Stepping
back a bit, this makes sense: the interpreter is defined in the shebang
`#!/usr/bin/php`. All flags after the script name are handed off to the
script.

By calling the `php` binary directly, we can define our custom settings:

    $ php -d apc.enable_cli=1 apc-status.php hello
    ["apc-status.php","hello"]
    apc.enable_cli = 1

Alternately, you could modify the shebang:

    #!/usr/bin/php -d apc.enable_cli=1<?php

Use one of these techniques if you need to pass ini settings to wrapper
scripts like [PHP_CodeSniffer][phpcs]'s `phpcs` runner, or [`phpunit`][phpunit].

  [scripts that are executable]: http://www.php.net/manual/en/features.commandline.usage.php#features.commandline.usage
  [phpcs]: http://pear.php.net/package/PHP_CodeSniffer
  [phpunit]: http://phpunit.de/
