Title: Web Dynamics
Slug: 252/dynamics
Summary: I am officially sock-less.
Date: 2004-09-25 15:24
Tags: Web
WordPress-Post-ID: 252
WordPress-Post-Type: post

If you're into web development, check out the [International Herald
Tribune][]. The site is amazing, from a technological standpoint. (I've
always been so wowed by the tech that I haven't read more than one or
two of the articles.) Got any other sites whose code knocks you off your
feet?



  [International Herald Tribune]: http://www.iht.com/
