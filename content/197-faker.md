Title: A Faker in our Midst
Slug: 197/faker
Summary: Comment spam, fake blogs, and pills, oh my.
Date: 2004-02-04 23:56
Category: Meta
Tags: Spam
WordPress-Post-ID: 197
WordPress-Post-Type: post

A strange comment popped up on my blog today, followed by a
less-than-normal string of events. I'm tired, so I'll let the paragraphs
I wrote when I was coherent speak for themselves:



[![Screenshot of comment.][]][]

Here's the offending comment. Looks like your standard spam comment,

but the link is strange...



  

[![Screenshot of fake blog.][]][]

What's this? An entire fake blog? You can see the fabricated blogroll,
fabricated links, and an array of fabricated posts. But wait, there's
more! It seems to bear a striking resemblance to [another page][]... (In
fact, most of the fake links and posts are identical, as are comments in
the HTML.) I wonder if [Joe Crawford][] would be surprised to hear his
site has been appropriated in the name of comment spam?



  

[![Screenshot of the fake blog.][]][]

All the links on the fake blog have been removed, save for this one at
the bottom of the page. It points to the root apahc.org website. We've
gone this far, why not follow?



  

[![Screenshot of apahc.org][]][]

Aha! We've gotten to the root of their

little scheme. The fake blog was a ruse to drive links to their website

where they can peddle their.. desperate photovoltaic cell data... that

overdrew some gray scale. On second thought, we've just hit another
dummy

page. Mousing around shows that there aren't any links on *this*

page, either, save for "Clients List," which redirects to a URL at

health-infosheet.com.



  

[![Screenshot of pillsexpert.com sales website.][]][]

After several automatic redirects, it looks like we've finally hit the
last page. (Unless we add something to our shopping cart, of course.)
Just another online pharmacy. At least the trip was exciting.



  

I've left [the offending comment][] online in case others want to do
their own sleuthing. Would anyone wager a guess why the fakers went
through all this trouble? From what I know about Google [PageRank][]
this scheme won't

make their page show up in any more searches. Thoughts?



  [Screenshot of comment.]: /media/2004/02/04/t_comment.png
  [![Screenshot of comment.][]]: /media/2004/02/04/comment.png
  [Screenshot of fake blog.]: /media/2004/02/04/t_fake-blog.png
  [![Screenshot of fake blog.][]]: /media/2004/02/04/fake-blog.png
  [another page]: http://artlung.com/blog/-2002_12_01_archive.php
  [Joe Crawford]: http://artlung.com/blog/
  [Screenshot of the fake blog.]: /media/2004/02/04/t_fake-blog-link.png
  [![Screenshot of the fake blog.][]]: /media/2004/02/04/fake-blog-link.png
  [Screenshot of apahc.org]: /media/2004/02/04/t_fake-website.png
  [![Screenshot of apahc.org][]]: /media/2004/02/04/fake-website.png
  [Screenshot of pillsexpert.com sales website.]: /media/2004/02/04/t_real-website.png
  [![Screenshot of pillsexpert.com sales website.][]]: /media/2004/02/04/real-website.png
  [the offending comment]: http://blogs.bwerp.net/archives/2003/06/30/a_lone_echo_post#c-934
  [PageRank]: http://www.google.com/technology/
