Title: Mmm.. Web Development
Slug: 135/mmm-web-development
Summary: Not terribly interesting to most of you.
Date: 2003-02-13 00:10
Tags: Computers
WordPress-Post-ID: 135
WordPress-Post-Type: post

Last Tuesday's XML class sparked my interest in content management for
websites, and I spent a few hours today setting up a new system for [my
domain][]. I can now update content in a directory on my local machine,
use XWeb to apply stylesheets to the bare content and populate my local
Apache directory, and then rsync on the main web server to update the
public content.



Not terribly interesting to most of you, but it has a high geek factor.



So, now I've had a little experience running concurrent development and
production web servers, and using a content-management system to
organize documents and simplify site-wide updates. I'll probably make a
page describing the process, and I'm wont to do.



  [my domain]: http://www.bwerp.net/
