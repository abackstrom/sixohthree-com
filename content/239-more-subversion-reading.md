Title: More Subversion Reading
Slug: 239/more-subversion-reading
Summary: You know you love it.
Date: 2004-08-19 21:07
Tags: Computers, Subversion, svn
WordPress-Post-ID: 239
WordPress-Post-Type: post

[ONLamp.com][]: [The Top Ten Subversion Tips for CVS Users][], by Brian
W. Fitzpatrick. The more I use Subversion, the more I like it.
Similarly, the more I use Subversion, the less I like CVS. I've almost
finished converting my CVS repositories to SVN. The day I finish will be
a personal holiday, which I will celebrate by spinning in a circle until
I fall and hit my head on something hard, in memory of how CVS made me
feel.

OK, that's a little harsh, but the CVS model could learn a lot from SVN.

  [ONLamp.com]: http://www.onlamp.com/
  [The Top Ten Subversion Tips for CVS Users]: http://www.onlamp.com/pub/a/onlamp/2004/08/19/subversiontips.html
    "Tip one: stop using CVS"
