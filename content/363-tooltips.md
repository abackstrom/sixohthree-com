Title: World of Warcraft: Ajax item tooltips
Slug: 363/tooltips
Summary: Geeky navel gazing ahead.
Date: 2006-03-27 11:49
Tags: world of warcraft
Category: Games
WordPress-Post-ID: 363
WordPress-Post-Type: post

Update 16 April 2010: I have created a drop-in extension,
[wowhead-mediawiki][], to easily create Wowhead tooltips and links
within Mediawiki.

World of Warcraft uses a floating tooltip to display detailed
information about items and armor. The convention and style is
duplicated on many websites, including the [Warcraft website][] itself.
I've wanted tooltips on the [Apocalypse][] site, but there were several
criteria that needed to be met:

-   Some form of data mining had to be implemented.
-   The data had to be available to hand-written pages, dynamic pages,
    MediaWiki, and phpBB.
-   It had to be nifty.

### Mining for Tooltips

Most WoW veterans are familiar with [Thottbot][]. The Thottbot addon
uploads in-game data to a third-party server, giving players a
searchable database of items, recipes, bosses, quests, and just about
everything else in the game. The [item pages][] are modeled around the
Warcraft tooltip style. This information is about as comprehensive as it
gets, so that means the hard part is done: data has already made it out
of Warcraft, and is just waiting for a script to pick it up.

Using a combination of the Curl library (for fetching web pages) and the
Document Object Model methods (for parsing XML and HTML files), a
Thottbot page can be dissected and the "tooltip" extracted. This data
can then be stored in a local database to lessen the load on external
servers.

### Show what you Know

It turns out the World of Warcraft website uses tooltip functions from
the folks over at [Dynamic Drive][]. All that's left is to populate the
tooltip with cached data from Thottbot. We have the tooltips in our
database, but MediaWiki and phpBB don't have access. So, Ajax enters
from stage right.

Ajax lets us offload the tooltip functionality, making some way to hook
into JavaScript events the only prerequisite. Through the [Yahoo! UI
library][] we can abstract our database SELECT, making the data
available through [an HTTP request][]. JavaScript lets us rewrite parts
of our document on the fly, enabling us to update the tooltip for the
current item and moving the tooltip to follow the cursor.

Through Ajax, [large item tables][] become relatively painless. A custom
MediaWiki plugin can create new wiki tags, such as the `<thottbot>` tag
used in the previous example:

    *<thottbot rarity="legendary">Bindings of the Windseeker</thottbot> Keridwen
    *<thottbot rarity="epic">Arcanist Bindings</thottbot> Jackstroud
    *<thottbot rarity="epic">Felheart Belt</thottbot> Osynling
    *<thottbot rarity="epic">Felheart Shoulder Pads</thottbot> Dakkota

The whole process becomes seamless. An author creates a link. A reader
mouses over the link, generating an XMLHttpRequest to fill the tooltip.
The request causes a PHP script to consult the local database. If the
data has not been cached, Thottbot is parsed. The final data is returned
as a block of HTML, which is then inserted into the tooltip. Data is
cached in a local JavaScript variable, preventing futher Ajax connection
requests for the same item on the same page.

Easy.

  [wowhead-mediawiki]: http://wiki.sixohthree.com/wiki/Wowhead_tooltips
  [Warcraft website]: http://www.worldofwarcraft.com/info/basics/factions/thorium/
  [Apocalypse]: http://www.apocalypseguild.info/
  [Thottbot]: http://thottbot.com/
  [item pages]: http://thottbot.com/?i=35667
  [Dynamic Drive]: http://www.dynamicdrive.com/
  [Yahoo! UI library]: http://developer.yahoo.com/yui/
  [an HTTP request]: http://www.apocalypseguild.info/wow-includes/post/thott-item.php?id=Dreamfoil
  [large item tables]: http://www.apocalypseguild.info/main/Raid_Items/March-2006
