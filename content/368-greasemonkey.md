Title: Greasemonkey & Opera
Slug: 368/greasemonkey
Summary: It's not just for Firefox anymore.
Date: 2006-05-10 10:28
Tags: Personal
WordPress-Post-ID: 368
WordPress-Post-Type: post

Some of you may be familiar with [Greasemonkey][], an extension for
[Firefox][] for adding custom JavaScript to any page. (The short version
meaning you can restyle and modify pages to your liking with a bit of
work.) I haven't been using Firefox much, myself, having been bitten by
the [Opera][] bug. As a result, I've been missing out on one of my
favorite browser mods.

Turns out, that needn't be the case. Opera has [supported Greasemonkey
scripts][] since v8.0. I knew that the browser had its own version of
"User JavaScript," but not only do most GM scripts work natively, but
the browser recognizes the inclusion/exclusion syntax. Nifty.

Hopefully I'll be sharing some of my scripts in the near future, as well
as writing more. For starters, here's [an irreverent script][] for use
on the World of Warcraft community forums, recently updated for Opera
compatibility.

  [Greasemonkey]: http://greasemonkey.mozdev.org
  [Firefox]: http://www.mozilla.com/firefox/
  [Opera]: http://www.opera.com/
  [supported Greasemonkey scripts]: http://www.opera.com/support/tutorials/userjs/
  [an irreverent script]: http://forums.worldofwarcraft.com/thread.aspx?fn=wow-off-topic&t=1559556
