Title: Automatic Ellipsis with CSS
Slug: 1662/automatic-ellipsis-with-css
Summary: How do I keep forgetting about <code>text-overflow: ellipsis</code>? Maybe a blog post will help cement it in my memory.
Date: 2011-10-16 21:33
Tags: Web, css
WordPress-Post-ID: 1662
WordPress-Post-Type: post

How do I keep forgetting about `text-overflow: ellipsis`? Maybe a blog
post will help cement it in my memory.

{@style=border: 1px solid #000; width: 10em; padding: 0.2em; text-overflow: ellipsis; overflow: hidden;}
{@title=I am the very model of a modern major general.}
<span style="white-space: nowrap">I am the very model of a modern major general.</span>
