Title: Quote Test
Slug: 336/quote
Summary: Testing, one, two.
Date: 2005-09-08 13:48
Tags: Blogging
WordPress-Post-ID: 336
WordPress-Post-Type: post

WordPress seems to be doing some weird stuff with quote characters:
"test."

That looks fine. What about this:


    "Bird" is the word.



Odd, it's escaping quotes. Just \<code\>:

`"How are you?" she questioned.`

Now just \<pre\>:


    "Fine," he answered.



So, quote characters are not unescaped if they live in a preformatted
text block.
