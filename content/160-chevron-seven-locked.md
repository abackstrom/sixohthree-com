Title: Chevron Seven Locked
Slug: 160/chevron-seven-locked
Date: 2003-06-12 00:07
Tags: Personal
WordPress-Post-ID: 160
WordPress-Post-Type: post

Friday. 8:00. SCI FI. [Stargate SG-1][]. Don't miss it. I know where
*I'll* be.



The three-hour block will start off with Stargate: The Lowdown, followed
by the two-hour season premiere. I've been big into SG1 lately. Used to
watch it back on HBO. How long ago was *that*?



So, anyway, it's almost here. If you're into science fiction, I
recommend checking it out. This concludes our public service
announcement.



  [Stargate SG-1]: http://www.scifi.com/stargate/
