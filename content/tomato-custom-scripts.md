Title: Debugging Tomato Router Custom Scripts
Slug: tomato-router-custom-scripts
Summary: I found this to be non-obvious.
Date: 2014-08-06 11:19
Tags: linux
Status: draft

I found the operation and debugging of Tomato custom script non-obvious.

Script "Custom 1" is dumped into /tmp/sch_c1_cmd.sh, prepended with #!/bin/sh.

`cru` (the Tomato cron) calls `sched sch_c1` at the configured times. You can
inspect this with `cru l`:

    root@unknown:/tmp# cru l
    19 7 * * * /usr/sbin/tomatoanon checkver #checkver#
    19 7 * * 1 /usr/sbin/tomatoanon #anonupdate#
    18 16,20,0,4,8,12 * * * ntpsync --cron #ntpsync#
    0 */1 * * * logger -p syslog.info -- -- MARK -- #syslogdmark#
    */1 * * * 0,1,2,3,4,5,6 sched sch_c1 #sch_c1#

Scheduler logging can be enabled in Adminstration > Logging, look for
"Scheduler." The messages aren't that useful:

    Aug  6 11:22:01 unknown cron.info crond[15493]: crond: USER root pid 16322 cmd sched sch_c1
    Aug  6 11:22:01 unknown user.info sched[16323]: Performing scheduled custom #1...

Instead, redirect output from your script somewhere:

    ssh foo@host some_remote_command &>/tmp/sch_c1.log

In my case, ssh was dying with:

    /usr/bin/ssh: Failed reading termmodes

Adding the `-T` flag fixed my script.
