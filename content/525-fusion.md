Title: VMware Fusion 2
Slug: 525/fusion
Summary: Digest version: it's sweet.
Date: 2008-09-02 20:35
Tags: Linux, Mac OS X, ubuntu, virtualization, vmware, vmwarefusion
WordPress-Post-ID: 525
WordPress-Post-Type: post

I bit the bullet and downloaded the VMware Fusion 2 release candidate
today. For completeness, I also trashed my Ubuntu 7.04 Server virtual
machine and installed Ubuntu 8.04 Desktop. Here's some thoughts after a
few hours of use.

### Installation


During the machine setup, I was prompted to do a "Linux Easy Install."
The software auto-populated my full name and username, and asked me for
a new password. I let it do its thing, having already pointed it to the
Ubuntu installation ISO. A short while later, with no additional
interaction, I was at the Gnome login screen. The username and password
I had chosen during installation worked great.

### Interface


![Screenshot of Linux apps running in seamless mode][seamless]

The feature that finally pushed me over the 2.0 edge was Unity
support in Linux. In the past I have only run server applications on my
VM, but I had to see Linux Unity in action, layering my Linux
applications with Mac OS X applications. It doesn't disappoint.

At the other end of the spectrum is "Headless" mode. I've been waiting
for this for a long time. When I'm using the server capabilities of my
VM, I don't want an extra application cluttering up my alt-tab. Now:
"Enter Headless," quit VMWare Fusion. Simple. I assume the VM will
suspend if I shut down the computer, but I haven't tested it yet.

![Screenshot of the Virtual Machine Library][vm library]

The new library window is a bit more attractive, and the
machine settings list is more compact. The list also shows a live
thumbnail of your VM's display, be it console or graphical. Along the
same lines, the console display can now be resized to zoom the terminal
text in or out.

![Screenshot of Gnome's app launch menu in the Mac OS X Dock][launcher]

The Gnome application menu is also accessible from the Mac OS
X Dock. Apps can be launched in Unity mode without restoring the Gnome
desktop.

Seamless cursor movement between the Linux and Mac desktop, along with
VM resolution adjusting to the size of the machine window, are also
great features, though I suspect they were present in Fusion 1.0.

### Some Other Section

Looks like the VMware Fusion team has been up to good things since the
first release. I haven't yet tried Parallels, but I can't see a reason
to try anything new from where I'm sitting.

  [seamless]: /media/2008/09/unity.jpg
  [vm library]: /media/2008/09/library.jpg
  [launcher]: /media/2008/09/launcher.jpg
