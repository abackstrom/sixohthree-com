Title: Anyone for static Warcraft?
Slug: 354/static
Summary: Hooray for hard-to-organize things.
Date: 2005-12-30 17:29
Tags: world of warcraft
Category: Games
WordPress-Post-ID: 354
WordPress-Post-Type: post

Over in Final Fantasy XI, we had a thing called a "static," essentially
a group of characters who were only played when the whole group was
assembled. Karl and Craig and I did an informal version of this on
Burning Legion during Craig's trial period. Justin and I did the same
for a bit before he settled down on Lothar. I'd like to do it again, if
others are interested.

If there's any interest at all, I'm guessing we're looking at 2-4
players. I don't think there's any need for real scheduling, I see this
as something to do for fun when we're not busy on our mains. Somebody
ping me back about this.
