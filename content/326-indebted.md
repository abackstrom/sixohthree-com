Title: In Debt, and Indebted
Slug: 326/indebted
Summary: A quick "Thank you".
Date: 2005-08-24 20:03
Tags: Personal
WordPress-Post-ID: 326
WordPress-Post-Type: post

RIT is expensive, I know. It is expensive to run, and it is expensive to
attend. But as with many institutions, there is a wealth of aid
available.



This is a quick message to C & B Beal, whoever you are, wherever you
are. Your scholarship has no living donors, but through your generosity
I was able to attend college, earn my degree, and experience all those
things that have mattered so much in my short life.



Would that I could have such an impact on someone's life. Thank you.



