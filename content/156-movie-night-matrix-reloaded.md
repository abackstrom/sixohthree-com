Title: Movie Night: Matrix Reloaded
Slug: 156/movie-night-matrix-reloaded
Summary: Excellent flick, I recommend you go see it.
Date: 2003-05-17 03:09
Tags: Movies
WordPress-Post-ID: 156
WordPress-Post-Type: post

Just got back from watching *Matrix: Reloaded* with Karl. Excellent
flick, I recommend you go see it. More comments in the extended entry...



<!--more-->

One of the first things that struck me about *Reloaded* was the
philosophical overtones of many of the scenes. Neo and Counsellor
Harmann discuss their simultaneous war and reliance on machines, noting
that their "power" is really no power at all, since they can not survive
without the life support the machines provide. Later, in his meeting
with the father of the Matrix, Neo says the machines are in almost the
same situation: kill all the humans, and there is no source of power.
Quite a bind for each party, no?



Some of my personal favorite parts of the movie: the ghost scenes, and
Zion. I thought that whole supernatural bit was presented pretty well,
with the idea that programs from older versions of the Matrix sometimes
misbehaved and a few of these were held over into the current version.
(Nice touch with the silver bullets.) Zion did not fail to impress. I'm
a big fan of the details, like the soldier in the battle robot spinning
his guns, and seeing more of the ships up close. Check out the
[Animatrix][] short "Second Renaissance" if you want to see more of
that.



So, it was well worth my \$7.75. Hopefully I'll get to see it again
before *Revolutions* comes out.



  [Animatrix]: http://www.intothematrix.com/
