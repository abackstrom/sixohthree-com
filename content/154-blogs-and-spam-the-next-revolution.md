Title: Blogs and Spam: The Next Revolution
Slug: 154/blogs-and-spam-the-next-revolution
Summary: The first of many.
Date: 2003-05-05 20:51
Tags: Blogging, Spam
WordPress-Post-ID: 154
WordPress-Post-Type: post

A few days ago, Mark Pilgrim replied to comments on his own blog
regarding, "people posting irrelevant links in order to drive traffic to
[their sites]." This topic has been on my mind for some time, and I
recently discovered a [comment][] on my own blog along the same lines,
so I feel the time is right to blog it. (It's the bottom comment; check
the URL his name links to.)



From what I have seen, the majority of blogs trust other blogs
implicitly. Comments are either on, or off; there is no middle ground.
It takes very little effort for a blogger to piggyback of another site's
readership. A comment that took fifteen seconds to post can drive
hundreds of bored sufers from, say, diveintomark.org, to Joe Hacker's
Site for Kewl Linkz. But let's take this a step further: spam-oriented
comments and trackbacks.



Instead, let's say Mark posts about a current problem with his hosting
provider, and mentions the importance of backups. (Sound familiar?)
Along comes a spider, and notices that "backups" are mentioned on the
front page. It grabs a paragraph from its database and fills in the
blanks: "Just read an interesting article about backups from
http://www.diveintomark.org/..." It then sends a trackback ping to
Mark's site, and before you know it, unsuspecting readers are clicking
trough to a site that's selling Joe's Super Backup+. Mark has a day job
(I assume) and gets several dozen comments and pings for each post, so
he doesn't notice the spam trackback for a few hours, or worse, never
notices it at all. Sound far-fetched? I don't think so, either.



The simplest solution is to approve all comments and trackbacks before
they are posted, but that's unappealing even to a casual blogger like
myself. Perhaps we will see webs of trust emerge around comments and
trackbacks, much like those that exist for PGP keyrings. It will be
interesting to see which direction this goes.



In the mean time, if anyone knows a way to discover geographical
locations for IP addresses, I'm all ears. Right, "ip address?"



  [comment]: http://blogs.bwerp.net/archives/2002/12/12/tim_oreilly_on_piracy.php#c-151
