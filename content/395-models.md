Title: World of Warcraft: New Models on the Horizon?
Slug: 395/models
Summary: Unfamiliar faces.
Date: 2007-02-16 07:32
Tags: gaming, world of warcraft
Category: Games
WordPress-Post-ID: 395
WordPress-Post-Type: post

<ins datetime="2009-09-22T15:54-04:00">**Update:** as commenters have
pointed out, these are actually very old models from pre-1.0 World of
Warcraft, so placing them in Old Hillsbrad is just another Blizzard
joke. See [the wayback machine][] for more screenshots.</ins>

Months ago Blizzard representatives confirmed a slight polygon increase
in the Burning Crusade expansion. I hadn't noticed any significant
change, but I've recently come across several new styles of human NPCs.
After two years with the same hairstyles and face textures, the
differences are striking.

[![img1][]][img1big]
[![img2][]][img2big]
[![img3][]][img3big]

The textures have always given some variety to the players with hair
being the most dramatic difference, but these new styles are a far cry
from what we've grown accustomed to.

[![compare][]][comparebig]

So, this raises the question: are the new styles restricted to NPCs, an
attempt to differentiate players from non-players? Is Blizzard expanding
player model choices, but hasn't yet incorporated the changes into the
character creation screen? Are they adding the salon people have been
asking about for so long? I think we'll find out soon.

  [the wayback machine]: http://web.archive.org/web/20031124092215/http://www.blizzard.com/wow/townhall/
  [img1]: /media/2007/02/16/female1_tn.jpg
  [img1big]: /media/2007/02/16/female1.jpg
  [img2]: /media/2007/02/16/female2_tn.jpg
  [img2big]: /media/2007/02/16/female2.jpg
  [img3]: /media/2007/02/16/male1_tn.jpg
  [img3big]: /media/2007/02/16/male1.jpg
  [compare]: /media/2007/02/16/female_comparison_tn.jpg
  [comparebig]: /media/2007/02/16/female_comparison.jpg
