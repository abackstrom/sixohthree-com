Title: A Lone Echo Post
Slug: 165/a-lone-echo-post
Date: 2003-06-30 19:48
Tags: Web, atom, Echo, syndication
WordPress-Post-ID: 165
WordPress-Post-Type: post

There has been quite a bit of talk lately regarding [Echo][], which aims
to replace RSS as the syndication de facto standard. This drive for a
replacement is due in no small part to the politics around RSS, and what
seems to be a general distaste for [Dave Winer][], father of RSS.



Really, I don't know all the details, and honestly, it doesn't really
make much of a difference to me at this point. I don't subscribe to any
RSS feeds, and the only reason they exist for my blog is because they're
part of the default Movable Type installation. But Dave [called for
support][] of RSS, so here's what he gets:



**If and when the Echo spec is finalized, I will support and publish
Echo-formatted feeds, and *only* Echo-formatted feeds.** If and when.



Echo is young. It has some powerful minds behind it, but there's still a
long road ahead. I have no doubts that a final product from this group
of people will be complete and well thought out. So, here's to the
developers. See you at the crossroads.



  [Echo]: http://www.intertwingly.net/wiki/pie/
  [Dave Winer]: http://www.scripting.com/
  [called for support]: http://scriptingnews.userland.com/2003/06/29#When:4:25:49PM
