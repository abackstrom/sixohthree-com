Title: CompactFlash Price Drops
Slug: 254/cf
Summary: When did CompactFlash cards get so cheap?
Date: 2004-10-07 14:24
Tags: Computers, Photography
WordPress-Post-ID: 254
WordPress-Post-Type: post

When did CompactFlash cards get so cheap? I'm seeing [super-fast 1GB
cards from Lexar][] for under \$100. It might be time to upgrade, mmm.



  [super-fast 1GB cards from Lexar]: http://www.macmall.com/macmall/shop/detail.asp?dpno=491666
