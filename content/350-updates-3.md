Title: Fall Updates, 2005 Edition
Slug: 350/updates-3
Summary: Not much going on, really.
Date: 2005-10-24 09:11
Tags: Games, Personal, Wiki
WordPress-Post-ID: 350
WordPress-Post-Type: post

Only light blogging recently. I've finally fixed my Windows XP PC, Aziz,
and I've been playing World of Warcraft when I have free time. I'm
pushing towards level 60 in an effort to see some endgame content at
last. Someday, someday...

Meanwhile, [the wiki][] is being hit by [spammers][] fairly regularly.
Mostly open proxies, from the look of it. There are 27 unique IPs in the
block list right now. If you have a MediaWiki installation of your own,
keep your eyes open for spam.

  [the wiki]: http://wiki.bwerp.net/
  [spammers]: http://wiki.bwerp.net/Special:Log/block
