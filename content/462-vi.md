Title: Vim, Terminal.app, and the Delete key
Slug: 462/vi
Summary: Vim rulse^H^Hes.
Date: 2008-07-09 08:55
Tags: Personal, Bash, delete, Mac OS X, screen, term, termcap, terminal.app
WordPress-Post-ID: 462
WordPress-Post-Type: post

One of my SSH servers continually gives me problems with the Mac
"Delete" key in Vim. While the key always works correctly in bash, it
will only work in Vim if I am inside a [screen][] session. If I have not
opened screen, Delete will function as "Forward Delete." Oddly, the key
is sending `^H` in both cases. This turned out to be a termcode issue.

### Fixing by Setting \$TERM


I failed to fix this with `~/.inputrc`, but did have success after
tweaking `$TERM`. Initially my `$TERM=xterm-color`, which fails. When
`$TERM=screen`, Delete works. Setting `$TERM=xterm` works, as well,
which means the following in `~/.bashrc` can be used as a fix:


    if [ "$TERM" = "xterm-color" ]; then
        export TERM=xterm
    fi



### Fixing by Remapping Delete


A second solution is to change Vim's [terminal options][].
`:set termcap` will show you what t\_kD (`<Del>`) is currently set to.
When I'm using `TERM=xterm-color`, that value is `^H`. Remapping t\_kD
in .vimrc makes Delete consistent across `$TERM` settings:


    set t_kD=<Ctrl-v><fn-Delete>


Use the modifier keys Ctrl-v and fn-Delete, which will expand to
`set t_kD=^[[3~`. Best practices would probably explicitly set t\_kb
(backspace) as well.

  [screen]: http://en.wikipedia.org/wiki/GNU_Screen
  [terminal options]: http://www.vim.org/htmldoc/term.html#terminal-options
