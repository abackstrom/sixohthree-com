Title: CS:S Physics Fun
Slug: 263/physics
Summary: Lookit them cabinets fly!
Date: 2004-11-04 21:47
Tags: Games
WordPress-Post-ID: 263
WordPress-Post-Type: post

Counter-Strike: Source lets server admins determine how strongly a
bullet's impact will affect the environment. Essentially, it is to
shooting an object what low gravity is to jumping. Alter this variable,
and stuff flies across the screen when you shoot it. It's incredibly
silly, and I love it do death.



I recorded [a demo][] of myself playing on a server with this setting
altered. (You can't watch the demo without CS:S, sorry.) The server was
a bit laggy and it shows in the demo, but you can watch some pretty
crazy physics and vicariously experience my amazing headshot ability.
Enjoy.



  [a demo]: /media/2004/11/04/fun_physics.zip
