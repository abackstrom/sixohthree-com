Title: Subversion: Missing Features
Slug: 972/subversion-missing-features
Date: 2010-01-25 12:31
Tags: Computers, git, Subversion, svn
WordPress-Post-ID: 972
WordPress-Post-Type: post

I love many things about [git][], but I use [Subversion][] at my day
job. Two git features please me enough that I took the time to build
them into my svn workflow: paged diffs and "commit -v." As of 15
September 2011, this script will also diff properties using the command
`svn propdiff PROPNAME`.

<script src="https://gist.github.com/abackstrom/1219371.js?file=svn"></script>

This creates an `svn less` subcommand that runs `svn diff` with any
other arguments you provided, paging the result through `less(1)`. It
also provides a custom environment variable to `svn ci`, instructing Vim
to split screen, set some options suitable for a read-only diff, and run
`svn diff` on all files included in the default commit message, i.e. the
list of changed files. Finally, it wraps `svn propdiff` to highlight
modifications to a property.

Any unrecognized subcommands (update, checkout, etc.) are passed through
unchanged. The automatic diff is only applied to the `ci` subcommand, so
the `svn commit` variant can be used when a diff is not required.

Installation
------------



You can quickly grab this script with curl or wget:


    bash$ curl https://raw.github.com/gist/1219371/svn > ~/bin/svn



Or similar.

  [git]: http://git-scm.com/
  [Subversion]: http://subversion.tigris.org/
