Title: 2018 Retrospective
Slug: 2018-retrospective
Summary: As is tradition, I find myself thinking about the past
    year and future plans over the December holiday.
Date: 2018-12-30 16:58
Category: Personal
Tags: new year, irresolutions

As is [tradition](/tag/irresolutions), I find myself thinking about the past
year and future plans over the December holiday.

## Social Networking

In 2018, I ditched all my big social accounts. I deleted my Twitter accounts in
August, Facebook in November, and this month deleted my little-used Instagram
account. (Instagram was acquired by Facebook in 2012.) I've been all-in on
[Mastodon](https://joinmastodon.org/) since 2017, creating my first account in
March before settling at [@annika@xoxo.zone](https://xoxo.zone/@annika) the
following month.

The incentives for the big social networks are all wrong, and I think that sort
of centralization weakens the web. I would rather see a diverse ecosystem of
smaller interconnected servers, than these large ad-supported databases that
prey on our worst impulses. I plan to keep advocating for the Fediverse and
similar alternatives in 2019.

## Shopping

I severely cut back on my Amazon purchasing this year. I live in a rural area and
delivery services overall hold great appeal, but it's harder and harder to
justify supporting Amazon. It's easy to mindlessly purchase everything from
Amazon, but with a little effort, I'm finding that other shops are very
competitive on price and delivery time.

Ideally, I could completely eliminate Amazon delivery in 2019. We still have
Kindle readers, but I'm looking into [Kobo](https://www.kobo.com/) as an
alternative. I'm a heavy [Twitch](https://twitch.tv) user (Amazon bought Twitch
in 2014) and I'm stuck with that for a long while, but I hope to stop Amazon
Prime next year and I don't *think* I would miss any Twitch benefits.

## Mobile Phone

This actually happened near the end of 2017, but I made the jump from Apple iOS
(iPhone) to Google Android (Samsung Galaxy Note8). Google's not *much* better
than Apple in a lot of regards, but at least the app store ecosystem is more
diverse.

There are some open-source mobiles on the market, and I'll continue to watch
them in 2019.

## Email

Gmail (via Google Apps) hosted my email for years. Their dashboard says I created my account
in 2008, but I don't have records of when Gmail became my primary. I think I was
still using self-hosted qmail before that move.

In 2018, I migrated from Gmail to
[Fastmail](https://www.fastmail.com/?STKI=19632000) (referral link). It's been a
very positive experience, I really have nothing but good things to say about
Fastmail.

## Web Search

Around the same time as the Fastmail move, I changed my primary search engine
back to [DuckDuckGo](https://duckduckgo.com). I tried to do this years ago, but
DDG just wasn't there yet. This time around, the change has stuck. I miss Google
about 5% of the time, and when I need it, Google (and hundreds of other
searches) are just a [bang](https://duckduckgo.com/bang) away.

## Final Thoughts

In 2019, I'll continue to look for alternatives that are better aligned with my
own values. For gaming, I'm hoping we'll see more DRM-free titles on Steam
alternatives like [Kartridge](https://www.kartridge.com/) and
[Itch.io](https://itch.io/). Music consumption also leaves room for improvement.
Maybe this means more consistent scrobbling from Spotify to
[Last.fm](https://www.last.fm/) and making sure I buy DRM-free or physical
copies of songs in heavy rotation.
