Title: Server Move
Slug: 390/move
Summary: For real this time.
Date: 2007-01-05 13:54
Category: Meta
WordPress-Post-ID: 390
WordPress-Post-Type: post

If you can read this, you're seeing dotdotdot on its new server. If you
saw the first version of this message, you were actually seeing
dotdotdot on its old server. Heh.

More on the server move later.
