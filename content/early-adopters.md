Title: Early Adoption
Slug: early-adoption
Summary: Quick shout out to mth-ci-runner-1, which was GitLab CI runner #22.
Date: 2016-03-16 22:33
Category: Technology
Tags: GitLab

Quick shout out to mth-ci-runner-1, which was GitLab CI runner #22. You were a
good one, but we had to upgrade!

![Screenshot showing Test Runner #22](/media/2016/03/mth-test-runner.png){ .border }
