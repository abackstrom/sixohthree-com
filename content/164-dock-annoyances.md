Title: Dock Annoyances
Slug: 164/dock-annoyances
Summary: "An application should always present a window when its Dock icon is clicked." Does anyone else find this feature slightly annoying?
Date: 2003-06-29 21:43
Tags: Computers, Mac OS X, Rants
WordPress-Post-ID: 164
WordPress-Post-Type: post

From [The Dock][], at developer.apple.com:




> An application should always present a window when its Dock icon is
> clicked. This behavior compensates for the all-too-familiar case where
> a user closes a document window and thinks she has quit the
> application. The next time the user wants to use the application,
> indicated by a click on its Dock icon, a new window opens so that the
> user is absolutely sure that the application is active.




Does anyone else find this feature slightly annoying? Maybe I'm just
used to Mac OS 9, but I don't appreciate applications opening a window
without my say so. Maybe I don't want a huge browser window to pop up
every time I switch to Safari. Maybe I switched to TextEdit for the open
dialog, *not* a new blank document. This behavior is somehow intuitive?
"All-too-familiar case," indeed.



  [The Dock]: http://developer.apple.com/ue/aqua/dock.html
