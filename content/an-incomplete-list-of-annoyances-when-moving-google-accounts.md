Title: An Incomplete List of Annoyances When Moving Google Accounts
Slug: an-incomplete-list-of-annoyances-when-moving-google-accounts
Summary: summary
Date: 2018-06-16 11:15
Category: Personal
Tags: tags
Status: draft

I've just renamed my 11 year old GSuite (formerly Google Apps) account so that I
can recreate the account as a standard Google account. Here's how it's gone so
far.

## The Basics

I was able to rename the GSuite account (which is the only admin account), which
creates an alias for the old email address. Deleting the alias freed up the
email for a new Google account, which I created through the Google home page.

My Android phone immediately complained about backups stopping and a few other
things.

## Google Voice

I was able to transfer my Google Voice number over to the new account. For some
reason it wouldn't work until I opened the Google Voice site on the new account.
(I didn't set up a new number on the new account, as I believe that makes the
new account ineligible for transfer.)

The phone number (my carrier number) wouldn't verify as a text message. I had to
use the "call my phone" verification process.

## YouTube

I lost all my Liked videos (which you can't export) and subscriptions. Annoying.

## Google Play Store

I don't yet understand the full extent of this. Presumably any apps I purchased
under the GSuite account will continue to be associated with that account, and
I'll have to sign into GSuite to download those apps in the future.


<!-- links -->

  [1]: 
  [2]: 
  [3]: 
  [4]: 
  [5]: 
  [6]: 
  [7]: 

