Title: OK, Real Quick...
Slug: 139/ok-real-quick
Date: 2003-03-04 00:32
Tags: Personal
WordPress-Post-ID: 139
WordPress-Post-Type: post

From [an article][] on [Kuro5hin.org][]:



Music sales in Germany have been in decline for the last five years,
dropping 11% last year, and German copyright holders seem to think
digital copying might have something to do with it. As Gerd Gebhardt,
president of Bundesverband Phono (Germany's RIAA), asked: "If it were
possible tomorrow to copy bread rolls, does anyone doubt that that would
mean a major economic problem for the baking industry?"



Well, we wouldn't want a major economic problem for the baking industry,
even if it meant that *nobody ever died of hunger again*. This is a
*terrible* analogy. Do people have no sense?



I'm going to bed. I've seen enough for one day.



  [an article]: http://www.kuro5hin.org/story/2003/3/3/12522/38395
  [Kuro5hin.org]: http://www.kuro5hin.org/
