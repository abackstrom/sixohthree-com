Title: Linux Trucking
Slug: 200/penguin
Summary: Open-source benefits all.
Date: 2004-02-26 23:21
Tags: Linux
WordPress-Post-ID: 200
WordPress-Post-Type: post

![Photograph of a utility truck with Tux, the Linux penguin, emblazoned
on the side][]

Spotted on campus on this cold winter morning. I wonder if they thanked
[Larry][].

  [Photograph of a utility truck with Tux, the Linux penguin, emblazoned on the side]: /media/2004/02/26/linux-trucking.jpg
  [Larry]: http://www.isc.tamu.edu/~lewing/linux/
