Title: Feminist Frequency: Twitter, Conspiracies, and Information Cascades
Slug: feminist-frequency-twitter-conspiracy-theories
Summary: Anita's response to Twitter Trust & Safety Council conspiracy theories.
Date: 2016-02-27 18:11
Tags: twitter, feminism

> It’s as if I’m a folk demon and this is yet another horror story people whisper
to each other about me and, by extension, about what effects feminism may have
on our culture if this imaginary menace is allowed to spread.

Anita Sarkeesian, [On Twitter, Conspiracy Theories, and Information Cascades][1], February 22, 2016.

  [1]: http://feministfrequency.com/2016/02/22/on-twitter-conspiracy-theories-and-information-cascades/
