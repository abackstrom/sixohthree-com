Title: Lonely, Lonely Blog
Slug: 129/lonely-lonely-blog
Summary: For all you <a href="http://www.rit.edu/~macrit/">MacRITters</a>, I've set up a new virtual host on my old iBook. The <a href="http://macrit.bwerp.net/forum/">MacRIT Forum</a> is now housed on the same server as this blog.
Date: 2003-01-12 18:28
Tags: Personal
WordPress-Post-ID: 129
WordPress-Post-Type: post

I've been ignoring my blog for the past few days, so I thought I'd sit
down and feed MovableType.



For all you [MacRITters][], I've set up a new virtual host on my old
iBook. The [MacRIT Forum][] is now housed on the same server as this
blog. Main reasons for moving: excessive downtime on the main bwerp.net
server, and no CGI ability on that server either. Along with the forum,
I'll also be installing a fresh copy of MT for the meeting minutes. It's
not quite ready yet, though.



That's all the news that's fit to print.. Hope you enjoyed the weekend,
'cause there it goes.



  [MacRITters]: http://www.rit.edu/~macrit/
  [MacRIT Forum]: http://macrit.bwerp.net/forum/
