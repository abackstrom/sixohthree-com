Title: You Are Being Lied To
Slug: 119/you-are-being-lied-to
Date: 2002-12-22 22:13
Tags: Freedom
WordPress-Post-ID: 119
WordPress-Post-Type: post

By whom? Anybody that justifies the loss of a few basic freedoms for so
called "national security." Benjamin Franklin said it best: "They that
can give up essential liberty to obtain a little temporary safety
deserve neither liberty nor safety."

[LewRockwell.com][] has an article by Nicholas Monahan, entitled
[Coffee, Tea, or Should We Feel Your Pregnant Wife's Breasts Before
Throwing You in a Cell at the Airport and Then Lying About Why We Put
You There?][coffee-tea] Pompus, patronizing security officers, wild, inaccurate
accusations, and confiscated scissors from a hygine bag. I'm glad those
in charge of the safety of free people everywhere are so dedicated to
their cause.

Watch out: your civil liberties are in jeopardy.

  [LewRockwell.com]: http://www.lewrockwell.com/
  [coffee-tea]: http://www.lewrockwell.com/orig3/monahan1.html
