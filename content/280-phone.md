Title: Free Fone, Friend
Slug: 280/phone
Summary: VoIP me.
Date: 2004-12-12 23:14
Tags: Computers
WordPress-Post-ID: 280
WordPress-Post-Type: post

Has anyone else taken an interest in [Skype][]? It's been on my radar
for a couple months and I have an account, but I don't know of anyone
else that uses it. For the uninitiated among you, it boils down to free
"phone calls" using your computer and Internet connection. That means
you're tied to the computer and a headset unless you have some sort of
hands-free kit, but hey, the price is right.

I've ordered a [Logitech© Internet Chat Headset][] from [ThinkGeek][],
and I'll be making an effort to stay in Skype when I'm around the
computer. If anyone wants to try it out, feel free to give me a ring.
I'll be the only Annika Backstrom in the user list.

  [Skype]: http://www.skype.com/
  [Logitech© Internet Chat Headset]: http://www.logitech.com/index.cfm/products/details/US/EN,CRID=103,CONTENTID=6339
  [ThinkGeek]: http://www.thinkgeek.com/
