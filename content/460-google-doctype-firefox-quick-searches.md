Title: Google Doctype: Firefox Quick Searches
Slug: 460/google-doctype-firefox-quick-searches
Summary: Easy is good.
Date: 2008-07-06 18:01
Tags: Personal
WordPress-Post-ID: 460
WordPress-Post-Type: post

In order to make something a habit, it helps to make that thing easy to
do. I was impressed with [Google Doctype][] (both the idea and the
execution), but I often forget about it when the time comes to look
something up. Enter my new set of [Firefox quick searches][]. Using some
intermediary PHP, I can turn a quick search for "html ins" into
`http://code.google.com/p/doctype/wiki/InsElement`. Likewise for DOM and
CSS pages. Hopefully that will make Doctype more present in my
day-to-day coding.

A good one-up would be a Doctype sidebar, akin to Edgewall's [Python
Sidebar][].

  [Google Doctype]: http://code.google.com/doctype/
  [Firefox quick searches]: http://wiki.bwerp.net/Firefox_Quick_Searches
  [Python Sidebar]: http://www.edgewall.org/python-sidebar/
