Title: Movie Night: Star Trek: Nemesis
Slug: 124/movie-night-star-trek-nemesis
Summary: Spoilers ahead, so don't read on unless you've seen the movie.
Date: 2002-12-28 17:22
Tags: Movies
WordPress-Post-ID: 124
WordPress-Post-Type: post

I saw Star Trek: Nemesis with the crew from New Hampshire last night
(well, part of the crew, but I digress). Spoilers ahead, so don't read
on unless you've seen the movie.

<!--more-->

I enjoy new movies in a series like Star Trek because they expand the
series' fictional universe. The writers can build upon the framework
laid down by all authors before them. These new elements alone are
enough to hold my interest in a movie. I enjoyed the Naboo-esque Romulan
architecture, and the planet that rotates at the precise speed needed to
keep one side facing the sun all the time. Stuff like that is fun to
think about.

I was surprised to see that the Enterprise E and her crew were in
military style. It was reminiscent of the Next Generation episode when
the Enterprise C travels through time and the Enterprise D turns into a
military vessel. Karl mentioned something about Enterprise E being built
to fight the Borg. Any other takers on theories?

All in all, I was pleased with Nemesis. I'm *really* upset, borderline
*depressed*, that Data died, though. It was one of those moments, like,
'Take Picard! Spare Data's life!'. Data had so much to live for! His
head didn't spend four hundred years in a cave in San Francisco just to
be blown up a few years later, damnit.

Oh well.. For the rest of us, life goes on.
