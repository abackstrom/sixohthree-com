Title: Better to Just Get High
Slug: 342/piracy
Summary: This is Freudian, somehow.
Date: 2005-09-23 16:01
Tags: Freedom, World
WordPress-Post-ID: 342
WordPress-Post-Type: post

According to the Wikipedia article [Caning in Singapore][], persons
commiting "piracy" or "rape" are subject to the same number of lashings:
"at least 12." Piracy charges are similarly proportioned in the United
States. It's nice to know Singapore takes intellectual property
seriously.

Interestingly, "trafficking in arms" will earn you "at least 6”
lashings.

<span style="font-weight:bold">Update:</span> Read the comments.

  [Caning in Singapore]: http://en.wikipedia.org/wiki/Caning_in_Singapore
