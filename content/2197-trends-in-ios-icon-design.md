Title: Trends in iOS Icon Design
Slug: 2197/trends-in-ios-icon-design
Summary: No comment, other than to remark at the visual similarity between the new Dropbox and Google+ iOS app icons.
Date: 2012-12-17 09:14
Tags: Personal, design, ios
WordPress-Post-ID: 2197
WordPress-Post-Type: post

No comment, other than to remark at the visual similarity between the
new Dropbox and Google+ iOS app icons.

![IMG1685][]

  [IMG1685]: https://sixohthree.com/files/2012/12/IMG_1685.png
