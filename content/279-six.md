Title: Six Degrees
Slug: 279/six
Summary: Everyone is connected somehow, right?
Date: 2004-12-11 01:22
Tags: Blogging, Personal
WordPress-Post-ID: 279
WordPress-Post-Type: post

There is a conversation, I believe it took place in a movie, concerning
family photographs. You look at a photograph taken in a restaurant, or
at the beach, or any public place, and you realize there is this total
stranger in the frame, and you wonder just how many foreign family
albums you yourself are a resident of.



Then, by extension, you wonder how many [other blogs][] have [your grimy
fingerprints][] smudged across the background image.



  [other blogs]: http://www.livejournal.com/users/righteous__babe/
  [your grimy fingerprints]: http://blogs.bwerp.net/archives/2004/01/15/ani
