Title: Google Search Options
Slug: 827/google-search-options
Date: 2009-05-13 11:32
Tags: Scripting, Web, Google, search
WordPress-Post-ID: 827
WordPress-Post-Type: post

![Google search options sidebar][img]

Google has added some search options to pages, including date-based searches, a
variety of ways to find related information, and more options for displaying
images with your results. Good stuff.

<!-- TODO: What was here?
[gallery link="file" columns="4"]
-->

  [img]: /media/2009/05/search-options.png
