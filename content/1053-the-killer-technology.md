Title: The Killer Technology
Slug: 1053/the-killer-technology
Summary: This is a rant against saying some device/software/platform will "kill" another.
Date: 2010-04-30 18:39
Tags: Personal, culture, rant, Technology
WordPress-Post-ID: 1053
WordPress-Post-Type: post
Category: Technology

**[tl;dr]** This is a rant against saying some device/software/platform
will "kill" another.

In [today's Penny-Arcade post][], Jerry Holkins writes:

> I find myself waiting with bated breath for each chapter of
> Arena.net's [ongoing manifesto][]. I mentioned as much, casually, in a
> dashed-off chirp while I was doing something else, and received a
> response to the effect that it was "too late" and that they'd "never
> beat WoW." Preparing my response, I shifted my feet to leverage the
> water-slicing stroke of the suio-ryu school. It was very mean, as I
> can be sometimes, but what I felt wasn't anger - it was something akin
> to nausea. In the end, I couldn't respond with my blade.
>
> As an aesthetic vision, "defeating World of Warcraft commercially"
> basically fucking sucks. It's everything that is wrong with
> everything, and the looking glass moment here is that this didn't come
> from Bobby Kotick or some tertiary corporate marketing organ, swollen
> with tar - it came from the community. We get infected with this horse
> race bullshit, and then we wonder why we end up with a truncation of
> this medium's Goddamned potential.

Related, Dion Almaer [tweets][] in response to an article on [Wired][],
"[Will the Mobile Web Kill Off the App Store?][]"

> "Kill." is a strong word. Black and white. Media-like. The mobile Web
> can be everywhere :)

A few days ago a high school freshman got on my case for being a Mac
user. Really? This still happens in a world of iPods and iPhones? I
honestly had zero interest in participating in that argument. It's 2010.
I'm 27 years old. I have, very literally, been defending my use of the
Macintosh since before this kid was born. Do an old gal a favor and find
something else to talk about.

### The Desire for Monoculture

There is a theme, here: a perception that there is one way to do things,
and no other path is appropriate. I find this idea offensive. There are
six and a half billion people in the world with varying needs and
interests and requirements. You can make a glass of water that everyone
enjoys, but outside that it's a matter of taste.

Maybe I am more sensitive to this issue because I was a diehard Mac user
for so long, and felt the need defend myself against detractors. Over
time, the Mac OS has always met my needs. If it did not, I would move on
like any rational person. To have someone find out I'm a Mac user and
profess, "You can't play games on that," or something equally irrelevant
was tiresome. I had my Nintendo for games. I had my Mac for music, or
the web, or whatever the flavor of the year was. Different expectations,
different tools, and different tasks. I wouldn't buy a blender and get
mad when it fails to toast my bread, and I didn't get mad when my Mac
failed to run [Counter-Strike][]. Judging without understanding is just
bigotry.

A Google search for "[iphone killer][]" yields 19,300,000 results. I
think this is counterproductive. It makes us seem like such binary
creatures, when in reality it is the shades of gray that make things
interesting. Choice is power. Creativity is our best reason for
existence.

I like my [Palm Pre][], and I confess I am an advocate for the webOS
platform, but having the Pre "kill" all other platforms would not
somehow validate my choice. The presence of the iPhone and Android do
not diminish my enjoyment of the Pre. Ultimately the markets (by virtue
of sales figure) have the final say in these matters, but I can't help
but make my case.

"I disapprove of what you say, but I will [defend to the death][] your
right to say it." There doesn't have to be one platform, just as there
doesn't have to be one voice, and no one else can decide which solution
is more valuable to me.

  [Tl;dr]: http://www.urbandictionary.com/define.php?term=tl;dr
  [today's Penny-Arcade post]: http://www.penny-arcade.com/2010/4/30/boy-must-learn-part-three/
  [ongoing manifesto]: http://www.guildwars2.com/en/news/
  [tweets]: http://twitter.com/dalmaer/status/12966411665
  [Wired]: http://www.wired.com/
  [Will the Mobile Web Kill Off the App Store?]: http://www.wired.com/gadgetlab/2009/12/firefox-mobile-vs-app-stores/
  [Counter-Strike]: http://store.steampowered.com/news/3569/
  [iphone killer]: http://www.google.com/search?q=iphone+killer
  [Palm Pre]: http://www.palm.com/us/products/phones/pre-family.html
  [defend to the death]: http://en.wikipedia.org/wiki/Freedom_of_speech#Freedom_of_speech.2C_dissent_and_truth
