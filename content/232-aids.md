Title: Gere on AIDS
Slug: 232/aids
Summary: How would you spend $300 billion?
Date: 2004-07-12 10:45
Tags: World
WordPress-Post-ID: 232
WordPress-Post-Type: post

CBS News: [AIDS Delegates Differ On Condoms][]:

> "The $200-300 billion spent in Iraq probably could have eradicated [AIDS]," actor Richard Gere -- one of several celebrities at the meeting -- told a panel discussion.

  [AIDS Delegates Differ On Condoms]: http://www.cbsnews.com/stories/2004/07/12/health/main628799.shtml
