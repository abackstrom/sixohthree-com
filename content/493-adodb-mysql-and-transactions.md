Title: ADOdb, MySQL, and Transactions
Slug: 493/adodb-mysql-and-transactions
Summary: Transactions should be supported by everything.
Date: 2008-07-23 11:03
Tags: Personal, ADOdb, PHP, Programming
WordPress-Post-ID: 493
WordPress-Post-Type: post

![][]I was just bit by some assumptions I made with [ADOdb][], MySQL 5,
and transaction support. I've been using [Smart Transactions][] with
success on other systems, but during my current project I noticed the
SQL commands were being processed without respecting my `FailTrans()`
call.

Turns out I was using a storage engine that did not support
transactions. While InnoDB is ACID-compliant, [MyISAM has no support for
transactions][]. Whoops. A few `ALTER TABLE`s later and I'm back in
business.

  [img]: /media/2008/07/myisam.png
  [ADOdb]: http://adodb.sourceforge.net/
  [Smart Transactions]: http://phplens.com/adodb/tutorial.smart.transactions.html
  [MyISAM has no support for transactions]: http://dev.mysql.com/doc/refman/5.0/en/ansi-diff-transactions.html
