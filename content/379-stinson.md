Title: View from the Top
Slug: 379/stinson
Summary: I can't see my house from here.
Date: 2006-08-14 17:40
Tags: Photography
WordPress-Post-ID: 379
WordPress-Post-Type: post

Looking south from [Stinson Mountain][], Rumney, NH. August 13, 2006. My
first time at the summit.

[![Panorama from the peak of Stinson Mountain][img]][img-big]

  [Stinson Mountain]: http://hikenh.netfirms.com/TDStinso.htm
  [img]: /media/2006/08/14/t_stinson.jpg
  [img-big]: /media/2006/08/14/stinson.jpg
