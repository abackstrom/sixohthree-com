Title: News to Me
Slug: 296/news
Summary: Lies within lies.
Date: 2005-02-11 18:17
Tags: Computers, Movies
WordPress-Post-ID: 296
WordPress-Post-Type: post

News flash: MPAA [defaces][] popular file-swapping site [[local
mirror][]].



I have several problems with this move. First, the site claims, "There
are websites that provide legal downloads." To the best of my knowledge,
this is a lie. Please show me a site where I can legally download
feature films, I would very much like to check it out.



Second, the position is awful high-and-mighty. Executives earning
hundreds of thousands of dollars are preying on our sympathies, asking
us to think of the "honest, hard-working people" out of one side of
their mouth, but at the same time [rewriting laws to their favor][] and
enforcing DVD region encoding to control sales and increase profits
despite the inconvenience to consumers.



I have no problem compensating people for a job well done, but give your
customers a little respect. I guarantee every person that has downloaded
a movie with LokiTorrent's help has supported the MPAA in some way.
Don't scream bloody murder when a movie is pirated, take the opportunity
to examine your situation, ask questions, give us alternatives that are
even remotely worthwhile.



  [defaces]: http://www.lokitorrent.com/
  [local mirror]: http://static.bwerp.net/~adam/archive/www.lokitorrent.com/
  [rewriting laws to their favor]: http://en.wikipedia.org/wiki/Sonny_Bono_Copyright_Term_Extension_Act
