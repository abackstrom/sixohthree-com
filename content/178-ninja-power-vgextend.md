Title: Ninja Power vgextend!
Slug: 178/ninja-power-vgextend
Summary: Logical volume management is changing the way I think about storage. (Warning: it only gets geekier from here.)
Date: 2003-09-05 07:37
Tags: Computers
WordPress-Post-ID: 178
WordPress-Post-Type: post

Logical volume management is changing the way I think about storage.
(Warning: it only gets geekier from here.)



We've all been in this situation before: time passes slowly, files
accumulate, hard drives fill up. Maybe you'd like to download a new
[Linux ISO][], but your files partition is down to 100MB free. What to
do? Save the ISO in /home, where there's still a few GB to spare? Buy a
new hard drive, and create a second files partition? Both of these are
hacks that go against your organizational structure, and you end up
either deleting files or creating dozens of symlinks to keep the
structure sane.



Enter [logical volume management][].



LVM makes mass storage much more flexible than I ever thought possible.
Normally, we associate a partition with a single mountpoint. /home is
mounted from /dev/hdb1; if hdb1 runs out of room, tough luck. LVM, on
the other hand, abstracts the filesystem, allowing you to add and remove
partitions and hard drives at will. You could create a volume group
"data" that encompasses several partitions and drives. "data" could be
divided into several logical volumes, say "home" and "media," each with
their own mountpoint. Each logical volume could start out at 5GB, and be
extended as needed from the pool of available space on the group. Need
to download an ISO to /media, but don't have the room? Grow the logical
volume by 1GB, and presto: instant free space.



There's a limit of EXTENTS\*2\^16 per logical volume, but it actually
turns out to be kinda fun. The extents are the blocks you divide each
physical volume into. Smaller extents mean less potential for wasted
space. An extents size of 4MB means a max of 3.9MB of wasted space (give
or take); a size of 512MB means a potential 511.9MB of waste. I could
live with probably 255.9MB of wasted space, which means I can slowly
grow my mountpoints to 16TB. Cue Montgomery Burns: *Ex-cellent*.



The nice thing about LVM is that I can probably pull it off. I don't
need to find identical hard drives or buy a RAID controller. I can even
mix and match SCSI and ATA drives. All I need is one super-100GB drive,
and the sky's the limit.



Ah, finally got that out of my system. Thanks for listening.



  [Linux ISO]: http://www.linuxiso.org/
  [logical volume management]: http://tldp.org/HOWTO/LVM-HOWTO/
    "LVM HOWTO"
