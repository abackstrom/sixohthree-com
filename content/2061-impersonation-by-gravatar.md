Title: Impersonation by Gravatar
Slug: 2061/impersonation-by-gravatar
Summary: There's a discussion going on at work regarding the potential for impersonation using Gravatar. There is some merit to this concern, though I consider it a little flimsy.
Date: 2012-08-15 22:27
Tags: Web, gravatar, identity
WordPress-Post-ID: 2061
WordPress-Post-Type: post

There's a discussion going on at work regarding the potential for
impersonation using [Gravatar][]. There is some merit to this concern,
though I consider it a little flimsy. Generally speaking, Gravatar is a
service that ties a picture to your email address. A malicious person
with knowledge of that email address, combined with a Gravatar-enabled
service that allows user-generated content but does not verify email
address ownership, makes for [trivially easy][] spoofing.

Gravatar solves a problem: social sites across the web require us to
create accounts and specify an avatar. For most people, the email
address is the common thread running through all these accounts. If your
goal is to provide a central avatar service, it makes sense to tie your
service to email addresses. As a consumer of this service, if your site
lets people provide an email address and you don't verify ownership of
that address, you're making it very easy for people to spoof each other.

I don't get too worked up about this possibility. A determined person
will impersonate whether or not Gravatar exists. Given a semi-famous web
personality or a Googlable name, anyone could register a fake email
address and use that account to create social accounts. Combine that
with readily-available photos and the illusion is complete, with or
without Gravatar. A potential employer Googles and finds
john\_smith20@gmail.com making inflammatory comments on GigaOM, and the
user photo matches up. Next resume, please.

As I see it, Gravatar is a valuable service, made more valuable when we
can verify identity. To encourage participation on our sites, we have to
lower the barriers to entry. As the people creating the modern web, we
have to reconcile these these two sometimes-competing aspects of the
social web.

  [Gravatar]: http://gravatar.com/
  [trivially easy]: http://www.flickr.com/photos/caterina/5467702712/
