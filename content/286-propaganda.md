Title: Modern-Day Propaganda
Slug: 286/propaganda
Summary: Scary.
Date: 2005-01-09 18:00
Tags: Politics
WordPress-Post-ID: 286
WordPress-Post-Type: post

Via BBC News, [N. Korea wages war on long hair][]. Guess I'm a bit
naive, I didn't think this sort of propaganda still existed. One notable
excerpt:




> [The Common Sense program] stressed the "negative effects" of long
> hair on "human intelligence development", noting that long hair
> "consumes a great deal of nutrition" and could thus rob the brain of
> energy.



  [N. Korea wages war on long hair]: http://news.bbc.co.uk/2/hi/asia-pacific/4157121.stm
