Title: Twitter vs. Mastodon: On Abandoning the Titanic
Slug: twitter-vs-mastodon-on-abandoning-the-titanic
Summary: I vent some frustrations about the 800 lbs blue bird.
Date: 2019-07-05 06:56
Category: Social
Tags: mastodon, twitter
Meta-Image-Square: /media/2019/07/mastodon.png
Meta-Image-FB: /media/2019/07/mastodon.png

I'm frustrated about Twitter vs. Mastodon[ref]I'm using this as shorthand to
mean "microblogging over ActivityPub," sorry Pleroma/GNU Social/other
folks.[/ref] so I'm going to dump some thoughts here. It's surely all been said
but I want to get it out of my head, and nobody on Twitter cares and everybody
on Mastodon already knows and doesn't want to hear it again.

Somebody in a Slack linked to [this thread][1] by Rebecca Cohen (@GynoStar) on
Twitter. The gist is she replied to some trolls using Princess Bride quotes and
got a temporary suspension for "abusive behavior". This is pretty much what
we've come to expect from the moderation teams of big platforms: rules are
inconsistently enforced, mod teams suffer [inhuman][2] [conditions][3] and don't
always make the decision we expect or want, trolls routinely weaponize
moderation tools against the marginalized groups those tools are meant to
protect.

I've been off Twitter since October 2017 and finally deleted my account in
late 2018. As someone who already left that particular flavor of toxicity behind,
it's frustrating watching this same story play out over and over.

People want Twitter to get better. They want to fix it. Twitter was great for a
time, but objectively, is it worth saving? Is that model, with its shitty
moderation and reliance on ads and its hostility towards app developers and its
neverending current of toxicity, even possible to save?

Mastodon is not a panacea. The protocols aren't perfect, the software is good
and improving but not everyone's cup of tea, and the moderation and funding
models are different than we're used to. But after two years, my only wish is
more people would join so we can make it better together. Not just with code,
but with community and collaboration.

Complain about Mastodon (the software) if you want. Question the longevity of
individual servers with no long-term plan. Point out the weak spots with
distributing and duplicating moderation across thousands of servers and admin
and mods. But please, don't just dismiss it without really committing, and later
complain that "nobody is on Mastodon." Seriously, you all sounds like a bunch of
Titanic passengers waiting for somebody else to prove the lifeboat is safe. Just
get off the damn boat.

![Everyone I know is on Mastodon!][4]

I respect that this is not an easy calculus for some people. I exercised a ton
of privilege deleting my own Twitter. The platform is hugely important to some
people's revenue streams, especially content creators. There are crossposters in
both directions to help you, if you need them. (Just, please, I beg you, don't
set up a Twitter to Mastodon crossposter only to ignore it.) Every engagement you
make on Twitter helps entrench that platform. You get something from them, but
they are taking whatever they can from you, and giving back as little as
possible completely on their terms.

This is the herd immunity of social media. Fixing any of the complex problems we
face these days takes effort. It's easy to be a naysayer. If you have the
spoons, please come and be part of the solution.

  [1]: https://twitter.com/GynoStar/status/1146050207618088960
  [2]: https://www.theverge.com/2019/2/25/18229714/cognizant-facebook-content-moderator-interviews-trauma-working-conditions-arizona
  [3]: https://www.theverge.com/2019/6/19/18681845/facebook-moderator-interviews-video-trauma-ptsd-cognizant-tampa
  [4]: /media/2019/07/mastodon.png "A group of people saying together that everyone they know is on Mastodon."

