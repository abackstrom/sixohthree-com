Title: Activism (or Lack Thereof)
Slug: 137/activism-or-lack-thereof
Date: 2003-02-21 22:13
Tags: Politics
WordPress-Post-ID: 137
WordPress-Post-Type: post

Between [Ani DiFranco][] and one of my current professors, I've been
hearing a lot about the terrible things done in the name of America.
I've known for a long time that the US isn't the bastion of freedom and
generosity that we're all led to believe. So much death and poverty
happens so we can live the life we live. We wear clothing made by
sweatshop labor; we eat food cultivated on land stolen from South
American natives; we wage war to avenge Daddy President and pick up a
little oil to boot. Hegemony, warmongering, multinational corporations..
What to do?

I want so badly to just stop it all, but it seems so big.



  [Ani DiFranco]: http://www.righteousbabe.com/
