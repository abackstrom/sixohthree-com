Title: Man Convicted for... Something?
Slug: 318/convicted
Summary: On drawing the wrong conclusions.
Date: 2005-07-05 15:08
Tags: Computers, Games
WordPress-Post-ID: 318
WordPress-Post-Type: post

I wasn't going to blog this one, but it showed up on [Slashdot][] *and*
[The Register][] with a somewhat misleading title. From the [BBC][]
article:

Man convicted for chipping Xbox: A 22-year-old man has become the first
person in the UK to be convicted for modifying a video games console.

Maybe I don't have all the facts, but it seems mod chipping was not the
central issue here. This guy was selling pirated games with each unit,
probably \$3,000 in titles for each modded Xbox. Is it any wonder he was
shut down? The issue here isn't modchips (which has substantial
non-infringing uses, I would argue), but selling software illegally.
These are two very different things.

  [Slashdot]: http://games.slashdot.org/games/05/07/04/2150254.shtml?tid=222&tid=10
  [The Register]: http://www.theregister.co.uk/2005/07/05/brit_xbox_chipping/
  [BBC]: http://news.bbc.co.uk/2/hi/technology/4650225.stm
