Title: Remote Control via BitTorrent Sync
Slug: remote-control-via-bittorrent-sync
Date: 2015-01-28 13:47
Category: Technology
Tags: bittorrent, bittorrent sync
Summary: IMPORTANT INSTRUCTIONS for remotely initiating BitTorrent downloads using BitTorrent Sync.

IMPORTANT INSTRUCTIONS for remotely initiating BitTorrent downloads using
BitTorrent Sync. This lets you initiate torrent downloads on a host by dragging
the torrent file to a folder on any other computer.

1. Set up a BitTorrent Sync shared folder if you haven't done so already ([instructions][2])
2. Add a new folder to your share, e.g. torrent-remote-control.
3. Configure your BitTorrent client to watch this folder for new .torrent files. ([example][1])
    * Optional: set the Downloads directory to another, non-sync'd folder if you
      don't want downloaded files copied to each BitTorrent Sync peer.
4. Drag a torrent into the watch folder from another computer, watch the torrent
   download automagically start on the other computer.

  [1]: /media/2015/01/bittorrent-watch.png
  [2]: http://help.getsync.com/customer/portal/articles/1574723-step-by-step-guide-to-syncing
