Title: Tracking Cash with You Need a Budget
Slug: tracking-cash-with-you-need-a-budget
Summary: You Need a Budget (YNAB) is a budgeting app for web and mobile that helps
    you track your spending and save for the future. Here's how I track cash spending
    in YNAB without wanting to tear my hair out.
Date: 2019-01-30 00:00
Category: Technology
Tags: money, budgeting, ynab

You Need a Budget (aka YNAB, [referral
link](https://ynab.com/referral/?ref=nAsryky6rzsUU8Nc)) is a budgeting app for
web and mobile that helps you track your spending and save for the future.
Here's how I track cash spending in YNAB without wanting to tear my hair out.

## What's YNAB?

If you're already familiar with YNAB, feel free to skip ahead! For those of you
who haven't used it, YNAB is a bit like a spreadsheet in Turbo Mode. As a
budgeting app, it lets you allocate your money into categories of your choosing,
like a virtual "envelope" system. You can track spending by category or category
groups, differentiate between budgeted funds and other assets like 401(k) or
investments, and link accounts to a bank to import transactions.

If you're using a debit card, YNAB is pretty straightforward. You plug your
paychecks in as income, that money goes into a "To be Budgeted" pool, you
allocate those funds into your categories. Enter in your transactions, assign
each to a category, and the "balance" in your categories goes down. If you
overallocate or overspend a category, YNAB will warn you that you've spent more
money that you have. Credit cards are a just a shade more complicated, but the
basics are the same.

For more background on the basics, check the quick intro video [on their help
site](https://www.youneedabudget.com/learn/).

## Why cash?

Credit and debit transactions are convenient, because both can be auto-imported
right from your bank or card company. Missing a transaction isn't the end of the
world: you can just get it off the credit card statement later. Your wallet has
no such safety net.

But, cash is a necessity in lots of situations. I tried to fudge it for a while,
using throwaway categories like "Stuff I Forgot to Budget For" (yes, this is a
default category). That wasn't helping me understand where my money was going,
which was one of my early goals. Rather than continuing to ignore the problem, I
tweaked my setup. Now I can use as much cash as I need without breaking my
budget.

## Taming your Wallet

The first thing I did was **create a new Cash account**. The initial balance of
the account should be whatever's in your wallet. YNAB will add that money into
*To be Budgeted*, and you'll have to allocate it to categories.

At this point, **your wallet is now part of the budget.** Just as importantly,
your budget is just **a pool of money** spread across several debit and credit
accounts. What's in your wallet isn't specifically allocated towards coffee or
gas or groceries or anything else. If you stop at a food truck to spend the last
$10 in your Dining Out category, YNAB doesn't force you to spend from a certain
source. Cash only merchant? No problem, it's in the budget.

This sounds straightforward but it takes discipline. You can't be lazy with
entering your expenses: the auto-import safety net is gone. There are a few
things that make cash tracking easier for me:

- **Enter ATM withdrawals as a transfer.** You don't have to recategorize
  transfers. Move money around between your checking and cash as it suits you,
  without affecting the budget.
- **Try and enter expenses as you make them.** It's tempting to batch receipts,
  but you can lose track of them, or the stack can become overwhelming. If
  that's a risk for you, try to enter your transactions as soon as you make a
  purchase. Added bonus: the app will associate the Payee with a location and
  suggest it next time.
- **Round up to the nearest dollar.** This one is critical for me. It's too
  overwhelming if I have to track pennies and reconcile them away when my wallet
  doesn't match YNAB. I usually make a handful of cash transactions every week,
  and the change gets donated or saved for parking meters.

Tracking cash helps me every week. I can carry cash without guilt that it's
unaccounted for. I can stop at cash-only shops. I have a few dollars on hand for
a tip. My spending habits are more accurately reflected in YNAB, not quite down
to the penny but certainly better than before. My budget is about as accurate as
it can be. 💸
