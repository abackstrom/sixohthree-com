Title: Abusing screen(1)'s Default Shell
Slug: 451/screen
Summary: Three other people will find this useful.
Date: 2008-04-25 08:32
Tags: Scripting, Bash, screen, ssh, Terminal
WordPress-Post-ID: 451
WordPress-Post-Type: post

Today I figured out a simple but non-obvious way of automatically
connecting to a remote shell when I create new [screen][] windows. I'm
using this in conjunction with a development server that does not have
screen installed.

Let's start at the beginning. My `~/.ssh/config` file has many entries
structured like so:


    Host bob
        HostName robert.example.com
        User backstrom


This setup allows me to SSH to robert.example.com using the shortcut
`ssh bob`. This shortcut could be made even shorter, though. I have a
script (say, `host-ssh`) in my \$PATH with the following content:


    #!/bin/sh
    HOST=$(basename $0)
    ssh $HOST $@


Now I can `ln -s host-ssh bob` to symlink host-ssh to bob, and `bob`
becomes a shortcut for `ssh bob`. At this point, the following commands
work as expected:


    annika@local:~$ ssh bob
    backstrom@robert:~$ exit
    annika@local:~$ bob
    backstrom@robert:~$ exit
    annika@local:~$ scp -q myfile bob:
    annika@local:~$



So, onto screen. Imagine that my workstation has screen, but my remote
server "bob" does not. I can mimic the functionality by running screen
locally and connecting each window to bob in its own SSH session. (In
the past I have done this via `exec bob` so I have one less `exit` to
type during disconnects.) However, screen allows you to set a custom
shell. You can leverage this to force an SSH connection to a specific
server whenever a new window is created. Just launch screen with
`screen -s bob` and every ^A-c will automatically run your new SSH
shortcut. Seamless, if you have public keys set up.

<ins datetime="2008-07-10T09:22-04:00">More information is available on
my [SSH wiki page][].</ins>

  [screen]: http://www.gnu.org/software/screen/
  [SSH wiki page]: http://wiki.bwerp.net/SSH
