Title: REST APIs and Security in PHP
Slug: 1645/rest-apis-and-security-in-php
Summary: When creating a REST endpoint, how do you protect your data from unauthorized access?
Date: 2011-10-14 23:44
Tags: Web, rest, security
WordPress-Post-ID: 1645
WordPress-Post-Type: post

**Disclaimer:** I am not speaking with authority on this topic. I am
simply recording my findings. This content is subject to change as I
learn more.


I made a quick and dirty REST API at work today, after some
whiteboarding with our [lead developer][]. We're coming into this pretty
fresh, only having consumed REST up to this point, so it's a learning
experience for sure. One obvious consideration is security: is SSL with
a key enough? Do we need more than one token per request, i.e. an
application identifier and a private key? What about hashing/signing the
request?

As it stands, we're requiring SSL, as well as two parameters: appid and
appkey. The dual identifiers would allow us to grant varying levels of
privileges to a given developer or host. SSL protects the security of
these values, one of which (the key) is considered private.

Signing Requests
----------------


So, hopefully we're making the right assumptions so far. If SSL isn't
enough, we may need to sign requests. This would protect keys against
far-future replay attacks, as every request would expire after a short
period. (Note that OAUTH 2.0 is dropping request signing in favor of a
token, while emphasizing the importance of SSL.)

HMAC-SHA1 is one possible way to sign and verify a request using a
shared secret that is not transferred over the wire. In this setup, we
would transmit the public identifier (so the server can lookup the
corresponding private key) and the HMAC digest. The signature is based
on the HTTP method (e.g. GET), the current timestamp, and the path to
the REST endpoint. This code allows for 60 seconds of drift (or network
congestion) between the client and server clocks. Other comments are
inline.

### hmac-sig-client.php


[code name="hmac-sig-client.php"]

### hmac-sig-server.php


[code name="hmac-sig-server.php"]

Resources
---------



-   [World of RESTCraft][]
-   [Securing an API: SSL & HTTP Basic Authentication vs Signature][]
-   [Top Differences between OAuth 1.0 and OAuth 2.0 for API Calls][]



  [lead developer]: http://borkweb.com/
  [World of RESTCraft]: http://bill.burkecentral.com/2011/09/16/world-of-restcraft/
    "World of RESTCraft"
  [Securing an API: SSL & HTTP Basic Authentication vs Signature]: http://stackoverflow.com/questions/5511589/securing-an-api-ssl-http-basic-authentication-vs-signature
  [Top Differences between OAuth 1.0 and OAuth 2.0 for API Calls]: http://blog.apigee.com/detail/oauth_differences/
