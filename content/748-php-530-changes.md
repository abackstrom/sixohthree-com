Title: PHP 5.3.0 Changes
Slug: 748/php-530-changes
Summary: PHP 5.3.0 looks promising.
Date: 2009-03-19 10:19
Tags: Personal, PHP, Programming, Web
WordPress-Post-ID: 748
WordPress-Post-Type: post

PHP 5.3.0 looks promising. So far:

- [__callStatic()][callstatic]
- [Circular reference garbage collection][]
- [get_called_class()][]
- [Closures][]
- [DateTime::getTimestamp()][]
- [Namespaces][]
- [Late Static Binding][]
- [Phar][] as part of the default install
- `__DIR__` — no more `dirname(__FILE__)`!

PHP 5.3.0 was [released][] on 23 June, 2009! A [migration guide][] is
available.

  [callstatic]: http://www.php.net/manual/en/language.oop5.overloading.php#language.oop5.overloading.methods
  [Circular reference garbage collection]: http://bugs.php.net/bug.php?id=33595
  [get_called_class()]: http://php.net/manual/en/function.get-called-class.php
  [Closures]: http://wiki.php.net/rfc/closures
  [DateTime::getTimestamp()]: http://www.php.net/manual/en/datetime.gettimestamp.php
  [Namespaces]: http://us.php.net/manual/en/language.namespaces.php
  [Late Static Binding]: http://us.php.net/lsb
  [Phar]: http://docs.php.net/manual/en/book.phar.php
  [released]: http://www.php.net/archive/2009.php#id2009-06-30-1
  [migration guide]: http://docs.php.net/migration53
