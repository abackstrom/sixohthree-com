Title: Still Playing Roles
Slug: 177/still-playing-roles
Date: 2003-08-31 23:29
Tags: Personal
WordPress-Post-ID: 177
WordPress-Post-Type: post

Lately, there has been [much][] [discussion][] regarding the complex
dynamic between professors and students. Many people have already
weighed with their thoughts on context, roles, hierarchy, respect and
boundaries. Should professors befriend their students? Would friendship
diminish their authority and level of respect in the classroom? There's
a lot to consider. I've been milling things over, and I have some
thoughts of my own on the roles people play.

I think many people play roles, whether they realize it or not. Often,
roles are work-related. (And I include the learning environment in this
category as well.) Take me, for example: for over a year now, I've been
working at the [ITS Help Desk][] at RIT. I'm primarily there for
customer support, and I've been honing my customer interaction skills
for some time now.

"ITS, this is Annika. Hi Bill, thanks for calling back. I'm glad to hear
you problem was solved. Give us a call if you need anything else.
Thanks, have a good day."

Professional, pleasant, and polite. (At least, I'd like to think so.)
This is my role: the polite techie, ever vigilant, always patient, and
apologetic over things he has no control over. I am the stereotypical
faceless support representative. Nice to meet you, thanks for calling.

I play this role, because I can't not play it. When that phone rings,
the mask goes on. I can't picture the dialog taking place any other way.

OK, need to converge on a point:

Disillusionment. I play a role. I don't exactly like playing a role. I
imagine that others feel the same way sometimes. We're all just people,
careening through the universe on this crazy, spinning rock. Yes, we're
all different, but in some ways we're all the same, too. I want to look
at other people, look them straight in the eye, and not see a complete
stranger in a mask. You're all out of french fries tonight? No problem,
man. I'll take onion rings. We've all had times in our life when there
just aren't any more french fries, right? No sense getting worked up
over it.

Man, my head comes up with the craziest stuff.

  [much]: http://www.rit.edu/~eroics/MT/WeezBlog/archives/000204.html
  [discussion]: http://www.wealthbondage.com/2003/08/29.html#a912
  [ITS Help Desk]: http://www.rit.edu/~wwwits/
