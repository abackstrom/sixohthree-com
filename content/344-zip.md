Title: Zip Technology
Slug: 344/zip
Summary: I can see the train of thought now.
Date: 2005-09-28 09:33
Tags: Computers
WordPress-Post-ID: 344
WordPress-Post-Type: post

From the [eReader.com][] [FAQ][]:


> <span style="font-weight:bold">Do I need to get a zip drive?</span>  
> </p>
> <p>
> <span style="margin-left: 3em"> </span>No, you do not need to purchase
> a zip drive to use eReader.



The assumption being that an Iomega Zip drive is required to read .zip
files, naturally.

  [eReader.com]: http://www.ereader.com/
  [FAQ]: http://www.ereader.com/help/faq
