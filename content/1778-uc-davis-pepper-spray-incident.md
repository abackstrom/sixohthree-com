Title: UC Davis and the Pepper Spray Incident
Slug: 1778/uc-davis-pepper-spray-incident
Summary: On November 18, 2011, seated members of a nonviolent protest on a University of California, Davis quad were pepper-sprayed at close range by Lt. John Pike.
Date: 2011-11-22 14:01
Tags: Freedom, Occupy, UC Davis
WordPress-Post-ID: 1778
WordPress-Post-Type: post

On November 18, 2011, seated members of a nonviolent protest on a
[University of California, Davis][] quad were pepper-sprayed at close
range by Lt. John Pike.

From the UC Davis [Department of English][] website:

> The faculty of the UC Davis English Department supports the Board of
> the Davis Faculty Association in calling for Chancellor Katehi's
> immediate resignation and for "a policy that will end the practice of
> forcibly removing non-violent student, faculty, staff, and community
> protesters by police on the UC Davis campus." Further, given the
> demonstrable threat posed by the University of California Police
> Department and other law enforcement agencies to the safety of
> students, faculty, staff, and community members on our campus and
> others in the UC system, we propose that such a policy include the
> disbanding of the UCPD and the institution of an ordinance against the
> presence of police forces on the UC Davis campus, unless their
> presence is specifically requested by a member of the campus
> community. This will initiate a genuinely collective effort to
> determine how best to ensure the health and safety of the campus
> community at UC Davis.

The school's home page currently features a photograph of Chancellor
Katehi, captioned "I'm here to apologize."

[![Screenshot of the UC Davis homepage featuring Chancellor Katehi][UC Davis Homepage Screenshot]][UC Davis Homepage Screenshot]

See BoingBoing for [much, much more][].

  [University of California, Davis]: http://ucdavis.edu/
  [Department of English]: http://english.ucdavis.edu/
  [UC Davis Homepage Screenshot]: https://sixohthree.com/files/2011/11/ucdavis-sorry.jpg "Screenshot of the UC Davis homepage featuring Chancellor Katehi, with a quote 'I'm here to apologize'"
  [much, much more]: http://boingboing.net/2011/11/18/police-pepper-spraying-arrest.html
