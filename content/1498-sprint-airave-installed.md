Title: Sprint AIRAVE installed
Slug: 1498/sprint-airave-installed
Date: 2011-03-11 20:26
Tags: Technology, AIRAVE, mobile, sprint
WordPress-Post-ID: 1498
WordPress-Post-Type: post
WordPress-Post-Format: Aside

Took longer than expected due to a DOA unit, but the AIRAVE is
installed! Nice to have five bars of service in my house.
