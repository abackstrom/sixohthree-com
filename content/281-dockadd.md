Title: Dockadd, Revealed
Slug: 281/dockadd
Summary: Hey, there it is.
Date: 2004-12-14 19:50
Tags: Mac OS X, Scripting
WordPress-Post-ID: 281
WordPress-Post-Type: post

I was checking out [my Google ranking][] and stumbled across something I
never expected to see online: my [Dockadd script][]. This script allows
the user to add files, folders, and applications to the Mac OS X Dock
via the command line or a script. It represents the pinnacle of my OS X
hackery in many ways.

And of course, `$LUSER` is just shorthand for "local user," as any
sysadmin knows.

<span style="font-weight: bold">Update, 7 April, 2005:</span> Looking
back at this script posting, the linebreaks are a bit out-of-whack, so a
copy/paste of the code will not work. Script monkeys will be able to
spot the errors, but if anyone has major headaches I can help point out
the problems.

  [my Google ranking]: http://www.google.com/search?q=adam+backstrom
  [Dockadd script]: https://mailman.rice.edu/pipermail/radmind/2004-July/007125.html
