Title: Vintage NYC
Slug: 2111/vintage-nyc
Date: 2012-11-03 13:10
Tags: Photography, New York
WordPress-Post-ID: 2111
WordPress-Post-Type: post
WordPress-Post-Format: Image
Summary: Found in old Moulton family papers. Date and photographer unknown.

[![Photograph of two old snapshots][img]][img-big]

Found in old Moulton family papers. Date and photographer unknown.

  [img]: https://sixohthree.com/files/2012/11/20121103-130950-e1351963728505-1024x609.jpg "A photograph of two old New York snapshots, including an unidenfied skyline and the Statue of Liberty"
  [img-big]: https://sixohthree.com/files/2012/11/20121103-130950-e1351963728505.jpg
