Title: Unemployment Starts at Home
Slug: 190/jobs
Date: 2003-12-21 08:28
Tags: Rants
WordPress-Post-ID: 190
WordPress-Post-Type: post

I've just read yet another article on US outsourcing to Asia for more
and more IT jobs. Specifically, [Corporate America's Silent Partner:
India][], via [Slashdot][]. From the article:


> True, corporations likely won't feel comfortable about openly talking
> about their offshore activities until job growth returns to the U.S.



Job growth? In the US? This is in an article about US corporations
*firing many of their workers*. It will boost revenue in the short term,
but what happens when all your old employees drain their savings and
can't find a job without relocating to Bombay?

When I was a senior in high school deciding what college to attend the
IT industry in the US was still going strong. By freshman year of
college the market was busting. Professors assured us that things would
turn around, and I believed it. I always thought there was a glut, that
the industry needed to find its happy medium between booming and
busting. If I thought there was a glut before, what will I think of the
next year or two? More IT professionals will be laid off, and an
increasing number of college graduates will become part of an already
struggling job market.

Blech, ranting. Please, somebody, say you have good news for me.

  [Corporate America's Silent Partner: India]: http://www.businessweek.com/bwdaily/dnflash/dec2003/nf20031215_8942_db046.htm
  [Slashdot]: http://slashdot.org/
