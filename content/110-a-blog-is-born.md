Title: A Blog is Born
Slug: 110/a-blog-is-born
Summary: Taking the plunge.
Date: 2002-12-06 22:19
Category: Meta
WordPress-Post-ID: 110
WordPress-Post-Type: post

After much deliberation, procrastination, and general supressing of my
own ego, I have finally decided to take the plunge and dive face-first
into the exciting world of [blogging][]. I have a fresh copy of [Movable
Type][], a fist full of links, and a brain just itching to be dumped out
over the web for all to see.

Let us wish for the best, eh?

  [blogging]: http://www.everything2.org/index.pl?lastnode_id=389002&node_id=389001
  [Movable Type]: http://www.movabletype.org/
