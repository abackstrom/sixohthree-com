Title: PHP 5.4 Changes
Slug: 1756/php-5-4-changes
Summary: From the PHP 5.4.0 RC1 "NEWS" file. Traits, array dereferencing, CLI built-in web server, and more.
Date: 2011-11-16 11:32
Tags: Programming, PHP
WordPress-Post-ID: 1756
WordPress-Post-Type: post

From the PHP 5.4.0 RC1 ["NEWS" file].

-   [Traits]
-   Class member access on instantiation: `(new Foo)->bar()`
-   Short array syntax: `['blue', 'red']`
-   ext/mysql, mysqli and pdo\_mysql now use mysqlnd by default
-   New typehint: `function( callable $callback )`
-   [`Closure::bind()`][closure bind] and [`Closure::bindTo()`][closure bindTo]
-   `Class::{"foo$bar"}()`
-   Method call through array:
    `$cb = array($obj, 'method'); $cb( $args );`
-   Built-in web server: `php -S localhost:8080`
-   `<?=` is now always available regardless of the short\_open\_tag
    setting (pure PHP templates ftw)

In addition to safe mode being removed, there's also this:

> Removed `magic_quotes_gpc`, `magic_quotes_runtime` and `magic_quotes_sybase`
> ini options. `get_magic_quotes_gpc`, `get_magic_quotes_runtime` are kept but
> always return false, `set_magic_quotes_runtime` raises an `E_CORE_ERROR`.
> (Pierrick, Pierre)

  ["NEWS" file]: http://www.php.net/releases/NEWS_5_4_0_RC1.txt
  [Traits]: http://us2.php.net/traits
  [closure bind]: http://us2.php.net/manual/en/closure.bind.php
  [closure bindTo]: http://us2.php.net/manual/en/closure.bindto.php
