Title: New Hampshire HB240: Approval Voting
Slug: 1470/new-hampshire-hb240-approval-voting
Summary: Wow\: "Will NH adopt approval voting?"
Date: 2011-01-30 16:15
Tags: Politics
WordPress-Post-ID: 1470
WordPress-Post-Type: post

Wow: "[Will NH adopt approval voting?][]" I've always felt that the
"vote for as many candidates as you want" method is the only true way to
achieve real change in politics, and break away from the current
divisive two-party system. How many people would vote differently if a
vote for the green party or other minority could no longer be considered
a wasted vote?


> To: Charles Brosseau <[charlesbrosseau@gmail.com][]>, James Aguiar
> <[jim.aguiar@leg.state.nh.us][]>  
> Cc: Dan McGuire <[dan@mcguire4house.com][]>  
> Subject: Supporting HB240  
> Date: January 30, 2011 at 3:41 PM
>
> Dear Representatives Aguiar and Brosseau,
>
> I am writing to you as a resident of Rumney, NH in regards to HB240,
> "allowing voters to vote for multiple candidates for an office." I
> believe this is a very empowering bill that allows people to
> demonstrate their true intentions during the electoral process. I am
> not an expert in election law, and every voting style has its critics,
> but I urge you to carefully consider any measure that works towards
> more accurate representation of our population. I look forward to
> hearing your positions as this bill moves through the system.
>
> Sincerely,
>
> Annika Backstrom  
> annika@sixohthree.com



  [Will NH adopt approval voting?]: http://freekeene.com/2011/01/28/will-nh-adopt-approval-voting/
  [charlesbrosseau@gmail.com]: mailto:charlesbrosseau@gmail.com
  [jim.aguiar@leg.state.nh.us]: mailto:jim.aguiar@leg.state.nh.us
  [dan@mcguire4house.com]: mailto:dan@mcguire4house.com
