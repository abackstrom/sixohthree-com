Title: Overheard
Slug: 114/overheard
Date: 2002-12-11 16:06
Tags: RIT
WordPress-Post-ID: 114
WordPress-Post-Type: post
Category: Personal

Heard this one on the way home from class yesterday:

> My dad said, "How about you get a job on campus?" I said, "How about I
> saw your face off with a soldering iron?"

My faith in my peers wanes more every day.
