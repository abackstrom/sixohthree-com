Title: OK Uber, that's not creepy or anything
Slug: ok-uber-thats-not-creepy-or-anything
Summary: When targeting marketing gets personal.
Date: 2016-09-20 16:26
Category: Personal
Tags: creepy

![Screenshot of iPhone notification from Uber app](http://i.abackstrom.com/share/2016/09/TripTracker.png "A notification on the locked iPhone reads 'Annika is taking an Uber. Track the trip now.'")
{: .figure }

From an Uber marketing campaign.
