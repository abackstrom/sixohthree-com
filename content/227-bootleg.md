Title: Ani DiFranco: Official Bootleg Series
Slug: 227/bootleg
Summary: I love New Ani Album Day.
Date: 2004-06-24 23:47
Tags: Music
WordPress-Post-ID: 227
WordPress-Post-Type: post

Ani DiFranco has released the first of her [Official Bootleg Series][]
albums. [Atlanta - The Tabernacle Theatre][], a full recording of her
Oct. 9, 2003 show, is now available on the RBR site for \$10.00. Be the
first on your street to get one. ;)



  [Official Bootleg Series]: http://www.righteousbaberecords.com/ani/bootleg/
  [Atlanta - The Tabernacle Theatre]: http://www.righteousbaberecords.com/ani/bootleg/atlanta.asp
