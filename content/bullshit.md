Title: Bullshit
Slug: bullshit
Summary: Harry Frankfurt, "Bullshit!"
Date: 2016-06-04 11:00
Tags: tags

Harry Frankfurt, "Bullshit!":

<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="//player.vimeo.com/video/167796382" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

via [kottke](http://kottke.org/16/06/on-bullshit-and-donald-trump)
