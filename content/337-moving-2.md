Title: Moving Out
Slug: 337/moving-2
Summary: It's time.
Date: 2005-09-12 13:11
Tags: Personal
WordPress-Post-ID: 337
WordPress-Post-Type: post

55 weeks ago, I [left RIT][]. This past weekend, I moved nearly all my
worldly possessions into an apartment in Plymouth, NH. Home was good,
but it's never really home once you've moved out. Now I get my own
space, a blank canvas. No more adapting to someone else's
already-furnished house and living out of boxes.

I'll be back on the map Tuesday after the cable modem is set up. Updated
contact information will be e-mailed shortly thereafter. See you on the
other side.

  [left RIT]: /archives/2004/08/19/moving
