Title: JavaScript Anagram Helper
Slug: 343/anagram
Summary: I'm tired.
Date: 2005-09-27 00:37
Tags: Web
WordPress-Post-ID: 343
WordPress-Post-Type: post

Brought to you by JavaScript and the DOM, here is my first attempt at an
[Anagram Helper][]. This is the product of a late-night hack session; I
threw it together in about 90 minutes of solid coding. It's only tested
in Firefox 1.5b1, but it does the trick.

In summary: give it a sentence, and it will generate a character pool.
Type into the bottom text box to pull letters from that pool, building
an anagram of the first sentence.

Deletions from the third box work. Copying and pasting most likely does
not. Neither does deleting a selection. I might fix it, I might not. For
now, I need rest.

  [Anagram Helper]: http://static.bwerp.net/~adam/2005/09/26/anagram.html
