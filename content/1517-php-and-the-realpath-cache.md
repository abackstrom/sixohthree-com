Title: PHP, symlinks, and the realpath cache
Slug: 1517/php-and-the-realpath-cache
Summary: I've had issues in the past where symlinked web directories don't point to the correct file after the symlink target is changed.
Date: 2011-05-06 11:01
Tags: Programming, performance, PHP
WordPress-Post-ID: 1517
WordPress-Post-Type: post

I've had issues in the past where modifying symlinks seems to break my web
apps: I change the symlink target, but PHP still loads the old target for
seconds or minutes. Yesterday I realized this was happening because of the PHP
[realpath cache][]. This cache can be emptied using
[`clearstatcache()`][clearstatcache], but the cache is maintained per-thread,
so it's not a silver bullet. Only the active thread will have its cache
cleared, where you may have dozens of threads in total (e.g. instances of
php-fpm, or httpd children in prefork mode).

So, from now on I'll just consider symlinks harmful, I suppose.

  [realpath cache]: http://php.net/manual/en/ini.core.php#ini.sect.performance
  [clearstatcache]: http://php.net/clearstatcache
