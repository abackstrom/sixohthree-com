Title: Cross-post: Warcraft item links in macros
Slug: 362/link
Summary: Fun and time-saving.
Date: 2006-03-03 14:59
Tags: Games
WordPress-Post-ID: 362
WordPress-Post-Type: post

Cross-posted here at at the [Bwerp Wiki][]: [linking items in the World
of Warcraft macro system][]. This is not straightfoward, and there does
not seem to be much information on the process on the web.

  [Bwerp Wiki]: http://wiki.bwerp.net/
  [linking items in the World of Warcraft macro system]: http://wiki.bwerp.net/World_of_Warcraft_macros#Linking_items
