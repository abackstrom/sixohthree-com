Title: Zoom.it API REST Considerations
Slug: 1657/zoom-it-api-rest-considerations
Summary: The Zoom.it API offers both RESTful and non-RESTful responses. The non-RESTful variant is suitable for clients that are limited in some way.
Date: 2011-10-15 22:13
Tags: Web, api, rest
WordPress-Post-ID: 1657
WordPress-Post-Type: post

The [Zoom.it API][] offers [both RESTful and non-RESTful responses][].
The non-RESTful variant is suitable for clients that are limited in some
way. For example, JavaScript's cross-origin request policy necessitates
JSONP in some cases, and Flash/Silverlight translate all non-200 OK
response codes to 404 Not Found.

Key differences for non-RESTful:

-   Non-RESTful behavior is triggered by the presence of a
    `format=(xml|json)` query string parameter, rather than RESTful's
    Accept header in the HTTP request.
-   All requests return 200 OK
-   The RESTful response is wrapped in a [Response][] object which
    provides data that would otherwise be handled via HTTP (e.g. headers
    like Location or Retry-After, status code and text, etc.).



  [Zoom.it API]: http://zoom.it/pages/api/
  [both RESTful and non-RESTful responses]: http://zoom.it/pages/api/formats/rest-vs-non-rest
  [Response]: http://zoom.it/pages/api/reference/v1/response
