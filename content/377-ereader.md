Title: Creating eReader Books
Slug: 377/ereader
Summary: Reading on the cheap.
Date: 2006-08-03 17:49
Tags: Computers, Palm
WordPress-Post-ID: 377
WordPress-Post-Type: post

I've been reading eBooks via the [eReader][] application since I got my
T5. (Hey, I never blogged about that.) The application itself is good,
and I've purchased around ten titles from their website. I've always
meant to look into eBook creation, though, so I can convert any text
into an eReader-compatible file. Turns out the necessary application is
available on the eReader site: I dub thee "[DropBook][]".

DropBook is a "[free-as-in-beer][]" application for converting
specially-formatted ASCII text into a PDB file suitable for eReader. It
understands the [Palm Markup Language][] (PML), a collection of escape
sequences for creating chapter breaks, titles, bold, italic, and other
assorted formatting cues.

I intend to use this application quite a bit to store short stories and
the like. Maybe I'll finally find time to read some of the
[Intergalactic Medicine Show][] when the back issues are always a tap
away.

See also: [PML special characters][].

  [eReader]: http://www.ereader.com/
  [DropBook]: http://www.ereader.com/dropbook
  [free-as-in-beer]: http://en.wikipedia.org/wiki/Gratis_versus_Libre#Free_as_in_beer_versus_free_as_in_speech
  [Palm Markup Language]: http://www.ereader.com/dropbook/pml
  [Intergalactic Medicine Show]: http://www.intergalacticmedicineshow.com/
  [PML special characters]: http://www.ereader.com/dropbook/pml/characters
