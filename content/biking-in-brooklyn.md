Title: Biking in Brooklyn
Summary: It took a year, but my bike finally followed me to Brooklyn.
Date: 2013-08-24 16:10
Tags: cycling, Brooklyn

It took a year, but my bike finally followed me to Brooklyn. OK, maybe I dragged
it across half of New England in the belly a couple busses, but the end result
is the same.

A mountain bike isn't my ideal ride in the city, but I'm making the best of it.
My [local bike shop] installed a pair of slicks, tires sized for a mountain bike
but smooth rather than knobby. Lights and a bell are a must (NYC: there's a law
for that), though I've avoided night riding to defer the cost of the former.
Past that, the wishlist includes fenders and [panniers]. Locking skewers and
[other security features] (via [reddit][security features credit]) may be a good
investment.

My goal is to switch to a lighter road bike, so I'm hesitant to buy parts that
may not carry over. It's all new to me, so I don't know if parts like fenders
and panniers are interchangable between all bike styles.

Riding itself is great. It's 10 miles from home to work, and a 20 mile daily
rount trip is pretty rewarding. My preferred commuting route is almost
exclusivly on bike lanes (a white line separating bikes from cars). Cars are
respectful for the most part. I worry way more about pedestirans in the Ocean
Parkway bike lane than I do about turning cars.

Pet peeve: continually leapfrogging the same cyclists who ride a more slowly but
run more red lights.

  [local bike shop]: http://roysbikes.com/ "Roy's Sheepshead Cycle"
  [panniers]: http://en.wikipedia.org/wiki/Pannier "Panniers on Wikipedia"
  [other security features]: http://i.imgur.com/pb3pWCh.png
  [security features credit]: http://www.reddit.com/r/NYCbike/comments/1kw2p1/what_do_you_to_to_protect_your_bike_when_its/cbt6tme "Comment on bike security by /u/scoofy on reddit.com"
