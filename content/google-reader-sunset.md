Title: Google Reader Sunset
Date: 2013-03-14 00:14
Category: Web
Tags: google reader, web services, syndication, google
Summary: Google today announced that Google Reader will be shut down on 1 July 2013.

Google today announced that [Google Reader will be shut down][1] on 1 July 2013.
As a daily user of Reader, this is pretty saddening. My Reader had a wide
variety of feeds, and it's the only way I keep track of some of these things. A
sampling:

* Product news (Linode, GitHub, Enyo JS, Zend Framework, WordPress)
* Programming blogs (Coding Horror, The Changelog)
* Web development (A List Apart)
* [Support thread for my WordPress plugin][2]
* Comics, comics, comics
* Design galleries
* Pinboard network
* Flickr feeds
* Podcasts
* Tumblrs

There are other feed readers out there, but it's not a good feeling when a
product you're comfortable with goes dark. It's the helplessness of the
situation: it makes you wonder what else is on the verge of disappearing. (I
still miss you, Annie's Radiatore Pasta with Sundried Tomato and Basil Sauce.)

Thanks, Google Reader team. I found a lot of awesome stuff thanks to your hard
work.

![Google Reader "Trends" page][3]

Bonus: [94,203 more items][4] from my old Google account.

  [1]: http://googlereader.blogspot.com/2013/03/powering-down-google-reader.html
  [2]: http://wordpress.org/tags/wp-whos-online
  [3]: //i.abackstrom.com/sixohthree.com/google-reader-trends.png
  [4]: //i.abackstrom.com/sixohthree.com/old-google-reader-trends.png
