Title: Ansible Playbook: Reboot Servers
Slug: ansible-playbook-reboot-servers
Summary: An Ansible playbook to reboot servers that require a reboot.
Date: 2018-10-08 10:50
Category: Technology
Tags: ansible, Linux

Here's a short Ansible playbook that reboots servers that need a reboot.

```
---
- hosts: all
  tasks:
    - name: check if reboot is required
      stat: path=/var/run/reboot-required
      register: reboot_file
    - name: reboot host
      reboot:
      when: reboot_file.stat.exists == true
```

My hosts are configured to auto-install security updates. Many times, when I log
into a host, I'll get a notification that a reboot is required. This playbook
lets me broadcast a conditional reboot to all my hosts.

The above playbook requires the [reboot module][1] added in Ansible 2.7. The
`command` module could be used on earlier hosts to manually issue a `reboot`.

  [1]: https://docs.ansible.com/ansible/latest/modules/reboot_module.html
