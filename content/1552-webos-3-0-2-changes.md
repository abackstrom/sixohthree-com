Title: webOS 3.0.2 Changes
Slug: 1552/webos-3-0-2-changes
Summary: Observed changes from the webOS 3.0.2 update.
Date: 2011-08-01 21:53
Tags: Computers, Hp, Palm, tablet, touchpad, webOS
WordPress-Post-ID: 1552
WordPress-Post-Type: post

The first webOS 3 update for the HP TouchPad was released today. In
addition to the [official list of changes][], I've noticed following:

Calendar

-   All-day events are always shown, rather than being part of the
    scrolling midnight-to-midnight view.
-   Much more responsive.


Clock

-   The Clock now supports alarms.


Photos & Video

-   Thumbnails are now larger, showing 3 photos and growing to 4 photos
    when the pane is maximized, rather than always showing four
    thumbnails and increasing whitespace when maximizing.


Maps

-   Maps is now driven by Google Maps. Haha, kidding, it's still Bing.


Some miscellaneous things I'm hoping are now fixed:

-   Messaging app would occasionally refuse to go online. Required
    reboot.
-   Email app would sometimes show blank message bodies. Required
    relaunching app.
-   Photos & Video was unable to play large video files. Initial test is
    not promising.


Only time will tell on some of those.

Oh, old and new user-agent string:


> Mozilla/5.0 (hp-tablet; Linux; hpwOS/3.0.0; U; en-US)
> AppleWebKit/534.6 (KHTML, like Gecko) wOSBrowser/233.72 Safari/534.6
> TouchPad/1.0
> </p>
> <p>
> Mozilla/5.0 (hp-tablet; Linux; hpwOS/3.0.2; U; en-US)
> AppleWebKit/534.6 (KHTML, like Gecko) wOSBrowser/234.40.1 Safari/534.6
> TouchPad/1.0


Anyway, three cheers to HP for pushing an update one month after the
tablet's release! Let's keep the momentum going.

  [official list of changes]: http://www.precentral.net/webos-3-0-2-68-update-now-available-hp-touchpad
