Title: Showing Up
Slug: showing-up
Summary: On showing up to the table
Date: 2016-02-01 17:12
Tags: diversity

Katherine Daniels ([@beerops][2]), [On Showing Up to the Table][1]:

> We all enjoyed the camaraderie that came from planning this and showing up
> together, and we want to have a significant representation of women at this
> meeting be such a regular occurrence that people are no longer surprised when
> engineering meetings are full of so many awesome women talking about their
> work.

[1]: http://beero.ps/2016/02/01/on-showing-up-to-the-table/
[2]: https://twitter.com/beerops
