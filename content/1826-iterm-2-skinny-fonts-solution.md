Title: iTerm 2 Skinny Fonts: Solution
Slug: 1826/iterm-2-skinny-fonts-solution
Summary: For reasons that were unknown to me, iTerm 2 would display a nice, thick font when the laptop was the primary screen, but a very thin font when the Acer was primary.
Date: 2011-12-12 11:35
Tags: Personal, bugs, Mac OS X, Terminal
WordPress-Post-ID: 1826
WordPress-Post-Type: post

[![][img]][img-big]

My dual-screen setup involves a MacBook Pro and an external Acer flat
panel, but despite my own preferences, I've kept the laptop screen as
the primary to avoid a font rendering issue. For reasons that were
unknown to me, iTerm 2 would display a nice, thick font when the laptop
was the primary screen, but a very thin font when the Acer was primary.
Switching the primary and relaunching iTerm would cause the thickness to
change. I first experienced the issue in Mac OS X 10.6, and it persisted
into Mac OS X 10.7. I've recycled the bug report-quality screenshot
above as an example of the difference.

Some searching finally brought me to a resolution involving Mac OS X's
built-in font smoothing:


    defaults -currentHost write -globalDomain AppleFontSmoothing -int 2



Thanks to [Steve Kuo][] for [the solution][].

  [img]: https://sixohthree.com/files/2011/12/iterm2-fonts.png
    "iterm2-fonts"
  [img-big]: https://sixohthree.com/files/2011/12/iterm2-fonts.png
  [Steve Kuo]: http://www.stevekuo.com/
  [the solution]: http://apple.stackexchange.com/questions/19468/terminal-text-size-different-when-connected-to-external-monitor
