Title: How "Oldschool" Graphics Worked
Slug: how-oldschool-graphics-worked
Summary: "The 8-Bit Guy" explains how color and graphics worked on old 8-bit systems like the NES and Apple II.
Date: 2016-09-28 15:55
Category: Computers
Tags: graphics, Games

"The 8-Bit Guy" explains how color and graphics worked on old 8-bit systems like the NES and Apple II.

<div class="embed-responsive embed-responsive-16by9 mt-2 mb-2">
    <iframe src="//www.youtube.com/embed/Tfh0ytz8S0k" class="embed-responsive-item" allowfullscreen></iframe>
</div>

<div class="embed-responsive embed-responsive-16by9 mt-2 mb-2">
    <iframe src="https://www.youtube.com/embed/_rsycfDliZU" class="embed-responsive-item" allowfullscreen></iframe>
</div>
