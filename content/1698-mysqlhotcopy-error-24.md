Title: mysqlhotcopy: error 24
Slug: 1698/mysqlhotcopy-error-24
Summary: What does it look like when you try to mysqlhotcopy, but don't have a high enough open files ulimit?
Date: 2011-10-21 00:16
Tags: Scripting, mysqlhotcopy, ulimit
WordPress-Post-ID: 1698
WordPress-Post-Type: post

What does it look like when you try to mysqlhotcopy, but your open files
ulimit is too low? If you answered "error 24 (too many open files)"
you'd be correct, but also:

[code name=hotcopy-batch]

Scary.
