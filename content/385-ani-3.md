Title: Ani DiFranco in Concord
Slug: 385/ani-3
Summary: And many more.
Date: 2006-11-09 12:06
Category: Music
Tags: anidifranco, concert
WordPress-Post-ID: 385
WordPress-Post-Type: post

Tonight, [Ani Difranco][], [Capitol Center for the Arts][], Concord, NH.
This time I will not miss [the show][].

This will be my fifth Ani show, after once in Rochester, NY, twice in
Buffalo, NY, and once at Meadowbrook in Gilford, NH.

**Update:** Good show. I could have done without that girl yelling, "I
love you Ani," at every pause in the music, but I did my best to block
her out. Setlist:

1.  God's Country (Puddle Dive, 1993)
2.  Manhole (Knuckle Down, 2005)
3.  Educated Guess (Educated Guess, 2004)
4.  Half-Assed (Reprieve, 2006)
5.  Lag Time (Knuckle Down, 2005)
6.  Napoleon (Dilate, 1996)
7.  Decree (Reprieve, 2006)
8.  Paradigm (Knuckle Down, 2005)
9.  78% H20 (Reprieve, 2006)
10. Swandive (Little Plastic Castle, 1998)
11. Joyful Girl (Dilate, 1996)
12. Rain Check (Educated Guess, 2004)
13. Grand Canyon (Educated Guess, 2004)
14. Alla This (Unreleased)
15. In the Way (Evolve 2003)
16. Gravel (Little Plastic Castle, 1998)
17. Hypnotized (Reprieve, 2006)

Three seconds into Hypnotized, some girl yelled, "Play some good shit."
Grr.

  [Ani Difranco]: http://www.righteousbabe.com/ani/
  [Capitol Center for the Arts]: http://www.ccanh.com/
  [the show]: /archives/2004/11/09/ani
