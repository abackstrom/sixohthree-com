Title: Aggressive Advertising
Slug: 314/vonage
Date: 2005-06-02 00:20
Tags: Computers
WordPress-Post-ID: 314
WordPress-Post-Type: post

A few days ago I clicked a link to an Ars Technica article and was
greeted by a full-screen advertisement for Vonage. This was not much of
a surprise, these commercial breaks are standard fare on many websites
today. What did surprise me was the page that followed:



[![3 Vonage ads on one page][]][]



Maybe Vonage thought I missed their ad the first time, and they wanted
to be *very sure* noticed them on the second page. Guess it worked.



  [3 Vonage ads on one page]: /media/2005/06/01/ars-vonage_t.jpg
  [![3 Vonage ads on one page][]]: /media/2005/06/01/ars-vonage.jpg
