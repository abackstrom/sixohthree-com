Title: Fall Updates
Slug: 179/fall-updates
Summary: Cheese sandwiches, co-ops, website updates, and thin clients, oh my.
Date: 2003-09-11 19:41
Tags: Personal
WordPress-Post-ID: 179
WordPress-Post-Type: post

[Today][], I ate a [cheese sandwich][].



I accepted a co-op with [Information & Technology Services][] at RIT on
Tuesday. In ten short weeks, I'll be 40 credits away from graduation.



I was looking forward to taking classes again after a summer of
full-time employment. Three-tier database was a fun prospect, and I
definitely need the Oracle experience. Deaf History looked to be
enjoyable as well. Such things will have to wait; co-ops don't come
along every ten weeks.



I finally got around to updating the [graffiti board][]. It's used a
text file since its inception, which I believe was nearly two years ago.
(It's a shame I don't document this stuff better.. Oh well. CVS comes to
my rescue this time.) Now it's got its very own [MySQL][] table, and it
saves post times and an IP hash in case abuse becomes a problem.



My [thin client][] is [for sale][] on eBay. It's a good machine, but I'm
not exactly rolling in dough, so I'll use the funds from the sale to
finance a new server. I'm thinking, nice big black case with lots of
internal expansion room, and some [removable hard drive brackets][].



  [Today]: /archives/cheese_sandwich
  [cheese sandwich]: http://kode-fu.com/shame/2003_03_02_archive.shtml#90420654
  [Information & Technology Services]: http://www.rit.edu/~wwwits/
  [graffiti board]: http://www.bwerp.net/graffiti/
  [MySQL]: http://www.mysql.com/
  [thin client]: http://blogs.bwerp.net/archives/2003/04/08/i_like_em_thin
  [for sale]: http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&item=2752698323
  [removable hard drive brackets]: http://static.bwerp.net/~adam/2003/09/11/drive_rack.jpg
