Title: Video in the Browser
Slug: 884/video-in-the-browser
Summary: Ogg Vorbis video, in the browser, no plugins required.
Date: 2009-06-15 18:25
Tags: Web, html5, ogg, oggtheora, Video, vorbis
WordPress-Post-ID: 884
WordPress-Post-Type: post

Ogg Theora video, in the browser, no plugins required.

[![ogg-vorbis-browser][]][ogg-vorbis-browser]

I will begin taking this for granted immediately. Thanks, [Firefox
3.5b4][].

Update: Ogg Theora has been [backed out][] of the HTML5 draft spec due
to lack of consensus from the big browser makers. Like the \<img\> tag,
support for formats will be left in the hands of developers. A bit sad,
really.

  [ogg-vorbis-browser]: /media/2009/06/ogg-vorbis-browser1.png
  [Firefox 3.5b4]: http://www.mozilla.com/en-US/firefox/3.5b4/releasenotes/
  [backed out]: http://arstechnica.com/open-source/news/2009/07/decoding-the-html-5-video-codec-debate.ars
