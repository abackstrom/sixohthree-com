Title: Linking to E-mail Messages
Slug: 439/email
Summary: But not in Thunderbird.
Date: 2007-12-19 09:19
Tags: Computers
WordPress-Post-ID: 439
WordPress-Post-Type: post

Via [Lifehacker][]: [Bookmark Your Email with Gmail or Mail.app][].
Unfortuantely, I don't use Gmail or Mail.app, the former because I don't
like being tied to specific vendors, and the latter because I can't deal
with its message threading. I've been searching for a way to do this
with Thunderbird (my client of choice) for some time now. Maybe this
will inspire some Thunderbird hacker to implement the functionality.

  [Lifehacker]: http://lifehacker.com/
  [Bookmark Your Email with Gmail or Mail.app]: http://lifehacker.com/software/email-apps/bookmark-your-email-with-gmail-or-mailapp-335377.php
