Title: Public Service Announcement
Slug: 384/vote
Date: 2006-11-07 10:49
Tags: Politics
WordPress-Post-ID: 384
WordPress-Post-Type: post

Four new e-mails in my box today, all saying the same thing: [vote][].

[![Rock the Vote banner][img]][img-big]

New Hampshire has on-site registration at the polls, you granite staters
have no excuse.

  [vote]: http://www.rockthevote.com/
  [img]: http://web.archive.org/web/20050525051603/http://www.rockthevote.com/banners/uptoyoubanners/aol/234x60.gif
  [img-big]: http://www.rockthevote.com/rtv_register.php?ms=Web_banners
