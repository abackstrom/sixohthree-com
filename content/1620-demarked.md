Title: Demarked
Slug: 1620/demarked
Summary: Mark Pilgrim seems to have removed himself from the Internet.
Date: 2011-10-05 00:30
Tags: Personal
WordPress-Post-ID: 1620
WordPress-Post-Type: post

Mark Pilgrim seems to have removed himself from the Internet.
[diveintohtml5.org][], [diveintomark.org][], and others are returning
410 Gone. His [Twitter][] and [Reddit][] accounts have been deleted.

Mark's writings on topics ranging from URIs to substance abuse
captivated me and helped to shape my own opinions. Some of the [earliest
posts][] on this blog mention him by name. "Me, but you, but me" (the
"25 year friend" essay) is frequently on my mind, haunting and
heartbreaking. Find it, if you have the means.

The Internet was better with you in it, Mark. Hope you're OK, whatever
the circumstances.

  [diveintohtml5.org]: http://diveintohtml5.org/
  [diveintomark.org]: http://diveintomark.org/
  [Twitter]: https://twitter.com/diveintomark
  [Reddit]: http://www.reddit.com/user/diveintomark
  [earliest posts]: /147/spamassassin-procmail-and-fetchmail-oh-my
