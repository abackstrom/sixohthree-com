Title: Debian Release Nicknames
Slug: debian-release-nicknames
Summary: I just learned that every public Debian release, starting with Debian 1.1 Buzz in June 1996, has been named after characters from Toy Story.
Date: 2016-01-23 10:09
Tags: Linux

I just learned that every public Debian release, starting with Debian 1.1 *Buzz*
in June 1996, has been named after characters from Toy Story.

I'm... not sure how I went this long without knowing that.

[1]: https://www.debian.org/doc/manuals/project-history/ch-releases.en.html
