Title: Alpha Five Updates: Syndicated Feed
Slug: 426/alpha
Summary: Syndicate your updates.
Date: 2007-08-30 15:48
Tags: Computers, Alpha Five
WordPress-Post-ID: 426
WordPress-Post-Type: post

I've created a cron job to scrape the [Alpha Software][] website and
generate [an Atom feed][] for new releases. It's updated every six hours
and I can confirm that it worked for the most recent update. (1759\_3105
showed up in [Google Reader][] the next morning.)

The feed is generated using [Django][]'s django.utils.feedgenerator
class. The code [is online][].

  [Alpha Software]: http://www.alphasoftware.com/
  [an Atom feed]: http://static.bwerp.net/~adam/alpha-updates/update.xml
  [Google Reader]: http://www.google.com/reader/
  [Django]: http://www.djangoproject.com/
  [is online]: http://static.bwerp.net/~adam/alpha-updates/generate.py.txt
