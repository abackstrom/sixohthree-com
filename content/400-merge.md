Title: Merging PDFs in Linux
Slug: 400/merge
Summary: And it all comes together.
Date: 2007-04-12 15:01
Tags: Computers, Linux, Scripting
WordPress-Post-ID: 400
WordPress-Post-Type: post

There are several free utilities for merging PDFs into one document. For
a long time, Ghostscript was enough for me:


    gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite \
        -sOutputFile=collated.pdf file1.pdf file2.pdf



Today I hit several PDFs with complex table formatting. As far as I
could tell, gs (and pdf2ps) were rasterizing these documents into
embedded bitmaps during processing. This dramatically increased file
size and processing time. Some searching brought up [this page][] and a
comment suggesting the pdftk utility.


    pdftk file1.pdf file2.pdf cat output collated.pdf



pdftk ended up producing much smaller files (far closer to what I would
expect based on starting file size) at very fast speeds. And
*watermarking*, oh how I searched for a utility that could watermark.

Thanks, [pdftk][] team.

  [this page]: http://ansuz.sooke.bc.ca/software/pdf-append.php
  [pdftk]: http://www.accesspdf.com/pdftk/
