Title: Cory Doctorow DRM talk
Slug: 223/doctorow
Summary: DRM only exists to hurt consumers.
Date: 2004-06-20 23:19
Tags: Freedom
WordPress-Post-ID: 223
WordPress-Post-Type: post

Take a look at [Cory Doctorow][]'s recent [DRM talk][] with Microsoft.
Very well put, with great examples of copyright battles of the past.
[via [Joi][] ]



  [Cory Doctorow]: http://www.craphound.com/
  [DRM talk]: http://www.dashes.com/anil/stuff/doctorow-drm-ms.html
  [Joi]: http://joi.ito.com/
