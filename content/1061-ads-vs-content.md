Title: Ads vs. Content
Slug: 1061/ads-vs-content
Date: 2010-05-04 11:00
Tags: Web, Advertising, AdWords, screenshot
WordPress-Post-ID: 1061
WordPress-Post-Type: post
Category: Technology

![Screenshot of hubpages.com with four separate groups of Google ads][img]

Red highlights added by me. I wonder if Google has policies regarding
this type of ad layout. I think having this poor a ratio of content to
ads dilutes all the advertising. Whatever the case, I hate wading
through this many ads in search of the article.

  [img]: /media/2010/05/hubpages-google-ads.jpg "hubpages-google-ads"
