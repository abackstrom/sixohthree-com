Title: Drawing ACS-mode boxes in the terminal with Ruby
Slug: 2051/drawing-acs-mode-boxes-in-the-terminal-with-ruby
Summary: VT100 included an alternate character set (ACS) that enabled box drawing within the terminal. Here's a script from StackOverflow to test support for ACS.
Date: 2012-08-15 10:52
Tags: Computers, Terminal, unicode
WordPress-Post-ID: 2051
WordPress-Post-Type: post

VT100 included an alternate character set (ACS) that enabled box drawing
within the terminal. Here's [a script from StackOverflow][] to test
support for ACS:

<script src="https://gist.github.com/abackstrom/f889e85b83bbae23cc55.js"></script>

If your stack supports Unicode, [some software][] may choose to instead
draw boxes using Unicode's ["box drawing" block][].

Related reading:

-   [About the 'alternate linedrawing character set'][]
-   [Tmux borders displayed as x q instead of lines?][]
-   [ncurses' `NCURSES_NO_UTF8_ACS`][no_utf8_acs]



  [a script from StackOverflow]: http://stackoverflow.com/questions/4494306/drawing-tables-in-terminal-using-ansi-box-characters
  [some software]: http://tmux.sourceforge.net/
  ["box drawing" block]: http://graphemica.com/blocks/box-drawing
  [About the 'alternate linedrawing character set']: http://www.in-ulm.de/~mascheck/various/alternate_charset/#provoke
  [Tmux borders displayed as x q instead of lines?]: http://stackoverflow.com/questions/8483798/tmux-borders-displayed-as-x-q-instead-of-lines
  [no_utf8_acs]: http://linux.die.net/man/3/ncurses
