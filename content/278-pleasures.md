Title: Life's Little Pleasures
Slug: 278/pleasures
Summary: Violent videogames are not for kids.
Date: 2004-12-10 00:40
Tags: Games
WordPress-Post-ID: 278
WordPress-Post-Type: post

I love driving along, listening to the radio, and hearing a song from
the Grand Theft Auto 3 soundtrack come on the air. Ah, memories. I just
have to remind myself to stay between the painted lines.



