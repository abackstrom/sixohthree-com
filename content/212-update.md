Title: Spring Update
Slug: 212/update
Summary: Puzzle in a paragraph.
Date: 2004-05-03 13:35
Tags: Personal
WordPress-Post-ID: 212
WordPress-Post-Type: post

There are just two short weeks left in spring quarter. I'm staying at
RIT during summer to work and take classes as I search for a job. It's
hard to believe I've been at school for almost four years, and my time
as a student is almost over. Very sobering.



(A [slight diversion][]. 255 characters, not including this explanatory
footnote.)



  [slight diversion]: http://bitworking.org/news/The_Pascal_String_Challenge
