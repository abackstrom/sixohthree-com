Title: Blizzard To Post Real Names on Forums
Slug: 1114/blizzard-to-post-real-names-on-forums
Date: 2010-07-06 22:52
Tags: world of warcraft
WordPress-Post-ID: 1114
WordPress-Post-Type: post
Category: Games

[{@align=right}![Forums screenshot][img]][img-big]

Blizzard today [announced] an upcoming change to the way users
are represented on the official forums: included with each post will be
[the user's real name][]. Obviously, the shitstorm was immediate and
intense. (Over <del>5,000 posts in the four hours</del> <ins>12,600
posts in the 7.5 hours</ins> after the announcement, not
to mention all the other threads the change has spawned.)

The highlights:

-   Your real name will show up next to your forum posts.
-   You may optionally pick a character name to display alongside your
    real name.
-   This change will affect the new forums going live around the
    Cataclysm release, so existing posts will not be affected.


I have an opinion, but maybe not the opinion one might expect if you've
ever had a conversation with me about privacy and freedom.

People have the right to privacy. I believe this right does not need to
be defended: there is no "but what about…" here. There are a variety of
defenses, of course, be they medical, religious, sexual, or political,
to name a few, but I think the desire to act in private is itself
enough. The "honest people have nothing to hide" argument is ignorant at
best and dangerous at worst.

Not every space is a private space, though. My home, the contents of
computer, my browser history, are private (or at least I consider them
private), but many of the spaces I frequent are public. In, say, a park,
you have the right to take a photograph, even if I am in the frame. I do
not have the same right to privacy that I have in my home.

Historically, the World of Warcraft (WoW) forums offered a level of
privacy. You could create characters on any of the multitude of servers
and post as those characters, rather than your "main" character,
effectively hiding your identity from everyone but Blizzard employees.
There are many potential uses of this feature, some good (asking
opinions about your guild leader's controversial decision) and some bad
(trolling or threatening posters).

I am not against Blizzard displaying the poster's real name next to
their post. I think it will reduce the number of posters on the site
(good) and result in more thoughtfully constructed posts (also good).

[
    <img src="https://sixohthree.com/files/2010/07/green-blackboards.jpg" align="left"/>
][fuckwad-theory]

NPR recently ran a piece entitled "[Website Editors Strive To Rein In
Nasty Comments][]." It elaborates on [John Gabriel's Greater Internet
Fuckwad Theory][fuckwad-theory], which posits that Normal Person + Anonymity +
Audience = Total Fuckwad ("Shitcock!"). I believe that the addition of
real names to the forums will prevent many of the flame wars from
starting as people consider that the words they say become part of their
online identity. I also believe that it's harder to berate someone when
you see their name on the screen in front of you. Picture two forum
posters, Happycowlol and Dave Burkhart. Both post the same question as
they struggle with basic game mechanics. How likely are you to tell Dave
to log off and delete WoW, particularly if your comment is signed with
your real name?

Blizzard recently added Real ID Friends to World of Warcraft. You can
add a person to your buddy list and see them online, no matter which
game they are playing or which character or server they're playing on.
This is opt-in: you explicitly grant the right to be your Real ID friend
and see you online. Likewise, posting on the forums is a choice. If
privacy is your concern, there are hundreds of other forums, some of
which may even attempt to protect your data. I do not believe Blizzard
is obligated to provide users with an anonymous (or quasi-anonymous)
message board. On the contrary, I believe this is one of the best things
they could do to foster real discussion and discourage trolls.

Some constructive, helpful, genuinely good posters will be driven away
by this change. But they have not had any of their rights taken away
from them. They are still anonymous in the game, they have just lost the
ability to be anonymous on the forums.

**Update:** As of 2010-07-07, there's an [additional blue post][1] by Wryxian
over on the European forums:

> We have been planning this change for
> a *very*long time. During this time, we have thought ahead about the
> scope and impact of this change and predicted that many people would no
> longer wish to post in the forums after this change goes live. We are
> fine with that, because we want to change these forums dramatically in a
> positive and more constructive direction.
>
> It's been very obvious over the last few years that the forums are an
> exceptionally valuable source of information both for players and for us
> to gather feedback. There are many threads on this forum now, and over
> the last few years, that people have been constructively discussing many
> aspects of the game. They've received new wisdom and have then been able
> to go back to the game and enjoy it further with the new knowledge
> acquired through the forums.
>
> These threads, however, can often be lost amongst a great deal of other
> threads that are basically filled with trolling, name calling, flaming,
> off-topic conversations and that's just a small amount of some of the
> content that has been found in these forums over the years. We don't
> want that anymore, and we believe the Real ID change will bring about a
> lot of the improvement that we are hoping for.
>
> There's a lot of scare-mongering going on about the change, but there
> seems a need to make something very clear. The forums have *always* been
> an optional extra -- something you can choose to participate in if
> you *wish* to. With our Real ID changes for the forums, *this is still
> the case*. The only difference will be, if you do choose to participate
> in the forums, then you will do so by using your real name. But only
> after you've been warned and accepted this in advance.

  [img]: https://sixohthree.com/files/2010/07/forum-post-300x149.png
    "Nethaera's Post about the Real ID Change"
  [announced]: http://forums.worldofwarcraft.com/thread.html?topicId=25712374700
  [the user's real name]: http://forums.battle.net/thread.html?topicId=25626109041
  [fuckwad-theory]: http://www.penny-arcade.com/comic/2004/3/19/
  [Website Editors Strive To Rein In Nasty Comments]: http://www.npr.org/templates/story/story.php?storyId=126782677
  [1]: http://forums.wow-europe.com/thread.html?topicId=13816838128&postId=140209202293&sid=1\#4053
