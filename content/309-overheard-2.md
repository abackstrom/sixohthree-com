Title: Must Read
Slug: 309/overheard-2
Summary: Do it.
Date: 2005-05-12 23:10
Tags: Humor, Music
WordPress-Post-ID: 309
WordPress-Post-Type: post

The newest feed in my aggregator: [Overheard in New York][]. (Not
entirely safe for work, but text-only.) I've laughed out loud many times
tonight.



As an aside, I like reading through and picking out places [Mike
Doughty][] and [Ani DiFranco][] have mentioned in their songs. (Ludlow
St., Astor Place, etc.) I do not know New York City, I would not survive
there, but I still have an emotional reaction to these places because of
music.



Thank you, Lord, for sending me the F-Train.



  [Overheard in New York]: http://www.overheardinnewyork.com/
  [Mike Doughty]: http://www.superspecialquestions.com/
  [Ani DiFranco]: http://www.righteousbabe.com/ani/
