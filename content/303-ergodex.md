Title: Ergodex DX1
Slug: 303/ergodex
Summary: Customize your input.
Date: 2005-04-08 08:09
Tags: Computers
WordPress-Post-ID: 303
WordPress-Post-Type: post

I was looking at photos of the [ZBoard][], and came across this spiffy
little input device: the [Ergodex DX1][]. It's a user-configurable
keyboard in the most literal sense. Not only can the keys perform any
action, but they can be place anywhere on the board.

  [ZBoard]: http://www.zboard.com/
  [Ergodex DX1]: http://www.ergodex.com/
