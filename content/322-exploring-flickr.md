Title: Exploring Flickr
Slug: 322/exploring-flickr
Summary: Where do I sign?
Date: 2005-07-28 09:25
Tags: Photography
WordPress-Post-ID: 322
WordPress-Post-Type: post

I've set up a [test account][] on [Flickr][], an online photo gallery
site. I've been using [Gallery][] for some time now. Gallery is
functional, but I'd like to offload the hosting responsibilities onto
someone else and see some fresh software in the process.



I'm pretty impressed with Flickr's photo management applications so far.
There's a lot of Flash involved, and I'll have to resort to Windows to
use the upload utility, but there's good metadata going on here. I'll
probably upgrade to the Pro version once I hit my monthly upload cap.



I'm hoping this will motivate me to use my camera more often. I like the
visual record, and I think I'm more likely to shoot if I can make
sharing a bit easier.



  [test account]: http://www.flickr.com/photos/adambackstrom/
  [Flickr]: http://www.flickr.com/
  [Gallery]: http://gallery.sf.net/
