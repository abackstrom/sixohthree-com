Title: Open Letter to Don McIver
Slug: 703/open-letter-to-don-mciver
Summary: Wherein I call a spade a spade.
Date: 2009-01-14 18:40
Tags: Internet, freecycle, plymouthnhfreecycle
WordPress-Post-ID: 703
WordPress-Post-Type: post

I sent the following message to the PlymouthNHFreecycle group today. It
is archived here for posterity.

> From: Annika Backstrom  
> To: Don McIver  
> Cc: PlymouthNHFreecycle  
> Subject: WANTED: Moderator civility (Wentworth)
>
> Don,
>
> I am writing to express my distaste for the way you handled a recent
> post to the PlymouthNHFreecycle group. This email, subject "WANTED:
> HOUSEHOLD ITEM: WARREN," was a well-intentioned request for donations of
> any unwanted items for a family with very little. While it is certainly
> your prerogative to moderate the group as you see fit, and indeed your
> moderation was in line with the goals of Freecycle, your replies were
> unnecessarily rude and they betrayed the spirit of the original email.
> You could have chosen to redirect the poster to local charity efforts,
> but instead you were curt and threatening. This was inappropriate, and I
> am not interested in membership in any group with such an attitude at
> its core. As a result I will be unsubscribing from Freecycle today.
>
> You may do well to re-read the Freecycle moderator guide, including the
> first line of the Moderator Oath: "I pledge to be a really nice and
> patient person when moderating our new Freecycle group."
>
> http://www.freecycle.org/faq/manual/mod\_oath
>
> http://www.freecycle.org/faq/manual/notices\_holiday

For context, here are the relevant posts. The original request:

> From: Leslie Backstrom  
> To: PlymouthNHFreecycle  
> Subject: [PlymouthNHFreecycle] WANTED: HOUSEHOLD ITEM: WARREN
>
> A family of 4 (2 adults, 2 kids ages 8 & 2) who have been homeless
> and now have found a trailer to rent in Warren, are in need of items
> to furnish their new place. If you have anything to donate please
> call them directly at 764-9904,
>
> Any help is greatly appreciated.

Your group reply:

> From: Don McIver  
> To: PlymouthNHFreecycle  
> Subject: [PlymouthNHFreecycle] ADMIN: Inappropriate posts
>
> A post made it to the group requesting items for a family. Posts like
> this are inappropriate and will not be tolerated. If a post makes it
> through as this one did, the post will be deleted and and the sender
> contacted. Please accept my apologize for this error.

And your private reply:

> From: Don McIver  
> To: Leslie Backstrom
>
> Your post is entirely inappropriate for freecycle. Freecycle is NOT
> a charity. Your message has been deleted from the group site.
> Any further post like this will result in your removal from the group.

### And then...

After the message was received by the group:

> From: Don McIver  
> To: Annika Backstrom  
> Subject: Re: WANTED: Moderator civility (Wentworth)
>
> You are free to do what you like. The poster has been a member for
> over a year well knew the the rules. And in no way was the response
> threatening other than to remove the member if such an event happens
> again. Redirecting the poster to charitable organizations was a moot
> point since the post had already been sent to the members. And your
> posting this directly to the membership is highly egregious. If you
> were not leaving on your own, you would be removed.
>
> Owner / Moderator  
> PlymouthNHFreecycle

And:

> From: Don McIver  
> To: PlymouthNHFreecycle  
> Subject: [PlymouthNHFreecycle] ADMIN: Inappropriate posts
>
> I must apologize for the most recent message post to this group. This
> should have sent directly to me and not to the board. The offending
> member has been dealt with. Again, sorry for subjecting all members to
> something that should have been dealt with personally.
>
> Don



