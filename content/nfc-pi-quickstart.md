Title: Post title
Slug: slug
Summary: summary
Date: 2018-06-16 11:15
Category: Personal
Tags: tags
Status: draft

1. Flash SD card with Raspbian
2. Enable SPI (edit config.txt or use rasbpi-config)
3. Give the current user SPI access: `sudo usermod -a -G spi,gpio $USER`
    * See permissions on `/dev/spidev0.*`
3. edit /usr/share/alsa/alsa.conf, change the line “pcm.front cards.pcm.front” to “pcm.front cards.pcm.default”
3. `sudo apt-get install build-essential cmake python3-dev python2.7-dev`
4. `dpkg -i NFC-Reader-Library-4.010-2.deb`
    https://www.nxp.com/products/identification-and-security/nfc/nfc-reader-ics/nfc-reader-library-software-support-for-nfc-frontend-solutions:NFC-READER-LIBRARY?&tab=In-Depth_Tab
5. `virtualenv ~/.virtualenv/nfc`
6. `mkdir ~/nfc`
7. `cd ~/nfc`
8. `. ~/.virtualenv/nfc/bin/activate`
9. `pip install nxppy`
    * https://github.com/svvitale/nxppy


<!-- links -->

  [1]: 
  [2]: 
  [3]: 
  [4]: 
  [5]: 
  [6]: 
  [7]: 

