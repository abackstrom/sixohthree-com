Title: Transit Markers in Apple Maps
Slug: 2180/transit-markers-apple-maps
Summary: Least favorite feature about Apple Maps in iOS 6: the transit markers are next to useless.
Date: 2012-12-09 15:33
Tags: Computers, apple, iOS 6, iphone
WordPress-Post-ID: 2180
WordPress-Post-Type: post

Least favorite feature about Apple Maps in iOS 6: the transit markers
are next to useless. I'm new to New York, and I don't have all the
subway stations memorized. If you're gonna show me a subway stop (with
an icon of a car, no less), you really need to show the lines that stop
here.

<figure class="left">
    <a href="https://sixohthree.com/files/2012/12/apple-maps.jpg">
        <img src="https://sixohthree.com/files/2012/12/apple-maps-200x300.jpg" />
    </a>
    <figcaption>Apple Maps (no station information)</figcaption>
</figure>

<figure class="left">
    <a href="https://sixohthree.com/files/2012/12/google-maps.png">
        <img src="https://sixohthree.com/files/2012/12/google-maps-200x300.png" />
    </a>
    <figcaption>Google Maps (with station information)</figcaption>
</figure>
