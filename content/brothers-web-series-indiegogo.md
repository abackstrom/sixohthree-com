Title: "Brothers" web series on Indiegogo
Slug: brothers-web-series-indiegogo
Summary: BROTHERS follows the daily lives, the ups and down, ins and outs of what it means to live as a transgender individual in today’s urban society.
Date: 2014-06-03 09:38
Tags: transgenderism

<div class="embed-responsive embed-responsive-16by9">
    <iframe src="//player.vimeo.com/video/96857133" class="embed-responsive-item" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

> There is a definite lack of trans masculine stories being told; stories that
> reflect us and the people we know. This is where you come in. BROTHERS follows
> the daily lives, the ups and down, ins and outs of what it means to live as a
> transgender individual in today’s urban society.

[BROTHERS Web Series](https://www.indiegogo.com/projects/brothers-web-series)
