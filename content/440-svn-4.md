Title: SVN Repository Explodes, No Serious Injuries
Slug: 440/svn-4
Summary: Close call.
Date: 2007-12-27 15:30
Tags: Computers
WordPress-Post-ID: 440
WordPress-Post-Type: post

One of my [Subversion][] repositories blew up today. During updates and
checkouts, I started getting this error:


    annika@shed:~/documents/people$ svn up
    svn: Checksum mismatch on rep '32i':
       expected:  1cd48bbb9a07b5ab299172917fdf7d2f
         actual:  2d1ddccd04a393cab4e3594e631a1560



I searched around and found some fixes involving db\_dump and
db\_recover, but these just resulted in PANIC error messages and
suggestions that I should run db\_recover. Apparently there are some
admitted flaws in Subversion's use of the Berkeley DB libraries, and new
stores are [fsfs][] by default. I've [switched][] the offending
repository to fsfs and things are working much better now. Didn't even
have to recreate my working directories. Here's the code, in case that
link dies:


    svnadmin create --fs-type=fsfs fsfs-repo
    svnadmin dump repo | svnadmin load fsfs-repo
    mv repo backup-repo
    mv fsfs-repo repo



TODO: grab a copy of [svn-fast-backup][] for repository backups.

  [Subversion]: http://subversion.tigris.org/
  [fsfs]: http://svn.collab.net/repos/svn/trunk/notes/fsfs
  [switched]: http://svn.haxx.se/users/archive-2005-02/0557.shtml
  [svn-fast-backup]: http://svn.collab.net/repos/svn/trunk/contrib/server-side/svn-fast-backup
