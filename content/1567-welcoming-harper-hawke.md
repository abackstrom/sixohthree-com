Title: Welcoming Harper Hawke
Slug: 1567/welcoming-harper-hawke
Summary: Harper Hawke Backstrom came into this world Wednesday, August 10, 2011, at 21:24. We are happy to announce that he is healthy, handsome, and nursing well.
Date: 2011-08-12 13:32
Tags: Kid
WordPress-Post-ID: 1567
WordPress-Post-Type: post

![Newborn Harper, swaddled][img]

Harper Hawke Backstrom came into this world Wednesday, August 10, 2011,
at 21:24. Due September 3, his arrival was accelerated by a torn bag of
waters, but we are happy to announce that he is healthy, handsome, and
nursing well.

Harper was born in the same hospital, in the same room, assisted by the
same midwife, though through a much different labor. I am thankful that
we were not completely unprepared, and I am thankful for those friends
and family who dropped what they were doing to help us in our time of
need: Patty C., Donna C., Jenny and Justin T., thank you for letting us
focus on the task at hand. Mom and Dad, thank you for being such good
caretakers for our firstborn as we wait for our family to be whole
again.

Thank you to our midwives and nurses, who have taken such good care of
us these past few days.

Thank you to our friends and family who wished us well and kept us in
their thoughts. We look forward to getting to know Harper with you, and
seeing the world through his eyes.

  [img]: https://sixohthree.com/files/2011/08/DSC_4031-300x199.jpg "Harper Hawke"
