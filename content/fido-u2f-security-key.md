Title: FIDO U2F Security Key
Slug: fido-u2f-security-key
Summary: I recently heard about FIDO U2F through GitHub, and picked up a key on Amazon. Here's how it works with Dropbox and Google.
Date: 2015-10-07 09:41
Tags: security
Meta-Image-Square: /media/2015/10/hyperfido-u2f-512x512.jpg

![HyperFIDO U2F USB Key](/media/2015/10/hyperfido-u2f-740x450.jpg)

I recently heard about [FIDO U2F][1] through GitHub's [announcement][2] and
partnership with Yubico. There are a few keys available on Amazon for under $20;
I [snagged one][3] for $10 and immediately added it to my Dropbox account as a
test run. Setup was painless and less complicated than using Google
Authenticator: where near-ubiquitous 2FA asks you to scan a QR code in your
Authenticator app before confirming the code, U2F just asks you to press the
button on your dongle, no confirmation required.

![Google Security: Adding a Security Key to your account](/media/2015/10/google-add-security-key-694x314.png)
{: .figure }

For unsupported platforms (pretty much anything besides the Chrome browser),
Dropbox falls back on 2FA via SMS. Similarly, Google asks for your security key
when you sign in through Chrome, but you can switch to other auth methods like
SMS or 2FA.

I was skeptical of this tech at first given that I'm so frequently on mobile
devices, but these providers have layered in U2F as a more convenient
alternative to 2FA codes while still supporting the old methods. With big names
like Google and GitHub pushing the tech (and subsidizing keys) I hope we see
adoption pick up over the next couple years.

![Google Sign In: Insert your security key](/media/2015/10/google-insert-security-key-418x546.png)
{: .figure }

![Google Sign In: Use an alternate sign in method](/media/2015/10/google-alternate-sign-in-422x633.png)
{: .figure }

  [1]: https://en.wikipedia.org/wiki/Universal_2nd_Factor
  [2]: https://github.com/blog/2071-github-supports-universal-2nd-factor-authentication
  [3]: http://www.amazon.com/gp/product/B00WIX4JMC
