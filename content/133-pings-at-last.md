Title: Pings at last!
Slug: 133/pings-at-last
Date: 2003-01-29 13:21
Category: Meta
WordPress-Post-ID: 133
WordPress-Post-Type: post

Thank God Almighty, pings at last! Since this blog's inception, I
haven't been able to automatically ping blo.gs and weblogs.com. MT would
say it pinged, but I wouldn't show up on either site. I noticed that I
was able to ping from another MT installation on the same server, and
realized that I downloaded the whole package with libraries for that
install. A quick copy of the full extlib folder fixed my problem right
up. So, in the end, my pinging problem was an incompatible version of
one of the libraries.

Hopefully this will help anybody else whose pings aren't working. I
looked all over the web and never found a good answer for my problem.
