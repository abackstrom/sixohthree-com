Title: Twist that Knife
Slug: 329/katrina
Summary: Ouch.
Date: 2005-08-31 18:23
Tags: World
WordPress-Post-ID: 329
WordPress-Post-Type: post

Here's an unwelcome sight at the local gas station:

![][]

Though I guess it could be worse... it's not like my car is under [three
feet of water][].

  [img]: /media/2005/08/31/post-katrina-gas.jpg
  [three feet of water]: http://www.flickr.com/photos/tidewatermuse/38963406/
