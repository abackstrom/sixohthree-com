Title: Information Gathering
Slug: 914/information-gathering
Summary: The lure of data is still strong, but how do you organize it all?
Date: 2009-08-26 08:07
Tags: Technology
WordPress-Post-ID: 914
WordPress-Post-Type: post

[![devonthink][]][]I've written before about the [lure of data][]. In a
similar vein, it's been  suggested that information [triggers the reward
response][] in our brains. Whatever the chemical reasons behind it all,
I am a data packrat.

My biggest problem with being a data packrat is organization. It took me
countless searches to find the above article at scienceblogs.com, in
part because I first combed through a number of my own repositories
before falling back on Google searches. Did I put that in [Delicious][]?
Is it in my [feed reader][]'s history? Repeat that for 20 minutes.

It's revealing to enumerate all the places I might put a piece of data:

1.  Twitter
2.  P2 blog
3.  Delicious
4.  Google Reader (which offers "share," "like" and "star," as well as
    custom tags)
5.  MediaWiki (public)
6.  MediaWiki (private, mostly a relic from before Google Docs)
7.  This blog
8.  [DEVONthink][]
9.  Firefox bookmarks (I generally avoid these)


That doesn't even hit possible storage locations for images. I can tick
off five of those without any effort. Most of these repositories will
become obsolete over time, having fallen out of fashion or been replaced
by something bigger and better. Some data moves forward into the new
tool, some stagnates out of sight.

I have no answers, here.

  [devonthink]: /media/2009/08/devonthink-300x199.png
  [![devonthink][]]: /media/2009/08/devonthink.png
  [lure of data]: /167/the-lure-of-data
  [triggers the reward response]: http://scienceblogs.com/notrocketscience/2009/07/why_information_is_its_own_reward_-_same_neurons_signal_thir.php
  [Delicious]: http://delicious.com/
  [feed reader]: http://www.google.com/reader/
  [DEVONthink]: http://www.devon-technologies.com/products/devonthink/
