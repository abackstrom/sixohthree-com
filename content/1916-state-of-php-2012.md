Title: State of PHP 2012
Slug: 1916/state-of-php-2012
Summary: Rasmus presented the State of PHP 2012 at Etsy. Here are some pretty raw notes, with approximate times.
Date: 2012-02-04 08:27
Tags: PHP
WordPress-Post-ID: 1916
WordPress-Post-Type: post

[Rasmus][] presented the [State of PHP 2012][] at Etsy. Here are some
pretty raw notes, with approximate times.

**Scaling:**


> *32:40* Don't put too much stuff in a single web server. Your site is
> getting busier and busier, the answer isn't just to crank up the
> concurrent processes. ... 4-6x number of cores in the machine,
> depending on memory.


**SPL Data Structures:**


> *48:40* We also have some new SPL data structures. Here as you pop
> things onto the stack, they'll foreach in the right order.

**Moving from mod\_php to nginx:**

> *50:22* .user.ini and path-specific ini settings. Set a TTL so they're
> not read every time. There's no mod\_rewrite in php-fpm or nginx.
> That's where you're gonna have to learn a little bit of [LUA][] and
> play with the nginx configuration format.

**On data standards:**

> *~1:18:00* Web apps these days should be UTF-8 and UTC. It's the only
> sane choice.

**On client skepticism of using PHP for a large site:**

> *~1:30:00* Other people have managed to do it. A big website is hard
> in every language. [...] It's so much more than the front-end
> language. Every large site in the world ends up using dozens of
> technologies, hundreds for some of them, so it's a matter of putting
> all these pieces together.

Other Takeaways
---------------

    // handle json_encode() on your object
    class Foo implements JsonSerializable{}


Error reporting:

> Setting `error_reporting = -1` turns everything on.

SQLite:

> You should never write a flat text file parser again. If you're ever
> looking at doing that, use SQLite.

  [Rasmus]: http://twitter.com/rasmus
  [State of PHP 2012]: http://www.livestream.com/etsycodeascraft/video?clipId=pla_e00bfe34-e377-42d1-b859-ccf97b12c519
  [LUA]: http://wiki.nginx.org/HttpLuaModule
