Title: Speaking of Coding...
Slug: 242/coding
Summary: I speak in tags.
Date: 2004-08-27 20:56
Tags: Personal
WordPress-Post-ID: 242
WordPress-Post-Type: post

I've had to disable so many of the HTML cleanup features in WordPress. I
[disabled XHTML markup][], completely disabled the filtering in posts
displays in `wp-includes/template-functions-post.php`, and just went
through and disabled the balanceTags() calls in `wp-admin/post.php`.
Sheesh. WordPress thinks it's smarter than I am, but I've got news for
it.



  [disabled XHTML markup]: http://www.hixie.ch/advocacy/xhtml
