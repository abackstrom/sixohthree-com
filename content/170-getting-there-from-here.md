Title: Getting There From Here
Slug: 170/getting-there-from-here
Summary: I've logged sixty-four hours of long drives since the beginning of June. That's a lot.
Date: 2003-07-29 19:33
Tags: Personal
WordPress-Post-ID: 170
WordPress-Post-Type: post

I've logged sixty-four hours of long drives since the beginning of June.
That's a lot.

No, this will not turn into yet *another* rant. It's just that every
time I take these long trips, I can't help but wish for a better way to
get where I'm going. Something safer, more comfortable, more convenient.
Yes, to have a car is to be independant and free and all that, but it
requires the driver's full attention, and so many drivers are downright
inconsiderate. Cars are not my favorite thing. In Amtrack we Trust.

A part of me sincerely hopes that trains will have a resurgence in
popularity. There's something about trains that draws me in. The gentle
rocking back and forth, the trees rushing by on either side... I find
them very soothing compared to the cutthroat driving of New York state.

Obviously, I need transportation once I get to my destination. Sadly,
this means putting 800 miles on my ailing Escort for the convenience of
having it at home. Someday soon, I'll be able to kill two birds with one
stone. Amtrak already offers the [Auto Train][], able to ferry cars from
Lorton, VA, to Sanford, FL. All the convenience of having my own car,
and none of the aggrivations of driving. It's a start, though at over
\$500 round-trip, it's hardly a bargain. Someday, though...

Midday musings of a bored college kid. Smile and nod, smile and nod.

  [Auto Train]: http://www.amtrak.com/trains/autotrain.html
