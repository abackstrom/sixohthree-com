Title: Hello Blog
Slug: 275/hello
Summary: Haven't been updating lately.
Date: 2004-12-04 01:55
Tags: Computers, Personal
WordPress-Post-ID: 275
WordPress-Post-Type: post

Haven't been updating lately. Haven't been doing much of anything
besides working. I get home and I don't feel like doing anything, like
coding or sysadmining or any of the other stuff I love.



I'm learning a new database application for work called Alpha Five V5,
from [Alpha Software][]. It shows promise. As a PHP/MySQL kind of guy,
so much of what I do revolves around basic functionality. *How do I make
this data available to the next page*, *how will I display this status
message to the user*, that sort of thing. It will be nice to have those
things taken care of for me so I can concentrate on the meat of the
application, where all the fun happens.



Today was the first real snowfall of winter. It was cold, but worth it.



There was a server move with [the company][] hosting www.bwerp.net. This
has resulted in some disarray of varying degrees, ranging from
troublesome to catastrophic. Mainly:



1.  SSH access is no longer available. This means I cannot maintain my
    site with CVS and Karl can no longer update his site using
    CheapBlog.
2.  The user directory paths have been modified, and are no longer
    prepended with a tilde. This has broken hundreds upon hundreds of
    existing links to sub-pages at bwerp.net.



The site will be moved to [DreamHost][] when motivation finds me.
Nothing against Simpli, Inc. I've been their customer for a long time
and have had a pretty positive experience, but it's time to move along.



Life goes on.



  [Alpha Software]: http://www.alphasoftware.com/
  [the company]: http://www.simpli.biz/
  [DreamHost]: http://www.dreamhost.com/
