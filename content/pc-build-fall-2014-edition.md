Title: PC Build (Fall 2014 Edition)
Slug: pc-build-fall-2014-edition
Summary: I'm building a new budget gaming PC with Marshall.
Date: 2014-10-09 9:46
Category: Computers
Tags: gaming, pc build

I'm building a new budget gaming PC with Marshall. Minecraft shouldn't be a
problem. Ideally it will handle World of Warcraft (likely) and my as-yet-unused
copy of Skyrim.

| Part | Name | Price |
| ---- | ---- | ----- |
| Motherboard | MSI A88X-G43 | [$82.99][1] |
| Case | Rosewill Challenger | [$49.99][2] |
| CPU | AMD Athlon Quad-core 3.8GHz (FM2) | [$80.99][4] |
| Graphics Card | GeForce GTX 750 Superclocked | [$124.99][5] |
| RAM | Crucial 4GB DDR3 | [$38.99][6] |
| HDD | Seagate Barracuda 1TB SATA 6 Gb/s | [$53.99][7] |
| Optical | Asus 24x DVD-RW DRW-24B1ST | [$20.39][11] |
| Power Supply | Rosewill 450W | [$59.99][8] |
| Keyboard | Logitech G105 | [$39.99][9] |
| Mouse | E-3lue Cobra | [$11.99][10] |

Plus Windows 8.1 System Builder edition ([$92.00][3]), the total is $656.30. I went a
little over budget but this feels pretty good.

Update 2017-02-17: the [December 2016 build][12] is now posted.

  [1]: http://smile.amazon.com/MSI-Computer-Corp-A88X-G43-Motherboards/dp/B00HPQ0Q74/ref=sr_1_1?s=electronics&ie=UTF8&qid=1412808527&sr=1-1&keywords=MSI+A88X-G43
  [2]: http://smile.amazon.com/Rosewill-Black-Gaming-Computer-CHALLENGER/dp/B003YVJJ5Y/?tag=logicaincrem-20
  [3]: http://smile.amazon.com/Windows-8-1-System-Builder-64-Bit/dp/B00F3ZN2W0/ref=sr_1_1?s=software&ie=UTF8&qid=1412809390&sr=1-1&keywords=Windows+8+System+Builder+OEM
  [4]: http://smile.amazon.com/AMD-Athlon-Processor-AD760KWOHLBOX-Richland/dp/B00D7BYMB2/?tag=logicaincrem-20
  [5]: http://smile.amazon.com/EVGA-Superclocked-Dual-Link-Graphics-01G-P4-2753-KR/dp/B00IDG3LUO/ref=sr_1_3?s=pc&ie=UTF8&qid=1412810080&sr=1-3&keywords=gtx+750
  [6]: http://smile.amazon.com/Crucial-Ballistix-PC3-12800-240-Pin-BLS4G3D1609DS1S00/dp/B006WAGG14/?tag=logicaincrem-20
  [7]: http://smile.amazon.com/Seagate-Barracuda-3-5-Inch-Internal-ST1000DM003/dp/B005T3GRNW?tag=logicaincrem-20
  [8]: http://smile.amazon.com/dp/B006BCKDGW/?tag=logicaincrem-20
  [9]: http://smile.amazon.com/Logitech-G105-Gaming-Keyboard-Backlighting/dp/B00BBUCCKO?tag=li-org-per-key-us-20
  [10]: http://smile.amazon.com/dp/B005CPGHAA/?tag=li-org-per-mou-us-20
  [11]: http://smile.amazon.com/Asus-Serial-ATA-Internal-Optical-DRW-24B1ST/dp/B0033Z2BAQ/ref=sr_1_2?s=pc&ie=UTF8&qid=1412943235&sr=1-2&keywords=dvd
  [12]: /pc-build-december-2016-edition
