Title: Tracking Comments
Slug: 149/tracking-comments
Date: 2003-04-22 18:34
Tags: Blogging
WordPress-Post-ID: 149
WordPress-Post-Type: post

I don't comment on other blogs very often, but when I do I tend to have
trouble keeping track of the conversations. MovableType's threadless
comments are hard enough to read as it is without completely forgetting
which blogs I've visited.

Here's what I propose: since I'm already giving out my URL with every
comment, why not let the host blog ping my site and keep me up-to-date
on the conversation? If only bandwidth and server load were as small a
concern for all bloggers as they are for me.. My server would happily
deal with pings and RSS feeds at each reply.
