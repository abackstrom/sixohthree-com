Title: Marked: Overriding Styles
Date: 2013-04-19 10:29
Slug: marked-overriding-styles
Summary: The GitHub style bundled with Marked is pretty good, but I prefer to substitute a couple fonts and remove the outer border. Here's how I modify the built-in stylesheet without duplicating the whole CSS file.

The GitHub style bundled with [Marked](http://markedapp.com/) is pretty good,
but I prefer to substitute a couple fonts and remove the outer border. Here's
how I modify the built-in stylesheet without duplicating the whole CSS file:

    /*
       Title: GitHub Modified
       Author: Annika Backstrom
       Description: Additional styles for Marked's GitHub style.

       Save this file to ~/Library/Application Support/Marked/Custom CSS
    */

    @import url("/Applications/Marked.app/Contents/Resources/github.css");

    body {
        font-family: Open Sans;
    }

    tt, code {
        font-family: Inconsolata;
        font-size: 14px;
    }

    #wrapper {
        box-shadow: none;
        padding: 15px;
    }
