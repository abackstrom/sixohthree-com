Title: The Message is Clear
Slug: the-message-is-clear
Summary: &ldquo;Boys are the norm, girls the variation.&rdquo;
Date: 2013-11-21 09:24
Tags: feminism, equality, gaming
Category: Social

[
![Screenshot of Ms. Male Character video](//i.abackstrom.com/sixohthree.com/katha-pollitt.1385044163.jpg)
][1]

> The message is clear. Boys are the norm, girls the variation; boys are
> central, girls peripheral; boys are individuals, girls types. Boys define the
> group, its story and its code of values. Girls exist only in relation to boys.

Katha Pollitt, &ldquo;Hers; The Smurfette Principle,&rdquo; via [Ms. Male
Character - Tropes vs Women in Video Games][1].

  [1]: http://youtu.be/eYqYLfm1rWA
