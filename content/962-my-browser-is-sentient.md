Title: My Browser is Sentient
Slug: 962/my-browser-is-sentient
Summary: Noticed some weird traffic coming into my router this morning.
Date: 2010-01-05 16:01
Tags: Web
WordPress-Post-ID: 962
WordPress-Post-Type: post

Noticed some weird traffic coming into my router this morning:

[![traffic][]][]

The incoming traffic (blue line) was maxed out at my bandwidth cap, even
though I wasn't downloading anything. After a few failed attempts at
figuring out what was going on, I started closing browser tabs,
resulting in the sustained dropoff near the end of the graph. I reopened
the tabs (small bump closer to the end) but the incoming traffic did not
spike.

Gmail and a few other pages were open, but nothing that would explain
the traffic spike I saw. Maybe the ghost in the machine was watching
Hulu.

  [traffic]: https://sixohthree.com/files/2010/01/traffic.png "traffic"
  [![traffic][]]: https://sixohthree.com/files/2010/01/traffic.png
