Title: Bash Tips: Set Remote Variables
Slug: 282/remote
Summary: Seldom necessary, always difficult.
Date: 2004-12-20 22:00
Tags: Computers, Linux, Scripting
WordPress-Post-ID: 282
WordPress-Post-Type: post

As I've mentioned, I'm now on [DreamHost][]. It's pretty dreamy (as you
might expect), but [`screen`][] is not allowed. This is a slight
inconvenience, since I rather like `screen` and use it constantly. Plan
B involves running `screen` locally and SSH'ing to DreamHost in every
window, but I lose my tailored bash prompt. Normally, my prompt looks
like this:

    annika@aziz[0]:~$

I can immediately tell that `screen` is running, as well as my window
number. This breaks down as soon as I SSH to DreamHost. I get a far more
generic prompt:

    aestus@jezebel:~$

So, what to do? Set a variable on the remote side, in a roundabout sort
of way:

    ssh -t jezebel.dreamhost.com 'export MYVAR=myvalue &&
        exec /bin/bash --login'

It doesn't look like much. It took me an hour to assemble. SSH to the
host, allocating a TTY with `-t`. Run `bash` on the remote side as a
login shell, executing the specified command. Export a variable, and
exec a sub-bash.

Yeah, that's it.

<ins>Update 12/15/2005: Trimmed the
command a bit: the outer /bin/bash call is unnecessary.{@class=update}</ins>

  [DreamHost]: http://www.dreamhost.com/
  [`screen`]: http://www.wlug.org.nz/Screen
