Title: Being Ultra Semantic
Slug: 247/semantic
Summary: HTML can be beautiful, if you let it.
Date: 2004-09-14 22:55
Tags: Web
WordPress-Post-ID: 247
WordPress-Post-Type: post

I started work on Jen's new site tonight. It's a partial redesign of a
site I helped her with several months ago; this time around there's a
little more time, resources, and design freedom to go around, and I hold
high hopes.



My involvement in the first incarnation was limited to [the front
page][]. The far right of the clover does some backflips to account for
something like nine or twelve possible rollover states, since three
links overlap. It was a pain to code, and I loved every minute of it.
(Funny how that works.)



The [new version][] is a world apart from the old. Jen has done some
great work redoing the clover images and creating a repeating
background, which is a great visual improvement right off the bat. Of
course, I'm a web geek, so it's the code that really gets my attention.
The old page was a modified export from Fireworks MX, so, of course,
it's table-based and laden with JavaScript. The new page, on the other
hand, is based around an \<h1\>, \<ul\>, and some very complicated CSS.
Yeah, that's right: the clover is now a bulleted list.



The list items are nested inside a relatively positioned \<div\>. The
links within the list items are absolutly positioned block-level
elements, forced to a size and given a background image. This creates
the illusion of one seamless clover spanning the page. The rollovers and
state changes are all based on link pseudo-classes, no JavaScript
required. I have tested this code in every major desktop browser I have
access to. Mac/IE5, Firefox 1.0PR, Win/IE6, and Safari 1.2.2 display the
clover flawlessly.



This code is cutting-edge. Everything about web design is changing, and
it's changing in this direction. What's next for this site? Evaluate the
flaws. Where does it break? Who will we exclude? To make the content
available is a no-brainer, but this is an artistically-oriented website.
What will take precedence? TBD.



  [the front page]: http://static.bwerp.net/~adam/jen/main/
  [new version]: /media/2004/09/14/clover/clover-test.html
