Title: Food Costs
Slug: 222/cost
Summary: I am excessively anal-retentive.
Date: 2004-06-13 17:24
Tags: Personal, Nutrition
WordPress-Post-ID: 222
WordPress-Post-Type: post

A large package of Wegman's boneless breast chicken (approx. 12 pc.)
costs \$10.81, or 90¢ per breast. A loaf of Wegman's hearty multigrain
bread (14 slices, excluding the ends) is \$2.49, about 18¢ per slice. A
package of teriyaki marinade will run you just 79¢. Given these figures,
a teriyaki chicken sandwich will run you about \$1.52, assuming you
marinated three breasts and used one for each sandwich. Wegman's Diet
Wedge soda (calorie free, caffeine free) costs \$2.50 for a 12 pack, a
scant 21¢ per 12 oz. can. Total meal cost: \$1.73.

Chicken on multigrade bread is far healthier than Kraft Macaroni &
Cheese Spirals, my quick-and-easy dish of choice. A bowl of Mac & Cheese
costs under \$1.25 by my estimate, including the 99¢ box and small
amounts of milk and butter needed for preparation.

I spend around \$5 every time I go to Taco Bell, which I consider the
most affordable fast food place around. I can usually stay under \$5 at
[Crossroads][] here at RIT as well. Lunch at [The Ritz][] runs me at at
least \$7, without fail.

I've been meaning to tally this up for a while, as my financial
situation is a little tight for the next few months. Has anyone else
done this for their daily meals?

  [Crossroads]: http://finweb.rit.edu/foodservice/cafeandmarket/
  [The Ritz]: http://finweb.rit.edu/foodservice/ritz/
