Title: Including Remotes in Team Offsites
Slug: including-remotes-in-team-offsites
Date: 2019-04-14 22:05
Tags: remotes
Category: Work

*This post was originally written in October 2014, but I lost it in my drafts.
It's still relevant in 2019, so I'm sending it out into the world.*

I'm starting this blog post on a northbound train, fresh off my team's annual
planning offsite. As a remote worker I'm fortunate to have a manager who would
bring me to Brooklyn for a single day, especially so soon after a week-long
office visit.

A trip like this is recognition that we're not great at including remote folks
in this type of brainstorming session. (I know this first-hand from last year's
planning.) So, yay for me, I was there in person this year. Unfortunately, our
other remote team member was *not* able to make the trip. His experience was far
inferior to the experience of people in the room. After 7 hours he probably had
trouble staying awake, much less engaged.

Our only piece of equipment during this session was a MacBook with a fisheye
lens. I noticed a few problems, much which I'm extrapolating from my own
experiences as a remote participant:

* Video quality. The wider angle of a fisheye gets more people into the frame,
  but makes it harder to read what those people may be writing. Even without
  fisheye, large enough writing would limit the amount of stuff you can fit on
  the board.

* Static view. You're totally dependent on people moving the camera when the
  speaker changes or people move around.

* Can't easily reference artifacts from earlier exercises. We had lots of notes
  taped to walls or written on whiteboards. A remote person can't wander around
  and reflect on these things.

* More generally: dropped audio is going to happen, and it's jarring. Also, some
  people just talk quietly, or get drowned out by background chatter if several
  groups are working nearby.

Right away, there's a short list of things I'd like to try.

First, use real microphones. Should the person leading a session have a lapel
mic and repeat questions and comments? Can you easily feed two or more mics into
one laptop? Are there omnidirectional mics that don't suck? Sometimes you want
audio from all over the room, sometimes you want a smaller range. Do you need a
bunch of microphones for different situations?

Remote people need reassurance that they can speak up when they can't see/hear.
I am sometimes reluctant to speak up as the only remote person, but the people
on the other end probably have no idea you can't hear them. It's a difficult but
necessary disruption. What can we do to make this easier on everyone involved?

Make a conscious effort to engage remote participants. Have they said anything
lately? Do they have that glassy look in their eyes? If you're writing things
down, take a moment to read them out loud, *especially* if people in the room
can see those notes. *Ask* if they can see/hear.

Other solutions are not as easy to come by. How do you share whiteboards in
real-time? How do you make artifacts available remotely? What's the digital
analog of moving sticky notes around a wall? I would love to know, or help
create these tools.

I love to work with my team face-to-face, but for those times when I can't, I
hope we can find and create the tools and processes that foster collaboration
and engagement.
