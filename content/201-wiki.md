Title: Wiki way
Slug: 201/wiki
Summary: Interesting things happen on the web.
Date: 2004-02-26 23:39
Tags: Computers
WordPress-Post-ID: 201
WordPress-Post-Type: post

I've been using [my wiki][] to keep track of information for some time
now. It's been a very personal resource up until now, not in the sense
that I use it for "personal" information, but that I'm the only one who
edits and (presumably) looks at it. Yesterday, I had my first guest
editor, though not the kind I expected.



I've been looking at custom silk-screened t-shirts, so in what has
become tradition I created [a wiki page][] to store the information I
found. A person who I can only assume works for "DesignAShirt.com"
altered the text, removing the phrase, "[they seem] to be two faces for
the same company," and adding, "DesignAShirt.com uses a flash based
system for their designing that is probably the easiest on the web."
MoinMoin (my wiki software) has [a visual record][] of the changes.
(Does anyone know how to force a comparison between two revisions, or
can it only be done between a past revision and the current copy?)



I find it interesting that the wiki was edited in this way. I'm curious
just how many hits they received through the page that they took notice.
I'll let the change stick around, I suppose. A little rhetoric won't
kill me.



  [my wiki]: http://wiki.bwerp.net/
  [a wiki page]: http://wiki.bwerp.net/PersonalizedTshirts
  [a visual record]: http://wiki.bwerp.net/PersonalizedTshirts?action=diff
