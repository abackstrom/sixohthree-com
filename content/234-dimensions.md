Title: Alternate Dimensions
Slug: 234/dimensions
Summary: Just don't turn it 90 degrees around the Y-axis.
Date: 2004-08-12 21:05
Tags: Computers
WordPress-Post-ID: 234
WordPress-Post-Type: post

[![Screenshot of the Metisse window manager running on Mac OS
X][]][]I've had a bit of a blogging backlog the past few weeks. I tend
to procrastinate, but I don't let myself to anything *too* fun, because
that would be Wrong. Also, if I blog, that's proof I'm not doing other
things. Very complicated, you see.

Anyway, Slashdot recently posted [a story][] about [Metisse][], a
relatively new 3D window manager. I installed it for a bit and it was
pretty impressed. You can do the basics (scale windows, rotate along
every axis) as well as a few extras (peel up the corner of a window to
see what's beneath, then let go to return the window to normal). The
interface was not the most intuitive I've ever used and there were a few
quirks with menu displays and click locations, but that's to be expected
of software early in its release lifecycle.



There's a lot of potential for window managers when you add a third
dimension. Anyone remember the computer interfaces in [The Minority
Report][]? That was intuitive. Need to clear some room on your screen?
Grab all the windows and shrink them into the corner. You can still see
the window contents, but they're out of the way.



There are a lot of possibilities, a few of which are explored by [Sun
Microsystems][] in their [Project Looking Glass][]. I'm looking forward
to some of the more mature window managers going 3D in the years to
come.



  [Screenshot of the Metisse window manager running on Mac OS X]: /media/2004/08/12/t_metisse.png
  [![Screenshot of the Metisse window manager running on Mac OS X][]]: /media/2004/08/12/metisse.png
  [a story]: http://developers.slashdot.org/article.pl?sid=04/06/29/229243&tid=104&tid=189&tid=130&tid=8
  [Metisse]: http://insitu.lri.fr/~chapuis/metisse/
  [The Minority Report]: http://www.imdb.com/title/tt0181689/
  [Sun Microsystems]: http://wwws.sun.com/
  [Project Looking Glass]: http://wwws.sun.com/software/looking_glass/
