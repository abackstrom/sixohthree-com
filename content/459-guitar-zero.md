Title: Guitar Zero
Slug: 459/guitar-zero
Summary: I'm a fanboy, so what?
Date: 2008-07-06 15:30
Tags: Games, guitarhero, nintendods
WordPress-Post-ID: 459
WordPress-Post-Type: post

I picked up [Guitar Hero: On Tour][] for the Nintendo DS via online
preorder, and I've played quite a bit since it arrived. Pros:

-   **Fun.** It's a good translation to the handheld platform, no less
    fun than Guitar Hero III on my PS2.
-   **Portable.** It fits nicely in my backpack or in the center console
    of the car.
-   **Decent sound.** Not great, but much better than I expected. Maybe
    comparable to 64Kbps MP3s?
-   **Fast access to game play.** Long startup and load times are a pet
    peeve of mine. In a single informal time trial, it took 37 seconds
    from turning on the DS to seeing the fret bar display in career
    mode. Not bad. Closing the handheld's lid means the game is
    available instantaneously, any time. The PS2 wastes nearly two
    minutes of my time before I reach a song.
-   **Judy Nails.** Mmm.


Cons:

-   **Limited track selection.** Only 25 songs total.
-   **Small fret bar.** The buttons would feel more comforatable if half
    an inch or so was added to the total width of the bar.


Overall, a great buy for a GH fan like myself. Great if you're into
branded accessories, too: picks, cases, and even a Guitar-Hero-branded
Nintendo DS. Dang.

  [Guitar Hero: On Tour]: http://en.wikipedia.org/wiki/Guitar_Hero:_On_Tour
