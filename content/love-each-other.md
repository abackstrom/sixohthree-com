Title: Love Each Other
Slug: love-each-other
Summary: We can disagree and still love each other, unless...
Date: 2016-11-23 00:06
Category: Social
Tags: trump, marginalization, oppression

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">This went up quickly<br>(at 24th and Bryant) <a href="https://t.co/tvMN7zFn07">pic.twitter.com/tvMN7zFn07</a></p>&mdash; Star Simpson (@starsandrobots) <a href="https://twitter.com/starsandrobots/status/800868628811812865">November 22, 2016</a></blockquote>

<blockquote>We can disagree and still love each other, unless your disagreement is rooted in my oppression and denial of my humanity and right to exist." -- Robert Jones</blockquote>
