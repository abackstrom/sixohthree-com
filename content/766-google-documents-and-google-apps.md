Title: Google Documents and Google Apps
Slug: 766/google-documents-and-google-apps
Summary: I've been loving Google Apps lately, but there have been some headaches as I reconcile my old Google account with the new Google Apps account.
Date: 2009-03-23 08:08
Tags: Web, Google, googleapps
WordPress-Post-ID: 766
WordPress-Post-Type: post

I've been loving [Google Apps][] lately, but there have been some
headaches as I reconcile my old Google account with the new Google Apps
account. Gmail was easy enough: just set a filter that forwards all new
messages. I managed to export my important calendars and bring them into
Apps, and the sharing settings have been updated by hand. Docs is a
bigger headache: I have many documents of my own, as well as a number of
personal and professional documents shared to me by others.

The migration tool in Google Apps leaves something to be desired:

[![missing-docs][]][missing-docs]

Sounds good, but results in this error:

[![unable-to-access][]][unable-to-access]

The manual migration continues.

  [Google Apps]: http://www.google.com/a/org/
  [missing-docs]: /media/2009/03/missing-docs.png
  [unable-to-access]: /media/2009/03/unable-to-access.png
