Title: Google Sitemaps and WordPress 2.0
Slug: 364/sitemaps
Summary: Quick and dirty.
Date: 2006-03-30 17:06
Category: Meta
Tags: WordPress
WordPress-Post-ID: 364
WordPress-Post-Type: post

I've created a script to dump my blog post history into an XML file
suitable for [Google Sitemaps][]. Note that I consider any XML-building
script that does not use [XML creation tools][] to be a hack. This is a
hack.


    <?php

    header("Content-type: application/xml");
    require_once( dirname(__FILE__) . '/wp-config.php'); // get the wordpress functions

    // sitemap header
    echo '<'.'?xml version="1.0" encoding="UTF-8"?>';
    echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">';

    // set upper limit to a sufficiently high number
    $posts = get_posts('numberposts=32767&order=ASC&orderby=post_date');

    // output all the posts
    foreach($posts as $post): setup_postdata($post);
    ?>
        <url>
            <loc><?php the_permalink(); ?></loc>
            <lastmod><?php echo get_post_modified_time('Y-m-d') ?></lastmod>
        </url>
    <?php

    endforeach;
    echo '</urlset>';



Example: [sitemap.php][].

I also add a rewrite condition to my `.htaccess` file:


    RewriteRule ^sitemap.xml$ sitemap.php [L]



Thus is born [sitemap.xml][].

  [Google Sitemaps]: http://www.google.com/webmasters/sitemaps/
  [XML creation tools]: http://us2.php.net/manual/en/ref.domxml.php
  [sitemap.php]: /media/2006/03/30/sitemap.phps
  [sitemap.xml]: http://blogs.bwerp.net/sitemap.xml
