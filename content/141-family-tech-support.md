Title: Family Tech Support
Slug: 141/family-tech-support
Date: 2003-03-17 14:57
Tags: Computers
WordPress-Post-ID: 141
WordPress-Post-Type: post

From "[Family Tech Support][]," a recent story on [Slashdot][]:



For a month, my mother became really productive (mom's productivity is
measured in forwarded joke emails), and then, abruptly, stopped being
productive at all... I sent several emails which went unanswered... The
next time I heard from her was on my answering machine - "You can cancel
my internet access, I've packed up the computer and put it in the
closet. Bye."



This makes me appreciate my mother's computer skills so much more. One
of my pet peeves is when a person has a problem, but is unwilling to
deal with it and resorts to ignoring it instead (or worse, complaining
about it). My mom wastes no time in phoning or messaging or whatever it
takes to let me know there's a problem. I have [VNC][] set up to speed
the troubleshooting process; failing that, we can work through most
anything over the phone.



What she lacks in computer knowledge, she makes up in patience and
persistence. I think that's a good lesson for us all.



  [Family Tech Support]: http://slashdot.org/article.pl?sid=03/03/15/0051258
  [Slashdot]: http://slashdot.org/
  [VNC]: http://www.uk.research.att.com/vnc/
