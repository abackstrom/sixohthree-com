Title: Termux for Android
Slug: termux-for-android
Date: 2018-03-17 16:56
Summary: Wherein Termux for Android lets me build my website on my phone.
Meta-Image-Square: /media/2018/03/note8.512x512.jpg
Meta-Image-FB: /media/2018/03/note8.986x518.jpg
Category: Technology
Tags: Linux, Android, Terminal

I'm a newbie to the Android scene, having made the switch from an iPhone 6 to
my Samsung Galaxy Note8 only last November. I've embraced the ecosystem rather
than trying to exactly replicate my old iOS setup, but struggled to find a Git
client I liked.

[Pedro Veloso][1]'s article on [How to have a proper Git client on Android][2]
got me looking at [Termux][3], which isn't a Git client but a terminal emulator
and Linux environment that works with your average Android device (rooting not
required).

I've only scratched the surface of Termux ([lots of functionality][4] is initially
hidden behind keyboard shortcuts and a menu gesture) but I've already got my
target workflow going: I'm writing this post directly inside my site's Git
repository in Vim, and my site will auto-deploy when I push this post in a new
commit.

I've installed a few other things like tmux and golang, and I'm looking forward
to playing around in this environment, especially with an external keyboard and
[DeX][5].

![Termux on the Galaxy Note8](/media/2018/03/note8.1024x768.jpg)

  [1]: https://mobile.twitter.com/pedronveloso
  [2]: https://pedronveloso.com/proper-git-client-android/
  [3]: https://termux.com
  [4]: https://wiki.termux.com/wiki/Touch_Keyboard
  [5]: /media/2018/03/dex.jpg
