Title: Giving Feedback
Slug: 405/feedback
Summary: Don't tell me what I can now do.
Date: 2007-05-01 08:12
Tags: Computers
WordPress-Post-ID: 405
WordPress-Post-Type: post

Here are two dialogs related to disconnecting removable drives in
Windows XP. The first is the failure case, the second is the success
case. Can anyone identify what is wrong with these dialogs?

[<img>][]

[<img>][1]

If you said, "they are nearly identical," I'd have to agree with you. I
see this dialog at least once every single day and I still cannot
identify them at a glance.

  [<img>]: /media/2007/04/23/cannot.jpg
  [1]: /media/2007/04/23/cannow.jpg
