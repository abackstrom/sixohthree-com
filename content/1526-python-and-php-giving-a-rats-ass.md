Title: Python and PHP: Giving a Rat's Ass
Slug: 1526/python-and-php-giving-a-rats-ass
Summary: Should you give a rat's ass about being competitive with PHP?
Date: 2011-06-01 06:59
Tags: Programming, PHP, python
WordPress-Post-ID: 1526
WordPress-Post-Type: post

So this happened:

https://twitter.com/alex\_gaynor/statuses/75730453201555456

I'm not going to stand up in a very strong defense of PHP. There is
[plenty wrong with it][], but its popularity is due to its low barrier
to entry:

-   It's ubiquitous in cheap shared hosting environments
-   It's easy to create your first script (create a .php file in the web
    directory)
-   There are a plethora of tutorials


This attracts a pretty scary demographic: the non-programmer. Just look
at the [PHP questions][] on [Stack Overflow][]. On any given day it's a
[complete mess][], a land where HTML is intermingled with PHP and
everyone is using the [old-school MySQL API][]. Separation of concerns,
escaping data, MVC, OOP, and any number of other concepts are above
these users, for now. They're just trying to get something on the page
in the way their language seems to encourage.

The crux, then: if your language has a high barrier to entry, you
effectively lock out some portion of unskilled users. Saying you don't
want to "compete" with PHP means retaining your high barrier to entry.
Is it better to attract programmers who already have some ability, or to
encourage a wider installed base? Would you rather tell a newbie, "Write
your app with PHP, it's easy" or "Write your app with Python, it's
good?" I prefer the latter.

  [plenty wrong with it]: http://www.phpsadness.com/
  [PHP questions]: http://stackoverflow.com/questions/tagged/php
  [Stack Overflow]: http://stackoverflow.com/
  [complete mess]: http://stackoverflow.com/questions/6198830/php-problem-with-variables
  [old-school MySQL API]: http://www.php.net/manual/en/book.mysql.php
