Title: Total Lunar Eclipse, Right Now
Slug: 260/lunar
Summary: If you're reading this, you've already missed it.
Date: 2004-10-27 22:57
Tags: Photography
WordPress-Post-ID: 260
WordPress-Post-Type: post

![Photograph of the moon during a total lunar eclipse.][]

The moon, during a total solar eclipse, as seen by my Nikon 4500. There
are better pictures, but this one is mine.



  [Photograph of the moon during a total lunar eclipse.]: /media/2004/10/27/bloodmoon.jpg
