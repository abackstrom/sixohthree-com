Title: The Incredible Shrinking RAM
Slug: 822/the-incredible-shrinking-ram
Summary: Looks like RAM is getting smaller.
Date: 2009-05-12 20:08
Tags: Computers, hardware, memory, ram
WordPress-Post-ID: 822
WordPress-Post-Type: post

Looks like RAM is getting smaller. Here, older DDR 533 (11/16" tall)
next to newer DDR 800 (1 3/16"). Might be harder to install those tiny
sticks!

[![ram][]][]

  [ram]: /media/2009/05/ram-260x300.jpg
  [![ram][]]: /media/2009/05/ram.jpg
