Title: Unproxy Gmail Images
Summary: Use the hash, Luke.
Date: 2013-12-17 17:03
Tags: gmail, JavaScript
Category: Development

**Update:** A domain administrator can turn this off or set a
domain whitelist in [Google Apps domain settings][settings].
{: .update }

Gmail last week introduced a feature that [proxies all images][1]. This is bad
if you're developing emails, pointing the browser to images on your internal
network: Google can't see those, and you get broken images in Gmail. Here's a
JavaScript snippet that replaces image src attributes with the original image
URL, which Google kindly stores in the anchor of the proxied URL:

```javascript
NodeList.prototype.filter = function(callback) {
    return Array.prototype.filter.call(this, callback);
};

document.getElementsByClassName('adP').filter( function(elem) {
  elem.getElementsByTagName('img').filter( function(elem) {
    var src = elem.getAttribute('src'),
      parts = src.split('#', 2);
    if (parts[0].indexOf('googleusercontent.com/proxy/')) {
      elem.setAttribute('src', parts[1]);
    }
  })
});
```

Here's the same as a bookmarklet: <a href="javascript:(function(){NodeList.prototype.filter=function(callback){return Array.prototype.filter.call(this,callback);};document.getElementsByClassName('adP').filter(function(elem){elem.getElementsByTagName('img').filter(function(elem){var src=elem.getAttribute('src'),parts=src.split('#',2);if(parts[0].indexOf('googleusercontent.com/proxy/')){elem.setAttribute('src',parts\[1]);}})});})();">unproxy gmail</a>.

  [1]: http://gmailblog.blogspot.com/2013/12/images-now-showing.html
  [settings]: //i.abackstrom.com/sixohthree.com/gmail-image-proxy-whitelist.1388094876.png
