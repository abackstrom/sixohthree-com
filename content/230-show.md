Title: Ani DiFranco, Live in Buffalo
Slug: 230/show
Summary: I am the rain king.
Date: 2004-07-05 21:09
Tags: Music
WordPress-Post-ID: 230
WordPress-Post-Type: post

Yesterday I had the good fortune to see Ani DiFranco in her home town of
Buffalo, NY. (That's four shows in a little over two years, I believe.)
This one was a world apart from the other shows I've seen. The first two
were in theatres, the third in a covered music arena. Nice seats, if not
fully air conditioned.



Not this time.



The concert itself was a two day "Uncle Sam's Jam," a freebie show
presented by... well, I'm not sure who. The stage was set up on the
steps of Buffalo City Hall, the crowd sprawled half way across Niagara
Square. I showed up much later than the 4:00 PM I was shooting for, but
Hammel on Trial didn't even step on stage until around 6:00 PM. (I was
without my chronometer, but I think that's a good approximation.) Late
afternoon weather was hot and humid, which quickly gave way to
*torrential downpours*.



I was a just little damp when Hammel on Trial got on stage. Twenty
minutes into Ani's show I looked like I'd just been swimming in Lake
Erie with all my clothes on. I was standing in four inches of water,
having long since missed my chance for the high ground of the curbstone.
It's been twenty-four hours and my shoes are *still* drying. Ani herself
said it was a "unique experience," even after all her years on the road.



Ani was awesome, as always. There was a much better dynamic than the
last show I was at. She did "My IQ," "God's Country," "Swan Dive,"
"Educated Guess," and, of course, "Independence Day," to name a few
tracks. The set list was pretty similar to the Tabernacle CD. Todd
Sickafoose backed her up on bass, but his amp kicked out after the ten
minute rain intermission. Bummer.



Awesome, especially for a free show. Well worth the drive.



