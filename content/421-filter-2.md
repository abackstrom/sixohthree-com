Title: Stacking QuickFilters in Alpha Five V8
Slug: 421/filter-2
Date: 2007-07-18 13:08
Tags: Computers, Alpha Five
WordPress-Post-ID: 421
WordPress-Post-Type: post

[Alpha Five][] forms include a QuickFilter feature that lets you limit
the current result set to records that match (or don't match) the
contents of a specific field. You can stack these filters, limiting by
field after field as you dig down to the desired record set. After half
a dozen filters, Alpha ends up with something like this:


    Filter: ((((((ITMPRTCL="Fiber")) .AND. ((ITMPRTCL="Fiber")) .AND.
    (ITMFIL->ITMPRDCA"Product   ")) .AND. (((ITMPRTCL="Fiber")).AND.
    (ITMFIL->ITMPRDCA"Product   ")).AND.(ITMFIL->ITMPRDCA"Component ").AND.
    (ITMPRDCA="Bulk Cable")) .AND. ((((ITMPRTCL="Fiber")).AND.
    (ITMFIL->ITMPRDCA"Product   ")).AND.(ITMFIL->ITMPRDCA"Component ").AND.
    (ITMPRDCA="Bulk Cable")).AND.(ITMFIL->ITM_STATUS"Inactive            ")) .AND.
    (((((ITMPRTCL="Fiber")).AND.(ITMFIL->ITMPRDCA"Product   ")).AND.
    (ITMFIL->ITMPRDCA"Component ").AND.(ITMPRDCA="Bulk Cable")).AND.
    (ITMFIL->ITM_STATUS"Inactive            ")).AND.
    (PHMENG->F_JACKET"Outdoor Aerial      "))
    .AND. ((((((ITMPRTCL="Fiber")).AND.(ITMFIL->ITMPRDCA"Product   ")).AND.
    (ITMFIL->ITMPRDCA"Component ").AND.(ITMPRDCA="Bulk Cable")).AND.
    (ITMFIL->ITM_STATUS"Inactive            ")).AND.
    (PHMENG->F_JACKET"Outdoor Aerial      ")).AND.
    (PHMENG->F_JACKET"Indoor/Outdoor      ")



Some of you might prefer a more structured layout:

    (
        (
            (
                (
                    (
                        (
                            ITMPRTCL="Fiber"
                        )
                    )
                    .AND.
                    (
                        (
                            ITMPRTCL="Fiber"
                        )
                    )
                    .AND.
                    (
                        ITMFIL->ITMPRDCA"Product   "
                    )
                )
                .AND.
                (
                    (
                        (
                            ITMPRTCL="Fiber"
                        )
                    )
                    .AND.
                    (
                        ITMFIL->ITMPRDCA"Product   "
                    )
                )
                .AND.
                (
                    ITMFIL->ITMPRDCA"Component "
                )
                .AND.
                (
                    ITMPRDCA="Bulk Cable"
                )
            )
            .AND.
            (
                (
                    (
                        (
                            ITMPRTCL="Fiber"
                        )
                    )
                    .AND.
                    (
                        ITMFIL->ITMPRDCA"Product   "
                    )
                )
                .AND.
                (
                    ITMFIL->ITMPRDCA"Component "
                )
                .AND.
                (
                    ITMPRDCA="Bulk Cable"
                )
            )
            .AND.
            (
                ITMFIL->ITM_STATUS"Inactive            "
            )
        )
        .AND.
        (
            (
                (
                    (
                        (
                            ITMPRTCL="Fiber"
                        )
                    )
                    .AND.
                    (
                        ITMFIL->ITMPRDCA"Product   "
                    )
                )
                .AND.
                (
                    ITMFIL->ITMPRDCA"Component "
                )
                .AND.
                (
                    ITMPRDCA="Bulk Cable"
                )
            )
            .AND.
            (
                ITMFIL->ITM_STATUS"Inactive            "
            )
        )
        .AND.
        (
            PHMENG->F_JACKET"Outdoor Aerial      "
        )
    )
    .AND.
    (
        (
            (
                (
                    (
                        (
                            ITMPRTCL="Fiber"
                        )
                    )
                    .AND.
                    (
                        ITMFIL->ITMPRDCA"Product   "
                    )
                )
                .AND.
                (
                    ITMFIL->ITMPRDCA"Component "
                )
                .AND.
                (
                    ITMPRDCA="Bulk Cable"
                )
            )
            .AND.
            (
                ITMFIL->ITM_STATUS"Inactive            "
            )
        )
        .AND.
        (
            PHMENG->F_JACKET"Outdoor Aerial      "
        )
    )
    .AND.
    (
        PHMENG->F_JACKET"Indoor/Outdoor      "
    )




You may have noticed that some of the conditions are repeated several
times. At times this will generate errors as the filter gets more and
more complex. The Xbasic command
[Expression\_Common\_Filter\_Eliminate][ecfe]() can trim these duplicate
query conditions, allowing you to add additional filters.


    current_filter = topparent.filter_get()
    new_filter = expression_common_filter_eliminate(current_filter)
    topparent.queryrun(new_filter)



  [Alpha Five]: http://www.alphasoftware.com/AlphaFive/
  [ecfe]: http://support.alphasoftware.com/alphafivehelpv8/index.htm#Functions/EXPRESSION_COMMON_FILTER_ELIMINATE().HTM
