Title: Thoughts on rel="nofollow"
Slug: 293/nofollow
Summary: Follow me follow me follow me down, down down down.
Date: 2005-01-28 19:55
Tags: Blogging, Spam, Web
WordPress-Post-ID: 293
WordPress-Post-Type: post

Google's introduction of [rel="nofollow"][] has rippled through the web
these past couple weeks, and has been met with lots of praise and
criticism along the way. I thought I'd weigh in as well.

I've heard others express concern over the sudden devaluation of our
biggest currency, the link. (To steal a phrase from [Weez's student][].)
Linking has built the Google-centric web as we know it. To stricken a
large part of the links from the record books changes Google ([et][]
[al][]), and will affect our searching in the future. This is true, I
will not contest it.

That being said, my blog is my house. I put effort into the posts that
go here, and I link to sites that have some value to me. Other sites
link to me in turn, for the same reason. I have a decent [page rank][],
and I'd like to think I have earned it. I create the content, I take the
time, I foot the bill. If links are currency, links in comments are
spent using my checkbook. PageRank represents the linking practices of
the Internet community, but this idea goes down the toilet when any fool
with a browser can link to his own site from the highest-linked-to (and
therefore, highest-ranked) of websites. Nofollow makes linking truly
representative.

Tweaking Nofollow
-----------------


While this is one important aspect, that doesn't mean blog comments have
no value to me. Entries can spark very good discussions with very
pertinent links, and there is little reason to blanket an entire history
of comments with nofollow.

Why do I hate spam comments? They have no value to me. They are
completely off-topic, and leech off my bandwidth, my server space, my
PageRank, and my free time. I will delete a spam comment, whether
nofollow is around or not. What nofollow does is remove the spammer's
incentive to comment on my site. Yes, people might still click on the
links directly, but I imagine the improved search engine ratings are the
driving forces behind spam commenting.

So, comments with links are good, involuntary linking to sites about
online perscription drugs is bad. What to do?

Why not allow comments to be marked as "approved?" I can delete the spam
for the day, and mark the entire back archive (or select posts) as
"clean." The nofollow filter passes over these clean posts, only
touching potentially harmful comments. It's a possibility, one among
many, I'm sure.

Fin
---


This attribute is not armageddon, just a more fine-tuned version of
[something we've had for a long time][]. It's also very young, and still
needs to spread and settle before the real impact will be realized.

In forty years we can all look back and tell our grandkids about
rel="nofollow" and how we though it was such a big deal.

  [rel="nofollow"]: http://www.google.com/googleblog/2005/01/preventing-comment-spam.html
  [Weez's student]: http://weez.oyzon.com/index.php?/weezblogtemplates/comments/1098/
  [et]: http://search.msn.com/
  [al]: http://www.yahoo.com/
  [page rank]: http://www.google.com/technology/
  [something we've had for a long time]: http://www.google.com/remove.html#exclude_pages
