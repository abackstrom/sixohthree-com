Title: Google+ for Google Apps
Slug: 1708/google-plus-for-google-apps
Summary: It's finally here.
Date: 2011-10-27 15:35
Tags: Web, Google, googleapps
WordPress-Post-ID: 1708
WordPress-Post-Type: post

[Finally][], + comes to Apps. Thank you, Google. Notable:


> For those of you who’ve already started using Google+ with a personal
> Google Account and would prefer to use your Google Apps account, we’re
> building a migration tool to help you move over. With this tool, you
> won’t have to rebuild your circles, and people who’ve already added
> you to their circles will automatically be connected to your new
> profile. We expect this migration option to be ready in a few weeks,
> so if you’d like, you can go ahead and get started with your Apps
> account today and merge your connections once the tool is available.



  [Finally]: http://googleenterprise.blogspot.com/2011/10/google-is-now-available-with-google.html
