Title: Audio Excellence
Slug: 302/audio
Summary: Ear candy.
Date: 2005-04-07 21:14
Category: Music
Tags: headphones, hardware
WordPress-Post-ID: 302
WordPress-Post-Type: post

I've had a pair of Sony MDR-V500DJ Monitor-Series headphones for a
while now, but they've been sitting idle while my computers have been in
transition. I've finally hooked them back up, and I have to say: if you
have never used a high-quality set of headphones, I highly recommend it.
This is not just better frequency response, this is music I didn't know
I had. It's like I received a completely new set of high-fidelity MP3s
in the box.

Serious.
