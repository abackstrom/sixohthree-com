Title: What'd I miss?
Slug: 169/whatd-i-miss
Summary: Smile, you're still alive.
Date: 2003-07-26 21:18
Tags: Personal
WordPress-Post-ID: 169
WordPress-Post-Type: post

I seem to have grown a life offline, and I've spent less than an hour a
day on the computer in the past two weeks or so (not counting work,
which requires me to be on a computer). Haven't had much opportunity for
blogging, and there isn't anything I'm compelled to say at the moment.
So maybe this:

Live in the moment, because you never know what tomorrow will bring.



Smiles all around. =)



