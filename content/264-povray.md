Title: POV-Ray Fumblings
Slug: 264/povray
Summary: A cat, right? It's a cat, isn't it?
Date: 2004-11-04 22:05
Tags: Computers
WordPress-Post-ID: 264
WordPress-Post-Type: post

I'm sure there's more than one person out there wondering why I've been
playing so much Counter-Strike, when I should be working on her website.
Let me assure you, I haven't spent all my time playing games. I've been
hand-writing scene description files of industrial objects, as well.

[![Sign post][sm]][large]

The above image was created in [POV-Ray][], a raytracer for creating
images and animations on the computer. The object was created by
[joining and differencing][] a variety of primitive objects, such as
boxes and cylinders. (Actually, I used a prism in this version.) Don't
let my sign post throw you off, though, POV-Ray is capable of [some
pretty amazing stuff][]. I am only a beginner, and I will likely carry
that title for the rest of my life. If I complete this stop sign (ie.
create the actual sign part) I will consider it a job well done, and
move to other activities that I have some talent for.

  [sm]: /media/2004/11/04/stopsign_sm.png
  [large]: /media/2004/11/04/stopsign.png
  [POV-Ray]: http://www.povray.org/
  [joining and differencing]: /media/2004/11/04/stopsign.pov
  [some pretty amazing stuff]: http://www.povray.org/community/hof/
