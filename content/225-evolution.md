Title: Ximian Connector: First Thoughts
Slug: 225/evolution
Summary: Short version: even Evolution can't make Exchage not suck.
Date: 2004-06-23 16:03
Tags: Linux
WordPress-Post-ID: 225
WordPress-Post-Type: post

I finally have a properly configured copy of Ximian Evolution with the
[Novell Connector][] thanks to Shawn here at ITS. I've had the software
installed for some time via [Dropline Gnome][] (a must for any Slackware
destop install), but two problems kept me from using the software more
regularly: the inability to access calendars to which I have delegate
rights, and a problem reaching the GAL. I assume the first problem
stemmed from the second, but I was unable to find a fix for either.



Shawn pointed out the `ximian-connector-setup` utility for configuring
Evolution Exchange accounts. Running this setup utility in preference to
a manual account setup fixed every Exchange problem I had. If necessary
I can run Evolution using a forwarded SSH connection, which is important
since I use Microsoft Entourage 2004 at work. The Connector has the
following features that Entourage lacks:



-   <span class="feature">Resource scheduling.</span> No Macintosh
    client supports the scheduling of resources~~, nor does Outlook Web
    Access~~.
-   <span class="feature">Segmented views for delegate calendars.</span>
    Entourage has only one calendar view. I have access to several of my
    coworkers calendars as a delegate. Their calendar events [show up
    alongside mine][] in the calendar view. It is the definition of
    "clutter."
-   <span class="feature">Folder permission modification.</span>
    Evolution lets me modify folder permissions, allowing other users
    access to my calendar, etc.



I'm sure that's just scratching the surface. I've only had the software
set up for a couple hours. I'll be testing it off and on for the rest of
the summer, after which I hope to be free of Exchange Server. We'll see.



If anyone has an interest in setting up this software for access to the
RIT mail server, please let me know and I'll provide instructions.
(Though it's pretty easy once the setup assistant is at your disposal.)



  [Novell Connector]: http://www.novell.com/products/connector/
  [Dropline Gnome]: http://www.dropline.net/gnome/
  [show up alongside mine]: /media/2004/06/23/entourage-clutter.png
