Title: Metadata Rhymes with Metadata
Slug: 176/metadata-rhymes-with-metadata
Summary: Metadata! What is it? It's data, but it's more than that: it's data <em>about</em> data. "So," you're asking, "why should I care?" Well, I'm glad you asked! Sit a spell, won't you?
Date: 2003-08-27 22:35
Tags: Computers
WordPress-Post-ID: 176
WordPress-Post-Type: post

Metadata!



What is it? It's data, but it's more than that: it's data *about* data.
What does that mean? Well, to pick a hypothetical situation, let's say
[Karl][] takes [a photograph of me biting Abby][]. The photograph is
*data*. But let's say, the photo was taken on August 9th, 2003, at the
Won Kee in Plymouth, New Hampshire, while the photographer was picking
his nose. That's data about our photograph, or *data about data*. In a
word, it's *metadata*.



"So," you're asking, "why should I care?" Well, I'm glad you asked! Sit
a spell, won't you?



[I][] [read][] [a][] [lot][] [of][] [web][] [comics][]. Generally,
they're pretty funny, and occasionally I like to pass certain comics off
to friends. Trouble is, I can hardly ever find said comics when I really
want to. Most of the comics I read have several hundred archived strips.
[Sinfest][] alone has nearly 1,300. Since they exist only as images,
there is virually no way to search through past strips for a particular
word or phrase or character.

Enter [Goats][].



That came out wrong.



Anyway, [Goats][]. Goats records the script for each comic, as well as
each frame's location and the props involved. Do a search for "[kittens,
pop tarts][]" to see just how useful this saved script data is. (Hint:
kittens = pop tarts.) This is *amazing*, and every comic should be
creating this sort of metadata. It's something I've been thinking about
of late, and it's great to see that a webcomic has already picked it up,
and seems to have done a stellar job to boot.



I envision a distributed network of comic readers, each reading a few
past strips of their favorite comics and creating a huge database of all
the comic metatdata you could ask for. That would be joyous. *Deep
sigh*. I'll Google for it tomorrow. ;)




  [Karl]: http://blogs.bwerp.net/~riff/
  [a photograph of me biting Abby]: /media/2003/08/27/adam-bite.jpg
  [I]: http://www.achewood.com/
  [read]: http://www.dieselsweeties.com/
  [a]: http://www.briworld.com/
  [lot]: http://www.mallmonkeys.com/
  [of]: http://www.megatokyo.com/
  [web]: http://www.penny-arcade.com/
  [comics]: http://www.wigu.com/
  [Sinfest]: http://www.sinfest.net/
  [Goats]: http://www.goats.com/
  [kittens, pop tarts]: http://www.goats.com/search/?q=kittens%2C+pop+tarts&search=comics
