Title: Friday, October 13, 2017
Slug: friday-october-13-2017
Summary: summary
Date: 2017-10-13 21:30
Category: Personal
Tags: ansible, twitter
Status: draft

What would I write about, if I couldn't blast out low-effort Twitter posts [for a
day][1]?

<!--
## Provisioning, Ansbile, and Let's Encrypt
I'm on a years-long, on-and-off quest to move some services off a 9+
year old Linode. This mostly means putting my web configs into git and
Ansible and ensuring I can deploy those configs to new hosts. The most
recent headaches were from HTTPS: [Let's Encrypt][3] has lowered the
barrier to entry (at least after I [said goodbye][4] to StartSSL) but the
easy-to-use HTTP challenge wasn't a good fit for spinning up new vms.
-->

**[certbot-route53][5]** is a handy script for generating [Let's Encrypt][3]
certs using DNS challenges. I want to bundle certs with my Ansible configs,
centralizing cert management and simplifying things for new hosts that can't
pass the http-01 challenge. certbot-route53 will let me update certs from my vm,
then push them out to all servers with one command.

  [1]: https://www.nytimes.com/2017/10/13/technology/twitter-boycott-rose-mcgowan.html
  [2]: https://github.com/jed/certbot-route53
  [3]: https://letsencrypt.org/
  [4]: https://arstechnica.com/information-technology/2017/07/google-drops-the-boom-on-wosign-startcom-certs-for-good/
  [5]: https://github.com/jed/certbot-route53
