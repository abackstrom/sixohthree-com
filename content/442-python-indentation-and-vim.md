Title: Python Indentation and Vim
Slug: 442/python-indentation-and-vim
Summary: Holy wars, to some.
Date: 2008-03-22 14:55
Tags: Computers, Programming, python, Vim
WordPress-Post-ID: 442
WordPress-Post-Type: post

Python's syntax relies on indentation for statement grouping. The
[convention][] is to use spaces, but I've always found tabs to be easier
to work with in [vim][]. My largest gripe: if I mis-tabbed, I had to
press "delete" four times to erase all the spaces I created. Turns out,
that needn't be the case: vim can detect multi-space indentation and
delete one "tab" at a time.

Here are some good resources I found while digging into this:

1.  [Secrets of tabs in vim][]
2.  [Notes on using Vim and Python][] -- check out the autoindentation
    based on file name, just below the main table
3.  [Converting tabs to spaces][]



Bonus: check out vimrc in the [python "Vim" folder][] for other helpful
files.

  [convention]: http://www.python.org/dev/peps/pep-0008/
  [vim]: http://www.vim.org/
  [Secrets of tabs in vim]: http://jaeger.festing.org/changelog/1128.html
  [Notes on using Vim and Python]: http://www.vex.net/~x/python_and_vim.html
  [Converting tabs to spaces]: http://www.vim.org/tips/tip.php?tip_id=12
  [python "Vim" folder]: http://svn.python.org/view/python/trunk/Misc/Vim/
