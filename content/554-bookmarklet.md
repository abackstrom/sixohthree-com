Title: JavaScript Bookmarklets
Slug: 554/bookmarklet
Summary: Automation is good.
Date: 2008-10-06 08:38
Tags: Web, bookmarklet, Web
WordPress-Post-ID: 554
WordPress-Post-Type: post

I wrote my first [bookmarklet][] as a proof-of-concept the other day. I
often find myself shortcutting up to the location bar to switch between
development and production web servers. Here's a bookmarklet that
toggles "dev" and "www" subdomains for me, broken into a few lines for
readability:


    javascript:dL=document.location;
    nH=dL.host.split('.')[0]=='www'?'dev':'www';
    dL.href=dL.href.replace(/^(https?:\/\/)[^\.]+/i,"$1"+nH);

And in its ready-to-drag-to-your-bookmark-bar version: [switch][].

  [bookmarklet]: http://en.wikipedia.org/wiki/Bookmarklet
  [switch]: javascript:dL=document.location;nH=dL.host.split('.')[0]=='www'?'dev':'www';dL.href=dL.href.replace(/^(https?:\/\/)[^\.]+/i,"$1"+nH);
