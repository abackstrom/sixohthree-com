Title: Tim O'Reilly on Piracy
Slug: 115/tim-oreilly-on-piracy
Date: 2002-12-12 10:58
Tags: Computers
WordPress-Post-ID: 115
WordPress-Post-Type: post
Category: Technology

Tim O'Reilly has a very well thought-out and informed [article on
piracy][] over at [OpenP2P.com][]. He says many good things about the
benefits of the P2P world and what has been labeled as "piracy", the
desire for users to have immediate access to entertainment, books, and
the like, and the willingness of users to pay for a high-quality version
of this service.

I for one would be happy to pay for the right to download music, if I
was guaranteed a wide selection and high quality. The online offerings
you find on P2P today are excessively mislabeled and incomplete. Plus,
you're about as likely to download a poorly encoded, popping, cracking
MP3 as you are to blink on any given day.

I'm glad such a visible person in the industry is chiming in. I could
say the same thing until the cows came home and nobody would give a
damn, but Tim O'Reilly is visible, and depends on people paying for his
stuff to boot. 'Bout time.

  [article on piracy]: http://www.openp2p.com/pub/a/p2p/2002/12/11/piracy.html
  [OpenP2P.com]: http://www.openp2p.com/
