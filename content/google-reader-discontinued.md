Title: Google Reader Discontinued
Slug: google-reader-discontinued
Date: 2013-07-15 13:12
Category: Technology
Tags: google reader
Summary: For posterity: the Google Reader site, post-sunset.

For posterity: the Google Reader site, post-sunset.

[![Google Reader Discontinued][1]][1]

Thanks for stopping by.

[1]: //i.abackstrom.com/sixohthree.com/google-reader-discontinued.1373908250.png
