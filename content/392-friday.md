Title: Friday Randomness
Slug: 392/friday
Summary: iPhone, ftrain, Warcraft.
Date: 2007-01-12 12:01
Tags: world of warcraft
Category: Games
WordPress-Post-ID: 392
WordPress-Post-Type: post

Apparently the iPhone will not be an "[open platform][]." This is a
shame. The device is just begging to become a platform for Dashboard
widgets performing home automation wirelessly via touchscreen. Apple is
two inches away from making this phone a full-fledged PDA. Make the
jump, give us some love. Disallow third-party widgets over EDGE, I won't
complain.

[Paul Ford][] is back in full force. If he's not in your [aggregator][],
give a visit. I recommend [*The Alley Watchers*][] and [*The Thumb-sized
Heart of TK the Cat*][] for starters.

World of Warcraft: [The Burning Crusade][] will be released next
Tuesday. I hear the Nashua Best Buy will have a midnight release, but
given the population density it may be a crowded store. Anyone looking
to be in W. Lebanon at \~8:00 AM on Tuesday should get in touch with me.
(Time subject to change. I will be there before the doors open.)

  [open platform]: http://www.msnbc.msn.com/id/16566968/site/newsweek/page/2/
  [Paul Ford]: http://www.ftrain.com/
  [aggregator]: http://en.wikipedia.org/wiki/Aggregator
  [*The Alley Watchers*]: http://www.ftrain.com/TheAlleyWatchers.html
  [*The Thumb-sized Heart of TK the Cat*]: http://www.ftrain.com/EndTK.html
  [The Burning Crusade]: http://www.worldofwarcraft.com/burningcrusade/
