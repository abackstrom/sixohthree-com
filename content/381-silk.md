Title: Web Icons: "Silk"
Slug: 381/silk
Summary: Lickable.
Date: 2006-09-06 13:37
Tags: Web
WordPress-Post-ID: 381
WordPress-Post-Type: post

Check out these great looking icons from [Mark James][]: [Silk][].

![Bar chart icon][img] ![Color wheel icon][1] ![Play button icon][2] ![Pound sign icon][3] ![Add User icon][4]

1,000 icons in total, Creative Commons licensed.

  [Mark James]: http://www.famfamfam.com/
  [Silk]: http://www.famfamfam.com/lab/icons/silk/preview.php
  [img]: /media/2006/09/06/chart_bar.png "Bar chart icon"
  [1]: /media/2006/09/06/color_wheel.png "Color wheel icon"
  [2]: /media/2006/09/06/control_play_blue.png "Play button icon"
  [3]: /media/2006/09/06/money_pound.png "Pound sign icon"
  [4]: /media/2006/09/06/user_add.png "Add User icon"
