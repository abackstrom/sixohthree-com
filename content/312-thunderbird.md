Title: Thunderbird Display Errors
Slug: 312/thunderbird
Summary: I never liked those buttons, anyway.
Date: 2005-06-02 00:12
Tags: Computers
WordPress-Post-ID: 312
WordPress-Post-Type: post

I had a bit of a problem with Thunderbird under Windows this morning. I
opened it up, and was greeted with a mostly-blank screen with no buttons
to speak of:

[![Thundbird 1.0.2 display errors][]][]

Turns out my profile was referencing some Thunderbird extensions I had
mistakenly deleted. Instead of erroring out or giving me a warning, it
buggered itself up and just sat there confused. The references were in
the `chrome` directory:
`C:\Documents and Settings\<username>\Application Data\Thunderbird\Profiles\<random>.default\chrome`.
I happily deleted this entire directory and went on my merry way.

  [Thundbird 1.0.2 display errors]: /media/2005/06/01/thunderbird-blank_t.jpg
  [![Thundbird 1.0.2 display errors][]]: /media/2005/06/01/thunderbird-blank.jpg
